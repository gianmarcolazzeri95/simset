# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################
 
def _functional(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.functional" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    def_inp = [f'{self.last}.rat.out']
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_col = [  ]
    def_thr = [ 0]
    def_per = [PERSISTENCE]
        
    # specific parameters
    start = parse(args,'start'    ,def_sta)
    stop  = parse(args,'stop'     ,def_sto)
    step  = parse(args,'step'     ,def_ste)
    frame = parse(args,'frame'    ,def_fra)
    frame = parse(args,'frames'   ,frame  )
    xlist = parse(args,'x'        ,[]     )
    xlist = parse(args,'xvg'      ,xlist  )
    column= parse(args,'col'      ,def_col)
    column= parse(args,'column'   ,column )
    column= parse(args,'columns'  ,column )
    thres = parse(args,'th'       ,def_thr)
    thres = parse(args,'thres'    ,thres  )
    thres = parse(args,'threshold',thres  )
    pers  = parse(args,'pers'     ,def_per)
    pers  = parse(args,'persistence',pers )
    nan   = parse(args,'nan'        ,True )
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # extension
    exten = ['rat.out']
    
    # default input
    found = False
    for ext in exten:
        fname = f'{self.last}{"."*bool(ext)}{ext}'
        if isfile(fname):
            def_inp = [fname]
            found = True
            break
    
    # no input
    if not len(inpt):
        if not found:
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return xvg()
        inpt = def_inp
        descr = f'TAKING "{path_name(def_inp[0])}" as input file\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    inpt = ls(inpt,str)
    line = False # formatting issues
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'WARNING: no files found\n'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: warning
        line = True
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'ERROR: none of {names} found'
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
        continue
    
    # reconstruct input
    inpt = ls(inpt)
    n = len(inpt)
    if not n:
        descr = f'ERROR: no ratchet logs files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return xvg()
    elif verbo and line: print()
    
    # start-stop-step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([],ndmin=2)
    
    # frame
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # adjust length
    start = adjust_list(start,n)
    stop  = adjust_list(stop ,n)
    step  = adjust_list(step ,n)
    frame = adjust_list(frame,n)
    
    # load ratchet logs with start-stop-step-frame options
    ratch = []
    for inp, sta,  sto, ste, fra in\
    zip(inpt,start,stop,step,frame):
        ratch.append(xvg(inp,start=sta,stop=sto,step=ste,frames=fra,
                             verbose=False)[3])
        if not len(ratch[-1]):
            descr = f'ERROR: ratchet log file "{inp}" is not regular\n'
            descr = ansi(descr,'ref')
            comment(descr,self.logre,self.logsh,verbo)
            return array([],ndmin=2)
    
    # remove already-used arguments in args
    args.pop('start'  )
    args.pop('stop'   )
    args.pop('step'   )
    args.pop('frame'  )
    args.pop('frames' )
    args.pop('verbose')
    
    # columns
    column = ls(column,str,int)
    
    # threshold
    thres = ls(thres,int,float)
    
    # xvg
    if sum([thr != 0 for thr in thres]):
        x = xvg(*xlist,verbose=False)
        x = x.select(*column,**args,verbose=False)
        m = len(x)//n # cap x length at len(x)//n*n
        if m:
            descr = f'SELECTED {m*n} columns of the xvg object'
        else:
            descr = f'WARNING: no columns selected in the xvg object'
            descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
    else:
        x = xvg()
        m = 0
    X = [x[i:m*n:n] for i in range(n)]
    
    # persistence - check if ok
    pers = ls(pers,int,float)
    if m and not len(pers):
        descr = f'ERROR: bad "persistence" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([],ndmin=2)
    
    # adjust lengths
    thres = adjust_list(thres,m)
    pers  = adjust_list(pers, m)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0) # current iteration
    lock = Manager().Lock()
    
    def task(interval):
        
        """
        Parameters
        ----------
        interval : ``(first,last)`` tuple of int, subprocess interval
        
        Returns
        -------
        interval : ``(first,last)`` tuple of int, subprocess interval,
                   to reconstruct output in order
        result   : ``(2,-1)``-shaped array with folding time and bias
                   functional (``[inf,NaN]`` if not folded)
        record    : list, infos to print record of the computation, only
                   if ``verbose`` option is ``True`` and ``thres``
                   option is specified
        """
        
        # output initialization
        first,last = interval
        result = array([[inf,NaN]]*(last-first))
        record  = array([[[0.]*m]*5]*(last-first))
        
        # iterate through process interval
        for i in range(last-first):
            times = ratch[first+i].times
            datap = ratch[first+i].datap[0]
            
            # measure and print progress
            if current.value:
                elapsed = now()-t0
                speed = current.value/elapsed
                etr = (n-current.value)/speed
                bar = progress_bar(current.value,n,elapsed,etr,speed,'',width)
                print(f'{bar}\r',end='')
            lock.acquire()
            try:     current.set(current.get()+1)
            finally: lock.release()
            
            # initialization
            t,b = inf,NaN
            breach = [-inf]*m # last time threshold was crossed
            status =[False]*m # being at the right side of the threshold
            valmax = [-inf]*m # minimum value reached
            valmin = [+inf]*m # maximum value reached
            
            # empty ratchet log
            if not len(times):
                result[i] = t,b
                record [i] = thres,status,breach,valmax,valmin
                continue
            
            # no thresholds
            if not m:
                t = times[-1]-times[0]
                b = datap[-1]-datap[0]
                result[i] = t,b
                continue
            
            # select observables
            y = X[first+i]
            
            # j: y's frame at which all observables reach threshold
            # i: r's frame at which all observables reach threshold
            # skip frames BEFORE times[0]
            for j0,time in enumerate(y.times):
                if time >= times[0]:
                    break
            
            # time is over
            if time < times[0]:
                result[i] = t,b
                record [i] = thres,status,breach,valmax,valmin
                continue
            
            # iterate through times
            k = 0 # index of ratchet times
            K = len(y.times) # length of observables times
            time = y.times[j0]
            for j in range(j0,K):
                while k < len(times)-1 and time > times[k]:
                    k += 1
                if time > times[-1]:
                    break
                for h,thr in enumerate(thres):
                    dat = y.datap[h][j]
                    if isnan(dat):
                        continue
                    valmax[h] = max(dat,valmax[h])
                    valmin[h] = min(dat,valmin[h])
                    
                    # crossed threshold
                    if (dat<thr if thr>0 else dat>-thr):
                        if not status[h]:
                            breach[h] = time
                            status[h] = True
                    else:
                        status[h] = False
                
                # check convergence - rightmost time NOT included
                if j < K-1: time = y.times[j+1]
                if sum(status)+\
                   sum([time-breach[h]>=pers[h] for h in range(m)]) == 2*m:
                    t = times[k]-times[0]
                    b = datap[k]-datap[0]
                    break
            
            # update result and record
            result[i] = t,b
            if not verbo:
                continue
            record[i] = thres,status,breach,valmax,valmin
        
        # task output
        return interval,result,record
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = n//PROCESSES+(n<PROCESSES)
    span2  = n%PROCESSES*(n>PROCESSES)
    intervals = []
    index,first = 0,0
    while first < n:
        last = min(n,first+span1+(span2>0))
        intervals.append((first,last))
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(intervals)
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    comment(descr,self.logre,self.logsh,verbo)
    
    # initialize progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,n,0,etr,speed,'',width)
    if verbo and n>1: print(f'{bar}\r',end='')
    
    # run pool of processes
    try:
        raw = list(pool.uimap(task,intervals))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return xvg()
    
    # terminate pool and progress bar
    pool.join()
    pool.clear()
    cursor.show()
    if n>1:
        elapsed = now()-t0
        speed = n/elapsed
        etr = 0
        bar = progress_bar(n,n,elapsed,etr,speed,'',width)
        comment(bar,self.logre,self.logsh,verbo)
    if verbo and n>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # reconstruct result in order
    result = array([[inf,NaN]]*n)
    record  = array([[[0.]*m]*5]*n)
    while len(raw):
        interval,sub_result,sub_record = raw.pop()
        result[interval[0]:interval[1]] = sub_result
        record [interval[0]:interval[1]] = sub_record
    
    # print record - terminal width and scaling
    if not m or not verbo:
        return result
    try:
        width = min(max(42,get_terminal_size()[0]),77)
    except:
        width = DEF_WID
    scale = width//7
    width = scale*7
    padA  = scale*2
    padB  = scale
    padC  = padB-2
    labB  = 'thres'
    labC  = 'current' if padB > 8 else 'curr'
    labD  = 'crossed' if padB > 8 else 'cross'
    
    # print record
    m = 0 # number of triggered trajectories
    descr = [ansi(f'{"observable":{padA}}{labB :{padB}}{labC :{padB}}'\
                  f'{labD:{padB}}{"max":{padB}}{"min":{padB}}','bold')]
    for i,rec,res in zip(range(n),record,result):
        descr.append(ansi(f'{inpt[i]} {"-"*(width-len(inpt[i])-1)}'))
        thres,status,breach,vmax,vmin = rec
        t,b = res
        if t < inf:
            m+= 1
        for   obs,              thr,  sta,   bre,   vmx, vmn in\
        zip(x.obser[i:len(x):n],thres,status,breach,vmax,vmin):
            obs = format(obs.replace(XVG_DIV,' '),-padA)
            thr = f'< {format(thr,padC)}'if thr>0 else f'> {format(-thr,padC)}'
            sta = format(bool(sta),padB)
            bre = format([bre]+['ps'],padB) if bre>-inf else format('--',padB)
            vmx = format(vmx if vmx >-inf else '--',padB)
            vmn = format(vmn if vmn < inf else '--',padB,0)
            descr.append(f'{obs}{thr}{sta}{bre}{vmx}{vmn}')
        if t < inf:
            b = f'{b:.2f}' if b<100 else f'{b:.1f}' if b<1000 else f'{b:.0f}'
            temp = f'triggered at {t+ratch[i].times[0]:.2f} ps, '\
                   f'bias functional {b}'
            descr.append(ansi(temp,'green'))
        else:
            descr.append(ansi(f'not triggered','yellow'))
    descr = '\n'.join(descr)
    descr+= ansi(f'\nTRIGGERED {m}/{n} ({100*m/n:.2f}%) trajectories\n','bold')
    comment(descr,self.logre,self.logsh,verbo)
    
    return result
