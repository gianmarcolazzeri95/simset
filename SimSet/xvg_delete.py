# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _delete(self,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    times = self.get_times()
    N = len(times)
    H = len(self)
    cursor.show()
    
    ###########################################################################
    ################################ PARSE OPTIONS ############################
    ###########################################################################
    
    # defaults
    def_ori =  []
    def_obs =  []
    def_fil =  []
    def_tit =  []
    def_xax =  []
    def_yax =  []
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_mat = False
    
    # specific parameters
    match_orig  = parse(args,'origin'      ,def_ori    )
    match_orig  = parse(args,'match_origin',match_orig )
    match_obser = parse(args,'obs'         ,def_obs    )
    match_obser = parse(args,'obser'       ,match_obser)
    match_obser = parse(args,'observable'  ,match_obser)
    match_obser = parse(args,'observables' ,match_obser)
    match_obser = parse(args,'match_obser' ,match_obser)
    match_files = parse(args,'file'        ,def_fil    )
    match_files = parse(args,'files'       ,match_files)
    match_files = parse(args,'match_files' ,match_files)
    match_title = parse(args,'title'       ,def_tit    )
    match_title = parse(args,'titles'      ,match_title)
    match_title = parse(args,'match_title' ,match_title)
    match_xaxis = parse(args,'xaxis'       ,def_xax    )
    match_xaxis = parse(args,'match_xaxis' ,match_xaxis)
    match_yaxis = parse(args,'axis'        ,def_yax    )
    match_yaxis = parse(args,'yaxis'       ,match_yaxis)
    match_yaxis = parse(args,'match_axis'  ,match_yaxis)
    match_yaxis = parse(args,'match_yaxis' ,match_yaxis)
    start = parse(args,'start'  ,def_sta)
    stop  = parse(args,'stop'   ,def_sto)
    step  = parse(args,'step'   ,def_ste)
    frame = parse(args,'frame'  ,def_fra)
    frame = parse(args,'frames' ,frame  )
    nan   = parse(args,'nan'    ,DEF_NAN)
    verbo = parse(args,'verbose',VERBOSE)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # columns
    columns = ls(columns)
    for i,col in enumerate(columns):
        if type(col) is slice:
            columns[i] = range(*col.indices(H))
    columns = ls(columns,int,float,str)
    if not columns:
        columns = range(H)
    
    # matches
    match_orig  = ls(match_orig, str)
    match_obser = ls(match_obser,str)
    match_files = ls(match_files,str)
    match_title = ls(match_title,str)
    match_xaxis = ls(match_xaxis,str)
    match_yaxis = ls(match_yaxis,str)
    
    # start-stop-step, check if ok
    start = ls(start,int,float)
    stop  = ls(stop ,int,float)
    step  = ls(step ,int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        if verbo: print(ansi(descr,'red'))
        return
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initializations
    tmp_datap = [] # first step
    new_times = []
    new_obser = []
    new_files = []
    new_title = []
    new_xaxis = []
    new_yaxis = []
    indexes = [] # indexes of selected columns - keep in order
    
    # iterate through "columns"
    for col in columns:
        
        # "col" is a string
        if type(col) is str:
            col = col.casefold()
            
            # retrieve indeces that match that string
            for k,lab in enumerate(self.obser):
                if not match(col,lab):
                    continue
                
                # now "k" is an index - check "args" matches
                found = True
                ori = XVG_DIV.join(lab.split(XVG_DIV)[:-1])
                for mat in match_orig:
                    found = False
                    if match(mat,ori):
                        found = True
                        break
                if not found: continue
                obs = lab.split(XVG_DIV)[-1]
                for mat in match_obser:
                    found = False
                    if match(mat,obs):
                        found = True
                        break
                if not found: continue
                for mat in match_files:
                    found = False
                    if match(mat,self.files[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_title:
                    found = False
                    if match(mat,self.title[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_xaxis:
                    found = False
                    if match(mat,self.xaxis[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_yaxis:
                    found = False
                    if match(mat,self.yaxis[k]):
                        found = True
                        break
                if not found: continue
                indexes.append(k) # "k" is ok and can be included
        
        # "col" is an index - check if acceptable
        else:
            col = round(col)
            col+= 0 if col>=0 else H
            if not 0<=col<H:
                continue
            
            # check "args" matches
            found = True
            lab = self.obser[col]
            ori = XVG_DIV.join(lab.split(XVG_DIV)[:-1])
            for mat in match_orig:
                found = False
                if match(mat,ori):
                    found = True
                    break
            if not found: continue
            obs = lab.split(XVG_DIV)[-1]
            for mat in match_obser:
                found = False
                if match(mat,obs):
                    found = True
                    break
            if not found: continue
            for mat in match_files:
                found = False
                if match(mat,self.files[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_title:
                found = False
                if match(mat,self.title[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_xaxis:
                found = False
                if match(mat,self.xaxis[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_yaxis:
                found = False
                if match(mat,self.yaxis[col]):
                    found = True
                    break
            if not found: continue
            indexes.append(col) # "col" is ok and can be included
    
    # adjust lengths of time options to n columns to treat
    K = len(indexes)
    start = adjust_list(start,K)
    stop  = adjust_list(stop, K)
    step  = adjust_list(step, K)
    frame = adjust_list(frame,K)
    
    # build frame - initialization
    times_to_index = {}   # cache computation
    all_frame = [False]*K # check if deleting all frames

    # build frame    
    for       k, sta,  sto, ste, fra in\
    zip(range(K),start,stop,step,frame):

        # process start
        if type(sta) in FLOAT:
            sta = search_bigger(times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            sto = search_smaller(times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frame accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                fra = [sta]
                for i,t in enumerate(times[sta:sto]):
                    if t-times[fra[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
                if sta==0 and sto==N and ste==1:
                    all_frame[k] = True
        
        # process already existing "frame"
        else:
            temp,fra = fra,[]
            for f in temp:
                if type(f) in FLOAT:
                    f = search_equal(times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        frame[k] = fra
    
    # number of deleted columns and points
    K,M = 0,0
    
    # delete datapoints of columns in indexes
    indexes = sorted(indexes)
    indexes.reverse()
    frame.reverse()
    for k,fra,af in zip(indexes,frame,all_frame):
        
        # delete all frames and occasionally leave the empty column
        if af:
            if verbo:
                M+= sum([not isnan(dat) for dat in self.get_datap()[k]])
            if not nan:
                K+= 1
                self.get_datap().pop(k)
                self.obser.pop(k)
                self.files.pop(k)
                self.title.pop(k)
                self.xaxis.pop(k)
                self.yaxis.pop(k)
            else:
                for i in range(N):
                    self.get_datap()[k][i] = NaN
            continue
        
        # delete some frames and occasionally delete the empty column
        for i in fra:
            if not isnan(self.get_datap()[k][i]):
                self.get_datap()[k][i] = NaN
                M+= 1
        if nan or sum([not isnan(d) for d in self.get_datap()[k]]):
            continue
        self.get_datap().pop(k)
        self.obser.pop(k)
        self.files.pop(k)
        self.title.pop(k)
        self.xaxis.pop(k)
        self.yaxis.pop(k)
        K+=1
    
    # delete times with only NaNs left
    if self.size(): self.prune()
    if not M:
        M = 0
    if verbo:
        print(f'DELETED {K} column{"s"*(K!=1)} and {M} point{"s"*(M!=1)}')
    return K
