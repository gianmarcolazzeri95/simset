# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _mdset(self,name,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo  = parse(args,'verbose' ,VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.mdset" with:\n'
    descr+= f'  name = "{name}"\n' if name else ''
    descr+= f'  args = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # input arguments
    try:
        output = parse(args,'out'     ,''     )
        output = parse(args,'output'  ,output )
        gen    = parse(args,'gen_vel' ,GEN_VEL)
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # input name
    name = '.'.join(extension(name,'mdp')) if name else name
    if isfile(name):
        self.mdp = mdp(name)
    
    # output name
    if not output:
        output = self.mdp.root.split('/')[-1]
    args.pop('output')
    output = '.'.join(extension(output,'mdp'))
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # format output
    descr = f'CREATING "{path_name(output)}" '\
            f'by adapting "{path_name(self.mdp.root)}"\n'
    comment(descr,self.logre,self.logsh,verbo)
    self.mdp.write(output,**args)
