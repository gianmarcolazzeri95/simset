# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _select(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    
    Notes
    -----
    This function does not initialize a ``SimSet.xvg`` object, but
    modifies empty already-provided ``res``.
    """
    
    times = self.get_times()
    N = len(times)
    H = len(self)
    cursor.show()
    
    ###########################################################################
    ################################ PARSE OPTIONS ############################
    ###########################################################################
    
    # defaults
    def_ori =  []
    def_obs =  []
    def_fil =  []
    def_tit =  []
    def_xax =  []
    def_yax =  []
    def_lab =[None]
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    
    # specific parameters
    match_orig  = parse(args,'origin'      ,def_ori    )
    match_orig  = parse(args,'match_origin',match_orig )
    match_obser = parse(args,'obs'         ,def_obs    )
    match_obser = parse(args,'obser'       ,match_obser)
    match_obser = parse(args,'observable'  ,match_obser)
    match_obser = parse(args,'observables' ,match_obser)
    match_obser = parse(args,'match_obser' ,match_obser)
    match_files = parse(args,'file'        ,def_fil    )
    match_files = parse(args,'files'       ,match_files)
    match_files = parse(args,'match_files' ,match_files)
    match_title = parse(args,'title'       ,def_tit    )
    match_title = parse(args,'titles'      ,match_title)
    match_title = parse(args,'match_title' ,match_title)
    match_xaxis = parse(args,'xaxis'       ,def_xax    )
    match_xaxis = parse(args,'match_xaxis' ,match_xaxis)
    match_yaxis = parse(args,'axis'        ,def_yax    )
    match_yaxis = parse(args,'yaxis'       ,match_yaxis)
    match_yaxis = parse(args,'match_axis'  ,match_yaxis)
    match_yaxis = parse(args,'match_yaxis' ,match_yaxis)
    label = parse(args,'lab'    ,def_lab)
    label = parse(args,'label'  ,label  )
    label = parse(args,'labels' ,label  )
    start = parse(args,'start'  ,def_sta)
    stop  = parse(args,'stop'   ,def_sto)
    step  = parse(args,'step'   ,def_ste)
    frame = parse(args,'frame'  ,def_fra)
    frame = parse(args,'frames' ,frame  )
    nan   = parse(args,'nan'    ,DEF_NAN)
    nest  = parse(args,'nest'   ,NEST   )
    verbo = parse(args,'verbose',VERBOSE)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # columns
    columns = list(columns)
    for i,col in enumerate(columns):
        if type(col) is slice:
            columns[i] = range(*col.indices(H))
    columns = ls(columns,int,float,str)
    if not columns:
        columns = range(H)
    
    # matches
    match_orig  = ls(match_orig, str)
    match_obser = ls(match_obser,str)
    match_files = ls(match_files,str)
    match_title = ls(match_title,str)
    match_xaxis = ls(match_xaxis,str)
    match_yaxis = ls(match_yaxis,str)
    
    # labels
    label = ls(label,str,None)
    
    # start-stop-step, check if ok
    start = ls(start,int,float)
    stop  = ls(stop ,int,float)
    step  = ls(step ,int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options'
        if verbo: print(ansi(descr,'red'))
        return
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initializations
    new_datap = []
    new_obser = []
    new_files = []
    new_title = []
    new_xaxis = []
    new_yaxis = []
    indexes = [] # indexes of selected columns - keep in order 
    
    # iterate through "columns"
    for col in columns:
        
        # "col" is a string
        if type(col) is str:
            
            # retrieve indeces that match that string
            for k,lab in enumerate(self.obser):
                if not match(col,lab):
                    continue
                
                # now "k" is an index - check "args" matches
                found = True
                ori = XVG_DIV.join(lab.split(XVG_DIV)[:-1])
                for mat in match_orig:
                    found = False
                    if match(mat,ori):
                        found = True
                        break
                if not found: continue
                obs = lab.split(XVG_DIV)[-1]
                for mat in match_obser:
                    found = False
                    if match(mat,obs):
                        found = True
                        break
                if not found: continue
                for mat in match_files:
                    found = False
                    if match(mat,self.files[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_title:
                    found = False
                    if match(mat,self.title[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_xaxis:
                    found = False
                    if match(mat,self.xaxis[k]):
                        found = True
                        break
                if not found: continue
                for mat in match_yaxis:
                    found = False
                    if match(mat,self.yaxis[k]):
                        found = True
                        break
                if not found: continue
                indexes.append(k) # "k" is ok and can be included
        
        # "col" is an index - check if acceptable
        else:
            col = round(col)
            col+= 0 if col>=0 else H
            if not 0<=col<H:
                continue
            
            # check "args" matches
            found = True
            lab = self.obser[col]
            ori = XVG_DIV.join(lab.split(XVG_DIV)[:-1])
            for mat in match_orig:
                found = False
                if match(mat,ori):
                    found = True
                    break
            if not found: continue
            obs = lab.split(XVG_DIV)[-1]
            for mat in match_obser:
                found = False
                if match(mat,obs):
                    found = True
                    break
            if not found: continue
            for mat in match_files:
                found = False
                if match(mat,self.files[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_title:
                found = False
                if match(mat,self.title[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_xaxis:
                found = False
                if match(mat,self.xaxis[col]):
                    found = True
                    break
            if not found: continue
            for mat in match_yaxis:
                found = False
                if match(mat,self.yaxis[col]):
                    found = True
                    break
            if not found: continue
            indexes.append(col) # "col" is ok and can be included
    
    # build new xvg
    for k in indexes:
        new_datap.append(self.get_datap()[k].copy())
        new_obser.append(self.obser[k])
        new_files.append(self.files[k])
        new_title.append(self.title[k])
        new_xaxis.append(self.xaxis[k])
        new_yaxis.append(self.yaxis[k])
    
    # check if results
    K = len(new_datap)
    if not K:
        descr = f'WARNING: no columns matching requirements'
        if verbo: print(ansi(descr,'yellow'))
        return res
    
    # fix observables according to "label"
    label = adjust_list(label,K)
    new_obser =  [obs if lab is None else lab\
                  for lab,obs in zip(label,new_obser)]
    
    # nest data if you have to
    if nest and K > 1 and len(set(new_files)) > 1:
        new_orig  = [extension(fil.split('/')[-1])[0] for fil in new_files]
        new_obser = [f'{ori}{XVG_DIV*(len(ori)*len(obs)>0)}{obs}'\
                     for ori,obs in zip(new_orig,new_obser)]
    
    # adjust lengths of time options to n columns to select
    start = adjust_list(start,K)
    stop  = adjust_list(stop, K)
    step  = adjust_list(step, K)
    frame = adjust_list(frame,K)
    
    # build frame - iterate through columns
    for       k, sta,  sto, ste, fra in\
    zip(range(K),start,stop,step,frame):
        
        # process start
        if type(sta) in FLOAT:
            sta = search_bigger(times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            sto = search_smaller(times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frame accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                fra = [sta]
                for i,t in enumerate(times[sta:sto]):
                    if t-times[fra[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
        
        # process already existing "frame"
        else:
            temp,fra = fra,[]
            for f in temp:
                if type(f) in FLOAT:
                    f = search_equal(times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        frame[k] = fra
    
    # remove unwanted frames
    # iterate throgh added columns
    for k,fra in zip(range(K),frame):
        for i in range(N):
            if i in fra:
                continue
            new_datap[k][i] = NaN
    
    # ingredients to build the output xvg
    res.set_times(times.copy())
    res.set_datap(new_datap)
    res.obser = new_obser 
    res.files = new_files
    res.title = new_title
    res.xaxis = new_xaxis
    res.yaxis = new_yaxis
    
    # delete times with only NaNs left
    if res.size(): res.prune(nan)
    if verbo:
        M = K*len(res.get_times()) if not nan else res.size()
        print(f'SELECTED {K} column{"s"*(K!=1)} and {M} point{"s"*(M!=1)}')
    return res
