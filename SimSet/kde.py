from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde_evaluate import _evaluate

###############################################################################
################################## CLASS: KDE #################################
###############################################################################

class kde():
    
    """
    Representation of a kernel-density estimate using Gaussian kernels.
    The estimation works best for a unimodal distribution; bimodal or
    multi-modal distributions tend to be oversmoothed.
    
    Methods
    -------
    __call__ : alias for ``self.evaluate``
    
    Attributes
    ----------
    data    : (unchangeable) ndarray of floats, data columns to estimate
              the KDE pdf from
    N       : (unchangeable once ``weights`` is set) float, effective
              number of datapoints; equal to ``data`` length only when
              all weights are equal
    weights : ndarray of floats, the weights of datapoints; it can be
              set directly, OR by providing an array-like of floats,
              or a``SimSet.simple`` function; in the latter case: the
              the weights shall be computed as in the ``__init__``
              method; every time it is set, ``N``, ``bandw`` and
              ``covariance`` attributes ARE UPDATED
    bandw   : matrix of floats, the estimator bandwidth factor RELATIVE
              to the ``data``'s covariance; it has the same shape as
              ``covariance``; it can be set directly, OR by providing a
              str, a float, or an array-like of floats; in the latter
              case: the bandwidth factor shall be computed as in the
              ``__init__`` method
    method  : (unchangeable once ``bandw`` is set) str, current
              bandwidth method, it can be either ``"scott"``,
              ``"silverman"``, or ``""``
    covariance : (unchangeable) ndarray of floats, the covariance
                 matrix of ``data``
    
    Notes
    -----
    Bandwidth selection strongly influences the estimate obtained from
    the KDE (much more than the actual shape of the kernel).
    """
    
    ###########################################################################
    ############################ SET ATTRIBUTES' TYPE #########################
    ###########################################################################
    
    # data (unchangeable)
    def _get_data(self):
        return self.__data
    data = property(_get_data)
    
    # N (unchangeable once weights is set)
    def _get_N(self):
        return self.__N
    N = property(_get_N)
    
    # weights
    def _get_weights(self):
        return self.__weights
    def _set_weights(self,weights):
        d,N = self.data.shape # not the effective number
        if   type(weights) is simple:
            weights = [weights(i) for i in range(N)]
        else:
            weights = ls(weights,int,float)
            if not weights:
                raise ValueError('bad "weights" entry')
            weights = adjust_list(weights,N)
        W = sum(weights)
        self.__weights = array([w/W for w in weights]) # normalize
        self.__N = 1/sum(self.weights**2) # effective number of datapoints
        if self.__method: self.bandw = self.method # update bandwidth
        self.__covariance = cov(self.data,aweights=self.weights) if d > 1 else\
                            array([[std(self.data[0])**2]]) # update covariance
    weights = property(_get_weights,_set_weights)
    
    # bandw
    def _get_bandw(self):
        return self.__bandw
    def _set_bandw(self,bandw):
        N,d = self.N,self.data.shape[0]
        if type(bandw) is str:
            if   bandw.casefold() == 'scott':
                self.__method = 'scott'
                factor = N**(-1/(d+4))
            elif bandw.casefold() == 'silverman':
                self.__method = 'silverman'
                factor = (N*(d+2)/4)**(-1/(d+4))
            else: raise ValueError('bad "bandw" entry')
            bandw = array([factor]*d)
        else:
            self.__method = ''
        try   : bandw = array(bandw,ndmin=1,dtype=float)
        except: raise ValueError('bad "bandw" entry')
        if len(bandw.shape) == 1:
            self.__bandw = diag (adjust_list(bandw,d))
        else:
            self.__bandw = array(adjust_list([adjust_list(b,d)\
                                 for b in bandw],d))
    bandw = property(_get_bandw,_set_bandw)
    
    # method
    def _get_method(self):
        return self.__method
    method = property(_get_method)
    
    # covariance
    def _get_covariance(self):
        return self.__covariance
    covariance = property(_get_covariance)
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,*data,bandw=BANDW,weights=[1]):
        
        """
        Initialize ``self``.
        
        Parameters
        ----------
        data    : tuple of array-like of floats, data columns to
                  estimate the KDE pdf from
        weights : array-like of floats, or ``SimSet.simple`` function,
                  the weights of data columns; will adjust its length
                  to match that of ``data``, and normalize their sum to
                  ``1``; default is ``[1]``
        bandw   : if str: the method used to compute the estimator
                  bandwidth ('scott' or 'silverman') RELATIVE to the
                  data's covariance; if float: set the bandwidth for
                  all ``data``'s dimensions; if an array-like of floats:
                  will adjust its shape to that of ``data`` covariance;
                  if square matrix with the same shape of ``data``'s
                  covariance: set directly; default is ``SimSet.BANDW``
        """
        
        # dataset
        try:
            data = array(data,dtype=float,ndmin=2)
        except Exception as traceback:
            error = ansi('bad "data" entry, with traceback:')
            error = f'{error}\n{traceback}'
            raise ValueError(error)
        if not len(data):
            error = ansi('no "data"','red')
            raise ValueError(error)
        self.__data = data
        
        # weights & number of effective datepoints
        self.__method = '' # current bandwidth method
        try    : self.weights = weights
        except : raise ValueError(ansi('bad "weights" entry','red'))
        
        # bandwidth
        try    : self.bandw = bandw
        except : raise ValueError(ansi('bad "bandw" entry','red'))
    
    ###########################################################################
    ################################# EVALUATE ################################
    ###########################################################################
    
    def evaluate(self,*points,bandw=None,verbose=VERBOSE):
        
        """
        Evaluate the estimated pdf on a set of points.
        
        Parameters
        ----------
        points : tuple of array-like of floats, data columns of the
                 columns on which to evaluate ``self``
        bandw  : if not ``None``: updates ``self.bandw``
        verbose : bool, be loud noisy, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        values : ``len(points)``-shaped array of floats
        
        Raises
        ------
        ValueError : if the dimensionality of the input points is
                     different than the dimensionality of the KDE
        """
        
        return _evaluate(self,*points,bandw=bandw,verbose=verbose)
    
    # alias for self.evaluate
    __call__ = evaluate
    
    ###########################################################################
    ############################### LOG LIKELIHOOD ############################
    ###########################################################################
    
    def likelihood(self,*points,bandw=None,verbose=VERBOSE):
        
        """
        Compute the log-likelihood of data for a KDE estimation with
        current bandwidth settings.
        
        Parameters
        ----------
        points : tuple of array-like of floats, data columns of the
                 points on which to evaluate the log-likelihood of
                 ``self``
        bandw  : if not ``None``: updates ``self.bandw``
        verbose : bool, be loud noisy, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        result : log likelihood
        """
        
        return sum(log(p) for p in self(*points,bandw=bandw,verbose=verbose))
    
    ###########################################################################
    ########################## CV BANDWIDTH ESTIMATION ########################
    ###########################################################################
    
    def crossv(self,*bandw,n=DEF_CV,verbose=VERBOSE):
        
        """
        Estimate the goodness of a set of (one-dimensional)
        bandwidths through cross-validation, by computing the
        log-likelihood of data.
        
        Parameters
        ----------
        bandw : tuple of floats or array-like of floats, bandwidths to
                estimate the log-likelihood on; they can be even string
                provided they are ``"scott"`` or ``"silverman"``
        n     : int, number of random replica to consider on each step,
                default is ``SimSet.DEF_CV``
        verbose : bool, be loud noisy, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        likelihood : list of floats, log-likelihood estimates for each
                     input bandwidth
        error : list of floats, standard deviation of each estimate
        
        Examples
        --------
        >>> import SimSet
        >>> from numpy.random import normal
        >>> k = SimSet.kde([normal() for x in range(1000)],
        ...                [normal() for x in range(1000)])
        >>>
        >>> # values of bandwidth to be tested
        >>> bandw = ['scott','silverman',.1,.2,.3,.4,.5]
        >>> likelihood,error = k.cross_validation(bandw)
        >>>
        >>> # with an errorbar plot
        >>> import numpy as np
        >>> x = np.linspace(0.3,1.0,20)
        >>> SimSet.plt.errorbar(x,*k.cross_validation(x),ecolor='red')
        >>> SimSet.plt.draw()
        >>> SimSet.plt.pause(.1)
        """
        
        # process bandw
        bandw = ls(bandw,str,int,float)
        bandw = [b for b in bandw if (type(b) is str and\
                 b.casefold() in ['scott','silverman']) or type(b) is not str]
        if not bandw:
            return [],[]
        
        # initialize indexes for data shuffling
        data = self.data
        indexes = list(range(data.shape[1]))
        
        # distribute datapoints among replicas
        N = self.data.shape[1]
        span1 = N//n+(N<n)
        span2 = N%n*(N>n)
        intervals = []
        index,first = 0,0
        while first < N:
            last = min(N,first+span1+(span2>0))
            intervals.append((first,last))
            index+= 1
            first+= span1+(span2>0)
            if span2:
                span2-= 1
        replicas = len(intervals)
        
        # results initialization
        likelihood = []
        error      = []
        
        # initialize progress bar
        cursor.hide()
        current = 0
        N,t0,etr,speed = n*len(bandw),now(),None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(current,N,0,etr,speed,'',width)
        if verbose:
            p = max(len(bandw),PROCESSES)
            print(f'ESTIMATING {len(bandw)} log likelihoods')
            print(f'RUNNING on {p} core{"s"*(p!=1)}')
            print(f'{bar}\r',end='')
        
        # iterate through bandwidths
        for b in bandw:
            temp = [] # temporary list of log likelihoods
            shuffle(indexes) # shuffle data
            
            # iterate through training/test set replicas
            for interval in intervals:
                first,last = interval
                training =     data[:,indexes[first:last]]
                test = np.delete(data,indexes[first:last],axis=1)
                
                # compute log likelihood of this replica
                k = kde(*training,bandw=b)
                temp.append(k.likelihood(*test,verbose=False))
                
                # report progress
                if not verbose:
                    continue
                current+= 1
                elapsed = now()-t0
                speed = current/elapsed
                etr = (N-current)/speed
                bar = progress_bar(current,N,elapsed,etr,speed,'',width)
                print(f'{bar}\r',end='')
            
            # compute mean and error
            likelihood.append(mean(temp))
            error.append(std(temp,likelihood[-1]))
        
        # return output
        cursor.show()
        if verbose: print()
        return likelihood,error
