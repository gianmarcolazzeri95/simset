from SimSet.params import *
from SimSet.aux    import *

###############################################################################
################################## CLASS: SSH #################################
###############################################################################

class ssh:
    
    """
    Handle communication with remote cluster. It can:
      - update job submission parameters
      - create pbs files for running simulations with Gromacs
      - create ssh commands for running simulation with Gromacs
      - check a job or a cluster node status
    
    Attributes
    ----------
    add   : str, remote access
    dir   : str, remote working directory
    email : str, email linked to remote access
    gmx   : str, remote path to Gromacs
    queue : list of str, default queue name for a job, set to
            ``SimSet.DEF_QUE``
    nodes : int, default number of nodes for a job, set to
            ``SimSet.DEF_NOD``
    proce : int, default number of processors per node, set to
            ``SimSet.DEF_PRO``
    memor : str, default processor memories for a job, set to
            ``SimSet.DEF_MEM``
    wallt : str, default wall-time for a job, set to
            ``SimSet.DEF_WAL``
    """
    
    ###########################################################################
    ############################ SET ATTRIBUTES' TYPE #########################
    ###########################################################################
    
    # add
    def _get_add(self):
        return self.__add
    def _set_add(self,value):
        if not isinstance(value,str):
            raise TypeError('"add" must be set to a string')
        self.__add = value
    add = property(_get_add,_set_add)
    
    # dir
    def _get_dir(self):
        return self.__dir
    def _set_dir(self,value):
        if not isinstance(value,str):
            raise TypeError('"dir" must be set to a string')
        self.__dir = value
    dir = property(_get_dir,_set_dir)
    
    # email
    def _get_email(self):
        return self.__email
    def _set_email(self,value):
        if not isinstance(value,str):
            raise TypeError('"email" must be set to a string')
        self.__email = value
    email = property(_get_email,_set_email)
    
    # gmx
    def _get_gmx(self):
        return self.__gmx
    def _set_gmx(self,value):
        if not isinstance(value,str):
            raise TypeError('"gmx" must be set to a string')
        self.__gmx = value
    gmx = property(_get_gmx,_set_gmx)
    
    # queue
    def _get_queue(self):
        return self.__queue
    def _set_queue(self,value):
        value = ls(value,str)
        if not value:
            raise TypeError('"queue" must be set to a string'\
                            'or an array-like of strings')
        self.__queue = value
    queue = property(_get_queue,_set_queue)
    
    # nodes
    def _get_nodes(self):
        return self.__nodes
    def _set_nodes(self,value):
        if type(value) not in INTEGER:
            raise TypeError('"nodes" must be set to a integer')
        self.__nodes = value
    nodes = property(_get_nodes,_set_nodes)
    
    # proce
    def _get_proce(self):
        return self.__proce
    def _set_proce(self,value):
        if type(value) not in INTEGER:
            raise TypeError('"proce" must be set to a integer')
        self.__proce = value
    proce = property(_get_proce,_set_proce)
    
    # memor
    def _get_memor(self):
        return self.__memor
    def _set_memor(self,value):
        if not isinstance(value,str):
            raise TypeError('"memor" must be set to a string')
        self.__memor = value
    memor = property(_get_memor,_set_memor)
    
    # wallt
    def _get_wallt(self):
        return self.__wallt
    def _set_wallt(self,value):
        if not isinstance(value,str):
            raise TypeError('"wallt" must be set to a string')
        self.__wallt = value
    wallt = property(_get_wallt,_set_wallt)
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,**args):
        
        """
        Initialize ``self``.
        
        Parameters
        ----------
        args : dict, optional arguments
          'shadd' : str, remote access, default is ``SimSet.DEF_SHA``
          'shdir' : str, remote working directory, default is
                    ``SimSet.DEF_SHD``
          'email' : str, email linked to remote access, default is
                    ``SimSet.DEF_EMA``
          'gmx'   : str, remote path to Gromacs, default is
                   ``SimSet.DEF_GMX``
          'ssh_gmx' : str, remote path to Gromacs, default is ``gmx``
                      higher priority than ``gmx``
        
        Notes
        -----
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.system_mdrun._mdrun : uses this class
        SimSet.system : uses this class
        """
        
        #######################################################################
        ############################### PARSING ###############################
        #######################################################################
        
        # general parameters parameters
        self.add   = parse(args,'shadd'  ,DEF_SHA )
        self.dir   = parse(args,'shdir'  ,DEF_SHD )
        self.email = parse(args,'email'  ,DEF_EMA )
        self.gmx   = parse(args,'gmx'    ,DEF_GMX )
        self.gmx   = parse(args,'ssh_gmx',self.gmx)
        
        # job submission parameters
        self.queue = DEF_QUE
        self.nodes = DEF_NOD
        self.proce = DEF_PRO
        self.memor = DEF_MEM
        self.wallt = DEF_WAL
        
    ###########################################################################
    ######################### PRINT AND REPRESENT SELF ########################
    ###########################################################################
        
    def __str__(self):
        
        """
        Return info about the ssh communication/job submission.
        """
        
        # terminal width and scaling
        try:    width = min(max(40,get_terminal_size()[0]),120)
        except: width = DEF_WID
        scale = width//4
        width = scale*4
        
        # padding
        padA = scale*2-7
        padB = padA+2
        padD = scale//4
        padE = scale*2//5
        padF = padE+1
        padC = width-padD-padE*2-padF-35
        
        # preliminary formatting
        ppn = f'{self.proce}*{self.nodes}'
        
        # paths
        text =[ansi('PATHS','bold','cyan'),f' {"-"*(width-6)}\n',
               ansi('add:','bold','cyan'),'   ',format(self.add,  -padA),
               ansi('dir:'  ,'bold','cyan'),' ',format(self.dir,  -padB),'\n',
               ansi('email:','bold','cyan'),' ',format(self.email,-padA),
               ansi('gmx:'  ,'bold','cyan'),' ',format(self.gmx,-padB,0),'\n',
        
               # jobs
               ansi('JOB SUBMISSION','yellow','cyan'),f' {"-"*(width-15)}\n',
               ansi('queue:','bold','yellow'),' ',format(queue     ,-padC),
               ansi('nodes:','bold','yellow'),' ',format(self.nodes,-padD),
               ansi(' proc:','bold','yellow'),' ',format(ppn       ,-padE),
               ansi('memor:','bold','yellow'),' ',format(self.memor,-padE),
               ansi('wallt:','bold','yellow'),' ',format(self.wallt,-padF,0)]
        
        return ''.join(text)
    
    def __repr__(self):
        
        """
        Return a string representation of ``self``.
        """
        
        return f'ssh connection to '+ansi(f'{self.add}:{self.dir}','bold')
    
    def __format__(self,spec):
        
        """
        Return a proper formatted string of ``self``.
        """
        
        return f'{no_ansi(self.__str__())}\n'
    
    ###########################################################################
    ################################## UPDATE #################################
    ###########################################################################
    
    def update(self,**args):
        
        """
        Update ``self`` attributes.

        Parameters
        ----------
        args : dict, optional arguments
          'nodes' : int, default number of nodes for a job, updates
                    ``self.nodes``; if not a valid entry: don't update
          'ppn'   : int, default number of processors per node, updates
                    ``self.proce``; if not a valid entry: don't update
          'wallt' : str, default wall-time for a job, updates
                    ``self.wallt``; if not a valid entry: don't update
          'queue' : str or array-like of str, default queue name for a
                    job, updates ``self.queue``; if not a valid entry:
                    don't update ``self.queue``
          'memor' : str, default processor memories for a job, updates
                    ``self.memor``; if not a valid entry: don't update
        
        Notes
        -----
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        """
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # parse and check options
        try:    nodes = parse(args,'nodes',self.nodes)
        except: pass
        try:    proce = parse(args,'ppn'  ,self.proce)
        except: pass
        wallt = parse(args,'wallt',self.wallt)
        queue = parse(args,'queue',self.queue)
        memor = parse(args,'mem'  ,self.memor)
        
        # process options
        queue = [que for que in ls(queue,str) if que]
        try:    tosec(wallt)
        except: wallt = self.wallt
        if memor and not memor[0].isdigit():
            memor = self.memor
        
        # update ssh attributes
        self.nodes = nodes
        self.proce = proce
        self.wallt = wallt
        self.queue = queue if queue else self.queue
        self.memor = memor
    
    ###########################################################################
    ############################# CHECK JOB STATUS ############################
    ###########################################################################
    
    def check_jobs(self,*jobs,queue='*',verbose=VERBOSE):
        
        """
        Check `jobs` by ids, return the status ("Q", "R", "H", "E")
        and first digit of the job exit status of each job.
        
        Parameters
        ----------
        jobs  : tuple of int or str/array-like of int or str, job ids
        queue : str or array like of str, queues to check jobs on;
                regular expressions characters "*" and "?" are allowed;
                default is ``"*"`` (which means: look among ALL the
                cluster's queues)
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        result: list of tuples ``(job,status,exit)``, where ``job`` has
                status ``status`` (str), and exit status ``exit`` (int
                or ``None``)
        
        Notes
        -----
        When some jobs are not recognized, there are less elements in 
        ``result`` than in ``jobs``.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        self.check : method
        self.check_nodes : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> 
        >>> ssh = system.ssh
        >>> res = ssh.check(30000000,'3333333.hpc-head-n1.unitn.it'
        ...                 queue='short*')
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # jobs
        jobs = ls(jobs,str,int)
        if not len(jobs):
          return []
        
        # queue(s)
        queues = ls(queue,str)
        if not len(queues):
            return []
        
        # temp file
        NOW = round(now())
        tmp_fil = f'{DEF_TMP}{NOW}jobs'
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # launch "qstat"
        commd = []
        for job in jobs:
            commd.append(f'qstat -xf {job}\n')
        commd = f'ssh {self.add} \'\n{"".join(commd)} \' 1>"{tmp_fil}"'
        if not verbose:
            commd+= " 2>/dev/null"
        sh(commd)
        
        # retrieve raw result and delete temp file
        with open(tmp_fil) as f:
            raw = f.read().split('\n\n')
        sh(f'rm -f "{tmp_fil}"')
        
        # scrape "raw"
        result = []
        for r in raw:
            i,j = 0,0
            
            # scrape job id
            field = 'Job Id: '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            jobid = int(r[i:j].split('.')[0])
            
            # scrape job state
            field = 'job_state = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            state = r[i:j]
            
            # scrape queue
            field = 'queue = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            queue = r[i:j]
            
            # queue not in the list
            if not sum([len(match(que,queue)) for que in queues]):
                continue
            
            # scrape exit status
            field = 'Exit_status = '
            i = scan(r,field,j)
            if i is None:
                exit = None
            else:
                i+= len(field)
                j = scan(r,'\n',i)
                exit = int(r[i:j])
            
            # append to result
            result.append((jobid,state,exit))
        return result
    
    ###########################################################################
    ########################### CHECK NODE STATUS #############################
    ###########################################################################
    
    def check_nodes(self,*nodes,queue='*',verbose=VERBOSE):
        
        """
        Check ``nodes`` by name, return the number of processors and
        the number of available processors for each node, compatibly
        with ``queue``.
        
        Parameters
        ----------
        nodes : tuple of str or of array-like of str, nodes to be
                checked; regular expressions characters "*" and "?"
                are allowed; default is ``"*"`` (which means: look
                among ALL the cluster's nodes)
        queue : str or array like of str, queues to check nodes on;
                regular expressions characters "*" and "?" are allowed;
                default is ``"*"`` (which means: look among ALL the
                cluster's queues)
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        result: list of tuples ``(node,available,free)``, where ``node``
                has ``available`` available processors assigned to
                ``queue``, and ``free`` free processors; ONLY the nodes
                with ``available>0`` are in ``result``, and of course
                ``available>=free``
        
        Notes
        -----
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        self.check : method
        self.check_jobs : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> ssh = system.ssh
        >>> res = ssh.check('hpc-c03-node0*',queue='short*')
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # nodes
        nodes = ls(nodes,str)
        if not len(nodes):
          return []
        
        # queue
        queue = ls(queue,str)
        if not len(queue):
            return []
        
        # temp file
        NOW = round(now())
        tmp_fil = f'{DEF_TMP}{NOW}nodes'
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # launch "pbsnodes"
        commd = []
        for nod in nodes:
            if '*' in nod or '?' in nod:
                commd = ['pbsnodes -a']
                break
            commd.append(f'pbsnodes {nod}\n')
        commd = f'ssh {self.add} \'\n{"".join(commd)} \' 1>"{tmp_fil}"'
        if not verbose:
            commd+= " 2>/dev/null"
        sh(commd)
        
        # retrieve raw result and delete temp file
        with open(tmp_fil) as f:
            raw = f.read().split('\n\n')
        sh(f'rm -f "{tmp_fil}"')
        
        # scrape "raw"
        result = []
        for r in raw:
            i,j = 0,0
            
            # scrape node name
            field = ''
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            node = r[i:j].split('.')[0]
            
            # node not in the list
            if not sum([len(match(nod,node)) for nod in nodes]):
                continue
            
            # scrape state
            field = 'state = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            state = r[i:j]
            if state not in ['free','job-busy']: continue
            
            # scrape available processors
            field = 'resources_available.ncpus = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            available = int(r[i:j])
            
            # scrape available queues
            field = 'resources_available.queuelist = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            queues = r[i:j].split(',')
            
            # queue not in the list
            if not sum([len(match(que,*queues)) for que in queue]):
                continue
            
            # scrape free processors
            field = 'resources_assigned.ncpus = '
            i = scan(r,field,j)
            if i is None: continue
            i+= len(field)
            j = scan(r,'\n',i)
            free = available-int(r[i:j])
            
            # append to result
            result.append((node,available,free))
        return result
    
    ###########################################################################
    ############################ CHECK OBJ STATUS #############################
    ###########################################################################
    
    def check(self,*names,queue='*',verbose=VERBOSE):
        
        """
        Wrapper for ``self.check_jobs`` and ``self.check_nodes``.
        
        Parameters
        ----------
        names : tuple of int or str, or array-like of int or str: if a
                element is made of digits only: run ``self.check_jobs``;
                otherwise: run ``self.check_nodes``; for further details
                see the description of those methods
        queue : str or array like of str, queues to check nodes/jobs on;
                regular expressions characters "*" and "?" are allowed;
                default is ``"*"`` (which means: look among ALL the
                cluster's queues)
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        result : list of tuples, FIRST the ``result`` about the nodes,
                 then that about the jobs, concatenated; see related
                 methods for details
        
        Notes
        -----
        When some objects are not recognized / available, there are less
        elements in ``result`` than in ``name``.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        self.check_jobs : method
        self.check_nodes : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> ssh = system.ssh
        >>> res = ssh.check('hpc-c03-node01',3333333,queue='short*')
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # names
        names = ls(names)
        if not len(names):
            return []
        
        # split in jobs and nodes
        names = ls(names)
        jobs,nodes = [],[]
        for name in names:
            if str(name).isdigit():
                jobs.append(int(name))
            else:
                nodes.append(name)
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # result
        result  = self.check_jobs(jobs,queue=queue,verbose=verbose)
        result += self.check_nodes(nodes,queue=queue,verbose=verbose)   
        return result
    
    ###########################################################################
    ########################### OPTIMAL NODES LIST ############################
    ###########################################################################
    
    def best_nodes(self,n,nodes='*',queue='*',verbose=VERBOSE):
        
        """
        Find optimal nodes list for running ``n`` jobs at once,
        selected among ``nodes`` and for queue ``queue``.
        
        Parameters
        ----------
        n: number or required jobs
        nodes : tuple of str or of array-like of str, nodes to be
                checked; regular expressions characters "*" and "?"
                are allowed; default is ``"*"`` (which means: look
                among ALL the cluster's nodes)
        queue : str or array like of str, queues to check nodes on;
                regular expressions characters "*" and "?" are allowed;
                default is ``"*"`` (which means: look among ALL the
                cluster's queues)
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        nodes: list of nodes, the first ``self.nodes`` are intended to
               run job #1, etc.
        
        Notes
        -----
        Nodes are selected among ``nodes`` according to the principle
        of highest availability. If there are no nodes available for
        running all the required jobs at the same time, also nodes with
        not enough available processors shall be returned. If looking
        for an appropriate list of nodes: adapt ``self.check("*")``.
        
        See Also
        --------
        self.check_jobs : method
        self.check_nodes : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> ssh = system.ssh
        >>> ssh.nodes = 2
        >>> res = ssh.nodes(3)
        ['node01', 'node03', 'node05', 'node07', 'node02', 'node09']
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # n of required jobs
        if type(n) not in INTEGER+FLOAT:
            return []
        if int(n) <= 0:
            return []
        N = self.nodes # total nodes required: n*N
        
        # nodes
        nodes = ls(nodes,str)
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # check nodes availability
        result = self.check_nodes(*nodes,queue=queue,verbose=verbose)
        
        # order and select nodes with at least self.proce processors
        result = [res for res in sorted(result,key=lambda res:-res[2])\
                       if res[1] >= self.proce]
        
        # check resource availability
        if len(result) < self.nodes:
            descr = f'ERROR: not enough resources for {self.proce} ppn'
            if verbose: print(ansi(descr,'red'))
            return []
        
        # split nodes in stacks
        stacks    = [] # self.proce-long processors list
        available = [] # min number of available processors per stack
        for i in range(len(result)//N):
            stacks   .append(    [res[0] for res in result[i*N:(i+1)*N]] )
            available.append(min([res[2] for res in result[i*N:(i+1)*N]]))
        
        # duplicate stacks with enough processors available
        nodes = []
        i = 0 # index of stacks / available
        while len(nodes) < n*self.nodes:
            if max(available)>=self.proce\
               and available[i%len(stacks)]<self.proce:
                i = 0
                continue
            available[i%len(stacks)]-= self.proce # less processors available
            for node in stacks[i%len(stacks)]:
                nodes.append(node)
            i+= 1
        
        # adjust length
        return nodes
    
    ###########################################################################
    ########################### MAKE QSUB COMMAND #############################
    ###########################################################################
    
    def qsub(self,*output,after=0,nodes=[],queue='*',remote=False):
        
        """
        Make command for qsub of all ``output`` jobs at once, with
        ``SimSet.ssh`` parameters. Select nodes in ``nodes``.
        
        Parameters
        ----------
        output: tuple str/array-like of str, pbs fname or file prefix
        after: int, str, or array-like of both, wait for job to
               complete correctly, 0 means "wait for none", if
               ``output`` has length 1: wait for all jobs in ``after``
               to complete before running; otherwise: match ``after``
               and ``output`` elements; default is 0
        nodes : str or array-like or str; submit jobs on ``nodes`` list
                of nodes; adjust length to match number of required
                nodes; if not defined: don't control which nodes the
                job is assigned to; default is ``[]``
        queue : str, submit job on ``queue``; regular expression
                characters "*" and "?" are allowed, in this case: take
                the first element of ``self.queue`` that matches
                ``queue``; if not defined; take the first element of
                ``self.queue``
        remote: bool, if ``True``: assumes you are already on remote,
                so "ssh <self.add>" prefix is not needed
        
        Returns
        -------
        commd: str, command to be run
        
        Notes
        -----
        Lists' depth is reduced to one through ``SimSet.ls`` function,
        except for ``after`` when ``output`` length is >1.
        
        See Also
        --------
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> ssh = system.ssh
        >>> system.mdrun()
        >>>
        >>> # submit 10 jobs at once on cluster
        >>> jobs = [f'job{i} for i in range(10)]
        >>> commd = ssh.qsub(jobs)
        >>>
        >>> # jobs must be submitted after job 1000 has ended
        >>> commd = ssh.qsub(jobs,after=1000)
        >>>
        >>> # jobs must be submitted in a specific list of nodes
        >>> ssh.nodes = 2
        >>> nodes = ['node01','node02','node03','node04']
        >>> commd = ssh.qsub(jobs[:3],nodes=nodes)
        >>> # nodes assignment:
        >>> # "job1" > "node01", "node02"
        >>> # "job2" > "node03", "node04"
        >>> # "job3" > "node01", "node02"
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # output
        output = ls(output,str)
        output = ['.'.join(extension(out,'pbs')) for out in output]
        n = len(output)
        
        # after
        if n > 1:
            after = list(after) if type(after) in [list,tuple,ndarray] else\
                    [after]
            after = (after*n)[:n]
        else:
            after = [ls(after)]
        
        # nodes
        nodes = ls(nodes,str)
        if not nodes: nodes = ['']
        nodes = adjust_list(nodes,n*self.nodes)
        stacks = [nodes[i*self.nodes:(i+1)*self.nodes] for i in range(n)]
        
        # queue
        queue = ls(queue,str)
        if not queue: que = self.queue[0]
        for que in queue:
            if not ('*' in que or '?' in que):
                queue = que
                break
            mat = match(que,*self.queue)
            if mat:
                queue = mat[0]
                break
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # build command
        commd = f"ssh {self.add} '\n" if not remote else " '\n"
        for out,aft,sta in zip(output,after,stacks):
            commd+= f'echo $(qsub {aftok(aft)} -l select=1'
            commd+= '+1'.join([f':ncpus={self.proce}:mpiprocs={self.proce}'+\
                   (f':mem={self.memor}' if self.memor else '')+\
                    f'{(":host="+node)*(len(node)>0)}' for node in sta])
            commd+= f' -q {queue} "{self.dir}/{out}" 2>/dev/null )\n'
        return f"{commd} '"
    
    ###########################################################################
    ########################### CREATE LIST OF PBS ############################
    ###########################################################################
    
    def pbs(self,*output,inpt='',ndx='',title='mdrun',mdp=DEF_MDP,
            topol=DEF_TOP,extend=EXTEND,auto=False,nodes=[],
            verbose=VERBOSE):
        
        """
        Create ``output`` pbs files for Gromacs simulation, following
        to ``self`` job submission settings. If wall-time
        ``self.wallt`` is set and option ``extend`` is ``True``:
        include authomatic restart in pbs files if the simulation
        hasn't finished yet and wall-time is reached.
        
        Parameters
        ----------
        output : tuple str/array-like of str, pbs filename prefix
        inpt  : str or array-like or str, input structure filename,
                number of elements is adjusted to match ``pbs`` length
        ndx   : str or array-like or str, ndx filename, default is ""
                number of elements is adjusted to match ``pbs`` length
        title : str or array-like of str, pbs title default is "mdrun",
                number of elements is adjusted to match ``pbs`` length
        mdp   : str, mdp file, default is ``SimSet.DEF_MDP``
        topol : str, Gromacs top file, default is ``SimSet.DEF_TOP``
        extend: bool, if ``True``: restart from ``output`` cpt file,
                default is ``SimSet.EXTEND``
        auto  : bool, if ``extend`` and ``self.wallt`` is set: include
                authomatic restart in pbs files if the simulation
                hasn't finished yet and wall-time is reached; default
                is ``False``
        nodes : str or array-like or str; submit jobs on ``nodes`` list
                of nodes; adjust length to match number of required
                nodes; if not defined: don't control which nodes the
                job is assigned to; default is ``[]``
        verbose : bool, be loud, default is ``SimSet.VERBOSE``
        
        Notes
        -----
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function,
        except for ``after`` when ``output`` length is >1.
                 
        See Also
        --------
        self.qsub : method
        SimSet.parse : function
        SimSet.ls : function
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # output
        output = ls(output,str)
        if not len(output):
            return ''
        output = [extension(out)[0] for out in output]
        n = len(output)
        
        # inpt
        inpt = ls(inpt,str)
        if not len(inpt) or sum([inp == '' for inp in inpt]):
            descr = f'ERROR: {inpt} is not a valid input'
            if verbose: print(ansi(descr,'red'))
            return ''
        inpt = (inpt*n)[:n]
        
        # ndx
        ndxs = ls(ndx,str)
        ndxs = (ndxs*n)[:n]
        
        # title
        title = ls(title,str)
        title = (title*n)[:n]
        
        # walltime
        wallt = tosec(self.wallt)-TOLERANCE if self.wallt else 0
        wallt = wallt if wallt > 0 else 0
        
        # auto restart
        auto  = wallt and extend and auto
        
        # optimal nodes if auto and specified correctly
        if auto:
            best = self.best_nodes(n,nodes,False)
            stacks = [best[i*self.nodes:(i+1)*self.nodes] for i in range(n)]
        else:
            stacks = [[]]*n
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        for out,inp,ndx,tit,sta in zip(output,inpt,ndxs,title,stacks):
            
            # pbs header and preliminaries
            pbs = f'#!/bin/bash\n'\
                  f'#PBS -l walltime={self.wallt}\n'\
                  f'#PBS -N {"_".join(tit.split())}\n'\
                  f'#PBS -M {self.email}\n'\
                  f'#PBS -e {self.dir}/error.log\n'\
                  f'#PBS -o {self.dir}/output.log\n'\
                  f'module load cuda-10.0 gcc54\n'\
                  f'source {self.gmx}/GMXRC\n'\
                  f'cd "{self.dir}"\n\n'
            
            # gmx grompp
            pbs+= f'gmx grompp -f "{mdp}" -r "{inp}" '\
                  f'-c "{inp}" -p "{topol}" -o "{out}.tpr" '\
                  f'-po "{out}.mdp" '
            pbs+= f'-n "{ndx}" ' if ndx else ''
            pbs+= f'{" -nobackup"*(not BACKUP)} -maxwarn {MAXWARN}\n\n'
            
            # delete old cpt if not extend
            pbs+= f'rm "{out}.cpt"\n' if not extend else ''
            
            # auto option - measure time
            pbs+= f'TIME=$(date +"%s")\ntimeout {wallt} ' if auto else ''
            
            # restart
            pbs+= f'gmx mdrun{" -nobackup"*(not BACKUP)} '\
                  f'-deffnm "{out}" -ratchet "{out}.rat" '\
                  f'-cpi "{out}.cpt" -cpt {CHECKPT} '\
                  f'-pin on -ntmpi 1 -nt {self.proce}'
            fname = f'{out}.pbs'
            
            # auto option - restart
            if auto:
                pbs+= f'\nTIME=$(($(date +"%s")-$TIME))'\
                      f'\nif (($TIME >= {wallt})); '\
                      f'then {self.qsub(out,nodes=sta,remote=True)}; fi'
            
            # write pbs
            with open(f'{out}.pbs','w') as f:
                f.write(pbs)
