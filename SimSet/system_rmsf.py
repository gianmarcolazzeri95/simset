# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _rmsf(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.rmsf" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    inpt = ls(inpt,str)
    def_ext =  DEF_EXT
    def_sel = [DEF_SEL]
    def_ref = ['']
    def_sel = [DEF_SEL]
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra =[None]
    
    # specific parameters
    exten  = parse(args,'ext'      ,def_ext)
    refere = parse(args,'mol'      ,def_ref)
    refere = parse(args,'ref'      ,refere )
    refere = parse(args,'reference',refere )
    select = parse(args,'sel'      ,def_sel)
    select = parse(args,'select'   ,select )
    select = parse(args,'selection',select )
    start  = parse(args,'start'    ,def_sta)
    stop   = parse(args,'stop'     ,def_sto)
    step   = parse(args,'step'     ,def_ste)
    frame  = parse(args,'frame'    ,def_fra)
    recur  = parse(args,'recur'    ,RECUR  )
    recur  = parse(args,'recursive',recur  )
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # default input
    found = False
    for ext in exten:
        fname = f'{self.last}{"."*bool(ext)}{ext}'
        if isfile(fname):
            def_inp = [fname]
            found = True
            break
    if not found:
        for ext in exten:
            fname = f'{self.molec}{"."*bool(ext)}{ext}'
            if isfile(fname):
                def_inp = [fname]
                found = True
                break
    
    # no input
    if not len(inpt):
        if not found:
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return [],[]
        inpt = def_inp
        descr = f'TAKING "{path_name(def_inp[0])}" as input file\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    line = False # formatting issues
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'WARNING: no files found\n'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: warning
        line = True
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'WARNING: none of {names} found'
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
        continue
    
    # reconstruct input
    inpt = ls(inpt)
    n = len(inpt)
    if not n:
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    elif verbo and line: print()
    
    # reference - check if ok
    refere = ls(refere,str)
    if not len(refere):
        descr = f'ERROR: bad "reference" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    
    # selection - check if ok
    select = ls(select,str)
    if not len(select):
        descr = f'ERROR: bad "selection" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    
    # frame - check if ok
    frame = ls(frame,int,float,None)
    if not len(frame):
        descr = f'ERROR: bad "frame" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    
    # reference list
    line = False # formatting issues
    refere = adjust_list(refere,n)
    for i,ref in enumerate(refere):
        if ref == '.'.join(extension(ref)):
            if isfile(ref):
                continue
        
        # match required extensions
        found = False
        for ext in REF_EXT:
            fname = f'{ref}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if found:
            continue
        
        # input file is also reference
        if extension(inpt[i])[1] in REF_EXT:
            refere[i] = inpt[i]
            continue
        
        # look for alternatives
        found = False
        inp = extension(inpt[i])[0]
        for ext in REF_EXT:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if not found:
            for ext in REF_EXT:
                fname = f'{self.molec}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    refere[i] = fname
                    found = True
                    break
        
        # found alternative
        if found:
            line = True
            descr = f'WARNING: no reference specified '\
                    f'for input file "{path_name(inpt[i])}", '\
                    f'assigning "{path_name(refere[i])}" instead'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # found no alternatives
        descr = f'ERROR: no reference found '\
                f'for input file "{path_name(inpt[i])}"\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    if verbo and line: print()
    
    # adjust lengths to input (except for selections and labels)
    start = adjust_list(start,n)
    stop  = adjust_list(stop ,n)
    step  = adjust_list(step ,n)
    frame = adjust_list(frame,n)
    
    # expand recursively
    if recur:
        m = len(select) # expansion factor
        inpt   = m*inpt
        refere = m*refere
        start  = m*start
        stop   = m*stop
        step   = m*step
        frame  = m*frame
        select = expand_list(select,n)
        n*= m
    else:
        select = adjust_list(select,n)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    def task(index,inpt,refere,select,start,stop,step,frame):
        
        """
        Parameters
        ----------
        index : int, subprocess id to reconstruct results in order
        """
        
        # sub result initialization
        residues,rmsf = [],[]
        
        # main cycle
        for inp, ref,   sel,   sta,  sto, ste, fra in\
        zip(inpt,refere,select,start,stop,step,frame):
            
            # compute & load results
            result = Rmsf(inp,ref,sel,sta,sto,ste,fra,(n==1)*verbo)
            residues.append(result[0])
            rmsf    .append(result[1])
            
            # measure & print progress
            if not verbo or n==1:
                continue
            lock.acquire()
            try:     current.set(current.get()+1)
            finally: lock.release()
            elapsed = now()-t0
            speed = current.value/elapsed
            etr = (n-current.value)/speed
            bar = progress_bar(current.value,n,elapsed,etr,speed,'',width)
            print(f'{bar}\r',end='')
        
        # task output
        return index,residues,rmsf
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = n//PROCESSES+(n<PROCESSES)
    span2  = n%PROCESSES*(n>PROCESSES)
    params = []
    index,first = 0,0
    while first < n:
        last = min(n,first+span1+(span2>0))
        params.append([index,inpt[first:last],
        refere[first:last],select[first:last],
         start[first:last],  stop[first:last],
          step[first:last], frame[first:last]])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    comment(descr,self.logre,self.logsh,verbo)
    
    # initialize progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,n,0,etr,speed,width)
    if verbo and n>1: print(f'{bar}\r',end='')
    
    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return [],[]
    
    # terminate pool and progress bar
    pool.join()
    pool.clear()
    cursor.show()
    if n>1:
        elapsed = now()-t0
        speed = n/elapsed
        etr = 0
        bar = progress_bar(n,n,elapsed,etr,speed,width)
        comment(bar,self.logre,self.logsh,verbo)
    if verbo and n>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # reconstruct output
    residues,rmsf = [],[]
    for sorted_raw in sorted(raw,key=lambda x:x[0]):
        index,sub_residues,sub_rmsf = sorted_raw
        while len(sub_residues):
            residues.append(sub_residues.pop(0))
            rmsf    .append(sub_rmsf    .pop(0))
    
    return residues,rmsf
