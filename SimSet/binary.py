# import libraries
from SimSet.params import *
from SimSet.aux    import *

class binary:
    
    """
    List implementation as a binary file. List elements are ``float32``
    numbers.
    """
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,fname=None,*values):
        
        """
        Initialize ``self``, occasionally with ``values`` elements.
        
        Parameters
        ----------
        fname : str, file address linked to ``self``; if ``""``:
                take default name
        values: tuple of floats / array-like of floats: initial values
                of the list
        
        Notes
        -----
        ``values`` are passed to the ``SimSet.binary.merge`` method.
        If ``fname`` is already present, it erases it!
        """
        
        # process fname
        if type(fname) is not str:
            raise ValueError(f'"fname" {fname} is not a string')
        if exists(fname):
            raise ValueError(f'"fname" {fname} already exists')
        if not fname:
            fname = DEF_TMP+f'{random()}'[2:]+'.dat'
            while exists(fname): # nearly impossible
                fname = DEF_TMP+f'{random()}'[2:]+'.dat'
        
        # initialize attributes
        with open(fname,'wb') as f:
            f.write(pack('f',0)) # control
        self.name = fname
        self.__n = 0 # as iterator
        self.__l = 0 # length
        
        # write values
        self.merge(values)
    
    ###########################################################################
    ######################### PRINT AND REPRESENT SELF ########################
    ###########################################################################
    
    def __str__(self):
        
        """
        Of the array version of ``self``.
        """
        
        return self.array().__str__()
    
    def __repr__(self):
        
        """
        Of the array version of ``self``.
        """
        
        return self.array().__repr__()
    
    def __format__(self,spec):
        
        """
        Of the array version of ``self``.
        """
        
        return self.array().__format__()
    
    ###########################################################################
    ############################## BASIC OPERATORS ############################
    ###########################################################################
    
    def __bool__(self):
        
        """
        Return ``True`` if ``self`` has at list an element in its
        array version, ``False`` otherwise.
        """
        
        with open(self.name,'rb') as f:
            f.read(4) # pass over control
            if f.read(4):
                return True
        return False
    
    def __del__(self):
        
        """
        Destroy the object, delete ``self.name``.
        """
        
        if not hasattr(self,'name'):
            return
        if exists(self.name):
            rm(self.name)
    
    ###########################################################################
    ########################### COMPARISON OPERATORS ##########################
    ###########################################################################
    
    def __lt__(self,other):
        
        """
        "Less than". Iterate the pairs of elements of ``self``,
        ``other`` in order, and the first non-equal pair determines
        the winner of the ordering.
        """
        
        # confront with an iterable
        if type(other) in ITERABLE:
            i = 0 # index of other
            L = len(other)
            with open(self.name,'rb') as f:
                f.read(4) # pass over control
                while f.read(4):
                    f.seek(-4,1)
                    value1 = unpack('f',f.read(4))[0]
                    if i >= L: return True
                    value2 = unpack('f',pack('f',other[i]))[0]
                    if value1 != value2: return value1 < value2
                    i+= 1
            if i < L: return True # other is bigger
        
        # confront with a binary file
        elif type(other) is type(self):
            with open(self.name,'rb') as f:
                with open(other.name,'rb') as g:
                    f.read(4) # pass over control
                    g.read(4) # pass over control
                    while f.read(4) and g.read(4):
                        f.seek(-4,1)
                        g.seek(-4,1)
                        if f.read(4) != g.read(4):
                            f.seek(-4,1)
                            g.seek(-4,1)
                            return unpack('f',f.read(4))[0]\
                                 < unpack('f',g.read(4))[0]
                    if f.read(4) or g.read(4): return True # different lengths
        
        # biased towards False
        return False
    
    def __le__(self,other):
        
        """
        "Less equal". Iterate the pairs of elements of ``self``,
        ``other`` in order, and the first non-equal pair determines
        the winner of the ordering.
        """
        
        # confront with an iterable
        if type(other) in ITERABLE:
            i = 0 # index of other
            L = len(other)
            with open(self.name,'rb') as f:
                f.read(4) # pass over control
                while f.read(4):
                    f.seek(-4,1)
                    value1 = unpack('f',f.read(4))[0]
                    if i >= L: return True
                    value2 = unpack('f',pack('f',other[i]))[0]
                    if value1 != value2: return value1 <= value2
                    i+= 1
            if i < L: return True # other is bigger
            return True
        
        # confront with a binary file
        elif type(other) is type(self):
            with open(self.name,'rb') as f:
                with open(other.name,'rb') as g:
                    f.read(4) # pass over control
                    g.read(4) # pass over control
                    while f.read(4) and g.read(4):
                        f.seek(-4,1)
                        g.seek(-4,1)
                        if f.read(4) != g.read(4):
                            f.seek(-4,1)
                            g.seek(-4,1)
                            return unpack('f',f.read(4))[0]\
                                <= unpack('f',g.read(4))[0]
                    if f.read(4) or g.read(4): return True # different lengths
            return True
        
        # none of previous
        return False
    
    def __ge__(self,other):
        
        """
        "Greater equal". Iterate the pairs of elements of ``self``,
        ``other`` in order, and the first non-equal pair determines
        the winner of the ordering.
        """
        
        # confront with an iterable
        if type(other) in ITERABLE:
            i = 0 # index of other
            L = len(other)
            with open(self.name,'rb') as f:
                f.read(4) # pass over control
                while f.read(4):
                    f.seek(-4,1)
                    value1 = unpack('f',f.read(4))[0]
                    if i >= L: return True
                    value2 = unpack('f',pack('f',other[i]))[0]
                    if value1 != value2: return value1 >= value2
                    i+= 1
            if i < L: return True # other is bigger
            return True
        
        # confront with a binary file
        elif type(other) is type(self):
            with open(self.name,'rb') as f:
                with open(other.name,'rb') as g:
                    f.read(4) # pass over control
                    g.read(4) # pass over control
                    while f.read(4) and g.read(4):
                        f.seek(-4,1)
                        g.seek(-4,1)
                        if f.read(4) != g.read(4):
                            f.seek(-4,1)
                            g.seek(-4,1)
                            return unpack('f',f.read(4))[0]\
                                >= unpack('f',g.read(4))[0]
                    if f.read(4) or g.read(4): return True # different lengths
            return True
        
        # none of previous
        return False
    
    def __gt__(self,other):
        
        """
        "Greater than". Iterate the pairs of elements of ``self``,
        ``other`` in order, and the first non-equal pair determines
        the winner of the ordering.
        """
        
        # confront with an iterable
        if type(other) in ITERABLE:
            i = 0 # index of other
            L = len(other)
            with open(self.name,'rb') as f:
                f.read(4) # pass over control
                while f.read(4):
                    f.seek(-4,1)
                    value1 = unpack('f',f.read(4))[0]
                    if i >= L: return True
                    value2 = unpack('f',pack('f',other[i]))[0]
                    if value1 != value2: return value1 > value2
                    i+= 1
            if i < L: return True # other is bigger
        
        # confront with a binary file
        elif type(other) is type(self):
            with open(self.name,'rb') as f:
                with open(other.name,'rb') as g:
                    f.read(4) # pass over control
                    g.read(4) # pass over control
                    while f.read(4) and g.read(4):
                        f.seek(-4,1)
                        g.seek(-4,1)
                        if f.read(4) != g.read(4):
                            f.seek(-4,1)
                            g.seek(-4,1)
                            return unpack('f',f.read(4))[0]\
                                 > unpack('f',g.read(4))[0]
                    if f.read(4) or g.read(4): return True # different lengths
        
        # biased towards False
        return False
    
    def __eq__(self,other):
        
        """
        Return ``True`` if ``self`` and ``other`` have exactly the
        same elements``other`` may be either an array-like of floats
        or a ``SimSet.binary`` object.
        """
        
        # confront with an iterable
        if type(other) in ITERABLE:
            i = 0 # index of other
            L = len(other)
            with open(self.name,'rb') as f:
                f.read(4) # pass over control
                while f.read(4):
                    f.seek(-4,1)
                    value1 = unpack('f',f.read(4))[0]
                    if i >= L: return False
                    value2 = unpack('f',pack('f',other[i]))[0]
                    if value1 != value2: return False
                    i+= 1
            if i < L: return False # other is bigger
            return True
        
        # confront with a binary file
        elif type(other) is type(self):
            with open(self.name,'rb') as f:
                with open(other.name,'rb') as g:
                    f.read(4) # pass over control
                    g.read(4) # pass over control
                    while f.read(4) and g.read(4):
                        f.seek(-4,1)
                        g.seek(-4,1)
                        if f.read(4) != g.read(4): return False
                    if f.read(4) or g.read(4): return False # different lengths
            return True
        
        # none of previous
        return False
        
    def __ne__(self,other):
        
        """
        "Not equal". Return the opposite of ``self.__eq__(other)``.
        """
        
        return not self.__eq__(other)
    
    ###########################################################################
    ######################## LENGTH, ITERATOR & GETITEM #######################
    ###########################################################################
    
    def __len__(self):
        
        """
        Return length of ``self``.
        """
        
        return self.__l
    
    def __getitem__(self,key):
        
        """
        Return the element ``key`` of self.
        """
        
        # key is integer
        if type(key) in INTEGER:
            if key < 0: key+= self.__l
            with open(self.name,'rb') as f:
                f.seek((key+1)*4)
                if not f.read(4):
                    raise ValueError(f'list index {key} out of range')
                f.seek(-4,1)
                return array(unpack('f',f.read(4)),dtype=float32)[0]
        
        # key is slice
        if type(key) is slice:
            key = range(*key.indices(self.__l))
        
        # key is not an iterable
        if type(key) not in ITERABLE:
            return
        
        # key is an iterable
        result = []
        for k in key:
            if type(k) not in INTEGER:
                continue
            if k < 0: k+= self.__l
            if k < 0 or k >= self.__l:
                continue
            result.append(self.__getitem__(k))
        return array(result,dtype=float32)
    
    def __setitem__(self,key,value):
        
        """
        Set the element ``key`` to ``value``. Just numbers, not
        iterables.
        """
        
        if type(key) not in INTEGER:
            raise ValueError(f'key {key} is not an integer')
        
        with open(self.name,'rb+') as f:
            f.seek((key+1)*4)
            if not f.read(4):
                raise ValueError(f'list index {key} out of range')
            f.seek(-4,1)
            f.write(pack('f',value))
    
    def __delitem__(self,key):
        
        """
        Remove items in key.
        """
        
        # process key
        if   type(key) in INTEGER: key = [key]
        elif type(key) is slice:   key = key.indices(self.__l)
        if   type(key) not in ITERABLE: return
        
        # temporary name
        fname = DEF_TMP+f'{random()}'[2:]+'.dat'
        while exists(fname): # nearly impossible
            fname = DEF_TMP+f'{random()}'[2:]+'.dat'
        g = open(fname,'wb')
        
        # run through elements
        i = 0 # index of elements
        with open(self.name,'rb') as f:
            f.read(4) # pass over control
            while f.read(4):
                f.seek(-4,1)
                if i not in key:
                    g.write(f.read(4))
                else:
                    self.__l-= 1
                i+= 1
        g.close()
        
        # substitute file
        sh(f'mv {fname} {self.name}')
    
    def __contains__(self,value):
        
        """
        Return ``True`` if ``value`` is in ``self``, ``False``
        otherwise.
        """
        
        value = unpack('f',pack('f',value))[0]
        with open(self.name,'rb') as f:
            f.read(4) # pass over control
            while f.read(4):
                f.seek(-4,1)
                value1 = unpack('f',f.read(4))[0]
                if value1 == value:
                    return True
        return False
    
    def __missing__(self,key):
        
        """
        Return the opposite of ``self.__contains__(key)``.
        """
        
        return not self.__contains__(key)
    
    def __iter__(self):
        
        """
        Initialize ``self`` as an iterator.
        """
        
        self.__n = 0
        return self
    
    def __next__(self):
        
        """
        Return next istance (element) of ``self``.
        """
        
        if self.__n < self.__l:
            result = self[self.__n]
            self.__n+= 1
            return result
        else:
            raise StopIteration
    
    ###########################################################################
    ############################### LIST-SPECIFIC #############################
    ###########################################################################
    
    def append(self,value):
        
        """
        Append ``value`` to ``self``.
        """
        
        with open(self.name,'ab') as f:
            f.write(pack('f',value))
            self.__l+= 1
    
    def array(self):
        
        """
        Load ``self`` on memory as an array.
        """
        
        values = []
        with open(self.name,'rb') as f:
            f.read(4) # pass over control
            while f.read(4):
                f.seek(-4,1)
                values.append(unpack('f',f.read(4))[0])
        return array(values,dtype=float32)
    
    def copy(self,fname=''):
        
        """
        Return a deep copy of ``self``, occasionaly on a specified
        ``fname`` path.
        """
        
        x = binary(fname)
        with open(self.name,'rb') as f:
            with open(x.name,'ab') as g:
                f.read(4) # pass over control
                while f.read(4): # add value by value
                    f.seek(-4,1)
                    g.write(f.read(4))
                    x.__l+= 1
        return x
    
    def merge(self,*other):
        
        """
        Concatenate ``self`` to an either a ``SimSet.binary`` object or
        an array-like of floats, or a list of those.
        """
        
        # process input
        other = ls(other,int,float,type(self))
        
        # iterate through input
        l = 0
        for value in other:
            
            # append number
            if not type(value) is type(self):
                self.append(value)
                continue
            
            # merge binary file
            with open(self.name,'ab') as f:
                with open(value.name,'rb') as g:
                    g.read(4) # pass over control
                    while g.read(4): # add value by value
                        g.seek(-4,1)
                        f.write(g.read(4))
                        self.__l+= 1
    
    def pop(self):
        
        """
        Return last element of ``self``, delete it from self.
        """
        
        if not self.__l:
            return
        result = self.__getitem__(-1)
        sh(f'truncate -s -1 "{self.name}"')
        self.__l -= 1 
        return result
