# import libraries
from SimSet.params   import *
from SimSet.aux      import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _evaluate(self,*points,bandw=None,verbose=VERBOSE):
    
    """
    Infos are found in the ``SimSet.kde`` class.
    """

    ###########################################################################
    ################################ PRELIMIARIES #############################
    ###########################################################################
    
    # data
    data    = self.data
    weights = self.weights
    if bandw: self.bandw = bandw
    bandw   = self.bandw
    N = data.shape[1]
    
    # points
    try:
        points = array(points,ndmin=2,dtype=float)
    except Exception as traceback:
        error = ansi('ERROR: bad "points" entry, with traceback:')
        error = f'{error}\n{traceback}'
        print(error)
        return
    d,M = points.shape
    if d != data.shape[0]:
        error = f'ERROR: points have dimension {d}, '\
                f'dataset has dimension {self.d}'
        print(ansi(error,'red'))
        return
    
    # scale data and points
    covariance = self.covariance @ bandw**2
    inversecov = linalg.inv(covariance) if d > 1 else\
                 array(1/covariance)
    whitening = linalg.cholesky(inversecov)
    data   = whitening @ data
    points = whitening @ points
    normal = (linalg.det(2*pi*covariance))**(1/2)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
   
    # multiprocessing global variables
    current = Manager().Value('i',0)
    lock = Manager().Lock()
    n = min(N,M) # progress bar length
    
    # there are more points than data, so loop over data
    def task_data(interval):
        
        """
        Parameters
        ----------
        interval : int, subprocess id
        """
        
        first,last = interval
        result = zeros((M,),dtype=float)
        for i in range(last-first):
            diff   = data[:,first+i,newaxis]-points
            energy = np.sum(diff*diff,axis=0)/2
            result+= weights[first+i]*exp(-energy)
            
            # report progress
            if not verbose:
                continue
            lock.acquire()
            try:     current.set(current.get()+1)
            finally: lock.release()
            elapsed = now()-t0
            speed = current.value/elapsed
            etr = (n-current.value)/speed
            bar = progress_bar(current.value,n,elapsed,etr,speed,'',width)
            print(f'{bar}\r',end='')
        
        return result
    
    # there are more data than points, so loop over points
    def task_points(interval):
        
        """
        Parameters
        ----------
        interval : int, subprocess id to reconstruct results in
                   order
        """
        
        # sub result initialization
        first,last = interval
        result = zeros((last-first,),dtype=float)
        for i in  range(last-first):
            diff   = data-points[:,first+i,newaxis]
            energy = np.sum(diff*diff,axis=0)/2
            result[i] = np.sum(exp(-energy)*weights,axis=0)
            
            # report progress
            if not verbose:
                continue
            lock.acquire()
            try:     current.set(current.get()+1)
            finally: lock.release()
            elapsed = now()-t0
            speed = current.value/elapsed
            etr = (n-current.value)/speed
            bar = progress_bar(current.value,n,elapsed,etr,speed,'',width)
            print(f'{bar}\r',end='')
        
        return result,interval

    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1 = n//PROCESSES+(n<PROCESSES)
    span2 = n%PROCESSES*(n>PROCESSES)
    intervals = []
    index,first = 0,0
    while first < n:
        last = min(n,first+span1+(span2>0))
        intervals.append((first,last))
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(intervals)
    
    # run multiprocessing - results not in order
    pool = Pool(1 if processes>1 else 2) # resetting
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    bw = ', '.join([scientific(b,0,3) for b in bandw.reshape(-1)])
    if verbose: print(f'COMPUTING kernel distribution - bandwidth {bw}\n'\
                      f'RUNNING on {processes} core{"s"*(processes!=1)}')
    
    # initialize progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,n,0,etr,speed,'',width)
    if verbose: print(f'{bar}\r',end='')
    
    # run
    try:
        if M >= N:
            raw = list(pool.uimap(task_data,intervals))
        else:
            raw = list(pool.uimap(task_points,intervals))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbose: print(descr)
        return zeros((M,),dtype=float)
    
    # exit multiprocessing
    pool.join()
    pool.clear()
    cursor.show()
    if verbose: print()
    
    # reconstruct result
    if M >= N:
        result = np.sum(array(raw),axis=0)
    else:
        result = zeros((M,),dtype=float)
        while len(raw):
            sub_result,interval = raw.pop()
            result[interval[0]:interval[1]] = sub_result
    
    # normalize result
    return result / normal
