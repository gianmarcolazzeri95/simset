# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _merge(self,*xlist,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    cursor.show()
    
    ###########################################################################
    ################################ PARSE OPTIONS ############################
    ###########################################################################
    
    # defaults
    def_ext =['xvg']
    def_fil =[None]
    def_obs =[None]
    def_tit =[None]
    def_xax =[None]
    def_yax =[None]
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_sep = None
    
    # specific parameters
    exten = parse(args,'ext'        ,def_ext)
    files = parse(args,'file'       ,def_fil)
    files = parse(args,'files'      ,files  )
    obser = parse(args,'obs'        ,def_obs)
    obser = parse(args,'obser'      ,obser  )
    obser = parse(args,'observable' ,obser  )
    obser = parse(args,'observables',obser  )
    obser = parse(args,'label'      ,obser  )
    obser = parse(args,'labels'     ,obser  )
    title = parse(args,'title'      ,def_tit)
    title = parse(args,'titles'     ,title  )
    xaxis = parse(args,'xaxis'      ,def_xax)
    yaxis = parse(args,'axis'       ,def_yax)
    yaxis = parse(args,'yaxis'      ,yaxis  )
    start = parse(args,'start'      ,def_sta)
    stop  = parse(args,'stop'       ,def_sto)
    step  = parse(args,'step'       ,def_ste)
    frame = parse(args,'frame'      ,def_fra)
    frame = parse(args,'frames'     ,def_fra)
    sep   = parse(args,'sep'        ,def_sep)
    verbo = parse(args,'verbose'    ,VERBOSE)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ########################################################################### 
    
    # input
    objects,names,data,order = [],[],[],[]
    first_column = True
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # scan input list
    line = False # formatting issues
    for i,x in enumerate(xlist):
        
        # add objext
        if type(x) is type(self):
            first_column = True
            objects.append(x)
            order.append(0)
        
        # add filenames
        elif type(x) is str:
            first_column = True
            
            # match regular expression
            if '*' in x or '?' in x or '*' in exten or '?' in exten:
                regex,displ = [],[]
                for ext in exten:
                    p,e = extension(x,ext)
                    if '*' in p or '?' in p:
                        reg = f'{ p }{"."*bool(e)}{e}'
                    else:
                        reg = f'"{p}"{"."*bool(e)}{e}'
                    if reg not in regex:
                        dis = path_name(reg)
                        if not ('*' in p or '?' in p) and dis[0]!='"':
                            dis = f'"{dis}'
                        regex.append(reg)
                        displ.append(dis)
                regex = ' '.join(regex)
                displ = ' '.join(displ)
                HASH = hash(regex)
                tmp_lst = f'{DEF_TMP}{HASH}list'
                descr = f'MATCHING regex {ansi(displ,"italic")}'
                commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
                if verbo: print(descr)
                sh(commd)
                found = False
                descr = []
                with open(tmp_lst,'r') as f:
                    for name in f.readlines():
                        names.append(name[:-1])
                        descr.append(names[-1])
                        order.append(1)
                        found = True
                sh(f'rm -f "{tmp_lst}"')
                if found:
                    descr = '\n'.join(descr)+'\n'
                    if verbo: print(descr)
                    continue
                descr = f'WARNING: no files found'
                if verbo: print(ansi(descr,'yellow'))
                continue
            
            # match existing file
            if x == '.'.join(extension(x)):
                if isfile(x):
                    names.append(x)
                    order.append(1)
                    continue
            
            # match required extensions
            found = False
            for ext in exten:
                fname = f'{x}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    names.append(fname)
                    order.append(1)
                    found = True
                    break
            if found:
                continue
            
            # nothing found: warning
            line = True
            displ = path_name(x)
            names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                             for ext in exten])
            descr = f'WARNING: none of {names} found'
            if verbo: print(ansi(descr,'yellow'))
            names = []
        
        # add raw data points
        elif type(x) in [list,tuple,ndarray]:
            if first_column:
                first_column = False  
                data.append([x])
                order.append(2)
            else:
                data[-1].append(x)
    
    # check data
    toremove = []
    j = 0
    for i in range(len(data)):
        j = order[j:].index(2)+j
        if len(data[i])<2:
            line = True
            descr = f'WARNING: sequence n {i+1} of raw data has just '\
                    f'the times column, excluding it\n'
            if verbo: print(ansi(descr,'yellow'))
            toremove.append((i,j))
            continue
        L = [len(d) for d in data[i]]
        if L[1:] != L[:-1]:
            descr = f'WARNING: sequence n {i+1} of raw data has mismatching '\
                    f'lengths, excluding it\n'
            if verbo: print(ansi(descr,'yellow'))
            toremove.append((i,j))
            continue
        if L[0] == 0:
            descr = f'WARNING: sequence n {i+1} of raw data has no points, '\
                    f'excluding it\n'
            if verbo: print(ansi(descr,'yellow'))
            toremove.append((i,j))
    
    # remove bad data
    toremove.reverse()
    for i,j in toremove:
        data.pop(i)
        order.pop(j)
    if verbo and line: print()
    
    # check input size
    if not(len(objects)+len(names)+len(data)):
        descr = 'WARNING: nothing to merge'
        if verbo and len(xlist): print(ansi(descr,'yellow'))
        return 0
    else:
        descr = f'MERGING {len(objects)} object{"s"*(len(objects)!=1)}, '\
                f'{len(names)} xvg file{"s"*(len(names)!=1)}, '\
                f'and {len(data)} raw dataset{"s"*(len(data)!=1)}'
        if verbo: print(ansi(descr,'bold'))
    
    # separator
    if type(sep) is not str:
        sep = def_sep
    
    # files
    files = ls(files,str,None)
    if not files:
        files = def_fil
    
    # labels
    obser = ls(obser,str,None)
    if not obser:
        obser = def_obs
    
    # titles
    title = ls(title,str,None)
    if not title:
        title = def_tit
    
    # xaxis
    xaxis = ls(xaxis,str,None)
    if not xaxis:
        xaxis = def_xax
    
    # yaxis
    yaxis = ls(yaxis,str,None)
    if not yaxis:
        yaxis = def_yax
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop ,int,float)
    step  = ls(step ,int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        if verbo: print(ansi(descr,'red'))
        return
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # adjust lengths to n of objects / files / raw datasets
    n = len(order)
    start = adjust_list(start,n)
    stop  = adjust_list(stop, n)
    step  = adjust_list(step, n)
    frame = adjust_list(frame,n)
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initialization
    H = 0 # number of merged columns
    N = 0 # number of merged points
    
    # merge according to order
    for ord,  sta,  sto, ste, fra in\
    zip(order,start,stop,step,frame):
        
        # merge objects
        if ord == 0:
            if verbo: print(f'LOADING "SimSet.xvg" object',end='')
            K,M = __merge_obj(self,objects.pop(0),sta,sto,ste,fra,
                            files,obser,title,xaxis,yaxis)
            descr = f'{K} column{"s"*(K!=1)} and {M} point{"s"*(M!=1)}'
            descr = ansi(descr,'bold','green' if M else 'yellow')
            if verbo: print(f' > {descr}')
        
        # merge files
        if ord == 1:
            fname = names.pop(0)
            if verbo: print(f'LOADING "{path_name(fname)}"',end='')
            try:
                K,M = __merge_file(self,fname,sta,sto,ste,fra,
                                 files,obser,title,xaxis,yaxis,sep)
                descr = f'{K} column{"s"*(K!=1)} and {M} point{"s"*(M!=1)}'
                descr = ansi(descr,'bold','green' if M else 'yellow')
                if verbo: print(f' > {descr}')
            except:
                descr ='\nWARNING: not a valid xvg file, skipping operations'
                if verbo: print(ansi(descr,'yellow'))
                K,M = 0,0
        
        # merge columns of datapoints
        if ord == 2:
            if verbo: print(f'LOADING raw dataset',end='')
            K,M = __merge_data(self,data.pop(0),sta,sto,ste,fra,
                             files,obser,title,xaxis,yaxis)
            descr = f'{K} column{"s"*(K!=1)} and {M} point{"s"*(M!=1)}'
            descr = ansi(descr,'bold','green' if M else 'yellow')
            if verbo: print(f' > {descr}')
        
        # shift files,obser,title,xaxis,yaxis by the number of
        # added columns, so that correct assignment is achieved
        files = shift(files,K)
        obser = shift(obser,K)
        title = shift(title,K)
        xaxis = shift(xaxis,K)
        yaxis = shift(yaxis,K)
        
        # add columns & points
        H+= K
        N+= M
    
    if verbo:
        print(f'ADDED {H} column{"s"*(H!=1)} and {N} point{"s"*(N!=1)}')
    return H

###############################################################################
####################### AUXILIARY FUNCTION: MERGE_OBJECT ######################
###############################################################################

def __merge_obj(self,xvg,start,stop,step,frame,files,obser,title,xaxis,yaxis):
    
    """
    Merge ``SimSet.xvg`` object ``xvg`` into ``self``. ``files``,
    ``obser``, ``title``, ``xaxis``, ``yaxis`` are adjusted to match
    the number of added columns.
    
    Returns
    -------
    K : number of added columns
    M : number of added points
    """
    
    ###########################################################################
    ############################### RETRIEVE INFOS ############################
    ###########################################################################
    
    # avoid overwriting objects
    if self== xvg:
        xvg = xvg.copy()
    
    datap = self.get_datap()
    times = self.get_times()
    xvg_datap = xvg.get_datap()
    xvg_times = xvg.get_times()
    N = len(times)
    M = len(xvg_times)
    H = len(datap)
    K = len(xvg_datap)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # adjust lengths to number of columns to add
    files = adjust_list(files,K)
    obser = adjust_list(obser,K)
    title = adjust_list(title,K)
    xaxis = adjust_list(xaxis,K)
    yaxis = adjust_list(yaxis,K)
    
    # build frame - cache computation
    times_to_index = {}
    
    # process start
    if type(start) in FLOAT:
        start = search_bigger(xvg_times,start)
        start = start if start is not None else M
    start+= 0 if start>=0 else M
    start = max(0,min(start,M))
    
    # process stop
    if type(stop) in FLOAT:
        stop = search_smaller(xvg_times,stop)
        stop = stop if stop is not None else -M-1
    stop+= 1 if stop>=0 else M+1
    stop = max(0,min(stop,M))
    
    # build frame accorting to start-stop-step
    if not frame:
        if type(step) in FLOAT:
            frame = [start]
            for i,t in enumerate(xvg_times[start:stop]):
                if t-xvg_times[frame[-1]] >= step:
                    frame.append(start+i)
        else:
            frame = range(start,stop,step)
    
    # process already existing "frame"
    else:
        temp,frame = frame,[]
        for fra in temp:
            if type(fra) in FLOAT:
                fra = search_equal(xvg_times,fra)
                fra = fra if fra is not None else M
            fra+= 0 if fra>=0 else M
            if fra<0 or fra>=M:
                continue
            frame.append(fra)
    
    # new times and datapoints for self
    self.times =  []
    self.datap = [[] for h in range(H)]
    
    # new datapoints for ``xvg``
    new_datap  = [[] if self.memory else binary('',) for k in range(K)]
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initialization
    i = 0 # index of ``self`` times
    M = 0 # total number of added points
    
    # iterate through ``xvg`` times
    for j in frame:
        time =  xvg_times[j]
        
        # times that are present in ``self`` but not in ``xvg``
        while i < len(times) and times[i] < time:
            self.get_times().append(times[i])
            for k in range(K):
                new_datap[k].append(NaN)
            for h in range(H):
                self.get_datap()[h].append(datap[h][i])
            i+= 1
        
        # current ``xvg`` time
        self.get_times().append(time)
        for k in range(K):
            data = xvg_datap[k][j]
            new_datap[k].append(data)
            if not isnan(data):
                M+= 1
        
        # times that are present in ``xvg`` but not in self
        if i < len(times): # before the end of ``self``
            if times[i] > time:
                for h in range(H):
                    self.get_datap()[h].append(NaN)
            else:
                for h in range(H):
                    self.get_datap()[h].append(datap[h][i])
                i+= 1
        else:              # after  the end of ``self``
            for h in range(H):
                self.get_datap()[h].append(NaN)
    
    # times that are present in ``self`` after the end of ``xvg``
    for j in range(i,N):
        self.get_times().append(times[j])
        for h in range(H):
            self.get_datap()[h].append(datap[h][j])
        for k in range(K):
            new_datap[k].append(NaN)
    
    # assign observables,files,title,xaxis,yaxis name according to options
    self.obser+= [x if x is not None else y for x,y in zip(obser,xvg.obser)]
    self.files+= [x if x is not None else y for x,y in zip(files,xvg.files)]
    self.title+= [x if x is not None else y for x,y in zip(title,xvg.title)]
    self.xaxis+= [x if x is not None else y for x,y in zip(xaxis,xvg.xaxis)]
    self.yaxis+= [x if x is not None else y for x,y in zip(yaxis,xvg.yaxis)]
    
    # finally merge data
    self.set_datap(self.get_datap()+new_datap)
    return K,M

###############################################################################
####################### AUXILIARY FUNCTION: MERGE_FNAME #######################
###############################################################################

def __merge_file(self,fname,start,stop,step,frame,
                      files,obser,title,xaxis,yaxis,sep):
    
    """
    Merge data found xvg file: ``fname`` into ``self``. ``files``,
    ``obser``, ``title``, ``xaxis``, ``yaxis`` are adjusted to match
    the number of added columns
    
    Returns
    -------
    K : number of added columns
    M : number of added points
    """
    
    ###########################################################################
    ############################### RETRIEVE INFOS ############################
    ###########################################################################
    
    datap = self.get_datap()
    times = self.get_times()
    N = len(times)
    H = len(datap)
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # new times and datapoints for self
    self.times =  []
    self.datap = [[] for h in range(H)]
    
    # initialization
    new_times = [] if self.memory else binary('',)
    new_files = [fname]
    not_obser = [] # for not canonical XVG files
    new_obser = []
    new_title = ['']
    new_xaxis = ['']
    new_yaxis = ['']
    i = 0 # index  of ``self``  times
    M = 0 # index  of ``fname`` times - n of added time steps
    j = 0 # index  of merged times
    K = 0 # number of added columns
    m = 0 # number of added points
    new_index = [] # time indexes of added datapoints
    new_datap = []
    
    # scan file
    with open(fname) as f:
        for line in f:
            
            # retrieve infos - NOT a canonical xvg file
            if line[0] == '#':
                not_obser = line[1:].split(sep)[1:]
                not_obser = [' '.join(obs.split()) for obs in not_obser]
                continue
            
            # retrieve infos - canonical xvg file
            if line[0] == '@':
                raw = line[1:].split()
                if not len(raw):
                    continue
                if raw[0] == 'title':
                    new_title = [line.split('"')[1].split('"')[0]]
                if raw[0] == 'xaxis':
                    new_xaxis = [line.split('"')[1].split('"')[0]]
                if raw[0] == 'yaxis':
                    new_yaxis = [line.split('"')[1].split('"')[0]] 
                if raw[1] == 'legend':
                    # add observable
                    obs = line.split('"')[1].split('"')[0]
                    if obs.casefold() == 'time':
                        continue
                    new_obser.append(obs)
                continue
            
            # scrape data - ignore errors
            try    : values = [float(val) for val in line.split(sep)]
            except : continue
            if len(values) < 2: continue
            
            # initialize new columns
            if not K:
                if new_obser:
                    pass
                elif not_obser: # NOT a canonical xvg file with labels
                    new_obser = not_obser
                else:        # NOT a canonical xvg file without labels
                    new_obser = [f'obser{k}' for k in range(len(values)-1)]
                K = len(new_obser)
                for k in range(K):
                    new_datap.append([] if self.memory else binary('',))
            
            # adapt number of points to number of labels
            time = values[0]
            data = values[1:K+1]
            new_times.append(time)
            for k in range(K-len(data)):
                data.append(NaN)
            
            # times that are present in ``self`` but not in ``fname``
            while i < len(times) and times[i] < time:
                self.get_times().append(times[i])
                for k in range(K):
                    new_datap[k].append(NaN)
                for h in range(H):
                    self.get_datap()[h].append(datap[h][i])
                i+= 1
                j+= 1
            
            # current ``fname`` time
            self.get_times().append(time)
            for k in range(K):
                new_datap[k].append(data[k])
                if not isnan(data[k]):
                    m+= 1
            new_index.append(j)
            M+= 1
            
            # times that are present in ``fname``
            if i < len(times): # before the end of ``self`` but not in ``self``
                if times[i] > time:
                    for h in range(H):
                        self.get_datap()[h].append(NaN)
                else:          # before the end of ``self`` and in ``self``
                    for h in range(H):
                        self.get_datap()[h].append(datap[h][i])
                    i+= 1
                    j+= 1
            else:              # after  the end of ``self``
                for h in range(H):
                    self.get_datap()[h].append(NaN)
                j+= 1
    
    # times that are present in ``self`` after the end of ``xvg``
    for j in range(i,N):
        self.get_times().append(times[j])
        for h in range(H):
            self.get_datap()[h].append(datap[h][j])
        for k in range(K):
            new_datap[k].append(NaN)
    
    # adjust lengths to number of added columns
    obser = adjust_list(obser,K)
    files = adjust_list(files,K)
    title = adjust_list(title,K)
    xaxis = adjust_list(xaxis,K)
    yaxis = adjust_list(yaxis,K)
    new_files = adjust_list(new_files,K)
    new_title = adjust_list(new_title,K)
    new_xaxis = adjust_list(new_xaxis,K)
    new_yaxis = adjust_list(new_yaxis,K)
    
    # assign observables,files,title,xaxis,yaxis name according to options
    self.obser+= [x if x is not None else y for x,y in zip(obser,new_obser)]
    self.files+= [x if x is not None else y for x,y in zip(files,new_files)]
    self.title+= [x if x is not None else y for x,y in zip(title,new_title)]
    self.xaxis+= [x if x is not None else y for x,y in zip(xaxis,new_xaxis)]
    self.yaxis+= [x if x is not None else y for x,y in zip(yaxis,new_yaxis)] 
    
    # build frame - cache computation
    times_to_index = {}
    
    # process start
    if type(start) in FLOAT:
        start = search_bigger(new_times,start)
        start = start if start is not None else M
    start+= 0 if start>=0 else M
    start = max(0,min(start,M))
    
    # process stop
    if type(stop) in FLOAT:
        stop = search_smaller(new_times,stop)
        stop = stop if stop is not None else -M-1
    stop+= 1 if stop>=0 else M+1
    stop = max(0,min(stop,M))
    
    # build frame accorting to start-stop-step
    if not frame:
        if type(step) in FLOAT:
            frame = [start]
            for i,t in enumerate(new_times[start:stop]):
                if t-new_times[frame[-1]] >= step:
                    frame.append(start+i)
        else:
            frame = range(start,stop,step)
    
    # process already existing "frame"
    else:
        temp,frame = frame,[]
        for fra in temp:
            if type(fra) in FLOAT:
                fra = search_equal(new_times,fra)
                fra = fra if fra is not None else M
            fra+= 0 if fra>=0 else M
            if fra<0 or fra>=M:
                continue
            frame.append(fra)
    
    # delete datapoints that are not in frames (relatively to OLD time indexes)
    for k in range(K):
        for fra,j in enumerate(new_index):
            if fra not in frame:
                if not isnan(new_datap[k][j]):
                    m-= 1
                new_datap[k][j] = NaN
    
    # finally merge data
    self.set_datap(self.get_datap()+new_datap)
    return K,m

###############################################################################
######################## AUXILIARY FUNCTION: MERGE_DATA #######################
###############################################################################

def __merge_data(self,data,start,stop,step,frame,
                      files,obser,title,xaxis,yaxis):
    
    """
    Merge list of time+datapoints ``data`` into ``self``. ``files``,
    ``obser``, ``title``, ``xaxis``, ``yaxis`` are adjusted to match
    the number of added columns.
    
    Returns
    -------
    K : number of added columns
    M : number of added points
    """
    
    ###########################################################################
    ############################### RETRIEVE INFOS ############################
    ###########################################################################
    
    datap = self.get_datap()
    times = self.get_times()
    raw_times = data[0]
    raw_datap = data[1:]
    N = len(times)
    M = len(raw_times)
    H = len(datap)
    K = len(raw_datap)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # sort data according to times vector
    order = argsort(data[0])
    data = [[d[ord] for ord in order] for d in data]
    
    # adjust length to number of columns to add
    files = adjust_list(files,K)
    obser = adjust_list(obser,K)
    title = adjust_list(title,K)
    xaxis = adjust_list(xaxis,K)
    yaxis = adjust_list(yaxis,K)
    
    # build frame - cache computation
    times_to_index = {}
    
    # process start
    if type(start) in FLOAT:
        start = search_bigger(raw_times,start)
        start = start if start is not None else M
    start+= 0 if start>=0 else M
    start = max(0,min(start,M))
    
    # process stop
    if type(stop) in FLOAT:
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -M-1
    stop+= 1 if stop>=0 else M+1
    stop = max(0,min(stop,M))
    
    # build frame accorting to start-stop-step
    if not frame:
        if type(step) in FLOAT:
            frame = [start]
            for i,t in enumerate(raw_times[start:stop]):
                if t-raw_times[frame[-1]] >= step:
                    frame.append(start+i)
        else:
            frame = range(start,stop,step)
    
    # process already existing "frame"
    else:
        temp,frame = frame,[]
        for fra in temp:
            if type(fra) in FLOAT:
                fra = search_equal(raw_times,fra)
                fra = fra if fra is not None else M
            fra+= 0 if fra>=0 else M
            if fra<0 or fra>=M:
                continue
            frame.append(fra)
    
    # new times and datapoints for self
    self.times =  []
    self.datap = [[] for h in range(H)]
    
    # new datapoints for raw dataset
    new_datap  = [[] if self.memory else binary('',) for k in range(K)]
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initialization
    i = 0 # index of ``self`` times
    M = 0 # number of added points
    
    # iterate through the raw dataset times
    for j in frame:
        time =  raw_times[j]
        
        # times that are present in ``self`` but not in ``dataframe``
        while i < len(times) and times[i] < time:
            self.get_times().append(times[i])
            for k in range(K):
                new_datap[k].append(NaN)
            for h in range(H):
                self.get_datap()[h].append(datap[h][i])
            i+= 1
        
        # current ``xvg`` time
        self.get_times().append(time)
        for k in range(K):
            new_datap[k].append(raw_datap[k][j])
            if not isnan(raw_datap[k][j]):
                M+= 1
        
        # times that are present in ``dataframe`` but not in ``self``
        if i < len(times): # before the end of ``self``
            if times[i] > time:
                for h in range(H):
                    self.get_datap()[h].append(NaN)
            else:
                for h in range(H):
                    self.get_datap()[h].append(datap[h][i])
                i+= 1
        else:              # after  the end of ``self``
            for h in range(H):
                self.get_datap()[h].append(NaN)
    
    # times that are present in ``self`` after the end of ``dataframe``
    for j in range(i,N):
        self.get_times().append(times[j])
        for h in range(H):
            self.get_datap()[h].append(datap[h][j])
        for k in range(K):
            new_datap[k].append(NaN)
    
    # assign observables,files,title,xaxis,yaxis name according to options
    self.obser+= [x if x is not None else\
                     f'obser{i}' for i,x in enumerate(obser)]
    self.files+= [x if x is not None else '' for x in files]
    self.title+= [x if x is not None else '' for x in title]
    self.xaxis+= [x if x is not None else '' for x in xaxis]
    self.yaxis+= [x if x is not None else '' for x in yaxis]
    
    # finally merge data
    self.set_datap(self.get_datap()+new_datap)
    return K,M
