# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _vmd(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.vmd" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    inpt = ls(inpt,str)
    def_ext = DEF_EXT
    def_ref = ['']
    def_sel = ['all']
    def_bon = [[]]
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra =[None]
    def_smo = [ 0]
    def_rep = ['']
    def_col = ['']
    def_res = DEF_RES
    
    # specific parameters
    try:
        exten  = parse(args,'ext'      ,def_ext)
        refere = parse(args,'mol'      ,def_ref)
        refere = parse(args,'ref'      ,refere )
        refere = parse(args,'reference',refere )
        select = parse(args,'sel'      ,def_sel)
        select = parse(args,'select'   ,select )
        select = parse(args,'selection',select )
        bonds  = parse(args,'bond'     ,def_bon)
        bonds  = parse(args,'bonds'    ,bonds  )
        start  = parse(args,'start'    ,def_sta)
        stop   = parse(args,'stop'     ,def_sto)
        step   = parse(args,'step'     ,def_ste)
        frame  = parse(args,'frame'    ,def_fra)
        smooth = parse(args,'smooth'   ,def_smo)
        repres = parse(args,'repr'     ,def_rep)
        color  = parse(args,'col'      ,def_col)
        color  = parse(args,'color'    ,color  )
        color  = parse(args,'colors'   ,color  )
        resol  = parse(args,'res'      ,def_res)
        resol  = parse(args,'resolution',resol )
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # default input
    found = False
    for ext in exten:
        fname = f'{self.last}{"."*bool(ext)}{ext}'
        if isfile(fname):
            def_inp = [fname]
            found = True
            break
    if not found:
        for ext in exten:
            fname = f'{self.molec}{"."*bool(ext)}{ext}'
            if isfile(fname):
                def_inp = [fname]
                found = True
                break
    
    # no input
    if not len(inpt):
        if not found:
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return
        inpt = def_inp
        descr = f'TAKING "{path_name(def_inp[0])}" as input file\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    line = False # formatting issues
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'WARNING: no files found\n'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: warning
        line = True
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'WARNING: none of {names} found'
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
        continue
    
    # reconstruct input and extension
    inpt = ls(inpt)
    exten = [extension(inp)[1] for inp in inpt]
    n = len(inpt)
    if not n:
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    elif verbo and line: print()
    
    # reference - check if ok
    refere = ls(refere,str)
    if not len(refere):
        descr = f'ERROR: bad "reference" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # selection - check if ok
    select = ls(select,str)
    if not len(select):
        descr = f'ERROR: bad "selection" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # parse selection with MDAnalysis ":" syntax - check if ok
    for i,sel in enumerate(select):
        while ':' in sel:
            temp = sel.split(':')
            before = temp[0]
            after  = ':'.join(temp[1:])
            temp0= before.split()[-1]
            temp1= after.split()[0]
            try:
                temp0 = int(temp0)
                temp1 = int(temp1)
                expansion = ' '.join([f'{i}' for i in range(temp0+1,temp1)])
            except:
                descr = f'ERROR: selection "{sel}" not understood\n'
                descr = ansi(descr,'red')
                comment(descr,self.logre,self.logsh,verbo)
                return
            sel = f'{before} {expansion} {after}'
            select[i] = sel
    
    # bonds (signature-like)
    if not bonds or type(bonds) not in ITERABLE:
        bonds = def_bon
    temp = [] # adjust formatting - convert in couples
    for bon in bonds:
        if type(bon) in INTEGER:
            temp.append(bon)
        elif type(bon) in ITERABLE and len(bon)>=2:
            temp.append(bon[ 0])
            temp.append(bon[-1])
    bonds = [temp[i:i+2] for i in range(0,len(temp),2)]
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # frame - check if ok
    frame = ls(frame,int,float,None)
    if not len(frame):
        descr = f'ERROR: bad "frame" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # smooth - check if ok
    smooth = ls(smooth,int,float)
    if not len(smooth):
        descr = f'ERROR: bad "smooth" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # resolution
    resol = max(10.,min(resol,1000.0))
    
    # representations - check if ok
    repres = ls(repres,str)
    if not len(color):
        descr = f'ERROR: bad "repr" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    for i,rep in enumerate(repres):
        rep = ' '.join(rep.split()) # no spaces
        if   rep.casefold() == 'lines':
            repres[i] = f'Lines 1.0'
        elif rep.casefold() == 'bonds':
            repres[i] = f'Bonds 0.3 {resol}'
        elif rep.casefold() == 'dynamicbonds':
            repres[i] = f'DynamicBonds 1.6 0.3 {resol}'
        elif rep.casefold() == 'hbonds':
            repres[i] = f'HBonds 3.0 20.0 1.0'
        elif rep.casefold() == 'points':
            repres[i] = f'Points 2.0'
        elif rep.casefold() == 'vdw':
            repres[i] = f'VDW 1.0 100.0'
        elif rep.casefold() == 'cpk':
            repres[i] = f'CPK 1.0 0.3 {resol} {resol}'
        elif rep.casefold() == 'licorice':
            repres[i] = f'Licorice 0.3 {resol} {resol}'
        elif rep.casefold() == 'polyhedra':
            repres[i] = f'Polyhedra 1.6'
        elif rep.casefold() == 'trace':
            repres[i] = f'Trace 0.3 {resol}'
        elif rep.casefold() == 'tube':
            repres[i] = f'Tube 0.3 {resol}'
        elif rep.casefold() == 'ribbons':
            repres[i] = f'Ribbons 0.3 {resol} 2.0'
        elif rep.casefold() == 'newribbons':
            repres[i] = f'NewRibbons 0.3 {min(round(resol/2),50)} 3.0 0'
        elif rep.casefold() == 'cartoon':
            repres[i] = f'Cartoon 2.1 {resol} 5.0'
        elif rep.casefold() == 'newcartoon':
            repres[i] = f'NewCartoon 0.3 {min(round(resol/2),50)} 4.1 0'
        elif rep.casefold() == 'paperchain':
            repres[i] = f'PaperChain 1.0 10.0'
        elif rep.casefold() == 'twister':
            repres[i] = f'Twister 1.0 0.0 10.0 0.3 0.05 10.0 5.0'
        elif rep.casefold() == 'quicksurf':
            repres[i] = f'QuickSurf 0.64 0.64 0.64 {1+min(round(resol/50),2)}'
        elif rep.casefold() == 'msms':
            repres[i] = f'MSMS 1.5 1.5 0.0 0.0'
        elif rep.casefold() == 'nanoshaper':
            repres[i] = f'NanoShaper 0.0 0.0 0.5 1.4 0.45 -2.5'
        elif rep.casefold() == 'surf':
            repres[i] = f'Surf 1.4 0.0'
        elif rep.casefold() == 'volumeslice':
            repres[i] = f'VolumeSlice'
        elif rep.casefold() == 'isosurface':
            repres[i] = f'Isosurface'
        elif rep.casefold() == 'fieldlines':
            repres[i] = f'FieldLines'
        elif rep.casefold() == 'orbital':
            repres[i] = f'Orbital 0.05 0 0 0 0.75 1 0 0 0 1'
        elif rep.casefold() == 'beads':
            repres[i] = f'Beads 1.0 {resol}'
        elif rep.casefold() == 'dotted':
            repres[i] = f'Dotted 1.0 {resol}'
        elif rep.casefold() == 'solvent':
            repres[i] = f'Solvent 0.0 {min(round(resol/8),13)} 1.0'
    
    # colors - check if ok
    color = ls(color,str,int,float)
    if not len(color):
        descr = f'ERROR: bad "colors" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    for i,col in enumerate(color):
        if not type(col) is str:
            color[i] = f'ColorID {col:.0f}'
    
    # adjust lengths
    refere = adjust_list(refere,n)
    select = adjust_list(select,n)
    start  = adjust_list(start ,n)
    stop   = adjust_list(stop  ,n)
    step   = adjust_list(step  ,n)
    frame  = adjust_list(frame ,n)
    smooth = adjust_list(smooth,n)
    repres = adjust_list(repres,n)
    color  = adjust_list(color ,n)
    
    # reference list
    line = False # formatting issues
    for i,ref in enumerate(refere):
        if ref == '.'.join(extension(ref)):
            if isfile(ref):
                continue
        
        # match required extensions
        found = False
        for ext in REF_EXT:
            fname = f'{ref}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if found:
            continue
        
        # input file is also reference
        if exten[i] in REF_EXT:
            refere[i] = inpt[i]
            continue
        
        # look for alternatives
        found = False
        inp = extension(inpt[i])[0]
        for ext in REF_EXT:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if not found:
            for ext in REF_EXT:
                fname = f'{self.molec}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    refere[i] = fname
                    found = True
                    break
        
        # found alternative
        if found:
            line = True
            descr = f'WARNING: no reference specified '\
                    f'for input file "{path_name(inpt[i])}", '\
                    f'assigning "{path_name(refere[i])}" instead'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # found no alternatives
        descr = f'ERROR: no reference found '\
                f'for input file "{path_name(inpt[i])}"\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    if verbo and line: print()
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # script
    vmd = ''
    NOW = round(now())
    def_vmd = f'{DEF_TMP}{NOW}.vmd'
    
    # main cycle
    for inp, ext,  ref,   sel,   sta,  sto, ste, fra,  smo,   rep,   col in\
    zip(inpt,exten,refere,select,start,stop,step,frame,smooth,repres,color):
        
        # check number of atoms
        universe = Universe(inp)
        raw_times = [t.time for t in universe.trajectory]
        N = len(raw_times)
        if len(Universe(ref).atoms) != len(universe.atoms):
            descr = f'ERROR: atom mismatch between reference "{ref}" '
            descr+= f'({len(Universe(ref).atoms)}) and input "{inp}" '
            descr+= f'({len(universe.atoms)})\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return
        
        # process selection
        if not sel:
            sel = 'all'
        
        # process frame
        if fra is not None:
            sta = sto = fra
        
        # process start
        if type(sta) in FLOAT:
            sta = search_bigger(raw_times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            sto = search_smaller(raw_times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # process step - assume equally spaces frames
        if type(ste) in FLOAT:
            for i,t in enumerate(raw_times[sta:sto]):
                if t-raw_times[sta] >= ste:
                    ste = i
                    break
        
        # reference
        vmd+=  'mol new {'+ref+'} type {'+extension(ref)[1]+'} '
        vmd+= f'first 0 last 0 step 1 waitfor 1\n'\
              f'animate style Once\n'\
              f'set index [molinfo top get index]\n'
        
        # rename molecule
        vmd+=  'mol rename top {'+extension(inp.split('/')[-1])[0]+'}\n'
        
        # style - nucleic
        col_nuc = f'ResName' if not col else col
        rep_nuc = f'NewRibbons 0.3 {min(round(resol/2),50)} 3.0 0' if not rep\
                  else rep
        vmd+= f'mol delrep 0 top\nmol delrep 0 top\nmol color {col_nuc}\n'\
              f'mol representation {rep_nuc}\n'\
              f'mol selection "({sel}) and {NUCLEIC}"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 0 {smo}\n'
        
        # style - protein
        col_pro = f'Structure' if not col else col
        rep_pro = f'NewCartoon 0.3 {min(round(resol/2),50)} 4.1 0' if not rep\
                  else rep
        vmd+= f'mol color {col_pro}\n'\
              f'mol representation {rep_pro}\n'\
              f'mol selection "({sel}) and {PROTEIN}"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 1 {smo}\n'
        
        # style - lipids
        col_lip = f'Name' if not col else col
        rep_lip = f'Licorice 0.3 {resol} {resol}' if not rep else rep
        vmd+= f'mol color {col_lip}\n'\
              f'mol representation Points 2.0\n'\
              f'mol selection "({sel}) and {LIPID}"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 2 {smo}\n'
        
        # style - ions
        col_ion = 'Name' if not col else col
        vmd+= f'mol color {col_ion}\n'\
              f'mol representation VDW 0.5 {resol}\n'\
              f'mol selection "({sel}) and {IONS}"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 3 {smo}\n'
        
        # style - water
        vmd+= f'mol color ColorID 0\n'\
              f'mol representation Points 1.0\n'\
              f'mol selection "({sel}) and {WATER}"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 4 {smo}\n'
        
        # style - whatever else
        col_wha = 'Name' if not col else col
        rep_wha = f'Licorice 0.3 {resol} {resol}' if not rep else rep
        vmd+= f'mol color {col_wha}\n'\
              f'mol representation {rep_wha}\n'\
              f'mol selection "({sel}) and '\
              f'not({WATER} or {IONS} or {LIPID} or {PROTEIN} or {NUCLEIC})"\n'\
              f'mol material EdgyShiny\nmol addrep top\n'\
              f'mol smoothrep top 5 {smo}\n'
        
        # load frames
        vmd+= f'animate delete  beg 0 end 0 skip 0\n'
        vmd+=  'mol addfile {'+inp+'} type {'+ext+'} '
        vmd+= f'first {sta} last {sto} step {ste} waitfor 1\n'\
              f'animate style Once\n'
        
        # bonds
        if not bonds:
            continue
        for bon in bonds:
            vmd+= f'label add Bonds $index/{bon[0]} $index/{bon[1]}\n'
    
    # launch script
    with open(def_vmd,'w') as f:
        f.write(vmd)   
    descr = f'LAUNCHING VMD - type "quit" to exit'
    commd = f'vmd -e "{def_vmd}" 2>/dev/null\nrm -f "{def_vmd}"'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
