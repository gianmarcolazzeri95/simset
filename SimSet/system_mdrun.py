# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _mdrun(self,*output,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.mdrun" with:\n'
    descr+= f'  output = {output}\n' if output else ''
    descr+= f'  args   = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    def_inp = [self.molec]
    def_out = [extension(self.mdp.root)[0].split('/')[-1]]
    
    # input parameters
    try:
        inpt   = parse(args,'input' ,def_inp)
        sshrun = parse(args,'ssh'   ,0      )
        extend = parse(args,'extend',EXTEND )
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # output
    output = ls(output,str)
    if not len(output):
        output = def_out
        descr = f'TAKING {def_out[0]} as output file prefix\n'
        comment(descr,self.logre,self.logsh,verbo)
    output = [extension(out)[0] for out in output]
    n = len(output)
    
    # default input
    inpt = ls(inpt,str)
    if not len(inpt):
        inpt = def_inp
        descr = f'TAKING {def_inp[0]} as input file prefix\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    exten = REF_EXT
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING input regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            
            # for each prefix a single extension
            found = []
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    p = extension(name)[0]
                    if p not in found:
                        inpt[i].append(name[:-1])
                        found.append(p)
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: error
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'ERROR: none of input {names} found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # reconstruct input
    inpt = ls(inpt)
    if not inpt:
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    inpt = adjust_list(inpt,n)
    
    # update
    update = parse(args,'update',output==def_out)
    
    # index file (if required)
    ndx = ''
    if sum([group not in DIC_SEL for group in self.mdp.group]):
        ndx = f'{def_out[0]}.ndx'
        descr = f'CREATING {ndx} index file\n'
        comment(descr,self.logre,self.logsh,verbo)
        group = list(self.mdp.group.keys())
        select= list(self.mdp.group.values())
        Index(ndx,self.molec,select,group)
    args['ndx'] = ndx
    
    # sshrun
    if sshrun<0: sshrun+= n+1
    ssshrn = min(SSHRUN,max(sshrun,0))
    args['ssh'] = sshrun
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # run
    if sshrun == 0:
        __local(self,inpt,output,**args)
    else:
        __remote(self,inpt,output,**args)

###############################################################################
################################# RUN LOCALLY #################################
###############################################################################

def __local(self,inpt,output,**args):
    
    """
    Run md locally.
    """
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # input parameters
    extend = args['extend']
    ndx    = args['ndx']
    verbo  = args['verbose']
    update = args['update']
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    for inp,out in zip(inpt,output):
        
        # create tpr file
        descr = f'RUNNING grompp for "{out}.tpr"'
        commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
                f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
                f'gmx grompp -f "{self.mdp.root}" -po "{self.mdp.root}" '\
                f'-r "{inp}" -c "{inp}" -p "{self.topol}" '
        commd+= f'-n "{ndx}" ' if ndx else ''
        commd+= f'-o "{out}.tpr"{" -nobackup"*(not BACKUP)}'\
                f' -maxwarn {MAXWARN}'
        comment(descr,self.logre,self.logsh,verbo)
        execute(commd,self.logre,self.logsh,verbo)
        
        # run simulation
        descr = f'RUNNING mdrun for "{out}.tpr"'
        commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
                f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
                f'gmx mdrun{" -nobackup"*(not BACKUP)} '
        commd+= f'-cpi "{out}.cpt" ' if extend else ''
        commd+= f'-deffnm "{out}"'
        comment(descr,self.logre,self.logsh,verbo)
        execute(commd,self.logre,self.logsh,verbo)
        
        # do basic processing
        self.process(f'{out}.gro',center=DEF_SEL)
        
        # update self.last
        self.last = out
        
        # update self.molec and fix titles
        if update:
            descr = f'UPDATING "{self.molec}" with simulation results\n'
            comment(descr,self.logre,self.logsh,verbo)
            self.unive = Update(out,f'{self.molec}.pdb',
                                    f'{self.molec}.gro',title=self.title)
        else:
            Set_Title(f'{out}.gro',self.title)

###############################################################################
################################ RUN REMOTELY #################################
###############################################################################

def __remote(self,inpt,output,**args):
    
    """
    Run md remotely.
    """
    
    # defaults
    HASH = hash(''.join(output))
    tmp_act = f'{DEF_TMP}{HASH}active'  # temp file for active jobs checking
    tmp_sta = f'{DEF_TMP}{HASH}status'  # temp file for job status  checking
    tmp_job = f'{DEF_TMP}{HASH}jobid'   # temp file for job id      checking
    tmp_cur = f'{DEF_TMP}{HASH}current' # temp file for simul. pos. checking
    tmp_que = f'{DEF_TMP}{HASH}queue'   # temp file for all queues  checking
    
    ###########################################################################
    ############################# ROUTINE: SCRAPE #############################
    ###########################################################################
    
    # scrape job ids, look for last n lines
    def scrape(n=1,fname=tmp_job):
        lines = check_output(['tail',f'-{n}',fname])
        lines = [x.split(':') for x in lines.decode().split('\n')]
        lines = list(chain(*lines))
        result = [x.split('.')[0] for x in lines]
        return [int(res) for res in result if res.isdigit()]
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # input parameters
    extend = args['extend']
    sshrun = args['ssh']
    ndx    = args['ndx']
    verbo  = args['verbose']
    update = args['update']
    
    # defaults
    def_aft = [0]
    def_syn = SYNC
    def_qsb = QSUB
    def_wai = WAIT
    def_tol = TOLERANCE
    def_que = []
    def_nod = []
    
    # qsub parameters
    try:
        after = parse(args,'after',def_aft)
        sync  = parse(args,'sync' ,def_syn)
        qsub  = parse(args,'qsub' ,def_qsb)
        wait  = parse(args,'wait' ,def_wai)
        tolerance = parse(args,'tol'      ,def_tol  )
        tolerance = parse(args,'tolerance',tolerance)
        nodes = parse(args,'nodes_list',def_nod)
        queue = parse(args,'queue'     ,def_que)
        queue = parse(args,'queues'    ,queue  )
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # sshrun
    n = len(output)
    sshrun = sshrun if n >= sshrun > 0 else n
    
    # after
    if n > 1:
        after = list(after) if type(after) in [list,tuple,ndarray] else\
                [after]
        after = adjust_list(after,n)
    else:
        after = [ls(after)]
    
    # tolerance
    if tolerance<=0: tolerance = inf
    
    # nodes list
    temp = ls(nodes,str)
    
    # queues (keep priority order)
    if not queue: queue = self.ssh.queue
    queues = []
    all_queues = []
    for que in ls(queue,str):
        
        # all possible queues
        if not all_queues:
            commd = f'ssh {self.ssh.add} \' qstat -q \' > "{tmp_que}"'
            sh(commd)
            with open(tmp_que) as f:
                raw = f.readlines()
            for line in raw:
                line = line.split()
                if not line: continue
                else: line = line[0]
                if line[0] in ['\n','-']: continue
                if line == 'server:' or line == 'Queue': continue
                if line.isdigit(): continue
                all_queues.append(line)
        
        # match "queue" in "all_queues"
        for mat in match(que,*all_queues):
            if mat not in queues:
                queues.append(mat)
    
    # check if ok
    if not queues:
        descr = f'ERROR: empty queue list' if not queue else\
        f'ERROR: "queue" option {ansi(", ".join(queue),"italic")} not valid'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    # check input nodes, split across queues
    nodes = {} # keys: queues, values: nodes available
    if temp:
        descr = f'ASSIGNING input nodes to queues {", ".join(queues)}'
        comment(descr,self.logre,self.logsh,verbo)
        for queue in queues.copy():
            nodes[queue] = set([res[0] for res in\
            self.ssh.check_nodes(temp,queue=queue,verbose=False)])
            num = len(nodes[queue])
            descr = f'FOUND {num} node{"s"*(num!=1)} available '\
                    f'for queue "{queue}"'
            if not num:
                descr = ansi(descr,'yellow')
                nodes.pop(queue)
                queues.remove(queue)
            comment(descr,self.logre,self.logsh,verbo)
            if num and not wait:
                break # stay on the best queue
    else:
        for queue in queues:
            nodes[queue] = set()
    if verbo and temp: print()
    
    # update ssh
    args['queue'] = queues
    self.ssh.update(**args)
    
    ###########################################################################
    ############################## PREPARE FILES ##############################
    ###########################################################################
    
    # create rsync list
    flist = [self.mdp.root]
    flist+= [ndx] if ndx else []
    
    # create pbs files
    descr = f'CREATING {n} pbs file{"s"*(n!=1)}\n'
    comment(descr,self.logre,self.logsh,verbo)
    title = extension(self.mdp.root.split('/')[-1])[0]
    self.ssh.pbs(output,inpt=inpt,ndx=ndx,title=title,nodes=nodes,
                 mdp=self.mdp.root,topol=self.topol,extend=extend,
                 auto=not wait or not qsub,verbose=verbo)
    flist+= [f'{out}.pbs' for out in output]
    
    # complete rsync list
    if sync:
        flist+= list(set(inpt))
        flist.append(self.topol)
        if extend:
            for out in output:
                flist.append(f'{out}.cpt')
                flist.append(f'{out}.xtc')
                flist.append(f'{out}.log')
                flist.append(f'{out}.edr')
        if self.mdp.param['ratchet_contact_maps'][-1]:
            flist.append(self.mdp.param['ratchet_contact_maps'][-1])
    
    # rsync to cluster
    self.forward(flist)
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # not qsub option
    if not qsub:
        descr = f'SUBMIT {", ".join(output)} manually'
        comment(descr,self.logre,self.logsh,verbo)
        return
    
    ###########################################################################
    ############################# NOT WAIT OPTION #############################
    ###########################################################################
    
    # not wait option
    if not wait:
        
        # divide jobs in stacks
        ns = n//sshrun+(n%sshrun>0)
        jobid = []
        inpt  = [  inpt[i*sshrun:(i+1)*sshrun] for i in range(ns)]
        stacks= [output[i*sshrun:(i+1)*sshrun] for i in range(ns)]
        after = [ after[i*sshrun:(i+1)*sshrun] for i in range(ns)]
        
        # warning
        if extend and ns-1:
            descr = f'WARNING: if first stack of jobs does not complete'
            descr+= f'before {self.ssh.wallt}, dependencies shall be broken'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
        
        # qsub stack by stack with dependencies
        jobid = []
        for i,stack,aftlist in zip(range(len(stacks)),stacks,after):
            aftlist = [ls([aft]+jobid) for aft in aftlist]
            best = self.ssh.best_nodes(sshrun,nodes[queue],verbose=False)
            descr = f'SUBMITTING stack #{i+1} of {sshrun} job(s) on cluster'
            commd = self.ssh.qsub(stack,after=aftlist,nodes=best,queue=queue)
            comment(descr,self.logre,self.logsh,verbo)
            execute(commd,self.logre,self.logsh,verbo)
            
            # retrieve job indexes
            jobid+= scrape(sshrun)
            if len(jobid) != sshrun*(i+1):
                descr = f'ERROR: job submission failed\n'
                descr = ansi(descr,'red')
                comment(descr,self.logre,self.logsh,verbo)
                return
            
            # print submitted jobids
            descr = f'JOBIDS {" ".join([f"#{x}" for x in jobid[i*sshrun:]])}\n'
            comment(descr,self.logre,self.logsh,verbo)
        
        return jobid
    
    ###########################################################################
    ####################### WAIT OPTION - INITIALIZATION ######################
    ###########################################################################
    
    """
    smart re-sending and queue handling - max sshrun jobs running at once
    """
    
    # delete orphan jobs
    if isfile(tmp_act):
        with open(tmp_act) as f:
            sent = f.read()[:-1]
        if sent:
            n_del = len(sent.split())
            descr = f'ABORTING {n_del} orphan job{"s"*(n_del!=1)}'
            commd = f"ssh {self.ssh.add} ' qdel {sent} ' 2>/dev/null"
            comment(descr,self.logre,self.logsh,verbo)
            execute(commd,self.logre,self.logsh,False)
    
    # general variables
    t0 = now() # the beginning of all
    ncheck = 0 # number of times the program checked jobs' status
    sshsub = min(SSHSUB,sshrun) # maximum number of contemporay re-subs
    tinit = self.mdp.param['tinit'][-1]
    total = self.mdp.param['nsteps'][-1]*self.mdp.param['dt'][-1]/1e3+tinit
    min_speed = 3e9*self.ssh.proce*self.ssh.nodes # minimum simulation speed
    min_speed*= self.mdp.param['dt'][-1]
    min_speed/= len(self.unive.atoms)*log(len(self.unive.atoms))
    remaining = list(range(n)) # indexes of remaining jobs
    completed = []             # indexes of completed jobs
    
    # initialize basic jobs attributes
    submitted = [0]*n # fill with submission time
    started   = [0]*n # fill with start time - reset to 0 if not running
    jobid     = [0]*n # fill with last jobid associated to each job
    
    # jobs to check / to send / to del
    tocheck = {} # keys: job indexes, values: associated line in stdout
    tosend  = {} # keys: job indexes, values: associated line in stdout
    toscan  = {} # keys: job indexes, values: associated line in stdout
    todel   = {} # keys: job indexes, values: associated jobids
    
    # initialize logging jobs attributes
    origin  = [0]    *n # fill with first time had infos
    initial = [-inf] *n # fill with initial md time (ps)
    current = [-inf] *n # fill with current md time (ps)
    speed   = [NaN]  *n # fill job's average speed
    
    # initialize nodes jobs attributes
    nodspeed = {} # keys: nodes, values: [average speed,last 9 measures]
    assigned = {} # keys: job indexes, values: queue,assigned nodes
    indicted = {} # keys: job indexes, values: indicted nodes
    paused = {} # keys: queues, values: paused nodes
    for queue in queues:
        paused[queue] = set()
    
    # initialize status attributes
    nlines = sshrun+2 # number of "dynamic" lines printed
    report = ['']*nlines # fill with info about active jobs, errors, prog. bar
    refresh= [False]*nlines # which jobs you get updates of
    
    header = '\u001b[2K'+'\u001b[1F\u001b[2K'*(nlines-1) # reset lines
    
    # initialize progress bar
    m = 0 # number of completed jobs
    TOTAL = n*(total-tinit)*1000 # ps to simulate
    LEFT  = TOTAL # ps left to simulate
    elapsed = 0   # elpased time (s)
    ETR   = None  # LEFT / SPEED
    t_ETR = 0     # time of last ETR estimate
    SPEED = None  # average speed of active jobs: ps simulated/s
    RATE  = None  # average rate of active jobs: jobs completed/s
    cursor.hide()
    
    # waiting cycle
    print('\npress Ctrl-C to abort all')
    print(end='\n'*nlines)
    try:
        while True:
            
            # reset width
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            
            # elapsed time
            NOW = now()
            elapsed = round(NOW-t0)
            
            ###################################################################
            ################## CHECK TIME OF RUNNING SIMULATIONS ##############
            ###################################################################
            
            # parameters initialization
            commd = [] # command to be executed
            result= [] # results of the command
            tot_speed = 0 # speed of all running jobs
            if toscan:  LEFT = TOTAL-m*(total-tinit)*1000
            elif SPEED: LEFT-= SPEED
            
            # check every "CHECK" seconds
            if toscan and elapsed > ncheck*CHECK:
                for i in toscan:
                    commd.append(f'module load cuda-10.0 gcc54\n'\
                                 f'source "{self.ssh.gmx}/GMXRC"\n')
                    if self.ssh.dir in ['','.']:
                        commd.append(f'gmx check -f "{output[i]}.cpt" '\
                                     f'-lastener Potential '\
                                     f'>/dev/null 2>"{tmp_cur}"\n')
                    else:
                        commd.append(f'gmx check -f '\
                                     f'"{self.ssh.dir}/{output[i]}.cpt" '\
                                     f'-lastener Potential '\
                                     f'>/dev/null 2>"{tmp_cur}"\n')
                    commd.append(f'sed "38q;d" "{tmp_cur}"\n'\
                                 f'rm -f "{tmp_cur}"\n'\
                                 f'echo "next"\n')
                commd = f'ssh {self.ssh.add} \'\n{"".join(commd)}'\
                        f' \' 2>/dev/null'
                sh(f'{commd} > {tmp_sta}')  
                with open(tmp_sta) as f:
                    result = f.read().split('next')[:-1]
            
            # retrieve cached data about time of running simulations
            if len(result) != len(toscan):
                for i in toscan:
                    k = toscan[i]
                    
                    # estimate current position
                    if current[i] == -inf:
                        est = NaN
                        continue
                    elif isnan(speed[i]):
                        est = current[i]
                        LEFT-= est
                    else:
                        est = initial[i]+\
                              speed[i]*(now()-origin[i])/3600
                        est = max(est,total/1000)
                        LEFT-= est
                    if not isnan(speed[i]):
                        tot_speed+= speed[i]
                    else:
                        for nod in assigned[i][1]:
                            if nod in nodspeed:
                                tot_speed+= nodspeed[nod][0]
                    
                    # update "report" line
                    line = f'JOB "{output[i]}" ({jobid[i]}) hit '\
                           f'{est:8.2f} ps ({speed[i]:7.2f} ps/h)'
                    pbar = min(len(line),int(est*len(line)/(total*1000)))\
                           if not isnan(est) else 0
                    if refresh[k]:
                        if len(line)-pbar>=26: 
                            line = ansi(line[:pbar],'invert')+line[pbar:-26]+\
                                   ansi(line[-26:] ,'green')
                        else:
                            line = ansi(line[:-26],'invert')+\
                                   ansi(line[-26:pbar],'green','invert')+\
                                   ansi(line[pbar:],'green')
                    else:
                        line = ansi(line[:pbar],'invert')+line[pbar:]
                    if not isnan(est) and not isnan(speed[i]):
                        etr = round((total*1000-est)/speed[i]*3600)
                        if etr > 0:
                            temp = f' {time_format(etr):>8} etr'
                            line+= ansi(temp,'italic','blink')
                        else:
                            temp = f" should've finished"
                            line+= ansi(temp,'red','italic','blink')
                    report[k] = line
            
            # update (some) data about time of running simulations
            else:
                for i,res in zip(toscan,result):
                    k = toscan[i]
                    refresh[k] = False
                    
                    # check if updates are available
                    try:    sim_t = float(res.split()[-1])
                    except: sim_t = tinit
                    if initial[i] == -inf:
                        initial[i] = sim_t
                        current[i] = sim_t
                        origin [i] = round(now())
                    if sim_t > current[i]:
                        refresh[k] = True
                        current[i] = sim_t
                        speed[i] = (current[i]-initial[i])
                        speed[i]*= 3600/(now()-origin [i])
                    
                    # estimate current position
                    if current[i] == -inf:
                        est = NaN
                        continue
                    elif isnan(speed[i]):
                        est = current[i]
                        LEFT-= est
                    else:
                        est = initial[i]+\
                              speed[i]*(now()-origin[i])/3600
                        est = max(est,total/1000)
                        LEFT-= est
                    if not isnan(speed[i]):
                        tot_speed+= speed[i]
                        if refresh[k]:
                            for nod in assigned[i][1]:
                                if nod not in nodspeed:
                                    nodspeed[nod] = []
                                nodspeed[nod] = [0.]+nodspeed[nod][2:10]
                                nodspeed[nod].append(speed[i]/self.ssh.nodes)
                                nodspeed[nod][0] = mean(nodspeed[nod][1:10])
                    else:
                        for nod in assigned[i][1]:
                            if nod in nodspeed:
                                tot_speed+= nodspeed[nod][0]
                    
                    # update "report" line
                    line = f'JOB "{output[i]}" ({jobid[i]}) hit '\
                           f'{est:8.2f} ps ({speed[i]:7.2f} ps/h)'
                    pbar = min(len(line),int(est*len(line)/(total*1000)))\
                           if not isnan(est) else 0
                    if refresh[k]:
                        if len(line)-pbar>=26: 
                            line = ansi(line[:pbar],'invert')+line[pbar:-26]+\
                                   ansi(line[-26:] ,'green')
                        else:
                            line = ansi(line[:-26],'invert')+\
                                   ansi(line[-26:pbar],'green','invert')+\
                                   ansi(line[pbar:],'green')
                    else:
                        line = ansi(line[:pbar],'invert')+line[pbar:]
                    if not isnan(est) and not isnan(speed[i]):
                        etr = round((total*1000-est)/speed[i]*3600)
                        if etr > 0:
                            temp = f' {time_format(etr):>8} etr'
                            line+= ansi(temp,'italic','blink')
                        else:
                            temp = f" should've finished"
                            line+= ansi(temp,'red','italic','blink')
                    report[k] = line
            
            # update progress bar parameters
            if tot_speed:
                SPEED = tot_speed/3600 # simulated ps/s
                RATE  = SPEED/(total-tinit)/1000 # jobs/s
                ETR   = LEFT/SPEED
                t_ETR = now()
            
            # very bad situation
            if not toscan:
                SPEED = None
                RATE  = None
                ETR = None
            
            ###################################################################
            ######################### CHECK JOBS STATUS #######################
            ###################################################################
            
            # parameters initialization
            result = [] # results of the command exec.
            
            # check every "check" seconds
            if elapsed > ncheck*CHECK:
                ids = [jobid[i] for i in tocheck]
                result = self.ssh.check(ids,verbose=False)
                if len(result) != len(tocheck):
                    line = f'WARNING: something went wrong with checking'
                    line = ansi(line,'yellow','blink')
                    report[-2] = line
                else:
                    report[-2] = ''
            
            # separate treatment reserved to job waiting (while no errors)
            for i in tocheck:
                if started[i] or round(now())-submitted[i]<CHECK:
                    continue
                k = tocheck[i]
                t = TOLERANCE-round(now())+submitted[i]
                if t <= 0:
                    line =f'JOB "{output[i]}" ({jobid[i]}) surpassed '\
                          f'waiting tolerance'
                    line = ansi(line,'yellow')
                else:
                    line =f'JOB "{output[i]}" ({jobid[i]}) still waiting '\
                          f'({time_format(t):>9} more)'
                    pbar = min(len(line),
                               int((TOLERANCE-t)*len(line)/TOLERANCE))
                    line = ansi(line[:pbar],'magenta','invert')+\
                           ansi(line[pbar:],'magenta')
                report[k] = line
            
            # iterate through results
            for i,res in zip(list(tocheck.keys()),result):
                ji,js,je = res # job id / job exit / job status
                line  = ''  # reset line
                k = tocheck[i]
                
                # job ended well
                if js == 'F' and not bool(je):
                    t = round(now())-started[i]
                    t = t if t < 3600*24*365 else CHECK
                    started[i] = 0
                    line = f'JOB "{output[i]}" ({jobid[i]}) ended ok '\
                           f'in about {time_format(t):>9}'
                    line = ansi(line,'green')
                    if i in toscan:
                        toscan.pop(i) # stop scanning
                    tocheck.pop(i) # release job
                    completed.append(i)
                    m+= 1 # update counter
                    
                    # release paused nodes that are now free
                    paused[assigned[i][0]] =\
                    paused[assigned[i][0]].difference(assigned[i][1])
                    assigned.pop(i)
                    indicted.pop(i)
                
                # job ended bad
                if js == 'F' and bool(je):
                    t = round(now())-started[i]
                    t = t if t < 3600*24*365 else 0
                    started[i] = 0
                    line = f'JOB "{output[i]}" ({jobid[i]}) ended ko '\
                           f'at about {time_format(t):>9}'
                    line = ansi(line,'red')
                    if i in toscan:
                        toscan.pop(i) # stop scanning
                    if len(tosend) < sshsub: tosend[i] = k # resend job
                    
                    # don't run the job on those nodes anymore
                    if t <= CHECK:
                        for node in assigned[i][1]:
                            indicted[i].append(node)
                
                # job still waiting
                if js == 'Q':
                    t = round(now())-submitted[i]
                    
                    # waited too much: aborting
                    if t > TOLERANCE:
                        line = f'JOB "{output[i]}" ({jobid[i]}) surpassed '\
                               f'waiting tolerance, aborting'
                        line = ansi(line,'yellow','blink')
                        todel [i] = jobid[i] # delete job
                        if len(tosend) < sshsub: tosend[i] = k # resend job
                    
                        # don't send jobs to those nodes (unless extrema ratio)
                        paused[assigned[i][0]] =\
                        paused[assigned[i][0]].union(assigned[i][1])
                    
                    # keep waiting
                    elif t >= CHECK/2:
                        t = TOLERANCE-t
                        line = f'JOB "{output[i]}" ({jobid[i]}) still '\
                               f'waiting ({time_format(t):>9} more)'
                        pbar = min(len(line),
                                   int((TOLERANCE-t)*len(line)/TOLERANCE))
                        line = ansi(line[:pbar],'magenta','invert')+\
                               ansi(line[pbar:],'magenta')
                
                # job just started
                if js == 'R':
                    if not started[i]:
                        started[i] = round(now())
                        line = f'JOB "{output[i]}" ({jobid[i]}) started'
                        if assigned[i][1]:
                            line+= f' on node{"s"*(len(assigned[i][1])!=1)} '+\
                                   ', '.join(assigned[i][1])
                        line = ansi(line,'cyan')
                        toscan[i] = k # will check how much simulated
                
                    # job still running - but node too slow: aborting
                    elif speed[i] < min_speed:
                        toremove = [nod for nod in assigned[i][1]\
                                    if nod in nodspeed and\
                                    max(nodspeed[nod]) < min_speed]
                        started[i] = 0
                        line =f'JOB "{output[i]}" ({jobid[i]}) running '\
                              f'too slow ({speed[i]:6.2f} ps/h), aborting'
                        line = ansi(line,'yellow','blink')
                        toscan.pop(i)        # stop scanning
                        todel [i] = jobid[i] # delete job
                        if len(tosend) < sshsub: tosend[i] = k # resend job
                        
                        # NEVER run jobs on those nodes again!
                        #nodes[assigned[i][0]] =\
                        #nodes[assigned[i][0]].difference(toremove)
                        
                        # NOBODY is perfect, you know...
                        paused[assigned[i][0]] =\
                        paused[assigned[i][0]].union(assigned[i][1])
                
                # update report
                if line:
                    report[k] = line
            
            ###################################################################
            ############# PRINT REPORT, QDEL AND PREPARE FOR QSUB #############
            ###################################################################
            
            # update progress bar
            etr = max(0,ETR-NOW+t_ETR) if ETR is not None else None
            NOW = now()
            ela = round(NOW-t0)
            bar = progress_bar(m,n,ela,etr,RATE,'',width) if n-1 else\
                  progress_bar(TOTAL-LEFT,TOTAL,ela,etr,SPEED,'ps',width)
            report[-1] = f'{bar}\r'
            
            # print status
            if verbo:
                print(header+'\n'.join(report),end='')
            descr = '\n'+'\n'.join([rep for rep in report if rep])
            if elapsed > ncheck*CHECK:
                ncheck+= 1+(elapsed-ncheck*CHECK)//CHECK # update ncheck
                comment(descr,self.logre,self.logsh,False)
            
            # delete jobs that have to
            commd = []
            while len(todel):
                commd.append(f'qdel {todel.popitem()[-1]}\n')
            if commd:
                commd = f'ssh {self.ssh.add} \'\n{"".join(commd)}'\
                        f' \' 2>/dev/null'
                execute(commd,self.logre,self.logsh,False)
            
            # check if all jobs are done
            if m==n:
                if verbo: print('\n')
                break
            
            # check if new jobs have to be sent, up to ssrun simultaneously
            k = 0 # "report" index
            if len(tosend) < sshsub:
                while remaining:
                    if len(tocheck) >= sshrun:
                        break
                    while k in tocheck.values():
                        k+= 1
                    i = remaining.pop(0)
                    tocheck[i] = k
                    tosend [i] = k
            
            # stop here if there are no jobs to send
            sleep(1)
            if not tosend:
                continue
            
            ###################################################################
            ##################### SUBMIT JOBS THAT HAVE TO ####################
            ###################################################################
            
            # reset paused nodes
            if RESET <= elapsed <= RESET+2*CHECK:
                for queue in queues:
                    paused[queue] = set()
            
            # restrict to "good" nodes - select best nodes
            for i,queue in enumerate(queues):
                good = nodes[queue].difference(ls(indicted))
                if good.difference(paused[queue]):
                    break
                elif i == len(queues)-1: # all nodes are paused
                    for queue in queues[::-1]:
                        paused[queue] = set() # extrema ratio
                    good = nodes[queue].difference(ls(indicted))
            good = good.difference(paused[queue])
            best = self.ssh.best_nodes(len(tosend),good,
                                       queue=queue,verbose=False)
            line = f'CHOOSED queue {ansi(queue,"bold","yellow")}, '\
                   f'{len(good)} out of {len(nodes[queue])} nodes available'
            line = ansi(line,'italic','blink')
            report[-2] = line
            
            # debug tool - uncomment to deactivate
            """
            with open(f'.available{ncheck}','w') as f:
                f.write(f'n. {ncheck}')
                f.write(f'\nNODES ({len(set(ls(nodes)))}): {nodes}')
                f.write(f'\nINDICTED ({len(set(ls(indicted)))}): '+\
                ', '.join(ls(indicted)))
                f.write(f'\nPAUSED ({len(set(ls(paused)))}): {paused}')
                f.write(f'\nGOOD ({len(good)}): {good}')
                f.write(f'\nCURRENT QUEUE: {queue}')
                f.write(f'\nTOCHECK ({len(tocheck)})')
                f.write(f'\n{"JOB":20}{"ASSIGNED":20}{"INDICTED":20}')
                for i in tocheck:
                    out = output[i]
                    ass = assigned[i] if i in assigned else ('','')
                    ind = indicted[i] if i in indicted else ['']
                    nnn = max(len(ass),len(ind))
                    out = [out]+(nnn-1)*['']
                    ass = ass + (nnn-len(ass))*('','')
                    ind = ind + (nnn-len(ind))*['']
                    for o,a,i in zip(out,ass,ind):
                        f.write(f'\n{str(o):20}{str(a):20}{str(i):20}')
                f.write(f'\nTOSEND ({len(tosend)})')
                f.write(f'\n{"JOB":20}{"ASSIGNED":20}{"INDICTED":20}')
                for i in tosend:
                    out = output[i]
                    ass = assigned[i] if i in assigned else ('','')
                    ind = indicted[i] if i in indicted else ['']
                    nnn = max(len(ass),len(ind))
                    out = [out]+(nnn-1)*['']
                    ass = ass + (nnn-len(ass))*('','')
                    ind = ind + (nnn-len(ind))*['']
                    for o,a,i in zip(out,ass,ind):
                        f.write(f'\n{str(o):20}{str(a):20}{str(i):20}')
                f.write(f'\nON NODES ({len(set(best))}): '+', '.join(best))
            #"""
            
            # submit jobs
            commd = self.ssh.qsub([output[i] for i in tosend],nodes=best,
                            after=[after[i]  for i in tosend],queue=queue)
            sh(f'rm -f "{tmp_job}"')
            execute(commd,tmp_job,self.logsh,False)
            
            # retrieve job indexes
            sent = scrape(len(tosend))
            if len(sent)!=len(tosend):
                line = f'WARNING: something went wrong with qsub, aborting'
                line = ansi(line,'yellow','blink')
                report[-2] = line
                sent = ' '.join([str(job) for job in sent])
                commd = f"ssh {self.ssh.add} ' qdel {sent} ' 2>/dev/null"
                execute(commd,self.logre,self.logsh,False)
                
                # print status
                if verbo:
                    print(header+'\n'.join(report),end='')
                descr = '\n'+'\n'.join([rep for rep in report if rep])
                comment(descr,self.logre,self.logsh,False)
                
                # reset "tosend", "tocheck" and "remaining"
                while tosend:
                    i = tosend.popitem()[0]
                    tocheck.pop(i)
                    remaining.insert(0,i)
                continue
            
            # update with new jobs - reset counter
            for j,i in enumerate(tosend):
                k = tosend[i]
                submitted[i] = round(now())
                initial  [i] = -inf
                current  [i] = -inf
                origin   [i] = -inf
                speed    [i] = NaN
                assigned [i] = (queue,
                                best[j*self.ssh.nodes:(j+1)*self.ssh.nodes])
                if i not in indicted: indicted[i] = []
                jobid    [i] = sent[j]
                line = f'JOB "{output[i]}" ({jobid[i]}) sent'
                if assigned[i][1]:
                    line+= f' on node{"s"*(len(assigned[i][1])!=1)} '+\
                           ', '.join(assigned[i][1])
                line = ansi(line,'blue')
                report[k] = line
            
            # save jobids of currently running jobs for recovering
            with open(tmp_act,'w') as f:
                for i in tocheck:
                    f.write(f'{jobid[i]} ')
            
            # reset "tosend"
            tosend = {}
            
            # update progress bar
            NOW = now()
            ela = round(NOW-t0)
            etr = max(0,ETR-NOW+t_ETR) if ETR is not None else None
            bar = progress_bar(m,n,ela,etr,RATE,'',width) if n-1 else\
                  progress_bar(TOTAL-LEFT,TOTAL,ela,etr,SPEED,'ps',width)
            
            report[-1] = f'{bar}\r'
            
            # print status
            if verbo:
                print(header+'\n'.join(report),end='')
            descr = '\n'+'\n'.join([rep for rep in report if rep])
            comment(descr,self.logre,self.logsh,False)
            sleep(1)
    
    ###########################################################################
    ######################### WAIT OPTION - END CYCLE #########################
    ###########################################################################
    
    except KeyboardInterrupt:
        cursor.show()
    
        # delete current stack of jobs
        sent = []
        for i in tocheck:
            sent.append(str(jobid[i]))
            
        descr = f'\nABORTING {len(tocheck)} active jobs\n'
        sent = ' '.join(sent)
        commd = f"ssh {self.ssh.add} ' qdel {sent} ' 2>/dev/null"
        comment(descr,self.logre,self.logsh,verbo)
        execute(commd,self.logre,self.logsh,False)
        
        # remove jobs that didn't end well from list
        # starting from the end in order to not mess up with indeces
        for i in range(n-1,-1,-1):
            if i in completed:
                continue
            inpt.pop(i)
            output.pop(i)
            jobid.pop(i)
    
    # finished simulations
    sh(f'rm -f "{tmp_act}"')
    cursor.show()
    
    ###########################################################################
    ############################# FINAL OPERATIONS ############################
    ###########################################################################
    
    # anything left?
    if not len(output):
        return
    
    # rsync back
    if sync:
        self.backward(*output)
    
    # do basic processing and fix titles
    self.process([f'{out}.gro' for out in output],center=DEF_SEL)
    for out in output:
        Set_Title(f'{out}.gro',self.title)
    
    # update self.last
    out = output[-1]
    self.last = out
    
    # update self.molec
    if update:
        descr = f'UPDATING "{self.molec}" with simulation results\n'
        comment(descr,self.logre,self.logsh,verbo)
        self.unive = Update(f'{out}.gro',f'{self.molec}.pdb',
                            f'{self.molec}.gro',title=self.title)
