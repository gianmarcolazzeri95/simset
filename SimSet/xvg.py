# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

# class-specific functions library
from SimSet.xvg_stats    import _min,_max,_mean,_std
from SimSet.xvg_analysis import _derivative,_integral
from SimSet.xvg_merge    import _merge
from SimSet.xvg_select   import _select
from SimSet.xvg_delete   import _delete
from SimSet.xvg_write    import _write
from SimSet.xvg_plot     import _plot

###############################################################################
################################## CLASS: XVG  ################################
###############################################################################

class xvg:
    
    """
    Load, read, write and plot xvg files.
    
    Attributes
    ----------
    memory : (unchangeable) bool, "memory" option
    obser : list of observables names by xvg columns
    files : list of filenames from which the data are taken by columns
    title : list of titles by columns
    xaxis : list of x-axis labels by columns
    yaxis : list of y-axis labels by columns
    times : if ``self.__memory``: (ordered) list of time points
            associated to datapoints; otherwise: ``SimSet.binary``
            object
    datap : if ``self.__memory``: list of lists, datapoints by columns;
            otherwise: list of ``SimSet.binary`` objects
    """
    
    ###########################################################################
    ############################ SET ATTRIBUTES' TYPE #########################
    ###########################################################################
    
    # memory (unchangeable)
    def _get_memory(self):
        return self.__memory
    memory = property(_get_memory)
    
    # obser
    def _get_obser(self):
        return self.__obser
    def _set_obser(self,value):
        value = ls(value,str)
        self.__obser = value
    obser = property(_get_obser,_set_obser)
    
    # files
    def _get_files(self):
        return self.__files
    def _set_files(self,value):
        value = ls(value,str)
        self.__files = value
    files = property(_get_files,_set_files)
    
    # title
    def _get_title(self):
        return self.__title
    def _set_title(self,value):
        value = ls(value,str)
        self.__title = value
    title = property(_get_title,_set_title)
    
    # xaxis
    def _get_xaxis(self):
        return self.__xaxis
    def _set_xaxis(self,value):
        value = ls(value,str)
        self.__xaxis = value
    xaxis = property(_get_xaxis,_set_xaxis)
    
    # yaxis
    def _get_yaxis(self):
        return self.__yaxis
    def _set_yaxis(self,value):
        value = ls(value,str)
        self.__yaxis = value
    yaxis = property(_get_yaxis,_set_yaxis)
    
    # times
    def _get_times(self):
        if self.__memory:
            return self.__times
        return list(self.__times.array())
    def _set_times(self,value):
        if self.__memory:
            self.__times = ls(value,int,float)
        else:
            self.__times = binary('',value)
    times = property(_get_times,_set_times)
    
    # datap
    def _get_datap(self):
        if self.__memory:
            return self.__datap
        return [list(data.array()) for data in self.__datap]
    def _set_datap(self,value):
        if type(value) not in [list,tuple]:
            raise TypeError('"datap" must be set to a list of data series')
        self.__datap = []
        for i,val in enumerate(value):
            if self.__memory:
                self.__datap.append(ls(val,int,float))
            else:
                self.__datap.append(binary('',val))
    datap = property(_get_datap,_set_datap)
    
    # manipulate times and datap directly
    def get_times(self):
        """ Access to private ``self.__times``"""
        return self.__times
    def set_times(self,value):
        """ Modify private ``self.__times``"""
        self.__times = value
    def get_datap(self):
        """ Access to private ``self.__datap``"""
        return self.__datap
    def set_datap(self,value):
        """ Modify private ``self.__datap``"""
        self.__datap = value
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,*files,**args):
        
        """
        Parameters
        ----------
        files : tuple, optional, xvg filenames from which data are
                loaded
        args  : dict, optional arguments, see ``SimSet.xvg.load`` for
                details
          'memory' : bool, if ``True``: load data on RAM; otherwise:
                     use ``SimSet.binary`` objects; default is
                     ``SimSet.MEMORY``
        
        Notes
        -----
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.xvg.merge`` method.
        
        ``memory==False`` makes data i/o slower, but you can manage
        much bigger files wihout running out of RAM.
        
        Once the ``memory`` option is set it CANNOT be changed. The
        file names associated to ``SimSet.binary`` objects are
        picked at random.
        
        See Also
        --------
        SimSet.xvg.merge : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> # import all files matching "folding*.xvg" regexp
        >>> x = SimSet.xvg('folding',match='*')
        """
        
        # initialize random value
        self.__memory = parse(args,'memory',MEMORY)
        self.obser = []
        self.files = []
        self.title = []
        self.xaxis = []
        self.yaxis = []
        self.times = binary('')
        self.datap = []
        self.__n = 0 #  as iterator
        self.merge(*files,**args)
    
    ###########################################################################
    ######################### PRINT AND REPRESENT SELF ########################
    ###########################################################################
    
    def __str__(self):
        
        """
        Return info about ``self`` to be printed to stdout.
        """
        
        # retrieve infos
        N = len(self.__times)
        H = len(self)
        
        # retrieve attributes
        obser = self.obser
        files = self.files
        title = self.title
        xaxis = self.xaxis
        yaxis = self.yaxis
        datap = self.__datap
        
        # terminal width and scaling
        try:    width = min(max(50,get_terminal_size()[0]),120)
        except: width = DEF_WID
        
        # padding
        padA = max(4,int(log10(len(self.obser)))+3) if H else 4
        padB = max(7,max([len(obs) for obs in self.obser]+[0])+2)
        padC = width//16*3
        padD = width//16*2+1
        padE = max(7,int(log10(N))+3) if N else 7
        
        # adjust padding according to padA
        left = width-padA-padB-2*(padC+padD)-padE
        if left > 0:
            padB+= left//5+left%5
            padC+= left//5*2
        else:
            padB+= left
        
        # header
        text =[ansi(f'{"id":{padA}}{"obser":{padB}}{"file":{padC}}'\
                    f'{"title":{padC}}{"xaxis":{padD}}{"yaxis":{padD}}'\
                    f'{"points":{padE}}','bold'),f'{"-"*width}']
        
        # adjust pads
        padA = f'<{padA}.0f'
        padE = f'<{padE}.0f'
        
        # initialization
        M = 0 # total number of added points
        # loop through data points
        for h,       obs,  fil,  tit,  xax,  yax,  dat in\
        zip(range(H),obser,files,title,xaxis,yaxis,datap):
            
            # format data
            obs = obs.replace(XVG_DIV,' ')[-padB+2:]
            fil = extension(fil)[0][-padC+2:]
            tit = tit[:padC-2]
            xax = xax[:padD-2]
            yax = yax[:padD-2]
            poi = sum([not isnan(d) for d in dat])
            M  += poi
            
            # append line to text
            text.append(f'{h:{padA}}{obs:{padB}}{fil:{padC}}{tit:{padC}}'\
                                  f'{xax:{padD}}{yax:{padD}}{poi:{padE}}')
        
        # statistics
        if not M:
            M = 0
        text.append(ansi(f'xvg object with {H} column{"s"*(H!=1)} '\
                         f'and {M} point{"s"*(M!=1)} on ','bold')+\
                   (ansi(f'memory','yellow','bold') if self.__memory else\
                    ansi(f'ROM','yellow','bold')))
        return '\n'.join(text)
    
    def __repr__(self):
        
        """
        Return a string representation of ``self``.
        """
        
        return f'xvg object with {len(self)} column{"s"*(len(self)!=1)} '\
               f'and {self.size()} point{"s"*(self.size()!=1)} on '+\
         (ansi(f'memory','yellow','bold') if self.__memory else\
          ansi(f'ROM','yellow','bold'))
    
    def __format__(self,spec):
        
        """
        Return a proper formatted string of ``self``.
        """
        
        # string formatting
        if not spec or 's' in spec:
            return f'{no_ansi(self.__str__())}\n'
        
        # build temporary formatted array
        temp = array([[d.__format__(spec) for d in dat] for dat in self.datap])
        return temp.__format__('')
    
    ###########################################################################
    ############################## BASIC OPERATORS ############################
    ###########################################################################
        
    def __bool__(self):
        
        """
        Return ``True`` if ``self`` has at least a data point,
        ``False`` otherwise.
        """
        
        # find a point
        for dat in self.datap:
            for d in dat:
                if not isnan(d):
                    return True
        return False
    
    def __call__(self,*columns,**args):
        
        """
        Alias for ``self.select(*columns,**args)``.
        """
        
        return self.select(*columns,**args)
    
    def __del__(self):
        
        """
        Destroy the object. Alias for
        ``self.delete(nan=False,verbose=False)``.
        """
        
        self.delete(nan=False,verbose=False)
    
    ###########################################################################
    ############################ ARITMETIC OPERATORS ##########################
    ###########################################################################
    
    def __add__(self,other):
        
        """
        Return a ``SimSet.xvg`` object that merges ``self`` and
        ``other``.
        """
        
        return xvg(self,other,verbose=False)
    
    def __sub__(self,other):
        
        """
        Return a ``SimSet.xvg`` object that includes ONLY the columns
        of ``self`` that are not in ``other``. The order of ``self`` is
        preserved.
        """
        
        # initialize result
        result = xvg(memory=self.__memory)
        
        # iterate through the columns of "self"
        for x in self:
            if x not in other:
                result.merge(x,verbose=False)
        return result
    
    def __pos__(self):
        
        """
        Return ``self``.
        """
        
        return self
    
    def __neg__(self):
        
        """
        Return an empty ``SimSet.xvg`` object. Alias for ``self-self``.
        """
        
        return xvg(memory=self.__memory)
    
    def __mul__(self,n):
        
        """
        Repeat ``self`` columns for ``n`` times. ``n`` must be an
        integer.
        """
        
        result = xvg(memory=self.__memory)
        for i in range(n):
            result.merge(self)
        return result
    
    def __and__(self,other):
        
        """
        Return the intersection of ``self`` and ``other``. The order
        of ``self`` is preserved.
        """
        
        # initialize result
        result = xvg(memory=self.__memory)
        
        # iterate through the columns of "self"
        for x in self:
            if x in other:
                result.merge(x,verbose=False)
        return result
    
    def __or__(self,other):
        
        """
        Return the union of ``self`` and ``other``. The order of
        ``self`` is preserved. Alias for ``self+other``.
        """
        
        return self+other
    
    def __xor__(self,other):
        
        """
        Return ``(self-other) + (other-self)``.
        """
        
        # initialize result
        result = xvg(memory=self.__memory)
        
        # iterate through the columns of "self"
        for x in self:
            if x not in other:
                result.merge(x,verbose=False)
        
        # iterate through the columns of "other"
        for x in other:
            if x not in self:
                result.merge(x,verbose=False)
        return result
    
    ###########################################################################
    ########################### COMPARISON OPERATORS ##########################
    ###########################################################################
    
    def __lt__(self,other):
        
        """
        "Less than". Return ``True`` if the maximum data point of
        ``self`` is smaller than the maximum data point of ``other`,
        ``False`` otherwise.
        """
        
        max_self  = max([max(dat) for dat in self .datap])\
                    if len(self)  else -inf
        max_other = max([max(dat) for dat in other.datap])\
                    if len(other) else -inf
        return max_self < max_other
    
    def __le__(self,other):
        
        """
        "Less than or equal to". Return ``True`` if the maximum data
        point of ``self`` is smaller than or equal to the maximum data
        point of ``other`, ``False`` otherwise.
        """
        
        max_self  = max([max(dat) for dat in self .datap])\
                    if len(self)  else -inf
        max_other = max([max(dat) for dat in other.datap])\
                    if len(other) else -inf
        return max_self <= max_other
    
    def __eq__(self,other):
        
        """
        "Equal". Return ``True`` if all attributes of ``self`` (except
        for ``self.__n``) are equal to all attributes of ``other``.
        """
        
        if self.__times != other.__times:
            return False
        if self.__datap != other.__datap:
            return False
        if self.obser != other.obser:
            return False
        if self.files != other.files:
            return False
        if self.title != other.title:
            return False
        if self.xaxis != other.xaxis:
            return False
        if self.yaxis != other.yaxis:
            return False
        return True
    
    def __ne__(self,other):
        
        """
        "Not equal". Return the opposite of ``self.__eq__(other)``.
        """
        
        return not self.__eq__(other)
    
    def __ge__(self,other):
        
        """
        "Greater than or equal to". Return ``True`` if the minimum data
        point of ``self`` is greater than or equal to the minimum data
        point of ``other`, ``False`` otherwise.
        """
        
        min_self  = min([min(dat) for dat in self .datap])\
                    if len(self)  else +inf
        min_other = min([min(dat) for dat in other.datap])\
                    if len(other) else +inf
        return min_self >= min_other
    
    def __gt__(self,other):
        
        """
        "Greater than". Return ``True`` if the minimum data point of
        ``self`` is greater than or equal to the minimum data point of
        ``other`, ``False`` otherwise.
        """
        
        min_self  = min([min(dat) for dat in self .datap])\
                    if len(self)  else +inf
        min_other = min([min(dat) for dat in other.datap])\
                    if len(other) else +inf
        return min_self > min_other
    
    ###########################################################################
    ######################## LENGTH, ITERATOR & GETITEM #######################
    ###########################################################################
    
    def __len__(self):
        
        """
        Return length of ``self``.
        """
        
        return len(self.__datap)
    
    def __getitem__(self,key):
        
        """
        Return the columns of ``self`` matched by ``key``.
        """
        
        # faster version
        if type(key) in INTEGER:
            result = xvg(memory=self.memory)
            key+= 0 if key>=0 else len(self)
            if not 0<=key<len(self):
                return result
            result.times = [t for t,d in zip(self.__times,self.__datap[key])\
                                      if not isnan(d)]
            result.datap =[[d for d in self.__datap[key] if not isnan(d)]]
            result.obser = [self.obser[key]]
            result.files = [self.files[key]]
            result.title = [self.title[key]]
            result.xaxis = [self.xaxis[key]]
            result.yaxis = [self.yaxis[key]]
            return result
        
        return self.select(key,nan=True,nest=True,verbose=False)
    
    def __setitem__(self,key,other):
        
        """
        Insert the ``SimSet.xvg`` object ``other`` at the columns
        of ``self`` matched by ``key``.
        """
        
        # selected columns
        temp = self[n]
        
        for i in range(min(len(temp),len(other))):
            pass
    
    def __delitem__(self,key):
        
        """
        Alias for ``self.delete(key,nan=False,verbose=False)``.
        """
        
        self.delete(key,nan=False,verbose=False)
    
    def __contains__(self,other):
        
        """
        Return ``True`` if the columns of the ``SimSet.xvg`` object
        ``other`` are a subset of the columns of ``self``. No ordering
        is required.
        """
        
        # retrieve lengths
        H = len(self)
        K = len(other)
        
        # "other" has no columns
        if not K:
            return True
        
        # "other" has more columns than "self"
        if K>H:
            return False
        
        # "self" and "other" are the same
        if self==other:
            return True
        
        # auxiliary function: times and datap equality (no NaNs)
        def equal_times_datap(i,j):
            n,m = 0,0 # indexes of "self" and "obser"'s times
            while n < len(self.times) or m < len(other.times):
                timA = self .times[n]
                timB = other.times[m]
                datA = self. datap[i][n]
                datB = other.datap[j][m]
                while isnan(datA):
                    n+=1
                    if n >= len(self .times):
                        datA = None
                        break
                    datA = self. datap[i][n]
                while isnan(datB):
                    m+=1
                    if m >= len(other.times):
                        datB = None
                        break
                    datB = other.datap[j][m]
                if datA != datB or timA != timB:
                    return False
                n+= 1
                m+= 1
            return True
        
        # auxiliary function: obser equality
        def equal_obser(i,j):
            return self.obser[i] == other.obser[j]
        
        # auxiliary function: files equality
        def equal_files(i,j):
            return self.files[i] == other.files[j]
        
        # auxiliary function: title equality
        def equal_title(i,j):
            return self.title[i] == other.title[j]
        
        # auxiliary function: xaxis equality
        def equal_xaxis(i,j):
            return self.xaxis[i] == other.xaxis[j]
        
        # auxiliary function: yaxis equality
        def equal_yaxis(i,j):
            return self.yaxis[i] == other.yaxis[j]
        
        # already spot columns are included in "indexes" list
        indexes = []
        
        # iterate through "other"'s columns
        for j in range(K):
            result = False
            for i in range(H):
                if i in indexes:
                    continue
                if equal_obser(i,j) and equal_files(i,j) and\
                   equal_title(i,j) and equal_xaxis(i,j) and\
                   equal_yaxis(i,j) and equal_times_datap(i,j):
                    result = True
                    indexes.append(i)
                    break
            if not result:
                return False
        return True
    
    def __missing__(self,key):
        
        """
        Return the opposite of ``self.__contains__(key)``.
        """
        
        return not self.__contains__(key)
    
    def __iter__(self):
        
        """
        Initialize ``self`` as an iterator.
        """
        
        self.__n = 0
        return self
    
    def __next__(self):
        
        """
        Return next istance (data column) of ``self``.
        """
        
        if self.__n < len(self):
            result = self[self.__n]
            self.__n+= 1
            return result
        else:
            raise StopIteration
    
    def depth(self):
        
        """
        Return the number of time points fond in ``self``.
        """
        
        return len(self.__times)
    
    def size(self):
        
        """
        Return the number of data points fond in ``self``.
        """
        
        M = sum([sum([not isnan(d) for d in dat]) for dat in self.__datap])
        if not M:
            return 0
        return M
    
    ###########################################################################
    ################################### COPY ##################################
    ###########################################################################
    
    def copy(self,memory=None):
        
        """
        Return a deep copy of ``self``. May change the ``memory``
        option.
        
        Parameters
        ----------
        memory : bool, memory option; if ``None``: take
                 ``self.__memory`` (id est ``self``'s memory option);
                 default is ``None``
        """
        
        if memory is None: memory = self.__memory
        x = xvg(memory=memory)
        x.__obser = self.__obser.copy()
        x.__files = self.__files.copy()
        x.__title = self.__title.copy()
        x.__xaxis = self.__xaxis.copy()
        x.__yaxis = self.__yaxis.copy()
        if self.memory is memory:
            x.__times = self.__times.copy()
            x.__datap = []
            
            # monitor progress - uncomment to avoid progress bar
            # """
            cursor.hide()
            current = 0
            n,t0,etr,speed = len(self),now(),None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            if n and not memory: bar = progress_bar(0,n,0,etr,speed,'',width)
            # """
            
            for dat in self.__datap:
                x.__datap.append(dat.copy())
                
                # monitor progress - uncomment to avoid progress bar
                # """
                if memory: continue
                current+= 1
                elapsed = now()-t0
                speed = current/elapsed
                etr = (n-current)/speed
                bar = progress_bar(current,n,elapsed,etr,speed,'',width)
                print(f'{bar}\r',end='')
            cursor.show()
            if n and not memory: print()
            # """
        
        elif memory:
            x.__times =  list(self.__times.array())
            x.__datap = [list(dat.array()) for dat in self.__datap]
        else:
            x.__times =  binary('',self.__times)
            x.__datap = []
            
            # monitor progress - uncomment to avoid progress bar
            # """
            cursor.hide()
            current = 0
            n,t0,etr,speed = len(self),now(),None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(0,n,0,etr,speed,'',width)
            # """
            
            for dat in self.__datap:
                x.__datap.append(binary('',dat))
                
                # monitor progress - uncomment to avoid progress bar
                # """
                current+= 1
                elapsed = now()-t0
                speed = current/elapsed
                etr = (n-current)/speed
                bar = progress_bar(current,n,elapsed,etr,speed,'',width)
                print(f'{bar}\r',end='')
            cursor.show()
            print()
            # """
        
        return x
    
    ###########################################################################
    ################################ PRUNE NANS ###############################
    ###########################################################################
    
    def prune(self,nan=True):
        
        """
        Delete times with only NaNs left. Return ``self``.
        
        Parameters
        ----------
        nan : bool, if ``False``: delete a time step if a column has
              a NaN; if ``True``: delete a time step if ALL columns
              have a NaN.
        """
        
        # cache computation
        times = self.__times.copy()
        datap = [dat.copy() for dat in self.__datap]
        H = len(datap)
        N = len(times)
        self.times = []
        self.datap = [[] for h in range(H)]
        
        # keep ok lines
        for i in range(N):
            if nan:
                if not sum([not isnan(dat[i]) for dat in datap]):
                    continue
            elif sum([isnan(dat[i]) for dat in datap]):
                continue
            self.__times.append(times[i])
            for h in range(H):
                self.__datap[h].append(datap[h][i])
        return self
    
    ###########################################################################
    ############################## PACK COLUMNS ##############################
    ###########################################################################
    
    def join(self,memory=None):
        
        """
        Merge ``self``'s columns with the same ``obser``,
        ``files``, ``title``, ``xaxis``, and ``yaxis`` specifications
        (duplicates), so to obtain a more compact version of ``self``.
        Return ``self``.
        
        Parameters
        ----------
        memory : bool, memory option; if ``None``: take ``self.memory``
                 default is ``None``
        
        Notes
        -----
        If there is a conflict, i.e. two columns have a data point at
        the same time point, the column with a lower index wins.
        """
        
        # initialization
        N = len(self.__times)
        duplicates = {} # keys: column index, values: list of duplicated ids
        
        # fill "duplicates" - run through columns
        for k in range(len(self)):
            found = False
            obs = self.obser[k]
            fil = self.files[k]
            tit = self.title[k]
            xax = self.xaxis[k]
            yax = self.yaxis[k]
            for h in duplicates:
                if obs != self.obser[h] or fil != self.files[h]\
                or tit != self.title[h] or xax != self.xaxis[h]\
                or yax != self.yaxis[h]: continue
                found = True
                duplicates[h].append(k)
                break
            
            # k is not a duplicate
            if not found:
                duplicates[k] = []
        
        # initialize output
        if not memory: memory = self.memory
        result = xvg(memory=memory)
        result.datap = [[] for k in range(len(self)-len(ls(duplicates)))]
        result.set_times(self.__times.copy())
        
        # merge data
        for i in range(N):
            for k,h in enumerate(duplicates):
                for c in [h]+duplicates[h]:
                    if not isnan(self.__datap[c][i]):
                        break
                result.get_datap()[k].append(self.__datap[c][i])
        
        # attributes
        for h in duplicates:
            result.obser.append(self.obser[h])
            result.files.append(self.files[h])
            result.title.append(self.title[h])
            result.xaxis.append(self.xaxis[h])
            result.yaxis.append(self.yaxis[h])
        
        # delete merged columns
        return result
    
    def pack(self):
        
        """
        Merge in-place ``self``'s columns with the same ``obser``,
        ``files``, ``title``, ``xaxis``, and ``yaxis`` specifications
        (duplicates), so to obtain a more compact version of ``self``.
        Return ``self``.
        
        Notes
        -----
        If there is a conflict, i.e. two columns have a data point at
        the same time point, the column with a lower index wins.
        """
        
        # initialization
        N = len(self.__times)
        duplicates = {} # keys: column index, values: list of duplicated ids
        
        # fill "duplicates" - run through columns
        for k in range(len(self)):
            found = False
            obs = self.obser[k]
            fil = self.files[k]
            tit = self.title[k]
            xax = self.xaxis[k]
            yax = self.yaxis[k]
            for h in duplicates:
                if obs != self.obser[h] or fil != self.files[h]\
                or tit != self.title[h] or xax != self.xaxis[h]\
                or yax != self.yaxis[h]: continue
                found = True
                duplicates[h].append(k)
                break
            
            # k is not a duplicate
            if not found:
                duplicates[k] = []
        
        # merge data
        for i in range(N):
            for h in duplicates:
                for k in [h]+duplicates[h]:
                    if not isnan(self.__datap[k][i]):
                        break
                self.__datap[h][i] = self.__datap[k][i]
        
        # delete merged columns
        self.delete(duplicates.values(),verbose=False)
        return self
    
    ###########################################################################
    ########################## LOAD OBJECT/FILE/ARRAY #########################
    ###########################################################################
    
    def merge(self,*xlist,**args):
        
        """
        Merge ``self`` with either a list of ``SimSet.xvg`` objects /
        xvg files, or raw datasets presented as array-like of times
        and data points.
        
        Parameters
        ----------
        xlist: tuple of ``SimSet.xvg`` objects / str (xvg filenames) or
               tuple of array-like of int/floats; when xvg filenames:
               regexp special characters "*" and "?" are supported; when
               array-like of int/floats: the first array represents the
               time points, and the others represent the data points
        args: dict, optional arguments
          'file'  : str or array-like of str, alias for below, lower
                    priority
          'files' : str or array-like of str, filename(s) you want to
                    link to each observable; ideally, they are the files
                    the data come from, but the could be also non-
                    existing; if ``None``: take filename the data
                    actually come from (just in case a xvg file is
                    loaded, otherwise take ``""``); will adjust length
                    to match total number of data columns loaded;
                    default is ``[None]``
          'obs'   : str or array-like of str, alias for below, lower
                    priority
          'obser' : str or array-like of str, alias for below, lower
                    priority
          'observable' : str or array-like of str, alias for below,
                         lower priority
          'observables': str or array-like of str, alias for below,
                         lower priority
          'label' : str or array-like of str, alias for below,
                    lower priority
          'labels': str or array-like of str, observable name(s) you
                    want to link to each observable; ideally, they are
                    the files the data come from, but the could be also
                    non-existing; if ``None``: take observable found
                    where the data actually come from (if no name found
                    take ``f"obser{i}"``); will adjust length to match
                    total number of columns loaded; default is
                    ``[None]``
          'title' : str or array-like of str, alias for below,
                    lower priority
          'titles': str or array-like of str, xvg file title you
                    want to link to each observable; ideally, they are
                    the files the data come from, but the could be also
                    non-existing; if ``None``: take title found
                    where the data actually come from (if no name found
                    take ``""``); will adjust length to match total
                    number of data columns loaded; default is ``[None]``
          'xaxis' : str or array-like of str, xvg file x-axis label you
                    want to link to each observable; ideally, they are
                    the files the data come from, but the could be also
                    non-existing; if ``None``: take x-axis label found
                    where the data actually come from (if no name found
                    take ``""``); will adjust length to match total
                    number of data columns loaded; default is ``[None]``
          'axis'  : str or array-like of str, alias for below, lower
                    priority
          'yaxis' : str or array-like of str, xvg file y-axis label you
                    want to link to each observable; ideally, they are
                    the files the data come from, but the could be also
                    non-existing; if ``None``: take y-axis label found
                    where the data actually come from (if no name found
                    take ``""``); will adjust length to match total
                    number of data columns loaded; default is ``[None]``
          'start' : int/float or array-like of int/float; if int: for
                    each observable merged: first datapoint to be
                    considered (NaNs included); if negative: start
                    counting from last point; if float: for each 
                    observable merged: first time point to be
                    considered; will adjust length to match number of
                    objects / files / raw datasets loaded; default
                    is ``[0]``
          'stop'  : int/float or array-like of int/float; if int: for
                    each observable merged: last datapoint to be
                    considered (NaNs included); if negative: start
                    counting from last point; if float: for each 
                    observable merged: last time point to be
                    considered; will adjust length to match number of
                    objects / files / raw datasets loaded; default
                    is ``[-1]``
          'step'  : int/float or array-like of int/float; if int: for
                    each observable merged: interval between data points
                    frames (NaNs included); if float: minimum time
                    interval between time points; will adjust length to
                    match number of objects / files / raw datasets
                    loaded; default is ``[1]``
          'frame' : int/float or array-like of int/float or array-like
                    of array-like of int/floats, alias for below, lower
                    priority
          'frames': int or array-like of int/float or array-like of
                    array-like of int/floats, if int: the time points
                    indexes to be picked for each selected column; if
                    float: pick the indexes corresponding to the time
                    points in ``frames`` (when available); if not
                    specified: take all frames according to ``start``,
                    ``stop``, and ``step`` options; if specified:
                    higher priority than ``start``, ``stop``, and
                    ``step``; will adjust length to match number of
                    objects / files / raw datasets; default is ``[[]]``
          'sep'   : str, when loading a xvg filename: take ``sep`` as
                    columns separator
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        n : int, number of successfully added columns of observables
        
        Notes
        -----
        Objects, xvg files, and raw datasets are merged in the order
        they are found in ``xlist``.
        
        If loading objects or xvg files, data have to be rigorously
        provided IN ORDER.
        
        When merging ``SimSet.xvg`` objects, the "memory" option of
        those object shall be CONVERTED to that of ``self``.
        
        While ``start``, ``stop``, ``step`` will match the number of
        objects / files / raw datasets loaded, ``titles``, ``labels``,
        ``xaxis``, ``yaxis`` will match the total number of data
        columns loaded, so if you provide objects / files / raw datasets
        with MORE than a observable their final lengths will differ.
        
        If you want to directly provide data points inside ``xlist``,
        the first array-like of a series ALWAYS refer to time points,
        while observables are from the second on; you can also specify
        objects / filenames before-after such a sequence, but if you
        want to add an OTHER sequence of data points, you must put
        an array-like of time points AGAIN before providing other
        observables.
        
        The core of the function is found in ``SimSet.xvg_merge``.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg()
        >>>
        >>> # import all files matching "folding*.xvg" regexp
        >>> x.merge('folding*.xvg')
        >>>
        >>> # merge "unfolding*.xvg" files
        >>> y = SimSet.xvg('unfolding*')
        >>> x.merge(y)
        >>>
        >>> # merge `(t,d)` dataset
        >>> t,d = array([i for i in range(100)]),array([1.]*100)
        >>> x.merge(t,d,label='Volume')
        """
        
        return _merge(self,*xlist,**args)
    
    ###########################################################################
    ################################# SELECT ##################################
    ###########################################################################
    
    def select(self,*columns,**args):
        
        """
        Select a subset of ``self`` according to ``columns`` and
        ``args`` optional arguments.

        Parameters
        ----------
        columns: tuple of either observable names or column indeces
                 (counting from zero; if negative: start counting from
                 last column); if str: regex special characters "*" and
                 "?" are supported; if empty: take all columns,
                 compatibly with optional arguments
        args: dict of optional arguments
          'origin'      : str or array-like of str, alias for below,
                          lower priority
          'match_origin': str or array-like of str, text that has to be
                          found in a label before ``SimSet.XVG_DIV`` to
                          select that column; regex special characters
                          "*" and "?" are supported; default is ``[]``
                          which means: don't take into account
          'obs'         : str or array-like of str, alias for below,
                          lower priority
          'obser'       : str or array-like of str, alias for below,
                          lower priority
          'observable'  : str or array-like of str, alias for below,
                          lower priority
          'observables' : str or array-like of str, alias for below,
                          lower priority
          'match_obser' : str or array-like of str, text that has to be
                          found in a label after ``SimSet.XVG_DIV`` to
                          select that column; regex special characters
                          "*" and "?" are supported; default is ``[]``
                          which means: don't take into account
          'file'        : str or array-like of str, alias for below,
                          lower priority
          'files'       : str or array-like of str, alias for below,
                          lower priority
          'match_files' : str or array-like of str, text that has to be
                          found in ``self.files`` to select the column
                          corresponding to that position; regex special
                          characters "*" and "?" are supported; default
                          is ``[]`` which means: don't take into account
          'title'       : str or array-like of str, alias for below,
                          lower priority
          'titles'      : str or array-like of str, alias for below,
                          lower priority
          'match_title' : str or array-like of str, text that has to be
                          found in ``self.title`` to select the column
                          corresponding to that position; regex special
                          characters "*" and "?" are supported; default
                          is ``[]`` which means: don't take into account
          'xaxis'       : str or array-like of str, alias for below,
                          lower priority
          'match_xaxis' : str or array-like of str, text that has to be
                          found in ``self.xaxis`` to select the column
                          corresponding to that position; regex special
                          characters "*" and "?" are supported; default
                          is ``[]`` which means: don't take into account
          'axis'        : str or array-like of str, alias for below,
                          lower priority
          'yaxis'       : str or array-like of str, alias for below,
                          lower priority
          'match_axis'  : str or array-like of str, alias for below,
                          lower priority
          'match_yaxis' : str or array-like of str, text that has to be
                          found in ``self.yaxis`` to select the column
                          corresponding to that position; regex special
                          characters "*" and "?" are supported; default
                          is ``[]`` which means: don't take into account
          'lab'         : str or array-like of str, alias for below,
                          lower priority
          'label'       : str or array-like of str, alias for below,
                          lower priority
          'labels'      : str or array-like of str, figure lines labels;
                          if ``None``: guess labels from selected
                          columns; default is ``[None]``
          'start'       : int/float or array-like of int/float; if int:
                          for each observable selected: first datapoint
                          to be considered (NaNs included); if negative:
                          start counting from last point; if float: for
                          each observable selected: first time point to
                          be considered; will adjust length to match
                          number of selected columns; default is ``[0]``
          'stop'        : int/float or array-like of int/float; if int:
                          for each observable selected: last datapoint
                          to be considered (NaNs included); if negative:
                          start counting from last point; if float: for
                          each observable selected: first time point to
                          be considered; will adjust length to match
                          number of selected columns; default is
                          ``[-1]``
          'step'        : int/float or array-like of int/float; if int:
                          for each observable selected: interval between
                          data points frames (NaNs included); if float:
                          for each observable selected: minimum time
                          interval between frames; will adjust length to
                          match number of selected columns; default is
                          ``[1]``
          'frame'       : int or array-like of int/float or array-like
                          of array-like of int/floats, alias for below,
                          lower priority
          'frames'      : int or array-like of int/float or array-like
                          of array-like of int/floats, if int: the
                          time points indexes to be picked for each
                          selected column; if float: pick the indexes
                          corresponding to the time points in
                          ``frames`` (when available); if not specified:
                          take all frames according to ``start``,
                          ``stop``, and ``step`` options; if specified:
                          higher priority than ``start``, ``stop``, and
                          ``step``; will adjust length to match number
                          of selected columns; default is ``[[]]``
          'nan'         : bool, if ``False`` exclude times that have at
                          least a column with a ``NaN`` as datapoint;
                          default is ``SimSet.DEF_NAN``
          'nest'        : bool, put info about original file in columns
                          labels, if there are ambiguities (repreated
                          observables); defalt is ``SimSet.NEST``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        result : ``SimSet.xvg`` object
        
        Notes
        -----
        Order of matched ``columns`` is retained. Special characters
        "*" and "?" are supported for all string-like fields.
        
        Even if ``nan`` optional argument is set to ``True``, times for
        which ALL the columns have ``NaN`` as datapoint will be
        excluded.
        
        This method is called also by ``SimSet.xvg_read``,
        ``SimSet.xvg_write``, and ``SimSet.xvg_plot``. The script is
        similar to that found in ``SimSet.xvg_delete``.
        
        The core of the function is found in ``SimSet.xvg_select``.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.xvg.read  : method
        SimSet.xvg.write : method
        SimSet.xvg.plot  : method
        SimSet.xvg.delete: method
        SimSet.ls : function
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*')
        >>>
        >>> # select observables coming from *foldingHB*.xvg files
        >>> y = x.select(files='*foldingHB*')
        >>>
        >>> # select even columns, odd frames
        >>> z = x.select(range(0,len(x.datap),2),start=1,step=2)
        >>>
        >>> # select ALL "RMSD"-like observables
        >>> v = x.select(obs='*RMSD*')
        >>>
        >>> # select columns with observables names longer than 4 chars
        >>> w = x.select('????*')
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _select(self,xvg(memory=memory),*columns,**args)
    
    ###########################################################################
    ################################## DELETE #################################
    ###########################################################################
    
    def delete(self,*columns,**args):
        
        """
        Delete a subset of ``self`` according to ``columns`` and
        ``args`` optional arguments.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces
                 (counting from zero; if negative: start counting from
                 last column); if str: regex special characters "*" and
                 "?" are supported; if empty: take all columns,
                 compatibly with optional arguments
        args: dict of optional arguments; see ``SimSet.xvg.select`` for
              further details; the following argument has a DIFFERENT
              meaning than ``SimSet.xvg.select``
          'nan' : bool, if ``True``: keep columns even though they
                  don't have a single data points (either no times left
                  in xvg or all NaNs; default is ``SimSet.DEF_NAN``
        
        Returns
        -------
        K : int, number of deleted columns
        
        Notes
        -----
        The method is executed *IN PLACE*.
        The script is similar to that found in ``SimSet.xvg.select``.
        
        Time points with no data are automatically deleted to save
        space.
        
        ``nest`` and ``labels`` are arguments present in
        ``SimSet.xvg.select`` but they are USELESS here.
        
        ``start``, ``stop``, and ``step`` arguments specify which
        datapoints of a column have to be deleted; default options
        result in the deletion of the whole column. ``frames``
        specifies which datapoints of a column have to be deleted,
        with an higher priority than ``start``, ``stop``, and ``step``.
        
        The core of the function is found in ``SimSet.xvg_delete``.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.xvg.select : method
        SimSet.ls : function
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*')
        >>>
        >>> # delete observables coming from *foldingHB*.xvg files
        >>> x.delete(files='*foldingHB*')
        >>>
        >>> # delete odd frames in the odd columns left
        >>> x.delete(range(1,len(x.datap),2),start=1,step=2)
        >>>
        >>> # delete ALL "RMSD"-like observables
        >>> x.delete(obs='*RMSD*')
        """
        
        return _delete(self,*columns,**args)
    
    ###########################################################################
    ################################### READ ##################################
    ###########################################################################
    
    def read(self,*columns,**args):
        
        """
        Return a ``(TIME,DATA1,DATA2,etc...)`` tuple according to
        ``columns`` and ``args`` optional arguments.
        
        Parameters
        ----------
        columns: tuple of either observable names or (starting from 0)
                 column indeces; if empty: take all columns, compatibly
                 with optional arguments
        args: dict of optional arguments; see ``SimSet.xvg.select`` for
              details
        
        Returns
        -------
        result : ``(TIME,DATA1,DATA2,etc...)`` tuple of lists of floats
        
        Notes
        -----
        All string matching is casefold matching. ``columns`` order is
        retained.
        
        Even if ``nan`` optional argument is set to ``True``, times for
        which ALL the columns have ``NaN`` as datapoint will be
        excluded.
        
        This method calls ``SimSet.xvg_select``.
        The core of the function is found in ``SimSet.xvg_read``.
        
        See Also
        --------
        SimSet.xvg.select : method
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding',match='*')
        >>>
        >>> # read observables coming from *foldingHB*.xvg files
        >>> data1 = x.read(files='foldingHB',match='*')
        >>>
        >>> # read even columns, odd frames
        >>> data2 = x.read(range(0,len(x.datap),2),start=1,step=2)
        >>>
        >>> # read RMSD observables (casefold matching)
        >>> data3 = x.read(obs='RMSD',match='*')
        """
        
        x = self.select(*columns,**args)
        return tuple([x.times]+x.datap)
    
    ###########################################################################
    ################################## WRITE ##################################
    ###########################################################################
    
    def write(self,output,*columns,**args):
        
        """
        Write xvg object to ``output`` file, which is a subset of
        ``self`` if ``columns`` and ``args`` optional arguments are
        provided.
        
        Parameters
        ----------
        output : str, xvg output filename, if no extension is provided:
                 assign custom "xvg" extension
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'title'  : str, xvg file title
          'xaxis'  : str, x-axis label
          'axis'   : str, alias for below, lower priority
          'yaxis'  : str, y-axis label
        ``args`` are then passed to ``self.select`` method, WITHOUT the
        ``title`` options, whose "select" meaning is OVERWRITTEN by the
        above specifications
        
        Returns
        -------
        K : number of successfully written columns
        
        Notes
        -----
        All string matching is casefold matching. ``columns`` order is
        retained.
        
        ``nest`` optional argument is set to ``True`` by default.
        
        Even if ``nan`` optional argument is set to ``True``, times for
        which ALL the columns have ``NaN`` as datapoint will be
        excluded.
        
        This method calls ``SimSet.xvg_select``.
        The core of the function is found in ``SimSet.xvg_write``.
        
        See Also
        --------
        SimSet.xvg.select : method
        SimSet.ls : function
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg()
        >>> x.merge('folding',match='*')
        >>>
        >>> # write observables coming from *foldingHB*.xvg to file
        >>> x.write('out1.xvg',files='foldingHB',match='*')
        >>>
        >>> # write even columns, odd frames to file
        >>> x.write('out2.xvg',range(0,len(x.datap),2),start=1,step=2)
        >>>
        >>> # write RMSD observables (casefold matching)
        >>> # ...don't care about ambiguities
        >>> x.write('out3.xvg',obs='RMSD',match='*',nest=False)
        """
        return _write(self,output,*columns,**args)
    
    ###########################################################################
    ################################# STATISTICS ##############################
    ###########################################################################
    
    def min(self,*columns,**args):
        
        """
        Return the lowest data points found in ``self`` as a
        ``SimSet.xvg`` object, according to the options.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'fetch' : bool: if ``False``: compute the results columns-
                    wise; if ``True``: return the min result for
                    each time frame of the result
          'span'  : int or float or ``None``, or array-like of int or
                    float or None; will adjust its length to match the
                    number of selected columns; for each element of
                    ``span``: if integer ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    frames units); if float ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    time units); if ``<=0``: compute a result for
                    each time frame; otherwise: compute a single result
                    for all time frames; default is ``[None]``
          'memory': bool, "memory" attribute of the output xvg; if
                    not specified: take ``self.memory`` as default
        ``args`` are then passed to ``self.select`` method, with
        ``nan`` option set to ``True``.
        
        Returns
        -------
        result : ``SimSet.xvg`` object, with attributes derived from
                 ``self``, min values as datapoints, and the centers
                 of the "span" regions where those min values were
                 computed as time frames
        
        Notes
        -----
        The core of the function is found in ``SimSet.xvg_stats``.
        When ``fetch==True``: the result is NOT equal to the min of
        all the columns' data points collected together.
        
        The output ``SimSet.xvg`` object has the same ``memory``
        attribute as ``self``.
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*') # loads "folding*.xvg" files
        >>>
        >>> # search min values frames-wise, 
        >>> x.min(span=0)
        >>>
        >>> # search min values columns-wise, in 100 ps-wide regions
        >>> x.min(fetch=False,span=100.).plot() # plot results
        >>>
        >>> # search min value of all data, from 100 ps on
        >>> x.min(start=100)
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _min(self,xvg(memory=memory),*columns,**args)
    
    def max(self,*columns,**args):
        
        """
        Return the highest data points found in ``self`` as a
        ``SimSet.xvg`` object, according to the options.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'fetch' : bool: if ``False``: compute the results columns-
                    wise; if ``True``: return the max result for
                    each time frame of the result
          'span'  : int or float or ``None``, or array-like of int or
                    float or None; will adjust its length to match the
                    number of selected columns; for each element of
                    ``span``: if integer ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    frames units); if float ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    time units); if ``<=0``: compute a result for
                    each time frame; otherwise: compute a single result
                    for all time frames; default is ``[None]``
          'memory': bool, "memory" attribute of the output xvg; if
                    not specified: take ``self.memory`` as default
        ``args`` are then passed to ``self.select`` method, with
        ``nan`` option set to ``True``.
        
        Returns
        -------
        result : ``SimSet.xvg`` object, with attributes derived from
                 ``self``, max values as datapoints, and the centers
                 of the "span" regions where those max values were
                 computed as time frames
        
        Notes
        -----
        The core of the function is found in ``SimSet.xvg_stats``.
        
        The output ``SimSet.xvg`` object has the same ``memory``
        attribute as ``self``.
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*') # loads "folding*.xvg" files
        >>>
        >>> # search max values frames-wise, 
        >>> x.max(span=0.)
        >>>
        >>> # search max values columns-wise, in 100 ps-wide regions
        >>> x.min(fetch=False,span=100.).plot() # plot results
        >>>
        >>> # search max value of all data, from 100 ps on
        >>> x.max(start=100)
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _max(self,xvg(memory=memory),*columns,**args)
    
    def mean(self,*columns,**args):
        
        """
        Return the mean of the data points found in ``self`` as a
        ``SimSet.xvg`` object, according to the options.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'fetch' : bool: if ``False``: compute the results columns-
                    wise; if ``True``: return the MEAN result for
                    each time frame of the result
          'span'  : int or float or ``None``, or array-like of int or
                    float or None; will adjust its length to match the
                    number of selected columns; for each element of
                    ``span``: if integer ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    frames units); if float ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    time units); if ``<=0``: compute a result for
                    each time frame; otherwise: compute a single result
                    for all time frames; default is ``[None]``
          'memory': bool, "memory" attribute of the output xvg; if
                    not specified: take ``self.memory`` as default
        ``args`` are then passed to ``self.select`` method, with
        ``nan`` option set to ``True``.
        
        Returns
        -------
        result : ``SimSet.xvg`` object, with attributes derived from
                 ``self``, mean values as datapoints, and the centers
                 of the "span" regions where those mean values were
                 computed as time frames
        
        Notes
        -----
        The core of the function is found in ``SimSet.xvg_stats``.
        When ``fetch==True``: the result is NOT equal to the mean of
        all the columns' data points collected together.
        
        The output ``SimSet.xvg`` object has the same ``memory``
        attribute as ``self``.
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*') # loads "folding*.xvg" files
        >>>
        >>> # compute mean values frames-wise, 
        >>> x.mean(span=0.)
        >>>
        >>> # compute mean values columns-wise, in 100 ps-wide regions
        >>> x.mean(fetch=False,span=100.).plot() # plot results
        >>>
        >>> # compute the mean of all data, from 100 ps on
        >>> x.mean(start=100)
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _mean(self,xvg(memory=memory),*columns,**args)
    
    def std(self,*columns,**args):
        
        """
        Return the standard deviation of the data points found in
        ``self`` as a ``SimSet.xvg`` object, according to the options.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'fetch' : bool: if ``False``: compute the results columns-
                    wise; if ``True``: return the MEAN result for
                    each time frame of the result
          'span'  : int or float or ``None``, or array-like of int or
                    float or None; will adjust its length to match the
                    number of selected columns; for each element of
                    ``span``: if integer ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    frames units); if float ``>0``: compute floating
                    points results with width equal to ``span`` (in
                    time units); if ``<=0``: compute a result for
                    each time frame; otherwise: compute a single result
                    for all time frames; default is ``[None]``
          'memory': bool, "memory" attribute of the output xvg; if
                    not specified: take ``self.memory`` as default
        ``args`` are then passed to ``self.select`` method, with
        ``nan`` option set to ``True``.
        
        Returns
        -------
        result : ``SimSet.xvg`` object, with attributes derived from
                 ``self``, std values as datapoints, and the centers
                 of the "span" regions where those std values were
                 computed as time frames
        
        Notes
        -----
        The core of the function is found in ``SimSet.xvg_stats``.
        When ``fetch==True``: the result is NOT equal to the std of
        all the columns' data points collected together.
        
        The output ``SimSet.xvg`` object has the same ``memory``
        attribute as ``self``.
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*') # loads "folding*.xvg" files
        >>>
        >>> # compute std values frames-wise, 
        >>> x.std(span=0.)
        >>>
        >>> # compute std values columns-wise, in 100 ps-wide regions
        >>> x.std(fetch=False,span=100.).plot() # plot results
        >>>
        >>> # compute the std of all data, from 100 ps on
        >>> x.std(start=100)
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _std(self,xvg(memory=memory),*columns,**args)
    
    ###########################################################################
    ############################ TIME SERIES ANALYSIS #########################
    ###########################################################################
    
    def derivative(self,*columns,**args):
        
        """
        Take the derivative of a subset of ``self``. Authomatically
        handles non continuous functions.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'smooth' : int or float, if ``>=0``: return floating points
                     results with width equal to ``smooth`` (in frames
                     units if int, in time units if float); otherwise:
                     compute a single result for all time frames;
                     default is ``-1.``
          'memory': bool, "memory" attribute of the output xvg; if
                    not specified: take ``self.memory`` as default
        ``args`` are then passed to ``self.select`` method, with
        ``nan`` option set to ``True``.
        
        Returns
        -------
        result : ``SimSet.xvg`` object, with attributes derived from
                 ``self``, derivatives as datapoints, and the centers
                 of the region where those derivatives were computed
                 as time frames
        
        Notes
        -----
        The core of the function is found in ``SimSet.xvg_analysis``.
        
        The output ``SimSet.xvg`` object has the same ``memory``
        attribute as ``self``.
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding*') # loads "folding*.xvg" files
        >>>
        >>> # take derivative "as it is" 
        >>> x.derivative(span=0.)
        >>>
        >>> # take derivative smoothed in 100 ps - wide regions
        >>> x.std(smooth=100.).plot() # plot results
        """
        
        memory = parse(args,'memory',self.memory)
        try: args.pop('res')
        except: pass
        try: args.pop('columns')
        except: pass
        return _derivative(self,xvg(memory=memory),*columns,**args)
    
    ###########################################################################
    ################################### PLOT ##################################
    ###########################################################################
    
    def plot(self,*columns,**args):
        
        """
        Plot a subset of ``self`` data, selected according to
        ``columns`` and ``args`` optional arguments, and customize
        the figure.
        
        Parameters
        ----------
        columns: tuple of either observable names or column indeces;
                 if empty: take all columns, compatibly with ``args``
                 options; it is passed to ``self.select`` method
        args: dict of optional arguments
          'out'    : str or array-like of str, alias for below, lower
                     priority
          'output' : str or array-like of str, output figure filename
                     or list of output filenames; if no extension is
                     provided take ``SimSet.FIG_EXT``; if ``""`` or
                     ``None``: don't save the figure; default is ``""``
          'title'  : str, figure title; if not specified: guess from
                     plotted lines
          'lab'    : str or array-like of str, alias for below, lower
                     priority
          'label'  : str or array-like of str, alias for below, lower
                     priority
          'labels' : str or array-like of str, labels to be assigned
                     to plotted lines; if ``None``: guess from plotted
                     lines; will adjust length to match the number of
                     plotted lines; default is ``[None]``
          'start'  : int/float or array-like of int/float; if int: for
                     each observable merged: first datapoint to be
                     considered (based on the times of the selected
                     columns);  if negative: start counting from last
                     point; if float: for each observable merged: first
                     time point to be considered; will adjust length to
                     match number of plotted lines; default is ``[0]``
          'stop'   : int/float or array-like of int/float; if int: for
                     each observable merged: last datapoint to be
                     considered (based on the times of the selected
                     columns); if negative: start counting from last
                     point; if float: for each  observable merged: last
                     time point to be considered; will adjust length to
                     match number of plotted lines; default is ``[-1]``
          'step'   : int/float or array-like of int/float; if int: for
                     each observable merged: interval between data
                     points frames (based on the times of the selected
                     columns); if float: minimum time interval between
                     time points; will adjust length to match number of
                     plotted lines; default is ``[1]``
          'frame'  : int/float or array-like of int/float or array-like
                     of array-like of int/floats, alias for below, lower
                     priority
          'frames' : int/float or array-like of int/float or array-like
                     of array-like of int/float, if int: the time points
                     indexes to be picked for each selected column; if
                     float: pick the indexes corresponding to the time
                     points in ``frames`` (based on the times of the
                     selected columns, when the series to be plotted
                     has a data point available in that frame); if not
                     specified: take all frames according to ``start``,
                     ``stop``, and ``step`` options; if specified:
                     higher priority than ``start``, ``stop``, and
                     ``step``; will adjust length to match number of
                     plotted lines; default is ``[[]]``
          'avg'    : float or array-like of floats, alias for below,
                     lower priority
          'average': float or array-like of floats, for each data
                     plotted, draw also the ``average`` time units
                     moving average, if ``average`` at least twice as
                     big as the interval between data points, and at
                     most as small as half of the interval between
                     the first and the last data point; will adjust
                     length to match number of plotted lines; default
                     is ``[0]``
          'time'   : bool, if ``True``: arrange data in a t-x plot
                     (time-series); if ``False``: arrange data in a x-y
                     plot; raise error if it is not possible; default
                     is ``False`` if parameter ``heat <= 0``, ``True``
                     otherwise
          'heat'   : int, alias for below, lower priority
          'heatmap': int, if ``>0``, plot a (discrete) heatmap of data
                     with number of contours equal to ``heatmap``; the
                     contour is based on the log of an estimated
                     gaussian-kernel distribution, cut for values whose
                     absolute value is higher than ``SimSet.HEATCUT``;
                     default is ``0``
          'bw'     : float or array-like of floats, alias for below,
                     lower priority
          'bandw'  : float or array-like of floats, alias for below,
                     lower priority
          'bandwidth' : (when ploatting a heatmap) RELATIVE width of
                     the gaussian kernel; if str: the method used to
                     compute the estimator bandwidth ('scott' or
                     'silverman'); if float: the bandwidth for all
                     the input series's dimensions; if an array-like of
                     floats: will adjust its shape to that of the data's
                     covariance; if square matrix with the same shape of
                     the data'd covariance: set directly; default is
                     ``SimSet.BANDW``
          'w'      : float or array-like of floats, alias for below,
                     lower priority
          'weight' : float or array-like of floats, alias for below,
                     lower priority
          'weights': float or array-like of floats, weights associated
                     to each series in heatmap computation; will adjust
                     length to match the number of plotted series;
                     default is ``[1.]``
          'res'    : int, alias for below, lower priority
          'resolution' : ind, root number of bins in heatmap computation
          'cmap'   : str, color map for the heatmap, default is
                     ``SimSet.COLMAP``
          'xlab'   : str, alias for below, lower priority, x-axis label
          'xlabel' : str, x-axis label; if not specified: guess from
                     plotted data
          'ylab'   : str, alias for below, lower priority
          'ylabel' : str, y-axis label; if not specified: guess from
                     plotted data
          'xlim'   : array-like of float: limits of x-axis; if not
                     specified: let the program guess best option;
                     default is ``[]``
          'ylim'   : array-like of float: limits of y-axis; if not
                     specified: let the program guess best option;
                     default is ``[]``
          'color'  : str or array-like of str, alias for below, lower
                     priority
          'colors' : str or array-like of str, lines colors; will
                     adjust length to match the number of plotted
                     series; if not specified: take default
                     ``matplotlib.plot`` color cycle; default is ``[]``
          'alpha'  : float or array-like of float, alpha factor of
                     lines / countour areas; will adjust length to
                     match the number of plotted series; default is
                     ``[1.]``
          'hide'   : bool, if ``True`` plot just average lines;
                     default is ``False``
          'style'  : str or array-like of str, style of plotted lines;
                     will adjust length to match number of plotted
                     lines; default is ``[SimSet.DEF_STY]``
          'avgstyle' : str or array-like of str, style of average lines;
                     will adjust length to match number of non-average
                     plotted lines; default is ``[SimSet.DEF_AS]``
          'size'   : float or array-like of floats, alias for below,
                     lower priority
          'width'  : float or array-like of floats, alias for below,
                     lower priority
          'linewidth' : float or array-like of floats, width of plotted
                     lines / scatter points; will adjust length to match
                     number of plotted lines; average lines will be 1.5x
                     wider than non-average plotted lines
          'window' : array-like of floats, plot window size in inches;
                     if less than two numbers are provided: take default
                     size; default is ``SimSet.DEF_WIN``
          'font'   : float or array-like of floats, alias for below,
                     lower priority
          'fontsize' : float or array-like of floats, the first element
                       being the title font size, the second one the
                       axes labels / ticks and legend labels size; if
                       just the title font size is provided, the others
                       are assumed to be diminished by 2; if no numbers
                       are provided: take default sizes; default is
                       ``SimSet.DEF_FON``
          'loc'    : int or str, alias for below, lower priority
          'location' : int or str, the plot's legend position; if
                       ``None``: don't print the legend even if
                       recommended; if integer ``>0`` or not-empty-
                       string: always print the legend even if not
                       recommended
          'grid'   : bool, if ``True``: show grid in plot; default is
                     ``SimSet.GRID`` if ``heat <= 0`` and ``new`` is
                     ``True``, ``False`` otherwise
          'new'    : bool, if ``True``: initialize a new figure,
                     otherwise: use the last figure if availables;
                     default is ``True``
          'show'   : bool, if ``True``: show figure (without blocking
                     the shell); default is ``SimSet.DEF_SHW`` or always
                     true if not ``output``
        ``args`` are then passed to ``self.select`` method, WITHOUT the
        ``lab``, ``label``, ``labels``, ``start``, ``stop``, ``step``,
        ``frame``, and ``title`` options, whose meaning to "select" is
        OVERWRITTEN by the above specification
        
        Returns
        -------
        figure : ``matplotlib.figure.Figure`` object
        
        Notes
        -----
        ``columns`` order is retained. ``nan`` is set to ``True`` by
        default.
        
        If ``time`` optional argument is ``False``: x-y copuling is
        guessed from ``columns``, e.g ``[1,2,3,4]`` plots columns
        1 vs 3 and 2 vs 4, same as ``[[1,2],[3,4]]``. ``[1,2,3,4,5]``
        does NOT plot column 5.
        
        Doens't put legend if plotting a heatmap or if there are no
        labels. If ``average`` option is specified when plotting a
        heatmap, AVERAGE points shall be taken for computation
        instead of scatter points.
        
        This method calls ``SimSet.xvg_select`` and ``SimSet.kde``.
        The core of the function is found in ``SimSet.xvg_plot``.
        
        See Also
        --------
        SimSet.xvg.select : method
        SimSet.kde : class
        SimSet.ls : function
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>> x = SimSet.xvg('folding',match='*')
        >>>
        >>> x.plot()       # plot time series of all data
        >>> x.plot(1,2,3)  # plot time series of indeces 1,2,3
        >>> x.plot('Q')    # time series of all "Q" columns
        >>>
        >>> # plot evolution of points 1 vs 4, 2 vs 5, 3 vs 6
        >>> x.plot([1,2,3],[4,5,6],time=False)
        >>>
        >>> # plot also 100 t.u. average lines
        >>> x.plot([1,2,3],[4,5,6],time=False,avg=100)
        >>>
        >>> # plot evolution of all the "Q" points, regardless of
        >>> # the original filename, vs all the "rmsd" points
        >>> x.plot(['Q'],['rmsd'],time=False,match='')
        >>>
        >>> # make it a heatmap with 20 contours
        >>> x.plot(['Q'],['rmsd'],time=False,match='',heat=20)
        >>>
        >>> # graphical customization of time series of column 1
        >>> x.plot(1,avg=100,style='.',avgstyle='-',w=2,color='red')
        """
        
        return _plot(self,xvg(memory=self.__memory),*columns,**args)
