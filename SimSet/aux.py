from SimSet.params import *

###############################################################################
################################# DEPENDENCIES ################################
###############################################################################

# built-in package: os
from os import system as sh
from os import chdir  as cd
from os import remove as rm
from os import get_terminal_size
from os.path import exists
from os.path import isfile
from os.path import relpath
from os.path import abspath

# built-in package: sys
from sys  import platform,stdout

# built-in package: math
from math import frexp

# built-in package: zlib
from zlib import adler32 # replace hash function to something DETERMINISTIC
def hash(x): return adler32(f'{x}'.encode('utf-8'))

# built-in package: subprocess
from subprocess import check_output

# built-in package: itertools
from itertools import combinations
from itertools import product
from itertools import chain

# built-in package: inspect
from inspect import signature

# built-in package: struct
from struct import pack,unpack,calcsize

# built-in package: warnings;        disable warnings
from warnings import filterwarnings; filterwarnings('ignore')

# built-in package: re
import re

# built-in package: time
from time import time as now
from time import sleep

# built-in package: random
from random import random,shuffle

# built-in package: multiprocessing;   number of maximum pool processes
from multiprocessing import Manager,Lock
from multiprocessing import cpu_count; PROCESSES = cpu_count()

# package: pathos (multiprocessing)
from pathos.multiprocessing import ProcessingPool as Pool

# package: numpy
import numpy as np
from numpy import array,ndarray,zeros,diag,newaxis
from numpy import uint8,uint16,uint32,uint64,int8,int16,int32,int64,intp,uintp
from numpy import float32,float64
from numpy import complex64,complex128
from numpy import pi,inf,NaN,isnan
from numpy import exp,log,log10,arccos
from numpy import linalg,unique
from numpy import argsort,lexsort,minimum
from numpy import mgrid,cov
from numpy import fromfile
from numpy import real as realcomp

# package: MDAnalysis
import MDAnalysis as mda
from MDAnalysis                    import Writer
from MDAnalysis.analysis           import align,rms
from MDAnalysis.analysis.base      import AnalysisFromFunction
from MDAnalysis.coordinates.memory import MemoryReader

# package: parmed
from parmed import load_file as pmd_load

# package: matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LogNorm

# package: cursor
import cursor

# activate colors on windows
if platform == 'windows':
    sh('color')
# ansi escape sequences
ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

###############################################################################
################################## DEFINITIONS ################################
###############################################################################

INTEGER = [int,uint8,uint16,uint32,uint64,int8,int16,int32,int64,intp,uintp]
FLOAT   = [float,float32,float64]
COMPLEX = [complex,complex64,complex128]
ITERABLE= [list,tuple,ndarray,range,set,dict,type({}.values()),type({}.keys())]
FUNCTION= [type(lambda : 1),type(fromfile)]

###############################################################################
############################# CLASS: SIMPLE FUNCTION ##########################
###############################################################################

class simple:
    
    """
    Implementation of a "simple function".
    """    
    
    def __init__(self,bins,values):
        
        """
        Initialize ``self``.
        
        Attributes
        ----------
        bins   : array-like of floats, intervals of the simple function
                 must be already sorted and unique, otherwise crop data
        values : array-like of floats, values of each interval; must be
                 ``len(bins)==len(values)-1``, otherwise crop data
        """
        
        try: bins[0]
        except: bins = []
        try: values[0]
        except: values = []
        self.__bins   = bins
        self.__values = values
        self.__l = min(len(bins),len(values)-1)
   
    def __call__(self,x):
        
        """
        Get the value of ``self`` in ``x``.
        
        Parameters
        ----------
        x : float or array-like of floats, input value
        
        Returns
        -------
        y : float or array-like of floats, value of the function; if
            ``x`` is an array-like: return a list of floats
        """
        
        if type(x) in ITERABLE:
            return [self.__call__(x) for x in x]
        i = 0
        while i<self.__l and x >= self.__bins[i]:
            i+= 1
        return self.__values[i]

###############################################################################
############################## BINARY DATA ANALYSIS ###########################
###############################################################################

def binary_ones(x):
    
    """
    Return number of ones in binary representation of ``x``. Recursive.
    """
    
    if x:
        return (x&1) + binary_ones(x>>1)
    return x

def binary_int(x,bits=32):
    
    """
    Return the ``int<bits>`` number corresponding to binary ``x``.
    Two's complement method.
    """
    
    # preprocessing
    x = x % 2**bits
    
    # negative number
    if x>>(bits-1):
        return -(x^((1<<bits)-1))-1
    
    # positive number
    return x

###############################################################################
################################# STRINGS HANDLING ############################
###############################################################################

def scan(text,x,i=0):
    
    """
    Find the index of first occurrence of substring ``x`` in ``text``,
    starting from index ``i``.
    
    Parameters
    ----------
    text : str
    x : str, substring to be found
    i : int, first index to be searched
    
    Returns
    -------
    i : int, index of first occurence
    """
    
    L = len(text)
    l = len(x)
    if i is None: i = 0
    while text[i:i+l] != x:
        i+= 1
        if i > L-l:
            return None
    return i

def format(attribute,length,padding=2):
    
    """
    Format ``attribute`` with ``length`` as number of character,
    ``padding`` as minimum number of spaces included.
    
    Parameters
    ----------
    attribute : whatever object that can be formatted into string
    length    : int, number of string characters; if positive: cut
                extra characters at the end; if negative: cut extra
                characters at the beginning
    padding   : int, minimum number of spaces in the formatted string;
                default is ``2``
    """
    
    # process length
    left   = length > 0   # left align
    length = abs(length)
    length-= abs(padding)
    
    # format integer
    if type(attribute) in INTEGER:
        text = f'{attribute}'
    
    # format float
    elif type(attribute) in FLOAT and -inf < attribute < inf:
        if attribute  >= 0:
            sig_digits = 0
        else:
            sig_digits = 1
            attribute  =-attribute
        if attribute > 1:
            int_digits = int(log10(attribute))+1 # before floating point
        else:
            int_digits = 1
        flo_digits = len(f'{attribute}')-int_digits # floating point & decimals
        if flo_digits:
            pad = min(flo_digits-1,max(length-int_digits-sig_digits-1,0))
        else:
            pad = 0
        attribute = round(attribute,pad)
        text = f'{"-"*sig_digits}{attribute:.{pad}f}'
    
    # format list
    elif type(attribute) in ITERABLE:
        text = [f'{attr}' for attr in attribute]
        L = [len(txt) for txt in text]
        diff = sum(L)+(len(attribute)-1)-length
        
        # try to squeeze
        if diff > 0:
            temp,text = text,[]
            i = 0 # run through temp
            l = length # length left
            for i,txt in enumerate(temp):
                txt = temp[i]
                pad = diff//(len(temp)-i)
                if type(attribute[i]) is str:
                    minpad = 3
                else:
                    minpad = len(txt.split('.')[0])
                if pad > L[i]-minpad:
                    pad = max(L[i]-minpad,0)
                if L[i]-pad > l:
                    break
                if type(attribute[i]) is str:
                    text.append(txt[pad:])
                else:
                    text.append(txt[:L[i]-pad])
                diff-= pad
                l-= L[i]-pad+1
        text = ' '.join(text)
        
    # format whatever
    else:
        text = str(attribute)
    
    # final format
    text   = text[:length] if left else text[-length:]
    length+= padding
    return f'{text:{length}}'

def extension(fname,ext='pdb'):
    
    """
    Find extension of ``fname`` and return a ``(prefix,ext)`` tuple.
    If no exension is found, take default value. Preserve folders.
    
    Parameters
    ----------
    fname : str, filename
    ext   : str, extension; default is ``"pdb"``
    
    Returns
    -------
    (prefix,ext): tuple of str, prefix and extension
    
    Example
    -------
    >>> import SimSet
    >>> SimSet.extension('good.txt')
    ('good','txt')
    >>> SimSet.extension('bad','pdf' )
    ('bad' ,'pdf')
    >>> SimSet.extension('ugly')
    ('ugly','pdb')
    """
    
    if type(fname) is not str:
        return '',ext
    temp = fname.split('/')
    folder = '/'.join(temp[:-1])
    folder = f'{folder}/'*(len(folder)>0)
    fname  = temp[-1]
    temp = fname.split('.')
    if len(temp) > 1:
        return folder+'.'.join(temp[:-1]),temp[-1]
    return f'{folder}{temp[0]}',ext

def sel_name(sel):
    
    """
    Return a more viable name for a selection.
    
    Parameters
    ----------
    sel : str, selection name
    
    Returns
    -------
    result : str, with some long selection names being substituted
             by shorter expressions (e.g. "protein", "nucleic" etc.)
    """
    
    result = sel
    if not result:
        return 'all'
    result = result.replace(DEF_SEL,'default')
    result = result.replace(PROTEIN,'protein')
    result = result.replace(NUCLEIC,'nucleic')
    result = result.replace(LIPID,'lipid')
    result = result.replace(IONS,'ions')
    result = result.replace(WATER,'water')
    return result

def path_name(x,depth=DEPTH):
    
    """
    Reduce explicit folders in ``x`` to a maximum of ``depth``.
    
    Parameters
    ----------
    x : str, full path name
    
    Returns
    -------
    result : str, formatted path name
    """
    
    temp = x.split('/')
    if len(temp) > depth:
        x = '.../'+'/'.join(temp[-depth:])
    return x

###############################################################################
############################## TIME STRINGS HANDLING ##########################
###############################################################################

def tosec(x):
    
    """
    Convert formatted time to (rounded) seconds.
    
    Parameters
    ----------
    x : str, time format 'hh:mm:ss'
    
    Returns
    -------
    result : int, (rounded) number of seconds
    """
    
    tmp = x.split(':')
    tmp.reverse()
    return round(sum([float(x)*60**i for i,x in enumerate(tmp)]))

def time_format(seconds):
    
    """
    Simple seconds time formatting.
    
    Parameters
    ----------
    seconds : int, number of seconds
    
    Returns
    -------
    result : str, time formatted as "mm:ss" or "hh:mm:ss"
    """
    
    seconds = round(seconds)
    hours = seconds//3600
    seconds -= hours*3600
    minutes = seconds//60
    seconds -= minutes*60
    if hours:
        return f'{hours:02}:{minutes:02}:{seconds:02}'
    return f'{minutes:02}:{seconds:02}'

def progress_bar(current,total,
                 elapsed=0,etr=None,speed=None,unit='',width=None):
    
    """
    Return a simple ansi progress bar.
    
    Parameters
    ----------
    current : int or float, current iteration
    total   : int or float, total number of iterations
              (in ``current``'s units)
    elapsed : int or float, elapsed time in seconds; default is ``0``
    etr     : int or float, estimated time remaining in seconds;
              default is ``None``
    speed   : float, current speed iterations/s; default is ``None``
    unit    : str, unit of measure, default is ``""``
    width   : int, bar width (terminal columns), default is ``None``
              which means: take terminal width
    
    Returns
    -------
    bar : str, formatted progress bar
    """
    
    # progress percentage
    perc = current/total
    
    # unit
    if unit:
        unit = f' {unit}'
    
    # options formatting
    percent = f'{min(1,perc)*100:3.0f}%|'
    digit = 1 if total<10 else 2 if total<100 else 3 if total<1000 else\
            4 if total<10000 else 5 if total<100000 else 6
    current = f'| {current:{digit}.0f}/'
    total   = f'{total  :.0f}{unit} '
    elapsed = f'[{time_format(elapsed)}<'
    eta     = f'{time_format(etr)}, ' if etr is not None else '?, '
    if not unit:
        unit = ' it'
    if not speed:
        speed = f'?{unit}/s]'
    elif speed < 1/60:
        speed = f'{speed*3600:.2f}{unit}/h]'
    elif speed < 1:
        speed = f'{speed*60:.2f}{unit}/min]'
    elif speed < 100:
        speed = f'{speed:.2f}{unit}/s]'
    elif speed < 1000:
        speed = f'{speed:.1f}{unit}/s]'
    else:
        speed = f'{speed:.0f}{unit}/s]'
    after = f'{current}{total}{elapsed}{eta}{speed}'
    
    # width
    width = width if width else get_terminal_size()[0]
    width = max(1,width-len(percent)-len(after))
    
    # format bar
    n = int(perc*width)
    if n < width:
        return f'{percent}{"#"*n}{int((perc*width-n)*10)}{" "*(width-n-1)}'\
               f'{after}'
    return f'{percent}{"#"*width}{after}'

###############################################################################
############################## ANSI STRINGS HANDLING ##########################
###############################################################################

def split_ansi(text):
    
    """
    Take input string, split in substrings with ansi escape codes
    alternating with normal text.
    [text_1, escape_1, ..., escape_(n-1), text_n]
    """
    
    text = text.split('\u001b')
    result = [text[0]]
    for t in text[1:]:
        i = 0
        for i in range(len(t)):
            if t[i] in ['A','B','C','D','E','F','G','H','J','K','S','T',
                        'f','m','i','n','s','u','h','l']:
                result.append(f'\u001b{t[:i+1]}')
                result.append(t[i+1:])
                break
    return result

def ansi(text,*attributes):
    
    """
    Customize text with ansi escape codes.
    """
    
    text = split_ansi(text)
    if len(text) == 1:
        text = text[0]
        for attr in attributes:
            if attr == 'black':
                text = f'\u001b[30m{text}'
            if attr == 'red':
                text = f'\u001b[31m{text}'
            if attr == 'green':
                text = f'\u001b[32m{text}'
            if attr == 'yellow':
                text = f'\u001b[33m{text}'
            if attr == 'blue':
                text = f'\u001b[34m{text}'
            if attr == 'magenta':
                text = f'\u001b[35m{text}'
            if attr == 'cyan':
                text = f'\u001b[36m{text}'
            if attr == 'white':
                text = f'\u001b[37m{text}'
            if attr == 'bold':
                text = f'\u001b[1m{text}'
            if attr == 'italic':
                text = f'\u001b[3m{text}'
            if attr == 'blink':
                text = f'\u001b[5m{text}'
            if attr == 'invert':
                text = f'\u001b[7m{text}'
        return f'{text}\u001b[0m'
    text = [ansi(text[i],*attributes) if i%2==0 else text[i]\
            for i in range(len(text))]
    return ''.join(text)

def no_ansi(text):
    
    """
    Remove ansi escape codes.
    """
    
    text = split_ansi(text)
    return ''.join([t for t in text[0:len(text):2]])

def scan_ansi(text,index):
    
    """
    Return final index in text (aside from ansi escape codes).
    """
    
    text = split_ansi(text)
    L1 = [len(t) for t in text[0:len(text):2]]
    L2 = [len(t) for t in text[1:len(text):2]]
    i = 0 # scan no ansi space
    j = 0 # scan w/ ansi space
    for l1,l2 in zip(L1,L2):
        if i+l1 >= index:
            break
        i+= l1
        j+= l1+l2
    if i < index:
        j+= index-i
    return j

###############################################################################
############################### STRINGS MANIPULATION ##########################
###############################################################################

def split(text,*sep):
    
    """
    Split ``text`` in a list of substrings according to the possible
    separators ``sep``. ``sep`` elements are also included in the
    returned list.
    
    Parameters
    ----------
    text : str, text to be splitted
    sep  : tuple of str, list of separators; empty strings are ignored
    
    Returns
    -------
    result : list of str, substrings of ``text``, such that
             ``text = "".join(result)``
    
    Notes
    -----
    Enhanced version of ``str.split`` method.
    """
    
    if not sep: sep = [' ']
    s = sep[0]
    if s=='': result == [text]
    else:
        temp = text.split(s)
        result = []
        for t in temp[:-1]:
            result.append(t)
            result.append(s)
        result.append(temp[-1])
    if len(sep)>1:
        result = list(chain(*[split(res,*sep[1:]) for res in result]))
    return result

def match(regexp,*names):
    
    """
    Match ``regexp`` expression in a list of ``names``. Special
    characters "*" and "?" are supported.
    
    Parameters
    ----------
    regexp : str, regular expression
    names  : tuple of str, within to search ``regexp``
    
    Returns
    -------
    result : list of str, subset of ``names`` that match ``regexp``
    """
    
    # regexp pre-processing
    if not regexp:
        return []
    i = scan(regexp,'*?')
    while i is not None:
        regexp = regexp[:i]+'*'+regexp[i+2:]
        i = scan(regexp,'*?')
    
    # regexp processing
    splitted = [spl for spl in split(regexp,'*','?') if spl]
    nospecial= [spl for spl in splitted if spl not in ['*','?']]
    
    # iterate through names
    result = []
    for name in names:
        matching = True # up to proof of disbelief
        
        # no need to worry
        if len(splitted)==1 and regexp==name:
            result.append(name)
            continue
        
        # find indexes of non-special characters substring
        i,indexes = 0,[]
        for x in nospecial:
            i = scan(name,x,i)
            if i is None:
                matching = False
                break
            indexes.append(i)
            i+= len(x)
        if not matching: continue
        
        # scan special characters
        i = 0 # index of name
        j = 0 # index of indexes
        for spl in splitted:
            if spl == '*':
                if j<len(indexes):
                    i = indexes[j]
                continue
            if spl == '?':
                if j<len(indexes):
                    if indexes[j]-i-1:
                        matching = False
                        break
                elif len(name)==i:
                    matching = False
                    break
                i+= 1
                continue
            if i != indexes[j]:
                matching = False
                break
            i+= len(spl)
            j+= 1
        if not matching: continue
        
        # special cases with last element of splitted
        if spl != '*' and len(name)-i:
            continue # orphan characters left to match
        
        # add matching name
        result.append(name)
    return result

###############################################################################
############################## INDEXES MANIPULATION ###########################
###############################################################################

def ij(index,n):
    
    """
    Return ``i,j`` indeces of the ordered non-repeating combinations
    of ``n`` elements.
    
    Parameters
    ----------
    index : int, linearized ``i,j`` index if ``(i,j)`` are the indexes
            of all the ordered non-repeating combinations of ``n``
            elements
    n     : number of elemenets, so ``0 <= index<= n*(n-1)//2-1``
    
    Returns
    -------
    i,j   : floats
    """
    
    i,j = 0,index
    while j//(n-i-1):
        j-= n-i-1
        i+= 1
    return i,i+j+1

def search_equal(l,x,i0=0,i1=-1):
    
    """
    Find index of first occurrence of element ``x`` in an ordered
    array-like of floats/integers ``l``.
    
    Parameters
    ----------
    l : array-like of int/float
    x : int/float element whose index of first occurrence has to be
        found
    i0,i1 : int, search in ``l[i0:i1+1]`` subarray, default is
            ``i0==0`` and ``i1==-1``
    
    Returns
    -------
    i : index of first occurrence; if no occurrence found: return
        ``None``
    """
    
    # process i1
    if i1<0:
        i1+= len(l)
    
    # convergence
    if l[i0]==x:
        return i0
    if l[i1]==x:
        return i1
    
    # failure
    if i1-i0 == 1:
        return
    
    # split the vector in half
    i = (i0+i1)//2
    
    # element is found in the first half
    if l[i] > x:
        if l[i0] < x:
            return search_equal(l,x,i0,i)
        return
    
    # element is found in the second half
    elif l[i] < x:
        if l[i1] > x:
            return search_equal(l,x,i,i1)
        return
    
    # sure that l[i]==x
    return i

def search_bigger(l,x,i0=0,i1=-1,equal=True):
    
    """
    Find index of first occurrence of element bigger than ``x``
    in an ordered array-like of floats/integers ``l``.
    
    Parameters
    ----------
    l : array-like of int/float
    x : int/float element whose index of first occurrence has to be
        found
    i0,i1 : int, search in ``l[i0:i1+1]`` subarray, default is
            ``i0==0`` and ``i1==-1``
    equal : bool, if ``True``: accept elements equal to ``x``
    
    Returns
    -------
    i : index of first occurrence; if no occurrence found: return
        ``None``
    """
    
    # process i1
    if i1<0:
        i1+= len(l)
    
    # convergence
    if (l[i0] >= x and equal) or (l[i0] > x and not equal):
        return i0
    if (l[i1] < x and equal) or (l[i1] <= x and not equal):
        return
    
    # success
    if i1-i0 == 1:
        return i1
    
    # split the vector in half
    i = (i0+i1)//2
    
    # element is found in the first half
    if l[i] > x:
        return search_bigger(l,x,i0,i,equal)
    
    # element is found in the second half
    elif (l[i] <= x and not equal) or (l[i] < x and equal):
        return search_bigger(l,x,i,i1,equal)
    
    # sure that l[i]==x and equal
    return i

def search_smaller(l,x,i0=0,i1=-1,equal=True):
    
    """
    Find index of last occurrence of element smaller than ``x``
    in an ordered array-like of floats/integers ``l``.
    
    Parameters
    ----------
    l : array-like of int/float
    x : int/float element whose index of first occurrence has to be
        found
    i0,i1 : int, search in ``l[i0:i1+1]`` subarray, default is
            ``i0==0`` and ``i1==-1``
    equal : bool, if ``True``: accept elements equal to ``x``
    
    Returns
    -------
    i : index of first occurrence; if no occurrence found: return
        ``None``
    """
    
    # process i1
    if i1<0:
        i1+= len(l)
    
    # convergence
    if (l[i0] > x and equal) or (l[i0] >= x and not equal):
        return
    if (l[i1] <= x and equal) or (l[i1] < x and not equal):
        return i1
    
    # success
    if i1-i0 == 1:
        return i0
    
    # split the vector in half
    i = (i0+i1)//2
    
    # element is found in the second half
    if l[i] < x:
        return search_smaller(l,x,i,i1,equal)
    
    # element is found in the first half
    elif (l[i] >= x and not equal) or (l[i] > x and equal):
        return search_smaller(l,x,i0,i,equal)
    
    # sure that l[i]==x and equal
    return i

###############################################################################
################################ FLOATS MANIPULATION ##########################
###############################################################################

def scientific(x,dx=0,gauge=1,unit=None):
    
    """
    Scientific formatting of measures with significative digits and
    error.
    
    Parameters
    ----------
    x  : float, value
    dx : float, error associated to ``x``
    gauge : int, if ``dx>=0``: max number of significative digits to
            keep for ``dx``; otherwise: max number of significative
            digits to keep for ``x``
    unit  : int, if not ``None``: set power of 10 in representation,
            otherwise let it be decided by ``gauge``
    
    Returns
    -------
    result : str, formatted value(error) string
    
    Examples
    --------
    >>> import SimSet
    >>> SimSet.scientific(3.45432,0.4)
    35(4) * 1e-1
    >>> SimSet.scientific(3.45432,0.4,2)
    345(40) * 1e-2
    >>> SimSet.scientific(3.45432,0,2)
    3.5
    >>> SimSet.scientific(556.53,2)
    557(2)
    """
    
    # no errror - look for first power-of-10 of dx
    if not dx:
        if not x:
            return f'{0:.{gauge-1}f}'
        index = log10(abs(x))
        if index<0  and index != int(index):
            index = int(index)-1
        else:
            index = int(index)
        index-= gauge
        return f'{x:.{max(-index-1,0)}f}'
    
    # look for first power-of-10 of dx
    index = log10(dx)
    if index<0  and index != int(index):
        index = int(index)-1
    else:
        index = int(index)
    
    # renormalize according to gauge and index_dx
    dx = round(dx,-(index-gauge+1))
    if unit is None: unit = index-gauge+1
    x /= 10**unit
    dx/= 10**unit
    
    # format
    if not unit:
        return f'{x:.0f}({dx:.0f})'
    return f'{x:.0f}({dx:.0f}) * 1e{unit}'

###############################################################################
############################## VECTOR(S) MANIPULATION #########################
###############################################################################

def mean(a):
    
    """
    Return the mean of vector ``a``'s elements.
    
    Parameters
    ----------
    a : array-like of floats
    
    Returns
    -------
    result : float, mean
    """
    
    return sum(a)/len(a)

def std(a,m=None):
    
    """
    Return the standard deviation of vector ``a``'s elements.
    
    Parameters
    ----------
    a : array-like of floats
    m : cached mean of a; if ``None``: compute mean of ``a``; default
        is ``None``
    
    Returns
    -------
    result : float, standard deviation
    """
    
    result = 0
    if m is None:
        m = mean(a)
    for x in a:
        result += (x-m)**2
    return (result/len(a))**(1/2)

def dist(a,b):
    """
    Return the euclidean distance between vectors ``a`` and ``b``.
    
    Parameters
    ----------
    a,b : array-like of floats
    
    Returns
    -------
    result : float, distance between ``a`` and ``b``
    """
    
    return sum([(a-b)**2 for a,b in zip(a,b)])**(1/2)

def dot(a,b):
    
    """
    Return the dot product of vectors ``a`` and ``b``.
    
    Parameters
    ----------
    a,b : array-like of floats
    
    Returns
    -------
    result : float, dot product of ``a`` and ``b``
    """
    
    return sum([a*b for a,b in zip(a,b)])

def norm(a):
    
    """
    Return the norm of vector ``a``.
    
    Parameters
    ----------
    a : array-like of floats
    
    Returns
    -------
    result : float, norm of ``a``.
    """
    
    return dot(a,a)**(1/2)

def normalize(a):
    
    """
    Return the unit vector of ``a``.
    
    Parameters
    ----------
    a : array-like of floats
    
    Returns
    -------
    result : list of floats, representing a vector with same direction
             as ``a``, and norm ``1``.
    """
    
    n = norm(a)
    return [x/n for x in a]

def angle_between(a,b,degrees=False):
    
    """
    Returns the angle in radians between vectors ``a`` and ``b``.
    
    Parameters
    ----------
    a : array-like of float
    b : array-like of float
    degrees : bool, if ``True``: result is in degrees; if ``False``: it
              is in radiants; default is ``False``
    
    Returns
    -------
    result : float, angle between ``a`` and ``b``;  
    
    Examples
    --------
    >>> SimSet.angle_between((1, 0, 0), (0, 1, 0))
    1.5707963267948966
    >>> SimSet.angle_between((1, 0, 0), (0, 1, 0),degrees=True)
    90.0
    >>> SimSet.angle_between((1, 0, 0), (1, 0, 0))
    0.0
    >>> SimSet.angle_between((1, 0, 0), (-1, 0, 0))
    3.141592653589793
    """
    
    a = normalize(a)
    b = normalize(b)
    res = arccos(np.clip(dot(a,b), -1.0, 1.0))
    if not degrees:
        return res
    return res*180/pi

###############################################################################
################################ LIST(S) ANALYSIS #############################
###############################################################################

def search(lines,expression,one=True):
    
    """
    Search `expression` in a list of strings: `lines`.
    If `one` is True:  returns index of first match.
    If `one` is False: returns list of all mathing
    indeces. If no match is found: return None.
    
    Parameters
    ----------
    lines:      list of str
    expression: str
    one:        bool, default is True
    """
    
    index = []
    for i,line in enumerate(lines):
        if line[:len(expression)] == expression:
            if one:
                return i
            index.append(i)
    return index if len(index) else None

###############################################################################
############################## LIST(S) MANIPULATION ###########################
###############################################################################

def adjust_list(l,n):
    
    """
    Adjust length of ``l`` to ``n``.
    
    Parameters
    ----------
    l : list or tuple
    n : int, length of result
    
    Returns
    -------
    result : list of length ``n``; first ``len(l)`` elements in
             ``result`` are the same of ``l``, then they repeat
             cyclically
    """
    
    result = []
    L = len(l)
    for i in range(n):
        i = i%L
        if type(l[i]) is list:
            result.append(l[i].copy())
        else:
            result.append(l[i])
    return result

def expand_list(l,n):
    
    """
    Expand ``l`` for ``n`` times.
    
    Parameters
    ----------
    l : list or tuple
    n : int, expansion factor of ``l``
    
    Returns
    -------
    result : list or tuple of length ``n*len(l)``; first ``n`` elements
             in ``result`` are ``l[0]``; second ``n`` elements are
             ``l[1]``, and so on up to ``l[-1]``
    """
    
    return list(chain(*[[x]*n for x in l]))

def shift(l,n):
    
    """
    Shift list ``l`` by ``n`` elements, so that ``l[i%len(l)]`` goes to
    ``l[(i+n)%len(l)]``.
    
    Parameters
    ----------
    l : list or tuple
    n : int, shift factor of ``l``
    
    Returns
    -------
    result : list or tuple with the same length as ``l`` but with
             shifted elements
    """
    
    n = n%len(l)
    return l[n:]+l[:n]

def split_list(l,n):
    
    """
    Split list ``l`` in ``n`` lists, as much homogeneous as possible,
    keeping ``l``'s elements order.
    
    Parameters
    ----------
    l : list, tuple or range
    n : int, split factor of ``l``
    
    Returns
    -------
    result : ``n`` lists, tuples or ranges keeping ``l``'s elements
             order
    """
    
    L = len(l)
    i,span = 0,0
    result = []
    while n:
        span = L//n+bool(L%n)
        result.append(l[i:i+span])
        L-= span
        i+= span
        n-= 1
    return tuple(result)

def first_occurrence(x,value):
    
    """
    Find first occurrence of value in x.
    
    Parameters
    ----------
    x : array-like or dict; if x is an array-like: return index of
        first occurrence; if x if dictionary: return key of first
        occurrence
    
    Returns
    -------
    i: index or key; None if none found
    """
    
    # x is array-like
    if type(x) in [list,tuple,ndarray]:
        for element in enumerate(x):
            if element == value:
                return i
    
    # x is a dictionary
    if type(x) is dict:
        for key in x:
            if x[key] == value:
                return key

def ls(x,*types):
    
    """
    Take ``x``, and make it a list. Make explicit recursively each
    element of the list which is an iterable. Return a list of objects
    that are not iterables.
    
    Parameters
    ----------
    x : whatever object
    types : array-like of types, result must contain just elements whose
            types are in ``types``; if ``int``, ``float`` are in
            ``types``: match also their unsigned versions and other low
            bits variants
    
    Returns
    -------
    result : list of non-iterables
    
    Example
    -------
    >>> SimSet.ls([1,2,3,4,[4,5],['a',2],[10,22]],int)
    [1,2,3,4,4,5,2,10,22]
    """
    
    x =  [x] if type(x) not in ITERABLE else x if type(x) is not dict else\
          x.values()
    x = [[x] if type(x) not in ITERABLE else x if type(x) is not dict else\
          x.values() for x in x]
    x =  list(chain(*x))
    if sum([type(x) in ITERABLE for x in x]):
        return ls(x)
    if not types:
        return x
    if int in types:
        types = list(types)+INTEGER
    if float in types:
        types = list(types)+FLOAT
    if complex in types:
        types = list(types)+COMPLEX
    return [x for x in x if type(x) in types or (x is None and None in types)]

###############################################################################
########################## LISTS OF STRINGS MANIPULATION ######################
###############################################################################

def lcss(*strings):
    
    """
    Return the longest common substring between all strings in
    ``strings``. Credits: "stackoverflow.com/questions/2892931/
    longest-common-substring-from-more-than-two-strings-python"
    
    Parameters
    ----------
    strings : list of str
    
    Returns
    -------
    res: str, result
    """
    
    strings = ls(strings)
    n = len(strings)
    strings = [string for string in strings if type(string) is str]
    if n > len(strings):
        return ''
    if len(strings) == 0:
        return ''
    if len(strings) == 1:
        return strings[0]
    res = ''
    if len(strings) > 1 and len(strings[0]) > 0:
        for i in range(len(strings[0])):
            for j in range(len(strings[0])-i+1):
                if j>len(res) and all(strings[0][i:i+j] in x for x in strings):
                    res = strings[0][i:i+j]
    return res

def prune(*strings,sep=[' '],begin=True,end=True):
    
    """
    Take strings in ``strings``, split the strings in words by
    separators ``sep``, cut out the common words at the beginning and
    at the end, return the words joined by ``sep``.

    Parameters
    ----------
    strings : list or tuple of str
    sep     : str, list of words separator; default is ``[" "]``
    begin   : bool, if ``True``: prune words at the beginning; if
              ``False``: leave left side of the strings intact;
              default is ``True``
    end     : bool, if ``True``: prune words at the end; if ``False``:
              leave right side of the strings intact; default is
              ``True``

    Returns
    -------
    result : list of str

    Example
    -------
    >>> strings = ['unfolding0 rmsd','unfolding1 rmsd']
    >>> print(prune(strings))
    ['unfolding0','unfolding1']
    >>> strings = ['production_run1 rmsd',
                 'production_run2 rmsd']
    >>> print(prune(strings,sep=[' ','_']))
    ['run1','run2']
    """
    
    # preliminaries
    strings = ls(strings)
    if len(strings) <= 1:
        return ['']
    sep = ls(sep)
    if len(sep) == 0:
        return strings
    if not min([len(string) for string in strings]):
        return strings
    
    # split strings in words according to separators
    strings = [string.split(sep[0])     for string in strings]
    separat = [[sep[0]]*(len(string)-1) for string in strings]
    for s in sep[1:]:
        for i in range(len(strings)):
            j = 0
            while j < len(strings[i]):
                word = strings[i][j]
                tmp = word.split(s)
                if len(tmp) > 1:
                    strings[i] = strings[i][:j]+tmp+strings[i][j+1:]
                    separat[i] = separat[i][:j]+[s]*(len(tmp)-1)+separat[i][j:]
                j += 1
    
    # core of the function
    if begin:
        length  = [len(string) for string in strings]
        while True:
            words = [string[0] for string in strings]
            if words[1:] == words[:-1]: # all elements equal
                try:
                    [string.pop(0) for string in strings]
                    [     s.pop(0) for s      in separat]
                except:
                    break
            else:
                break
    if end:
        while min([len(string) for string in strings]):
            words = [string[-1] for string in strings]
            if words[1:] == words[:-1]: # all elements equal
                try:
                    [string.pop( ) for string in strings]
                    [     s.pop( ) for s      in separat]
                except:
                    break
            else:
                break
    
    # retain just the words you want
    # reconstruct strings by words
    result = [''.join([f'{strings[i][j]}{separat[i][j]}'\
                       for j in range(len(separat[i]))]+[strings[i][-1]])\
                       if len(strings[i]) else ''\
                       for i in range(len(strings))]
    return result

###############################################################################
########################### DICTIONARIES MANIPULATION #########################
###############################################################################

def parse(args,argument,default=None):
    
    """
    Look for ``argument`` in ``args`` dictionary. If it is not present:
    assign ``default`` value to ``args[argument]``; otherwise: convert
    ``args[argument]`` to the same type as ``default`` if ``default``
    is not ``None``. Return ``args[argument]``.
    
    Parameters
    ----------
    args     : dict
    argument : str, name of the arbument
    default  : object to be taken as "default"; default is ``None```
    
    Returns
    -------
    result : ``args[argument]``
    
    Examples
    --------
    >>> args = {'good':4,'bad':'me'}
    >>> parse(args,'good',0.0) # returns `4.0`
    >>> parse(args,'bad' ,[] ) # returns `['me']`
    >>> parse(args,'ugly','y') # returns `'y'`
    >>> print(args)
    {'good': 4.0, 'bad': ['me'], 'ugly': 'y'}
    """
    
    # basic assignment
    if argument not in args:
        args[argument] = default
    if default is None:
        pass
    
    # convert to list-tuple
    elif type(default) in [list,tuple]:
      args[argument] = type(default)(args[argument])\
                    if type(args[argument]) in [list,tuple,ndarray]\
                  else type(default)([args[argument]])\
                    if args[argument] is not None else None
    
    # convert to array
    elif type(default) in [ndarray]:
      args[argument] = array(args[argument])\
                    if type(args[argument]) in [list,tuple,ndarray]\
                  else array([args[argument]])\
                    if args[argument] is not None else None
    
    # convert to other type
    else:
      args[argument] = type(default)(args[argument])\
                    if args[argument] is not None else None
    return args[argument]

###############################################################################
########################### MDA OBJECTS MANIPULATION ##########################
###############################################################################

def Universe(*inpt,**args):
    
    """
    Take a list of files (supported formats: xtc, trr, dcd, pdb, gro).
    Check if files exist; if not, try to assign an extension according
    to xtc > trr > dcd > pdb > gro hierarchy. If some files still do
    not exist: raise error. Load files in a ``MDAnalysis.Universe``
    object.
    
    Parameters
    ----------
    inpt : list of str, input filenames
    args : optional arguments, are passed to ``MDAnalysis.Universe``
           initialization
    
    Returns
    -------
    universe: ``MDAnalysis.Universe`` object
    
    Examples
    --------
    >>> import SimSet
    >>> # load mol.pdb as topology, equilibration.xtc as coordinates
    >>> universe = SimSet.Universe('mol.pdb','equilibration')
    """
    
    inpt = ls(inpt)
    for i,inp in enumerate(inpt):
        if inp == '.'.join(extension(inp)):
            if exists(inp):
                continue
        if exists(f'{inp}.xtc'):
            inpt[i] = f'{inp}.xtc'
            continue
        if exists(f'{inp}.trr'):
            inpt[i] = f'{inp}.trr'
            continue
        if exists(f'{inp}.dcd'):
            inpt[i] = f'{inp}.dcd'
            continue
        if exists(f'{inp}.pdb'):
            inpt[i] = f'{inp}.pdb'
            continue
        if exists(f'{inp}.gro'):
            inpt[i] = f'{inp}.gro'
            continue
        tmp = inp.split('/')
        if len(tmp)>1:
            tmp = f'.../{tmp[-1]}'
        else:
            tmp = tmp[-1]
        descr = f'position/trajectory file "{tmp}" not found'
        raise ValueError(descr)
    return mda.Universe(*inpt,**args)

def Update(*inpt,title=None):
    
    """
    Take a list of coordinate files. Set all coordinates to those of the
    first existing file. Set all segids names to those of the first
    existing pdb file -or the first existing file if no pdb is found. Set
    all titles to that of the first existing file, or to ``title`` if it
    is not ``None``. Create the not existing files.
    
    Parameters
    ----------
    inpt  : array-like of str or array-like of array-like of str, input
            filenames, supported formats are pdb and gro
    title : str, title to assign to ``inpt`` files; if ``None``, do
            nothing; default is ``None``
    
    Returns
    -------
    result : ``MDAnalysis.Universe`` object with ``inpt`` data
    
    Notes
    -----
    ``inpt`` depth is reduced to one thanks to ``SimSet.ls`` function.
    
    See Also
    --------
    SimSet.ls : function
    SimSet.update : function
    
    Example
    -------
    >>> Update('mol.pdb','mol.gro','equilibration.gro')
    """
    
    # process input
    inpt = ls(inpt)
    first_exist = -1
    first_pdb   = -1
    for i,inp in enumerate(inpt):
        if exists(inp):
            first_exist = i
            if extension(inp)[1] == 'pdb':
                first_pdb=i
        elif exists(f'{inp}.pdb'):
            inpt[i] = f'{inp}.pdb'
            if first_exist < 0:
                first_exist = i
            if first_pdb < 0:
                first_pdb   = i
        elif exists(f'{inp}.gro'):
            inpt[i] = f'{inp}.gro'
            if first_exist < 0:
                first_exist = i
        else:
            inpt[i] = '.'.join(extension(inp,'pdb'))
    
    # core of the function
    if first_exist < 0:
        descr = 'no existing file found'
        raise ValueError(descr)
    if first_pdb < 0:
        first_pdb = first_exist
    first = inpt[first_exist]
    top   = inpt[first_pdb]
    title = title if title else Get_Title(first)
    universe = mda.Universe(top,first)
    for inp in inpt:
        universe.atoms.write(inp)
        Set_Title(inp,title)
    return universe

def Build(selection,trajectory):
    
    """
    Take a ``MDAnalysis.Universe.selection`` object and return a
    ``MDAnalysis.Universe`` with selection's atoms and the data in
    ``trajectory``. ``trajectory`` can be either an array of floats
    with shape ``(M,N,3)``, where ``M`` is the number of frames and
    ``N`` the number of atoms in ``selection``, or a trajectory file.
    In this case, check if trajectory files exist; if not, try to
    assign an extension according to xtc > trr > dcd > pdb > gro
    hierarchy. If some files still do not exist: raise error. Load files
    in a ``MDAnalysis.Universe`` object.
    
    Parameters
    ----------
    selection  : ``MDAnalysis.Universe.selection`` object
    trajectory : array of floats or list of str
    
    Returns
    -------
    result : ``MDAnalysis.Universe`` with ``selection`` topology and
             ``trajectory`` coordinates
    """
    
    if type(trajectory) in [ndarray]:
        universe = mda.Merge(selection)
        universe.load_new(trajectory,format=MemoryReader)
        return universe
    if trajectory != '.'.join(extension(trajectory)):
        if   exists(f'{trajectory}.xtc'):
            trajectory = f'{trajectory}.xtc'
        elif exists(f'{trajectory}.trr'):
            trajectory = f'{trajectory}.trr'
        elif exists(f'{trajectory}.dcd'):
            trajectory = f'{trajectory}.dcd'
        elif exists(f'{trajectory}.pdb'):
            trajectory = f'{trajectory}.pdb'
        elif exists(f'{trajectory}.gro'):
            trajectory = f'{trajectory}.gro'
        else:
            descr = f'file "{trajectory}" does not exists'
            raise ValueError(descr)
    universe = mda.Merge(selection)
    universe.load_new(trajectory,format=MemoryReader)
    return universe

def Translate(fname,*vector,box=True):
    
    """
    Translate all atoms of a coordinates file ``fname`` by ``vector``.
    Overwrite ``fname`` with the new data.
    
    Parameters
    ----------
    fname  : str, coordinates filename
    vector : iterable of floats, translation vector; adjust length to
             match 3 dimensions
    box    : bool, if ``True``: move all atoms in the main periodic
             cell; default is ``True``
    """
    
    vector = ls(vector)
    vector = adjust_list(vector,3)
    universe = Universe(fname)
    dim = universe.dimensions
    for atom in universe.atoms:
      inside = atom.position
      for i in range(3):
        inside[i] += float(vector[i])*10
        while inside[i] < 0 and box:
          inside[i] += dim[i]
        while inside[i] > dim[i] and box:
          inside[i] -= dim[i]
      atom.position = inside
    universe.atoms.write(fname)

def Set_Speed(fname,speed):
    
    """
    Assign to all atoms whose coordinates are found in ``fname`` the
    velocities of ``speed``. Overwrite ``fname`` with the new data.

    Parameters
    ----------
    fname : str, coordinates file name
    speed : array, velocities to assign to the universe with coordinates
            found in ``fname``
    """
    
    if speed is None:
        return
    universe = Universe(fname)
    universe.trajectory.ts.has_velocities = True
    universe.atoms.velocities = speed
    universe.atoms.write(fname)

def Get_Speed(fname,frame=0):
    
    """
    Return the atoms velocities found in file `fname`
    at frame `frame`. Return nothing if no velocities
    are found.
    
    Parameters
    ----------
    fname: str
    frame: int, default is 0
    
    Returns
    -------
    speed: `numpy.ndarray` object
    """
    
    try:
        universe = Universe(fname)
        universe.trajectory[frame]
        return universe.atoms.velocities
    except:
        return

def Positions(fname,frame=0):
    
    """
    Return the atoms positions found in file `fname`
    at frame `frame`. Return nothing if no positions
    are found.
    
    Parameters
    ----------
    fname: str
    frame: int, default is 0
    
    Returns
    -------
    positions: `numpy.ndarray` object
    """
    
    try:
        universe = Universe(fname)
        universe.trajectory[frame]
        return universe.atoms.positions
    except:
        return

###############################################################################
############################### FILES MANIPULATION ############################
###############################################################################

def Get_Title(fname):
    
    """
    Return system's title found in ``fname``. Supported formats: pdb,
    gro, top (``Gromacs`` topology).
    
    Parameters
    ----------
    fname: str, input filename
    
    Returns
    -------
    title: str, ``fname``'s title
    """
    
    # process fname
    prefix,ext = extension(fname)
    fname = f'{prefix}.{ext}'
    if not exists(fname):
        return
    
    # topology file
    if   ext == 'top':
        raw = check_output(['tail','-50',fname]).decode()
        for i,line in enumerate(raw.split('\n')):
            if line[0:10]=='[ system ]':
              return raw.split('\n')[i+2].replace('\n','')
    
    # pdb file
    elif ext == 'pdb':
        raw = check_output(['head',fname]).decode()
        for i,line in enumerate(raw.split('\n')):
            if line[0:5]=='TITLE':
                return ' '.join(line.split()[1:]).replace('\n','')
    
    # (assumed) gro file
    else:
        raw = check_output(['head','-1',fname]).decode()
        return raw.replace('\n','')

def Set_Title(fname,title=DEF_TIT):
    
    """
    Update system's title found in file ``fname``. Supported formats:
    pdb, gro, top (``Gromacs`` topology).
    
    Parameters
    ----------
    fname: str, filename
    title: str
    
    Returns
    -------
    index: int, line at which subtitution was done; ``-1`` if no
           substitution was done
    """
    
    # process input parameters
    if title is None:
        return -1
    prefix,ext = extension(fname)
    fname = f'{prefix}.{ext}'
    if not exists(fname):
        return -1
    with open(fname) as f:
        lines = f.readlines()
    
    # topology file
    if   ext == 'top':
        index = search(lines,'; Name',one=False)[-1]+1
        lines[index] = f'{title}\n'
    
    # pdb file
    elif ext == 'pdb':
        index = search(lines,'TITLE')
        if index is not None:
            lines[index] = f'TITLE     {title}\n'
        else:
            index = search(lines,'HEADER')
            if index is None: index = 0
            lines = lines[:index+1]+['TITLE     {title}\n']+lines[index+1:]
    
    # (assumed) gro file
    else:
        index = 0
        lines[index] = f'{title}\n'
    
    # write output & return index
    with open(fname,'w') as f:
        f.write(''.join(lines))
    return index

###############################################################################
################################ SHELL UTILITIES ##############################
###############################################################################

def comment(descrip,logre='',logsh='',verbose=VERBOSE):
    
    """
    Print ``descrip`` to standard output, ``logre`` and ``logsh``
    files (if present), the latter with a comment character "#" at the
    beginning of each new line.
    
    Parameters
    ----------
    descrip : str, description of the log activity
    logre   : str, description log filename
    logsh   : str, shell's commands log filename
    verbose : bool, if ``True`` prints ``descrip`` to stdout; default
              is ``SimSet.VERBOSE``
    
    Examples
    --------
    >>> descr = 'start of the program'
    >>> # prints to stdout and `logre` 
    >>> comment(descr,logre,'')
    >>> # prints to stdout and `logsh`
    >>> comment(descr,'',logsh)
    >>> # prints to log (default) files
    >>> comment(descr)
    >>> # prints nothing
    >>> comment(descr,verbose=False)
    """
    
    if verbose:
        print(descrip)
    descrip = no_ansi(descrip)
    if logre:
        with open(logre,'a+') as f:
            f.write(f'{descrip}\n')
    if logsh:
        with open(logsh,'a+') as f:
            for line in descrip.split('\n'):
                f.write(f'# {line}\n')

def execute(command,logre='',logsh='',verbose=VERBOSE):
    
    """
    Print ``command`` to standard output, ``logre`` and ``logsh``
    files (if present), the former with a "command: " prefix and
    proper indentation at the beginning of each new line. Run the
    command on shell. Append the command results to ``logre``.
    
    Parameters
    ----------
    command : str, command to be execute
    logre   : str, description log filename
    logsh   : str, shell's commands log filename
    verbose : bool, if ``True`` prints both the command and the
              command's output to stdout; errors are printed
              nevertheless
    
    Examples
    --------
    >>> commd = 'ls -a *.pdb'
    >>> # prints to stdout and `logre` 
    >>> execute(commd,logre,'')
    >>> # prints to stdout and `logsh`
    >>> execute(commd,'',logsh)
    >>> # prints to all (default) files
    >>> execute(commd)
    >>> # prints to log (default) files
    >>> execute(commd,verbose=False)
    """
    
    if verbose:
        print(command)
    command = no_ansi(command)
    if logre:
        with open(logre,'a+') as f:
            for i,line in enumerate(command.split('\n')):
                f.write(f'command: {line}\n') if not i else\
                f.write(f'         {line}\n')
        if logre:
            command = f'{command} >> "{logre}"'
        if logsh:
            with open(logsh,'a+') as f:
                f.write(f'{command}\n\n')
    sh(command)

def create(directory,logre='',logsh='',verbose=True):
    
    """
    Create ``directory`` folder if it does not exist, report it on
    ``logre`` and ``logsh`` (if present).
    
    Parameters
    ----------
    directory : str, directory to be created
    logre     : str, description log filename
    logsh     : str, shell's commands log filename
    verbose : bool, if ``True`` prints the descriptions and commands
              to stdout; errors are printed nevertheless
    """
    
    if exists(directory):
        return
    descr = f'CREATING directory: "{directory}"'
    commd = f'mkdir -p "{directory}"'
    comment(descr,logre,logsh,verbose)
    execute(commd,logre,logsh,verbose)

def move(wkdir,logre='',logsh='',verbose=True):
    
    """
    Create ``wkdir`` if it does not exist, then move to ``wkdir``,
    report it on `logre` and `logsh`.
    
    Parameters
    ----------
    wkdir: str, working directory
    logre     : str, description log filename
    logsh     : str, shell's commands log filename
    verbose : bool, if ``True`` prints the descriptions and commands
              to stdout; errors are printed nevertheless
    """
    
    create(wkdir,logre,logsh,verbose)
    cd(wkdir)
    descr = f'MOVING to working directory: "{wkdir}"'
    commd = f'cd "{wkdir}"'
    comment(descr,logre,logsh,verbose)
    execute(commd,logre,logsh,verbose)

###############################################################################
################################ GROMACS UTILITIES ############################
###############################################################################

def Index(fname,refere,select,title):
    
    """
    Generate ndx file ``fname`` starting from ``refere`` topology
    file (supported formats: pdb, gro), with ``select`` list of
    selections and ``title`` list of selection titles. If ``fname`` is
    already existent: append new lines to existing lines.
    
    Parameters
    ----------
    fname  : str, output filename
    refere : str, reference filename
    select : array-like of str or array-like of array-like of str,
             ``MDAnalysis`` selection syntax; if ``""``: take all atoms
    title  : array-like of str or array-like of array-like of str,
             titles to assign to ``select``
    
    Notes
    -----
    ``refere`` and ``select`` depth is reduced to one thanks to
    ``SimSet.ls`` function.
    
    See Also
    --------
    SimSet.ls : function
    
    Examples
    --------
    >>> Index('index.ndx','mol.pdb',
               ['protein','name CA'],
               ['prot',   'calpha'])
    """
    
    # process options
    select = ls(select)
    title  = ls(title)
    if len(select) != len(title):
        descr = f'n. of selections {len(select)} != n. of titles {len(title)}'
        raise ValueError(descr)
    
    # core of the function
    sh('rm -f "{fname}"')
    universe = Universe(refere)
    with open(fname,'w') as f:
        for tit,sel in zip(title,select):
            selection = universe.select_atoms(sel) if sel else universe.atoms
            f.write(f'[ {tit} ]\n ')
            for i,atom in enumerate(selection):
                f.write(f'{atom.index+1}')
                f.write('\n' if (i+1)%10==0 else ' ')
            f.write('\n')


def aftok(*after,explicit=True):
    
    """
    Build "afterok" regular expression for qsub option. If ``explicit``
    is ``True``: write "-W depend=afterok:" first.
    
    Parameters
    ----------
    after    : str or int or array-like of str or int
    explicit : bool, default is True
    
    Returns
    -------
    result : str, "afterok" expression
    
    Notes
    -----
    ``after`` depth is reduced to one thanks to ``SimSet.ls`` function.
    
    See Also
    --------
    SimSet.ls : function
    
    Example
    -------
    >>> import SimSet
    >>> SimSet.aftok(200,210,220)
    '-W depend=afterok:200:210:220'
    >>> SimSet.aftok([200,210],200)
    '-W depend=afterok:200:210:220'
    >>> SimSet.aftok([200,210],200,explicit=False)
    '200:210:220'
    >>> SimSet.aftok()
    ''
    >>> SimSet.aftok([200,210],200,'400:500')
    '-W depend=afterok:200:210:220:400:500'
    """
    
    after = ls(after)
    after = [int(aft) if str(aft).replace(' ','').isdigit() else aft\
             for aft in after]
    after = [str(aft) for aft in after if type(aft) is str or aft>0]
    prefx = '-W depend=afterok:'       if explicit   else ''
    return f'{prefx}{":".join(after)}' if len(after) else ''

###############################################################################
############################## CONTACT MAPS UTILITIES #########################
###############################################################################

def sigmoid(r):
   
    """
    Sigmoid function (adimensional).
    
    Parameters
    ----------
    r     : float, input
    
    Notes
    -----
    Removed abs(r) > 1.64*length scale -> 0 feature.
    """
    
    if not r:
        return 1.0
    if abs(r-1) < 1e-3:
        return 0.6
    return (1-(r)**6)/(1-(r)**10)

def get_cmap(positions,c0,function=sigmoid,verbose=VERBOSE):
    
    """
    Return a (linearized) contact map over ``positions`` according
    to signature ``c0`` and function ``function`` of the distances
    between the atoms couples in signature.
    
    Parameters
    ----------
    positions : ``(len(atoms),3)`` array of floats, atoms positions
    c0        : ``(n_of_couples,2)``-shape array-like of int, each row
                containing the indexes of the atoms in a contact
    function  : function of the distance between atoms, default is
                ``sigmoid``
    verbose   : bool, if ``True`` be loud and noisy, default is
                ``VERBOSE``
    
    Returns
    -------
    cmap : ``n_of_couples`` array of floats, value of the contact map
           for each couple
    """
    
    # initialize output
    cmap = []
    
    # initialize progress bar
    if verbose:
        cursor.hide()
        current,t0,eta,speed,n = 0,now(),None,None,len(c0)
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(0,n,0,eta,speed,'',width)
        if verbose: print(f'{bar}\r',end='')
    
    # iterate through the signature
    for i,j in c0:
        atom1,atom2 = positions[i],positions[j]
        cmap.append(function(dist(atom1,atom2)))
        
        # update progress bar
        if not verbose:
            continue
        current+= 1
        elapsed = now()-t0
        speed = current/elapsed
        eta = (n-current)/speed
        bar = progress_bar(current,n,elapsed,eta,speed,'',width)
        if verbose: print(f'{bar}\r',end='')
    
    cursor.show()
    if verbose:
        print(f'{bar}\n')
    return array(cmap)

def write_cmap(output,ctype,unit,signat,colvar,lambdas,cmap):
    
    """
    Write a list of contact mapw to a binary output file, in a format
    that can be handled by ``Gromacs``'s ratchet bias. Credits: Luca
    Teruzzis's ``FoldingAnalysis`` package.
    
    Parameters
    ----------
    output : str, output filename
    ctype  : str, e.g. ``"ABAA"``
    unit   : float, characteristic length scale of the function used
             to compute the value assigned to each contact
    signat : list of tuples of two integers, signature of all the
             possible (non repeating) couples of contacts for the
             molecular system; indexes start counting from zero, but
             are modified such that they start from one in ``output``
    colvar : array-like of float, progress value (0. to 1.) assigned to
             each contact map written to ``output``
    lambdas: array-like of float, weight assigned to each contact map
             written to ``output``
    cmap   : array-like of array-like of floats; list of (linearized,
             according to ``signature``) contact maps to be written to
             ``output``
    """
    
    # process options
    if type(cmap) not in [list,tuple,ndarray]:
        raise ValueError('"cmap" is not an array-like')
    if not len(cmap):
        raise ValueError('"cmap" has no length')
    if type(cmap[0]) not in [list,tuple,ndarray]:
        cmap = [cmap]
    
    # transform into vectors
    cmap    = array(cmap   ,copy=False,ndmin=2)
    lambdas = array(lambdas,copy=False,ndmin=1)
    colvar  = array(colvar ,copy=False,ndmin=1)
    signat  = array(signat ,copy=False,ndmin=2)+1 # count from one
    
    # number of frames
    frames  = cmap.shape[0]
    if frames != len(colvar) or frames != len(lambdas):
        descr = f'"cmap" ({len(cmap)}), "colvar" ({len(colvar)}), '\
                f'"lambdas" ({len(lambdas)}) have different lengths'
        raise ValueError(descr)
    
    # sort signature and cmap
    sort    = lexsort((signat[:,1],signat[:,0]))
    signat  = signat[sort]
    cmap    = cmap[:,sort]
    
    # all atoms indexes
    indexes = unique(array(signat,dtype=np.uint32).flatten())
    
    # (sorted) inverted signature
    tangis  = array(signat,copy=False,dtype=np.uint32)[:,::-1]
    sort    = lexsort((tangis[:,1],tangis[:,0]))
    tangis  = tangis[sort]
    
    # full signature (all indexes couples)
    full    = array([(j,i) for i,j in combinations(indexes,2)],dtype=np.uint32)
    full    = full[lexsort((full[:,1],full[:,0]))]
    
    # compute mask
    cur_pos = 0
    c_0 = True
    mask = []
    n_zeros = 0
    n_ones = 0
    
    # loop through full signature
    for i in range(len(full)):
        if c_0:
            if not sum([x!=y for x,y in zip(full[i],tangis[cur_pos])]):
                
                # the two arrays are the same
                mask.append(n_zeros)
                c_0 = False
                n_zeros = 0
                n_ones += 1
            else:
                if n_zeros == 65535:
                    mask.append(n_zeros)
                    mask.append(0)
                    n_zeros = 0
                n_zeros += 1
        else:
            if cur_pos < len(tangis) - 1:
                cur_pos += 1
            if not sum([x!=y for x,y in zip(full[i],tangis[cur_pos])]):
                
                # the two arrays are the same
                if n_ones == 65535:
                    mask.append(n_ones)
                    mask.append(0)
                    n_ones = 0
                n_ones += 1
            else:
                mask.append(n_ones)
                c_0 = True
                n_ones = 0
                n_zeros += 1
    if c_0:
        mask.append(n_zeros)
    else:
        mask.append(n_ones)
    
    # write contact maps
    with open(output, 'wb') as f:
        f.write(pack('4c',b'C',b'M',b'A',b'P'))
        f.write(pack('I',len(indexes)))
        f.write(pack('4c',*[x.encode('UTF-8') for x in ctype]))
        f.write(pack('d',unit))
        f.write(indexes.tobytes())
        f.write(pack('I',len(mask)))
        f.write(array(mask,dtype=np.uint16).tobytes())
        f.write(pack('I',frames))
        f.write(pack('I',2))
        for i in range(frames):
            f.write(pack('d',colvar[i]))
            f.write(pack('d',lambdas[i]))
            f.write((cmap[i,sort]*65535+0.5).astype(np.uint16).tobytes())

def read_cmap(inpt):
    
    """
    Retrieve infos from a contact maps input binary file ``inpt``.
    Credits: Luca Teruzzis's ``FoldingAnalysis`` package.
    
    Parameters
    ----------
    inpt   : str, input filename
    
    Returns
    -------
    ctype  : str, e.g. ``"ABAA"``
    unit   : float, characteristic length scale of the function used
             to compute the value assigned to each contact
    signat : list of tuples of two integers, signature of all the
             possible (non repeating) couples of contacts for the
             molecular system; indexes start counting from one in
             ``inpt``, but are modified such that they start from zero
    colvar : array-like of float, progress value (0. to 1.) assigned to
             each contact map written in ``inpt``
    lambdas: array-like of float, weight assigned to each contact map
             written in ``inpt``
    cmap   : array-like of array-like of floats; list of (linearized,
             according to ``signature``) contact maps that are written
             in ``inpt``
    """
    
    # read input file
    with open(inpt, 'rb') as f:
        f.read(calcsize('4c'))
        len_ext_ind =  unpack('I', f.read(calcsize('I')))[0]
        c_type = unpack('4c', f.read(calcsize('4c')))
        c_type = ''.join([x.decode('UTF-8') for x in c_type])
        unit =  unpack('d', f.read(calcsize('d')))[0]
        ext_indexes = np.frombuffer(f.read(4*len_ext_ind),dtype=np.uint32)
        
        # retrieve signature as a boolean mask
        indexes_all = array([(j,i) for i, j in combinations(ext_indexes,2)])
        indexes_all = indexes_all[lexsort((indexes_all[:,1],indexes_all[:,0]))]
        len_mask = unpack('I', f.read(calcsize('I')))[0]
        mask = np.frombuffer(f.read(2 * len_mask), dtype=np.uint16)
        bool_mask = np.full(indexes_all.shape[0], False)
        c_0 = True
        cur_pos = 0
        for i in mask:
            if c_0:
                cur_pos += i
            else:
                bool_mask[cur_pos:cur_pos+i] = True
                cur_pos += i
            c_0 = not c_0
        
        # sort indexes
        indexes = indexes_all[bool_mask]
        lower2upper = np.lexsort((indexes[:,0], indexes[:,1]))
        indexes = indexes[lower2upper,::-1]
        
        # initialize output
        n_frames = unpack('I', f.read(calcsize('I')))[0]
        precision = unpack('I', f.read(calcsize('I')))[0]
        colvar = np.empty(n_frames)
        lambdas = np.empty(n_frames)
        cmaps = np.empty((n_frames, indexes.shape[0]))
        
        # loop through frames
        for i in range(n_frames):
            colvar[i] = unpack('d', f.read(calcsize('d')))[0]
            lambdas[i] = unpack('d', f.read(calcsize('d')))[0]
            cmaps[i] = np.frombuffer(f.read(precision*indexes.shape[0]),
                                     dtype=np.uint16)/65535
            cmaps[i] = cmaps[i,lower2upper]
        
        return (c_type, unit, indexes-1, colvar, lambdas, cmaps)

###############################################################################
############################ ORDER MATRICES UTITLIES ##########################
###############################################################################

def encode_order(*order):
    
    """
    Encode order matrix in a list of compressed integers to save space.
    32 bit
    
    Paramters
    ---------
    order : values to be added, list of either 2 (first element of the
            matrix wins), 3 (none wins), 1 (second element of the
            matrix wins)
    
    Returns
    -------
    order : modified order matrix with implemented x
    """
    
    order  = ls(order,int,uint8,uint32,int64)
    nsplit = len(order)//16+(len(order)%16>0)
    result = array([0]*nsplit,dtype=uint32)
    for n in range(nsplit):
        for j,element in enumerate(order[n*16:(n+1)*16]):
            result[n] = (element<<j*2)|result[n]
    return result

def decode_order(*order):
    
    """
    Decode elements of compressed order matrix in 1,2,3 elements array
    
    Parameters
    ----------
    order : int, element of compressed order matrix
    
    Returns
    -------
    list int, either 1, 2, 3
    """
    
    result = []
    order = ls(order,int,uint8,uint32,int64)
    for compressed in order:
        while compressed:
            element = compressed&((1<<2)-1)
            if not element:
                break
            result.append(element)
            compressed = compressed >> 2
    return array(result,dtype=uint8)

def write_order(output,order):
    
    """
    Write a list of (linearized) order matrices to a binary npy file.
    Don't have to check if data are compressed: this is done directly
    by ``SimSet.read_order``.
    
    Parameters
    ----------
    output : str, output filename
    order : array-like of array-like of unsigned int, representing the
            linearized order matrices
    """
    
    length = order.shape[-1]
    array([length]+list(order.flatten()),dtype=uint32).tofile(output)

def read_order(inpt,compress=True):
    
    """
    Read a list of order matrices from a binary npy file.
    
    Parameters
    ----------
    inpt : str, input filename
    compress : bool, if ``True``: return result as compressed data,
               independently on the fact that the input file stores
               compressed data or not
    
    Returns
    -------
    order : array of linearized order matrices
    """
    
    # read raw data
    raw = fromfile(inpt,dtype=uint32)
    
    # process options
    length = raw[0]
    
    # not compressed data
    if raw[1] < 4:
        # retrieve number of matrices
        N = (raw.shape[0]-1)//length
        # reshape raw data
        raw = raw[1:].reshape((N,length))
        order = raw.astype(uint8)
        
        # compress data if required
        if compress:
            raw = order
            split = length//16+(length%16>0)
            order = array([[0]*split]*N,dtype=uint32)
            for n in range(N):
                order[n] = encode_order(raw[n])
        return order
    
    # compressed data - retrieve number of matrices
    split = length
    N = (raw.shape[0]-1)//split
    # reshape raw data
    order = raw[1:].reshape((N,split))
    
    # decompress data if required
    if not compress:
        raw = order
        length = max((split-1)*16,0)+len(decode_order(raw[0][-1]))
        order = array([[0]*length]*N,dtype=uint8)
        for n in range(N):
            order[n] = decode_order(raw[n])
    
    return order
