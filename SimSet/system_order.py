# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _order(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    create(self.andir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.order" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    inpt = ls(inpt,str)
    def_ext = ['xtc','trr','dcd']
    def_out = extension(inpt[0])[0] if len(set(inpt))==1 else\
              lcss(inpt) if lcss(inpt) else DEF_ORD
    def_out = def_out.replace('?','').replace('*','')
    def_ref = ['']
    def_sel = DEF_SEL
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_sig = [[]]
    def_cut = NATCUT
    def_dis = DIST
    def_per = PERSISTENCE
    
    # specific parameters
    try:
        exten  = parse(args,'ext'      ,def_ext)
        output = parse(args,'out'      ,def_out)
        output = parse(args,'output'   ,output )
        refere = parse(args,'mol'      ,def_ref)
        refere = parse(args,'ref'      ,refere )
        refere = parse(args,'ref'      ,def_ref)
        refere = parse(args,'reference',refere )
        select = parse(args,'sel'      ,def_sel)
        select = parse(args,'select'   ,select )
        select = parse(args,'selection',select )
        signat = parse(args,'signature',def_sig)
        start  = parse(args,'start'    ,def_sta)
        stop   = parse(args,'stop'     ,def_sto)
        step   = parse(args,'step'     ,def_ste)
        frame  = parse(args,'frame'    ,def_fra)
        frame  = parse(args,'frames'   ,frame  )
        cutoff = parse(args,'cutoff'   ,def_cut)
        dista  = parse(args,'d'        ,def_dis)
        dista  = parse(args,'dist'     ,dista  )
        dista  = parse(args,'distance' ,dista  )
        pers   = parse(args,'pers'     ,def_per)
        pers   = parse(args,'persistence',pers )
        last   = parse(args,'last'     ,False  )
        comp   = parse(args,'compress' ,True   )
        comp   = parse(args,'compressed',comp  )
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # default input
    found = False
    for ext in exten:
        fname = f'{self.last}{"."*bool(ext)}{ext}'
        if isfile(fname):
            def_inp = [fname]
            found = True
            break
    
    # no input
    if not len(inpt):
        if not found:
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return xvg()
        inpt = def_inp
        descr = f'TAKING "{path_name(def_inp[0])}" as input file\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    line = False # formatting issues
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'WARNING: no files found\n'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: warning
        line = True
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'WARNING: none of {names} found'
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
        continue
    
    # reconstruct input
    inpt = ls(inpt)
    n = len(inpt)
    if not n:
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    elif verbo and line: print()
    
    # output
    if output:
        output = f'{self.andir}/{".".join(extension(output,"npy"))}'
    else:
        output = ''
    
    # reference - check if ok
    refere = ls(refere,str)
    if not len(refere):
        descr = f'ERROR: bad "reference" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    refere = [ref if ref else self.molec for ref in refere]
    
    # signature - check if ok
    if not signat or type(signat) not in ITERABLE:
        signat = def_sig
    temp = [] # adjust formatting - convert in couples
    for sig in signat:
        if type(sig) in INTEGER:
            temp.append(sig)
        elif type(sig) in ITERABLE and len(sig)>=2:
            temp.append(sig[ 0])
            temp.append(sig[-1])
    signat = [temp[i:i+2] for i in range(0,len(temp),2)]
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # adjust lengths
    refere = adjust_list(refere,n)
    start  = adjust_list(start, n)
    stop   = adjust_list(stop,  n)
    step   = adjust_list(step,  n)
    frame  = adjust_list(frame, n)
    
    # reference list
    line = False # formatting issues
    for i,ref in enumerate(refere):
        if ref == '.'.join(extension(ref)):
            if isfile(ref):
                continue
        
        # match required extensions
        found = False
        for ext in REF_EXT:
            fname = f'{ref}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if found:
            continue
        
        # input file is also reference
        if extension(inpt[i])[1] in REF_EXT:
            refere[i] = inpt[i]
            continue
        
        # look for alternatives
        found = False
        inp = extension(inpt[i])[0]
        for ext in REF_EXT:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if not found:
            for ext in REF_EXT:
                fname = f'{self.molec}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    refere[i] = fname
                    found = True
                    break
        
        # found alternative
        if found:
            line = True
            descr = f'WARNING: no reference specified '\
                    f'for input file "{path_name(inpt[i])}", '\
                    f'assigning "{path_name(refere[i])}" instead'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # found no alternatives
        descr = f'ERROR: no reference found '\
                f'for input file "{path_name(inpt[i])}"\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    if verbo and line: print()
    
    # process time options
    temp,frame = frame,[]
    numframes  = 0
    for inp, sta,  sto, ste, fra in\
    zip(inpt,start,stop,step,temp):
        
        # retrieve trajectory infos
        universe = Universe(inp)
        N = len(universe.trajectory)
        dt = universe.trajectory.dt
        T0 = universe.trajectory.time
        raw_times = []
        
        # process start
        if type(sta) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sta = search_bigger(raw_times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sto = search_smaller(raw_times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frames accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra = [sta]
                for i,t in enumerate(raw_times[sta:sto]):
                    if t-raw_times[frames[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
        
        # process already existing "frames"
        else:
            tmp,fra = fra,[]
            for f in tmp:
                if type(f) in FLOAT:
                    if len(raw_times) != N:
                        raw_times = [t.time for t in universe.trajectory]
                    f = search_equal(raw_times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        
        # check frames
        if not len(fra):
            descr = f'ERROR: no selected frames for "{inp}"\n'\
                    f'with start = {sta}, stop = {sto}'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return array([[]],dtype=uint32)
        frame.append(fra)
        numframes+= len(fra)
    
    # target signature
    signature,r0 = Cmap(refere[0],select,signat,cutoff,dista,verbo)
    if not signature.shape[-1]:
        descr = f'ERROR: empty target signature array\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    
    nc = len(signature) # number of native contacts
    formed= 0           # formed contacts at current time
    
    # step function to assess contact formation
    step = lambda r : True if r <= cutoff else False
    
    # output initialization
    length = nc*(nc-1)//2
    nbits = length*2 # n of bits in compressed order matrix
    # split bits in groups of 32
    nsplit= nbits//32+(nbits%32>0)
    last_bits = nbits%32
    order = array([[-1]*nsplit]*len(inpt),dtype=uint32)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    # multiprocessing task
    def task(Index,inpt,refere,frame):
        
        # sub result initialization
        order = array([[-1]*nsplit]*len(inpt),dtype=uint32)
        
        # main cycle
        for           k,     inp, ref,   fra in\
        zip(range(len(inpt)),inpt,refere,frame):
            
            # fix last order element
            order[k][-1] = (1<<last_bits)-1
            
            # load universe - check if ok
            try:
                universe  = Universe(ref,inp)
            except Exception as traceback:
                descr = f'unable to load universe with input file '\
                        f'"{inp}" and reference "{ref}"\n{traceback}'
                raise Exception(descr)
            N = len(universe.trajectory)
            
            # initialization
            ftime  = array([+inf]*nc)  # folding time by frame
            breach = array([-inf]*nc)  # last time threshold was crossed
            status = array([False]*nc) # being at the right side
            
            # build order matrix frame by frame - main cycle
            for f,t in enumerate(fra):
                universe.trajectory[t]
                positions = universe.atoms.positions
                cmap = get_cmap(positions,signature,step,False)
                
                # iterate through contacts
                for m in range(nc):
                    
                    # contact is already formed
                    if ftime[m] < inf:
                        continue
                    
                    # check if contact just formed
                    if cmap[m]:
                        if not status[m]:
                            breach[m] = universe.trajectory.time
                            status[m] = True
                            if universe.trajectory.time-breach[m] < pers:
                                continue # unsuccess
                    else:
                        status[m] = False
                        continue # unsuccess
                            
                    # contact just formed
                    ftime[m] = universe.trajectory.time
                    
                    # assess folding of matrix element i,m
                    for i in range(m):
                        if  ftime[i] < ftime[m]:
                            element = 0b10
                        elif ftime[i] > ftime[m]:
                            element = 0b01
                        else:
                            continue
                        # modify order matrix bit-wise
                        # go to right section of order[k]
                        index = i*(nc-1)-i*(i+1)//2+m-1
                        section = index//16
                        index   = index%16
                        element = (element<<index*2)+(1<<index*2)-1
                        element = ((1<<32)-(1<<index*2+2))|element
                        # now "element" has "01" or "10" in position
                        # "order[i,j]", the others bits being all 1s    
                        order[k][section] = order[k][section] & element
                        # now compressed number has "01" or "10" in pos
                        # "order[i,j]", the others bits being unchanged
                    
                    # assess folding of matrix element m,j
                    for j in range(m+1,nc):
                        if  ftime[m] < ftime[j]:
                            element = 0b10
                        elif ftime[m] > ftime[j]:
                            element = 0b01
                        else:
                            continue
                        # modify order matrix bit-wise
                        # go to right section of order[k]
                        index = m*(nc-1)-m*(m+1)//2+j-1
                        section = index//16
                        index   = index%16
                        element = (element<<index*2)+(1<<index*2)-1
                        element = ((1<<32)-(1<<index*2+2))|element
                        # now "element" has "01" or "10" in position
                        # "order[i,j]", the others bits being all 1s
                        order[k][section] = order[k][section] & element
                        # now compressed number has "01" or "10" in pos
                        # "order[i,j]", the others bits being unchanged
                
                # measure & print progress
                if not verbo:
                    continue
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-t0
                speed = current.value/elapsed
                etr = (numframes-current.value)/speed
                bar = progress_bar(current.value,numframes,
                                   elapsed,etr,speed,'frames',width)
                print(f'{bar}\r',end='')
            
            # print statistics
            formed = sum(ftime < inf)
            descr = f'\nFORMED {formed}/{nc} contacts '\
                    f'({100*formed/nc:4.2f}%) for input file {inp}'
            message.value+= descr
            if verbo: print(descr)
        
        # task output
        return Index,order
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = n//PROCESSES+(n<PROCESSES)
    span2  = n%PROCESSES*(n>PROCESSES)
    params = []
    index,first = 0,0
    while first < n:
        last = min(n,first+span1+(span2>0))
        params.append([index,inpt[first:last],
          refere[first:last],frame[first:last]])
        index+=1
        first+=span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'\nRUNNING on {processes} core{"s"*(processes!=1)}'
    comment(descr,self.logre,self.logsh,verbo)
    
    # initialize progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,numframes,0,etr,speed,'frames',width)
    if verbo and n>1: print(f'{bar}\r',end='')
    
    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return array([[]],dtype=uint32)
    
    # terminate pool and progress bar - save messages to log
    pool.join()
    pool.clear()
    cursor.show()
    elapsed = now()-t0
    speed = numframes/elapsed
    etr = 0
    bar = progress_bar(numframes,numframes,elapsed,etr,speed,'frames',width)
    comment(bar,self.logre,self.logsh,False)
    comment(message.value,self.logre,self.logsh,False)
    if verbo: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # reconstruct output
    index = 0
    for _,sub_order in sorted(raw,key=lambda x:x[0]):
        for i in range(len(sub_order)):
            order[index] = sub_order[i]
            index+= 1
    
    # decompress output
    if not comp:
        try:
            raw = order
            order = array([[0]*length]*N,dtype=uint8)
            for n in range(N):
                order[n] = decode_order(raw[n])[:length]
        except:
            descr = f'WARNING: could not de-compress data'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            order = raw       
    
    # write output to file
    if output:
        descr = f'SAVING order matrix to "{path_name(output)}"\n'
        comment(descr,self.logre,self.logsh,verbo)
        write_order(output,order)
    
    return order
