# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################# COMPUTE MIN #################################
###############################################################################

def _min(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    
    Notes
    -----
    This function does not initialize a ``SimSet.xvg`` object, but
    modifies empty already-provided ``res``.
    """
    
    cursor.show()
    
    ###########################################################################
    ######################### RETRIEVE SPECIFIC OPTIONS #######################
    ###########################################################################
    
    # defaults
    def_fet = True
    def_spa = [None]
    
    # parse and process
    verbo = parse(args,'verbose',VERBOSE)
    fetch = parse(args,'fetch'  ,def_fet)
    span  = parse(args,'span'   ,def_spa)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # select data according to options
    xvg = self.select(*columns,**args)
    
    # retrieve system infos
    times = xvg.times
    datap = xvg.datap
    N = len(times)
    H = len(xvg)
    
    # nothing to compute
    if not H: return res
    
    # span
    span = ls(span,int,float,None)
    if not span:
        descr = 'ERROR: invalid "span" option'
        if verbo: print(ansi(descr,'red'))
        return res
    span = [1 if spa is not None and spa<=0 else spa for spa in span]
    span = adjust_list(span,H)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    def task(first,span,res):
        
        """
        Parameters
        ----------
        index : int, subprocess id to reconstruct results in order
        """
        
        # sub result initialization
        new_data = {} # data to be added as a dictionary times->datap
        
        # iterate through columns
        for h,spa in enumerate(span):
            h+= first
            for i0,d,t0 in zip(range(N),datap[h],times):
                if isnan(d): continue
                
                # "span" regions, elements results
                value = None
                cum,n = 0,0 # cumulative times of the region, region's length
                for i,d,t in zip(range(i0,N),datap[h][i0:],times[i0:]):
                    if type(spa) in INTEGER and i-i0 >= spa: break
                    if type(spa) in FLOAT   and t-t0 >= spa: break
                    if isnan(d): continue
                    cum+= t
                    n  += 1 
                    if value is None or d < value: value = d
                if type(spa) in INTEGER and i-i0 < spa: break # end of the
                if type(spa) in FLOAT   and t-t0 < spa: break # ...region
                if value is None: continue
                t = cum/n # now have couple t,value
            
                # fetch: update new times and new datapoint entry
                if fetch and t in new_data: new_data[t]=min(value,new_data[t])
                else:                       new_data[t]=value
                
                # just one region
                if spa is None: break
            
            # measure and print progress
            if verbo:
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-T0
                speed = current.value/elapsed
                etr = (H-current.value)/speed
                bar = progress_bar(current.value,H,
                                   elapsed,etr,speed,'columns',width)
                print(f'{bar}\r',end='')
            
            # not fetch: merge and reset new_data
            if fetch: continue
            res.merge(list(new_data.keys()),list(new_data.values()),
                      obser=f'{xvg.obser[h]} min',
                      files=xvg.files[h],title=xvg.title[h],
                      xaxis=xvg.xaxis[h],yaxis=xvg.yaxis[h],verbose=False)
            new_data = {}
        
        if fetch: return new_data
        return first,res
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = H//PROCESSES+(H<PROCESSES)
    span2  = H%PROCESSES*(H>PROCESSES)
    params = []
    index,first = 0,0
    while first < H:
        last = min(H,first+span1+(span2>0))
        params.append([first,span[first:last],res.copy()])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    if verbo: print(descr)
    
    # initialize progress bar
    cursor.hide()
    T0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,H,0,etr,speed,'columns',width)
    if verbo and H>1: print(f'{bar}\r',end='')

    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbo: print(descr)
        return res
    
    # terminate pool and progress bar - save messages to log
    pool.join()
    pool.clear()
    cursor.show()
    if verbo and H>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # fetch
    if fetch:
        new_data = {}
        for data in raw:
                for t in data:
                    if t in new_data: new_data[t] = min(new_data[t],data[t])
                    else:             new_data[t] = data[t]
        times = list(new_data.keys())
        datap = list(new_data.values())
        res.merge(times,datap,
                  obser=f'{lcss(xvg.obser)} mean',
                  files=lcss(xvg.files),title=lcss(xvg.title),
                  xaxis=lcss(xvg.xaxis),yaxis=lcss(xvg.yaxis),verbose=False)
        return res
    
    # don't fetch
    for sorted_raw in sorted(raw,key=lambda x:x[0]):
        res.merge(sorted_raw[1],verbose=False)
    return res

###############################################################################
################################# COMPUTE MAX #################################
###############################################################################

def _max(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    
    Notes
    -----
    This function does not initialize a ``SimSet.xvg`` object, but
    modifies empty already-provided ``res``.
    """
    
    cursor.show()
    
    ###########################################################################
    ######################### RETRIEVE SPECIFIC OPTIONS #######################
    ###########################################################################
    
    # defaults
    def_fet = True
    def_spa = [None]
    
    # parse and process
    verbo = parse(args,'verbose',VERBOSE)
    fetch = parse(args,'fetch'  ,def_fet)
    span  = parse(args,'span'   ,def_spa)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # select data according to options
    xvg = self.select(*columns,**args)
    
    # retrieve system infos
    times = xvg.times
    datap = xvg.datap
    N = len(times)
    H = len(xvg)
    
    # nothing to compute
    if not H: return res
    
    # span
    span = ls(span,int,float,None)
    if not span:
        descr = 'ERROR: invalid "span" option'
        if verbo: print(ansi(descr,'red'))
        return res
    span = [1 if spa is not None and spa<=0 else spa for spa in span]
    span = adjust_list(span,H)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    def task(first,span,res):
        
        """
        Parameters
        ----------
        index : int, subprocess id to reconstruct results in order
        """
        
        # sub result initialization
        new_data = {} # data to be added as a dictionary times->datap
        
        # iterate through columns
        for h,spa in enumerate(span):
            h+= first
            for i0,d,t0 in zip(range(N),datap[h],times):
                if isnan(d): continue
                
                # "span" regions, elements results
                value = None
                cum,n = 0,0 # cumulative times of the region, region's length
                for i,d,t in zip(range(i0,N),datap[h][i0:],times[i0:]):
                    if type(spa) in INTEGER and i-i0 >= spa: break
                    if type(spa) in FLOAT   and t-t0 >= spa: break
                    if isnan(d): continue
                    cum+= t
                    n  += 1 
                    if value is None or d > value: value = d
                if type(spa) in INTEGER and i-i0 < spa: break # end of the
                if type(spa) in FLOAT   and t-t0 < spa: break # ...region
                if value is None: continue
                t = cum/n # now have couple t,value
            
                # fetch: update new times and new datapoint entry
                if fetch and t in new_data: new_data[t]=max(value,new_data[t])
                else:                       new_data[t]=value
                
                # just one region
                if spa is None: break
            
            # measure and print progress
            if verbo:
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-T0
                speed = current.value/elapsed
                etr = (H-current.value)/speed
                bar = progress_bar(current.value,H,
                                   elapsed,etr,speed,'columns',width)
                print(f'{bar}\r',end='')
            
            # not fetch: merge and reset new_data
            if fetch: continue
            res.merge(list(new_data.keys()),list(new_data.values()),
                      obser=f'{xvg.obser[h]} max',
                      files=xvg.files[h],title=xvg.title[h],
                      xaxis=xvg.xaxis[h],yaxis=xvg.yaxis[h],verbose=False)
            new_data = {}
        
        if fetch: return new_data
        return first,res
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = H//PROCESSES+(H<PROCESSES)
    span2  = H%PROCESSES*(H>PROCESSES)
    params = []
    index,first = 0,0
    while first < H:
        last = min(H,first+span1+(span2>0))
        params.append([first,span[first:last],res.copy()])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    if verbo: print(descr)
    
    # initialize progress bar
    cursor.hide()
    T0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,H,0,etr,speed,'columns',width)
    if verbo and H>1: print(f'{bar}\r',end='')

    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbo: print(descr)
        return res
    
    # terminate pool and progress bar - save messages to log
    pool.join()
    pool.clear()
    cursor.show()
    if verbo and H>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # fetch
    if fetch:
        new_data = {}
        for data in raw:
                for t in data:
                    if t in new_data: new_data[t] = max(new_data[t],data[t])
                    else:             new_data[t] = data[t]
        times = list(new_data.keys())
        datap = list(new_data.values())
        res.merge(times,datap,
                  obser=f'{lcss(xvg.obser)} mean',
                  files=lcss(xvg.files),title=lcss(xvg.title),
                  xaxis=lcss(xvg.xaxis),yaxis=lcss(xvg.yaxis),verbose=False)
        return res
    
    # don't fetch
    for sorted_raw in sorted(raw,key=lambda x:x[0]):
        res.merge(sorted_raw[1],verbose=False)
    return res

###############################################################################
################################# COMPUTE MEAN ################################
###############################################################################

def _mean(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    
    Notes
    -----
    This function does not initialize a ``SimSet.xvg`` object, but
    modifies empty already-provided ``res``.
    """
    
    cursor.show()
    
    ###########################################################################
    ######################### RETRIEVE SPECIFIC OPTIONS #######################
    ###########################################################################
    
    # defaults
    def_fet = True
    def_spa = [None]
    
    # parse and process
    verbo = parse(args,'verbose',VERBOSE)
    fetch = parse(args,'fetch'  ,def_fet)
    span  = parse(args,'span'   ,def_spa)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # select data according to options
    xvg = self.select(*columns,**args)
    
    # retrieve system infos
    times = xvg.times
    datap = xvg.datap
    N = len(times)
    H = len(xvg)
    
    # nothing to compute
    if not H: return res
    
    # span
    span = ls(span,int,float,None)
    if not span:
        descr = 'ERROR: invalid "span" option'
        if verbo: print(ansi(descr,'red'))
        return res
    span = [1 if spa is not None and spa<=0 else spa for spa in span]
    span = adjust_list(span,H)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    def task(first,span,res):
        
        """
        Parameters
        ----------
        index : int, subprocess id to reconstruct results in order
        """
        
        # sub result initialization
        new_data = {} # data to be added as a dictionary times->datap
        
        # iterate through columns
        for h,spa in enumerate(span):
            h+= first
            for i0,d,t0 in zip(range(N),datap[h],times):
                if isnan(d): continue
                
                # "span" regions, elements results
                data = []   # values of the region
                cum,n = 0,0 # cumulative times of the region, region's length
                for i,d,t in zip(range(i0,N),datap[h][i0:],times[i0:]):
                    if type(spa) in INTEGER and i-i0 >= spa: break
                    if type(spa) in FLOAT   and t-t0 >= spa: break
                    if isnan(d): continue
                    cum  += t
                    n    += 1 
                    data.append(d)
                if type(spa) in INTEGER and i-i0 < spa: break # end of the
                if type(spa) in FLOAT   and t-t0 < spa: break # ...region
                if not data: continue
                t = cum/n # now have couple t,value
                value = mean(data)
                
                # fetch: update new times and new datapoint entry
                if fetch:
                    if t in new_data: new_data[t].append(value)
                    else:             new_data[t] = [value]
                else:                 new_data[t] =  value
                
                # just one region
                if spa is None: break
            
            # measure and print progress
            if verbo:
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-T0
                speed = current.value/elapsed
                etr = (H-current.value)/speed
                bar = progress_bar(current.value,H,
                                   elapsed,etr,speed,'columns',width)
                print(f'{bar}\r',end='')
            
            # not fetch: merge and reset new_data
            if fetch: continue
            res.merge(list(new_data.keys()),list(new_data.values()),
                      obser=f'{xvg.obser[h]} mean',
                      files=xvg.files[h],title=xvg.title[h],
                      xaxis=xvg.xaxis[h],yaxis=xvg.yaxis[h],verbose=False)
            new_data = {}
        
        if fetch: return new_data
        return first,res
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = H//PROCESSES+(H<PROCESSES)
    span2  = H%PROCESSES*(H>PROCESSES)
    params = []
    index,first = 0,0
    while first < H:
        last = min(H,first+span1+(span2>0))
        params.append([first,span[first:last],res.copy()])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    if verbo: print(descr)
    
    # initialize progress bar
    cursor.hide()
    T0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,H,0,etr,speed,'columns',width)
    if verbo and H>1: print(f'{bar}\r',end='')

    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbo: print(descr)
        return res
    
    # terminate pool and progress bar - save messages to log
    pool.join()
    pool.clear()
    cursor.show()
    if verbo and H>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # fetch
    if fetch:
        new_data = {}
        for data in raw:
                for t in data:
                    if t in new_data: new_data[t]+= data[t]
                    else:             new_data[t] = data[t]
        times = list(new_data.keys())
        datap = [mean(new_data[t]) for t in times]
        res.merge(times,datap,
                  obser=f'{lcss(xvg.obser)} mean',
                  files=lcss(xvg.files),title=lcss(xvg.title),
                  xaxis=lcss(xvg.xaxis),yaxis=lcss(xvg.yaxis),verbose=False)
        return res
    
    # don't fetch
    for sorted_raw in sorted(raw,key=lambda x:x[0]):
        res.merge(sorted_raw[1],verbose=False)
    return res

###############################################################################
################################# COMPUTE STD #################################
###############################################################################

def _std(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    
    Notes
    -----
    This function does not initialize a ``SimSet.xvg`` object, but
    modifies empty already-provided ``res``.
    """
    
    cursor.show()
    
    ###########################################################################
    ######################### RETRIEVE SPECIFIC OPTIONS #######################
    ###########################################################################
    
    # defaults
    def_fet = True
    def_spa = [None]
    
    # parse and process
    verbo = parse(args,'verbose',VERBOSE)
    fetch = parse(args,'fetch'  ,def_fet)
    span  = parse(args,'span'   ,def_spa)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # select data according to options
    xvg = self.select(*columns,**args)
    
    # retrieve system infos
    times = xvg.times
    datap = xvg.datap
    N = len(times)
    H = len(xvg)
    
    # nothing to compute
    if not H: return res
    
    # span
    span = ls(span,int,float,None)
    if not span:
        descr = 'ERROR: invalid "span" option'
        if verbo: print(ansi(descr,'red'))
        return res
    span = [1 if spa is not None and spa<=0 else spa for spa in span]
    span = adjust_list(span,H)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    message = Manager().Value('s','') # log messages
    lock = Manager().Lock()
    
    def task(first,span,res):
        
        """
        Parameters
        ----------
        index : int, subprocess id to reconstruct results in order
        """
        
        # sub result initialization
        new_data = {} # data to be added as a dictionary times->datap
        
        # iterate through columns
        for h,spa in enumerate(span):
            h+= first
            for i0,d,t0 in zip(range(N),datap[h],times):
                if isnan(d): continue
                
                # "span" regions, elements results
                data = []   # values of the region
                cum,n = 0,0 # cumulative times of the region, region's length
                for i,d,t in zip(range(i0,N),datap[h][i0:],times[i0:]):
                    if type(spa) in INTEGER and i-i0 >= spa: break
                    if type(spa) in FLOAT   and t-t0 >= spa: break
                    if isnan(d): continue
                    cum  += t
                    n    += 1 
                    data.append(d)
                if type(spa) in INTEGER and i-i0 < spa: break # end of the
                if type(spa) in FLOAT   and t-t0 < spa: break # ...region
                if not data: continue
                t = cum/n # now have couple t,value
                value = std(data)
                
                # fetch: update new times and new datapoint entry
                if fetch:
                    if t in new_data: new_data[t].append(value)
                    else:             new_data[t] = [value]
                else:                 new_data[t] =  value
                
                # just one region
                if spa is None: break
            
            # measure and print progress
            if verbo:
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-T0
                speed = current.value/elapsed
                etr = (H-current.value)/speed
                bar = progress_bar(current.value,H,
                                   elapsed,etr,speed,'columns',width)
                print(f'{bar}\r',end='')
            
            # not fetch: merge and reset new_data
            if fetch: continue
            res.merge(list(new_data.keys()),list(new_data.values()),
                      obser=f'{xvg.obser[h]} std',
                      files=xvg.files[h],title=xvg.title[h],
                      xaxis=xvg.xaxis[h],yaxis=xvg.yaxis[h],verbose=False)
            new_data = {}
        
        if fetch: return new_data
        return first,res
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = H//PROCESSES+(H<PROCESSES)
    span2  = H%PROCESSES*(H>PROCESSES)
    params = []
    index,first = 0,0
    while first < H:
        last = min(H,first+span1+(span2>0))
        params.append([first,span[first:last],res.copy()])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'RUNNING on {processes} core{"s"*(processes!=1)}'
    if verbo: print(descr)
    
    # initialize progress bar
    cursor.hide()
    T0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,H,0,etr,speed,'columns',width)
    if verbo and H>1: print(f'{bar}\r',end='')

    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbo: print(descr)
        return res
    
    # terminate pool and progress bar - save messages to log
    pool.join()
    pool.clear()
    cursor.show()
    if verbo and H>1: print()
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # fetch
    if fetch:
        new_data = {}
        for data in raw:
                for t in data:
                    if t in new_data: new_data[t]+= data[t]
                    else:             new_data[t] = data[t]
        times = list(new_data.keys())
        datap = [mean(new_data[t]) for t in times]
        res.merge(times,datap,
                  obser=f'{lcss(xvg.obser)} mean',
                  files=lcss(xvg.files),title=lcss(xvg.title),
                  xaxis=lcss(xvg.xaxis),yaxis=lcss(xvg.yaxis),verbose=False)
        return res
    
    # don't fetch
    for sorted_raw in sorted(raw,key=lambda x:x[0]):
        res.merge(sorted_raw[1],verbose=False)
    return res
