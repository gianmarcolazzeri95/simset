# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _plot(self,xvg,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    cursor.show()
    
    ###########################################################################
    ################################ PARSE OPTIONS ############################
    ###########################################################################
    
    # defaults - except for title, xlabel, ylabel, xlim, ylim
    def_out = [  ]
    def_lab =[None]
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_avg = [0.]
    def_hea =   0
    def_bw  = None
    def_wei = [1]
    def_col =  []
    def_alp = [1.]
    def_sty = [DEF_STY]
    def_as  = [DEF_AS]
    def_wid = [1.]
    def_win = DEF_WIN
    def_loc =   0
    
    # specific parameters - except for title, xlabel, ylabel, xlim, ylim, font
    try:
        new    = parse(args,'new'    ,True   )
        verbo  = parse(args,'verbose',VERBOSE)
        output = parse(args,'out'    ,def_out)
        output = parse(args,'output' ,output )
        title  = parse(args,'title'  ,None   )
        label  = parse(args,'lab'    ,def_lab)
        label  = parse(args,'label'  ,label  )
        label  = parse(args,'labels' ,label  )
        start  = parse(args,'start'  ,def_sta)
        stop   = parse(args,'stop'   ,def_sto)
        step   = parse(args,'step'   ,def_ste)
        frame  = parse(args,'frame'  ,def_fra)
        frame  = parse(args,'frames' ,frame  )
        averg  = parse(args,'avg'    ,def_avg)
        averg  = parse(args,'average',averg  )
        heat   = parse(args,'heat'   ,def_hea)
        heat   = abs(parse(args,'heatmap',heat))
        time   = parse(args,'time'   ,heat<=0)
        bandw  = parse(args,'bw'     ,def_bw )
        bandw  = parse(args,'bandw'    ,bandw)
        bandw  = parse(args,'bandwitdh',bandw)
        weight = parse(args,'w'      ,def_wei)
        weight = parse(args,'weight' ,weight )
        weight = parse(args,'weights',weight )
        res    = parse(args,'res'    ,NBINS  )
        res    = parse(args,'resolution',res )
        cmap   = COLMAP
        if 'cmap' in args and type(args['cmap']) in\
        [str,type(cm.get_cmap('Spectral')),type(cm.get_cmap('magma'))]:
              cmap = args['cmap']
        color  = parse(args,'col'    ,def_col)
        color  = parse(args,'color'  ,color  )
        color  = parse(args,'colors' ,color  )
        alpha  = parse(args,'alpha'  ,def_alp)
        style  = parse(args,'style'  ,def_sty)
        avgst  = parse(args,'avgstyle',def_as)
        hide   = parse(args,'hide'   ,False  )
        width  = parse(args,'size'   ,def_wid)
        width  = parse(args,'width'    ,width)
        width  = parse(args,'linewidth',width)
        window = parse(args,'window' ,def_win)
        locat  = parse(args,'loc'    ,def_loc)
        locat  = parse(args,'location',locat )
        grid   = parse(args,'grid'   ,GRID and heat<=0 and new)
        show   = parse(args,'show'   ,DEF_SHW or not output)
        nan    = parse(args,'nan'    ,True   )
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}'
        if verbo: print(descr)
        return plt.figure() if new else\
               plt.figure(plt.gcf().number)
    
    # devoid "args" of main options, pass the rest to "self.select"
    args.pop('title' )
    args.pop('lab'   )
    args.pop('label' )
    args.pop('labels')
    args.pop('start' )
    args.pop('stop'  )
    args.pop('step'  )
    args.pop('frame' )
    args.pop('frames')
    args['verbose'] = False
    
    ###########################################################################
    ########################## PROCESS "STATIC" OPTIONS #######################
    ###########################################################################
    
    # figure initialization - according to "new"
    figure =plt.figure(figsize=window) if new else\
            plt.figure(plt.gcf().number,figsize=window)
    
    # retrieve series
    XVG = self.select(*columns,**args)
    TIME = XVG.get_times()
    if time:
        y = XVG
    else:
        L = len(XVG)//2
        x = xvg
        y = xvg.copy()
        x.set_times(TIME.copy())
        x.set_datap(XVG.get_datap()[0:L])
        x.obser = XVG.obser[0:L]
        x.files = XVG.files[0:L]
        x.title = XVG.title[0:L]
        x.xaxis = XVG.xaxis[0:L]
        x.yaxis = XVG.yaxis[0:L]
        y.set_times(TIME.copy())
        y.set_datap(XVG.get_datap()[L:L*2])
        y.obser = XVG.obser[L:L*2]
        y.files = XVG.files[L:L*2]
        y.title = XVG.title[L:L*2]
        y.xaxis = XVG.xaxis[L:L*2]
        y.yaxis = XVG.yaxis[L:L*2]
    K = len(y) # number of time/data series (without averages)
    N = len(TIME) # n. of points per series (NaNs included)
    
    # print recap
    if K:
        descr = f'COLLECTED {K} {"time" if time else "x-y"} series'
        if verbo: print(descr)
    else:
        descr = f'WARNING: no {"time" if time else "x-y"} series selected'
        if verbo: print(ansi(descr,'yellow'))
        return figure
    
    # output
    output = ls(output,str)
    if output:
        output = ['.'.join(extension(out,'pdf')) for out in output]
    else:
        output = def_out
    
    # label
    label = ls(label,str,None)
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options'
        if verbo: print(ansi(descr,'red'))
        return figure
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # average
    averg = ls(averg,int,float)
    if not len(averg):
        descr = f'ERROR: bad "average" option\n'
        if verbo: print(ansi(descr,'red'))
        return figure
    
    # heatmap
    heat = abs(heat)
    
    # weights
    weight = ls(weight,int,float)
    if not weight:
        weight = def_wei
    
    # colors
    color = ls(color,str)
    
    # alpha
    alpha = [max(0,min(alp,1)) for alp in ls(alpha,int,float)]
    if not alpha: alpha = def_alp
    
    # style, avgstyle - check if ok
    style = ls(style,str)
    avgst = ls(avgst,str)
    if not len(style)*len(avgst):
        descr = f'ERROR: bad "style"/"avgstyle" options\n'
        if verbo: print(ansi(descr,'red'))
        return figure
    
    # width
    width = ls(width,int,float)
    if not len(width):
        descr = f'ERROR: bad "width" option\n'
        if verbo: print(ansi(descr,'red'))
        return figure
    
    # window size
    window = ls(window,int,float)
    window = window[:2] if len(window)>=2 else def_siz
    
    # adjust lengths
    start  = adjust_list(start ,K)
    stop   = adjust_list(stop  ,K)
    step   = adjust_list(step  ,K)
    frame  = adjust_list(frame ,K)
    averg  = adjust_list(averg ,K)
    weight = adjust_list(weight,K)
    alpha  = adjust_list(alpha ,K)
    
    # all origins & labels, prunes & lcss versions
    if not time:
        xorig = [''.join(obs.split(XVG_DIV)[-2:-1]) for obs in x.obser]
        xlab  = [obs.split(XVG_DIV)[-1]             for obs in x.obser]
    yorig = [''.join(obs.split(XVG_DIV)[-2:-1])     for obs in y.obser]
    ylab  = [obs.split(XVG_DIV)[-1]                 for obs in y.obser]
    
    ###########################################################################
    ############################## "DYNAMIC" OPTIONS ##########################
    ###########################################################################
    
    # find common words in x labels
    comm_xlab = ''
    if not time:
        if xlab[1:]==xlab[:-1] and new:
            comm_xlab = xlab[0]
            xlab = ['']*K
    
    # find common words in y labels
    comm_ylab = ''
    if ylab[1:]==ylab[:-1] and new:
        comm_ylab = ylab[0]
        ylab = ['']*K
    
    # labels - adjust text
    label = adjust_list(label,K)
    if not time and not heat:
        y.obser = []
        for xo,xl,yo,yl,lab in zip(xorig,xlab,yorig,ylab,label):
            if lab is not None:
                obs = lab
            elif new:
                if xo == yo:
                    obs = f'{xo}{" "*(len(xo)>0)}{xl}{" "*(len(xl)>0)}'\
                              f'{"vs"*(len(xl+yl)>0)}'\
                              f'{" "*(len(yl)>0)}{yl}'
                else:
                    obs = f'{xo}{" "*(len(xo)>0)}{xl}{" "*(len(xl)>0)}'\
                          f'{"vs"*(len(xo+xl+yl)>0)}{" "*(len(yo)>0)}'\
                          f'{yo}{" "*(len(yl)>0)}{yl}'
            elif xo == yo:
                obs = xo
            else:
                obs = f'{xo}{" "*(len(xo)>0)}{xl}{" "*(len(xl)>0)}'\
                      f'{"vs"*(len(xo+xl+yl)>0)}{" "*(len(yo)>0)}'\
                      f'{yo}{" "*(len(yl)>0)}{yl}'
            y.obser.append(obs)
    elif not heat:
        y.obser = []
        for yo,yl,lab in zip(yorig,ylab,label):
            if lab is not None:
                obs = lab
            else:
                obs = f'{yo}{" "*(len(yo)>0)}{yl}'
            y.obser.append(obs)
    
    # default title
    if new or not figure.axes:
        def_tit = lcss(y.title)
        def_tit = lcss(def_tit,x.title) if not time else def_tit
        def_tit = def_tit.replace('_',' ')
    else:
        def_tit = figure.axes[0].get_title()
    
    # default axis
    if new or not figure.axes:
        if not time:
            def_xax = lcss(x.yaxis)
            def_xax = f'{comm_xlab}{" "*(len(def_xax)>0)}{def_xax}'
        else:
            def_xax = lcss(y.xaxis)
        def_yax = lcss(y.yaxis)
        def_yax = f'{comm_ylab}{" "*(len(def_yax)>0)}{def_yax}'
    else:
        def_xax = figure.axes[0].xaxis.get_label().get_text()
        def_yax = figure.axes[0].yaxis.get_label().get_text()
    
    # default limits
    if new or not figure.axes or heat:
        def_xlm = []
        def_ylm = []
    else:
        def_xlm = list(figure.axes[0].get_xlim())
        def_ylm = list(figure.axes[0].get_ylim())
    
    # default font size
    if new or not figure.axes:
        def_fon = DEF_FON
    else:
        fon = figure.axes[0].yaxis.get_label().get_size()
        def_fon = (fon+2,fon)
    
    # parse options
    title = f'{title}' if title is not None else def_tit
    xaxis = parse(args,'xlab'   ,def_xax)
    xaxis = parse(args,'xlabel' ,xaxis  )
    yaxis = parse(args,'ylab'   ,def_yax)
    yaxis = parse(args,'ylabel' ,yaxis  )
    xlim  = parse(args,'xlim'   ,def_xlm)
    ylim  = parse(args,'ylim'   ,def_ylm)
    font  = parse(args,'font'   ,def_fon)
    font  = parse(args,'fontsize',font  )
    
    # process xlim & ylim
    xlim = ls(xlim,int,float)
    ylim = ls(ylim,int,float)
    if len(xlim) == 1:
        xlim = def_xlm
    if len(ylim) == 1:
        ylim = def_ylm
    xlim,ylim = xlim[:2],ylim[:2]
    
    # process fontsize
    font = ls(font,int,float)
    font = font[:2]         if len(font)>=2 else\
           font+[font[0]-2] if len(font)>=1 else def_fon
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # initialization
    times = self.get_times()
    if self.memory:
        T, X, Y = [],[],[] # scatter points
        AT,AX,AY= [],[],[] # floating point averages
    else:
        T, X, Y = binary('',),binary('',),binary('',)
        AT,AX,AY= binary('',),binary('',),binary('',)
    weight_bins   = [] # weights for the heatmap...
    weight_values = [] # ...implemented as a simple function
    legend = False # draw legend flag
    
    # iterate through series
    for       k,   obs  ,sta,  sto, ste, fra,  avg  ,alp,  wei in\
    zip(range(K),y.obser,start,stop,step,frame,averg,alpha,weight):
        
        # process start
        if type(sta) in FLOAT:
            sta = search_bigger(times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            sto = search_smaller(times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frame accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                fra = [sta]
                for i,t in enumerate(times[sta:sto]):
                    if t-times[fra[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
        
        # process already existing "frame"
        else:
            temp,fra = fra,[]
            for f in temp:
                if type(f) in FLOAT:
                    f = search_equal(times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        
        # iterate through time frames
        fra_remove = []
        if time:
            for i,f in enumerate(fra):
                if not isnan(y.get_datap()[k][f]):
                    T.append(TIME[f])
                    X.append(TIME[f])
                    Y.append(y.get_datap()[k][f])
                    continue
                fra_remove.append(i)
        else:
            for i,f in enumerate(fra):
                if not isnan(x.get_datap()[k][f]) and\
                   not isnan(y.get_datap()[k][f]):
                    T.append(TIME[f])
                    X.append(x.get_datap()[k][f])
                    Y.append(y.get_datap()[k][f])
                    continue
                fra_remove.append(i)
        
        # remove frames with no data
        if fra_remove:
            fra = list(fra)
            for i in fra_remove[::-1]:
                fra.pop(i)
        
        # compute floating points averages of T, X, Y points -> AT, AX, AY
        # if avg is zero: T==AT, X==AX, Y==AY
        if not avg:
            for at,ax,ay in zip(T,X,Y):
                AT.append(at)
                AX.append(ax)
                AY.append(ay)
        else:
            M = len(fra)
            for i in range(M):
                j = i # index of the center of the "average" region
                while j < M and TIME[fra[j]]-TIME[fra[i]] < avg/2: j+= 1
                h = j # index of the right corner of the "average" region
                if j == M: break
                while h < M and TIME[fra[h]]-TIME[fra[i]] < avg  : h+= 1
                if h == M: break
                AT.append(TIME[fra[j]])
                AX.append(mean(X[i:h]))
                AY.append(mean(Y[i:h]))
        
        # stop after here if you're plotting a heatmap
        if heat:
            if self.memory: # reset scatter points
                T, X, Y = [],[],[]
            else:
                T, X, Y = binary('',),binary('',),binary('',)
            # update weights bins/values
            weight_bins  .append(len(AT))
            weight_values.append(wei)
            continue
        
        # optional arguments
        plt_args = {}
        if obs:
            legend = True
            plt_args['label'] = obs
        if color:
            if style in ['.','+','*']:
                plt_args['edgecolors'] = color[k%len(color)]
            else:
                plt_args     ['color'] = color[k%len(color)]
        plt_args['linewidth'] = width[k%len(width)]
        plt_args['markersize']= width[k%len(width)]*8
        plt_args['alpha'] = alp
        
        # plot series
        if not hide:
            plt.plot(X,Y,style[k%len(style)],**plt_args)
        
        # plot average
        if avg:
            plt_args['label'] = f'{obs+" - "*bool(obs)}{avg} t.u. average'\
                                if not hide else obs
            if color:
                if avgst in ['.','+','*']:
                    plt_args['edgecolors'] = color[k%len(color)]
                else:
                    plt_args     ['color'] = color[k%len(color)]
            plt_args['linewidth'] = width[k%len(width)]*1.5
            plt_args['markersize']= width[k%len(width)]*12
            plt_args['alpha'] = alp
            
            # plot series
            plt.plot(AX,AY,avgst[k%len(style)],**plt_args)
        
        # reset scatter & average points ('cause not heatmap)
        if self.memory:
            T, X, Y = [],[],[] # scatter points
            AT,AX,AY= [],[],[] # floating point averages
        else:
            T, X, Y = binary('',),binary('',),binary('',)
            AT,AX,AY= binary('',),binary('',),binary('',)
    
    # plot customization
    if grid:
        plt.grid()
    plt.xlabel(xaxis,fontsize=font[1])
    plt.ylabel(yaxis,fontsize=font[1])
    plt.xticks(fontsize=font[1])
    plt.yticks(fontsize=font[1])
    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)
    if title:
        plt.title(title,fontsize=font[0])
    
    # final operations (in not heatmap)
    if new: plt.tight_layout()
    if not heat:
        if locat or K > 1 or sum([bool(avg) for avg in averg]) or not new:
            if locat is not None and legend:
                plt.legend(fontsize=font[1],loc=locat)
        for out in output:
            if extension(out)[1] in\
            ['eps','pdf','pgf','png','ps','raw','rgba','svg','svgz']:
                plt.savefig(out,bbox_inches='tight',transparent=True)
                if verbo: print(f'SAVED figure to "{path_name(out)}"')
            elif verbo:
                descr = f'WARNING: "{path_name(out)}" '\
                        f'extension not supported, saving failed'
                print(ansi(descr,'yellow'))
        if show:
            plt.draw()
            plt.pause(PAUSE)
        return figure
    
    # finally: heatmap - process parameters
    weights = simple(weight_bins,weight_values) # simple function class
    min_x,max_x = xlim if xlim else (min(AX),max(AX))
    min_y,max_y = ylim if ylim else (min(AY),max(AY))
    plt.xlim(min_x,max_x)
    plt.ylim(min_y,max_y)
    
    # compute kde
    k  = kde(AX,AY,weights=weights)
    if bandw:
        try: k.bandw = bandw
        except:
            descr = 'WARNING: could not set "bandwidth" option'
            if verbo: print(ansi(descr,'yellow'))
    X,Y = mgrid[min_x:max_x:res*1j,min_y:max_y:res*1j]
    Z = k(X.reshape(-1),Y.reshape(-1),verbose=verbo)
    Z = -log(Z)
    Z-= min(Z)
    Z = minimum(Z,HEATCUT)
    Z = Z.reshape(res,res)
    
    # plot heatmap
    plt_args = {}
    plt_args['linewidths'] = width[0]
    plt_args['alpha'] = alp
    if color: plt_args['colors'] = adjust_list(color,heat)
    else:     plt_args['colors'] = 'black'
    if width[0]: plt.contour(X,Y,Z,style[0],levels=heat,**plt_args)
    plt_args['linewidths'] = 0.
    plt_args['width']      = 0.
    plt_args['cmap' ]      = cmap
    plt_args.pop('colors')
    if heat > 100:  hmap = plt.imshow(Z[:,::-1].T,interpolation='lanczos',
                                      aspect='auto',cmap=cmap,alpha=alp,
                                      extent=(min_x,max_x,min_y,max_y))
    else:           hmap = plt.contourf(X,Y,Z,levels=heat,**plt_args)
    if new:
        cbar = plt.colorbar(hmap)
        cbar.ax.tick_params(labelsize=font[1]) 
        cbar.set_label('[k$_B$T]',fontsize=font[1])
        
        # final operations (if heatmap)
        plt.tight_layout()
    for out in output:
        if extension(out)[1] in\
        ['eps','pdf','pgf','png','ps','raw','rgba','svg','svgz']:
            plt.savefig(out,bbox_inches='tight',transparent=True)
            if verbo : print(f'SAVED figure to "{path_name(out)}"')
        elif verbo:
            descr = f'WARNING: "{path_name(out)}" '\
                    f'extension not supported, saving failed'
            print(ansi(descr,'yellow'))
    if show:
        plt.draw()
        plt.pause(PAUSE)
    return figure
