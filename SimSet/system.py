# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp as MDP
from SimSet.ssh import ssh as SSH
from SimSet.xvg import xvg

# class-specific functions library
from SimSet.system_generate import _generate
from SimSet.system_mdset    import _mdset
from SimSet.system_mdrun    import _mdrun
from SimSet.system_process  import _process
from SimSet.system_vmd      import _vmd
from SimSet.system_energy   import _energy
from SimSet.system_pca      import _pca
from SimSet.system_rmsd     import _rmsd
from SimSet.system_gyration import _gyration
from SimSet.system_axes     import _axes
from SimSet.system_distance import _distance
from SimSet.system_native   import _native
from SimSet.system_hbonds   import _hbonds
from SimSet.system_rmsf     import _rmsf
from SimSet.system_sasa     import _sasa
from SimSet.system_colvar   import _colvar
from SimSet.system_cmap     import _cmap
from SimSet.system_order    import _order
from SimSet.system_average  import _average
from SimSet.system_functional import _functional

###############################################################################
################################ CLASS: SYSTEM  ###############################
###############################################################################

class system:
    
    """
    Main SimSet class. Load a pdb/gro system file. It can:
      - solvate, ionize, create a topology for the system --either with
        "gmx pdb2gro" or "ambertools"
      - perform MD/ratchet dynamics simulation
      - process coordinate/trajectory files
      - perform basic analysis such as rmsd, rmsf, gyration radius,
        fraction of native contacts and sasa
      - perform bias functional analysis for "best" trajectory selection
    
    Attributes
    ----------
    molec : str, ``output`` value
    title : str, ``tite`` value
    topol : str, ``topology`` value
    posre : str, ``posre`` value
    logre : str, ``log`` value
    logsh : str, ``shell`` value
    wkdir : str, ``wkdir`` value
    andir : str, ``andir`` value
    force : str, ``SimSet.DEF_FFD`` value, force filed name
    boxsh : str, ``SimSet.DEF_BOX`` value, box shape
    dista : list of floats, ``SimSet.DEF_DIST`` value, solvation box
            size (x-y-z, in Angstroms)
    water : str, ``SimSet.DEF_WAT`` value, water model
    wions : str, ``SimSet.DEF_ION`` value, ions names separated by
            empty spaces
    nions : list of int, ``SimSet.DEF_NIO`` value, ions numbers
            (names-wise)
    mions : list of floats, ``SimSet.DEF_MIO`` value, ions molar
            concentrations (names-wise)
    unive : ``MDAnalysis.Universe``, initialized with system data
    segid : list of str: segids by resids
    ssh   : ``SimSet.ssh`` object, remote shell control, initialized
            with ``args`` as optional arguments
    mdp   : ``SimSet.mdp`` object, ``None`` if no simulation is
            initialized yet
    last  : str, last created gro file
    
    Notes
    -----
    Consider adapting default options in ``SimSet.params`` file.
    Consider adding a ssh key to authomatize ssh communication.
    """
    
    ###########################################################################
    ############################ SET ATTRIBUTES' TYPE #########################
    ###########################################################################
    
    # molec
    def _get_molec(self):
        return self.__molec
    def _set_molec(self,value):
        if not isinstance(value,str):
            raise TypeError('"molec" must be set to a string')
        self.__molec = value
    molec = property(_get_molec,_set_molec)
    
    # title
    def _get_title(self):
        return self.__title
    def _set_title(self,value):
        if not isinstance(value,str) and value is not None:
            raise TypeError('"title" must be set to a string or be None')
        self.__title = value
    title = property(_get_title,_set_title)
    
    # topol
    def _get_topol(self):
        return self.__topol
    def _set_topol(self,value):
        if not isinstance(value,str):
            raise TypeError('"topol" must be set to a string or be None')
        self.__topol = value
    topol = property(_get_topol,_set_topol)
    
    # posre
    def _get_posre(self):
        return self.__posre
    def _set_posre(self,value):
        if not isinstance(value,str):
            raise TypeError('"posre" must be set to a string or be None')
        self.__posre = value
    posre = property(_get_posre,_set_posre)
    
    # logre
    def _get_logre(self):
        return self.__logre
    def _set_logre(self,value):
        if not isinstance(value,str):
            raise TypeError('"logre" must be set to a string')
        self.__logre = value
    logre = property(_get_logre,_set_logre)
    
    # logsh
    def _get_logsh(self):
        return self.__logsh
    def _set_logsh(self,value):
        if not isinstance(value,str):
            raise TypeError('"logsh" must be set to a string')
        self.__logsh = value
    logsh = property(_get_logsh,_set_logsh)
    
    # wkdir
    def _get_wkdir(self):
        return self.__wkdir
    def _set_wkdir(self,value):
        if not isinstance(value,str):
            raise TypeError('"wkdir" must be set to a string')
        self.__wkdir = value
    wkdir = property(_get_wkdir,_set_wkdir)
    
    # andir
    def _get_andir(self):
        return self.__andir
    def _set_andir(self,value):
        if not isinstance(value,str):
            raise TypeError('"andir" must be set to a string')
        self.__andir = value
    andir = property(_get_andir,_set_andir)
    
    # force
    def _get_force(self):
        return self.__force
    def _set_force(self,value):
        if not isinstance(value,str):
            raise TypeError('"force" must be set to a string')
        self.__force = value
    force = property(_get_force,_set_force)
    
    # boxsh
    def _get_boxsh(self):
        return self.__boxsh
    def _set_boxsh(self,value):
        if not isinstance(value,str):
            raise TypeError('"boxsh" must be set to a string')
        self.__boxsh = value
    boxsh = property(_get_boxsh,_set_boxsh)
    
    # dista
    def _get_dista(self):
        return self.__dista
    def _set_dista(self,value):
        value = ls(value,int,float)
        if not value:
            raise TypeError('"dista" must be set to a int/float'\
                            ' or an array-like of int/float')
        self.__dista = value
    dista = property(_get_dista,_set_dista)
    
    # water
    def _get_water(self):
        return self.__water
    def _set_water(self,value):
        if not isinstance(value,str):
            raise TypeError('"water" must be set to a string')
        self.__water = value
    water = property(_get_water,_set_water)
    
    # wions
    def _get_wions(self):
        return self.__wions
    def _set_wions(self,value):
        if not isinstance(value,str):
            raise TypeError('"wions" must be set to a string')
        self.__wions = value
    wions = property(_get_wions,_set_wions)
    
    # nions
    def _get_nions(self):
        return self.__nions
    def _set_nions(self,value):
        value = ls(value,int,float)
        if not value:
            raise TypeError('"nions" must be set to a int/float'\
                            ' or an array-like of int/float')
        self.__nions = value
    nions = property(_get_nions,_set_nions)
    
    # mions
    def _get_mions(self):
        return self.__mions
    def _set_mions(self,value):
        value = ls(value,int,float)
        if not value:
            raise TypeError('"mions" must be set to a int/float'\
                            ' or an array-like of int/float')
        self.__mions = value
    mions = property(_get_mions,_set_mions)
    
    # unive
    def _get_unive(self):
        return self.__unive
    def _set_unive(self,value):
        if not isinstance(value,mda.Universe):
            raise TypeError('"unive" must be set to a MDAnalysis.Universe'\
                            ' object')
        self.__unive = value
    unive = property(_get_unive,_set_unive)
    
    # segid
    def _get_segid(self):
        return self.__segid
    def _set_segid(self,value):
        value = ls(value,str)
        self.__segid = value
    segid = property(_get_segid,_set_segid)
    
    # ssh
    def _get_ssh(self):
        return self.__ssh
    def _set_ssh(self,value):
        if not isinstance(value,SSH):
            raise TypeError('"ssh" must be set to a SimSet.ssh object')
        self.__ssh = value
    ssh = property(_get_ssh,_set_ssh)
    
    # mdp
    def _get_mdp(self):
        return self.__mdp
    def _set_mdp(self,value):
        if not isinstance(value,MDP):
            raise TypeError('"mdp" must be set to a SimSet.mdp object')
        self.__mdp = value
    mdp = property(_get_mdp,_set_mdp)
    
    # last
    def _get_last(self):
        return self.__last
    def _set_last(self,value):
        if not isinstance(value,str):
            raise TypeError('"last" must be set to a string')
        self.__last = value
    last = property(_get_last,_set_last)
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,name='',**args):
        
        """
        Initialize ``self``.
        
        Parameters
        ----------
        name : str, optional, pdb/gro input filename; if prefix: assign
               "pdb" as default extension; if default: look for an input
               filename in directory  ``wkdir``, matching argument
               ``output``; default is ``""``
        args: dict, optional arguments
          'out'      : str, alias for below, default is
                       ``SimSet.DEF_MOL``
          'output'   : str, system prefix filename, default is ``out``
          'title'    : str, system title, default is found in pdb/gro
                       file when title is present, otherwise default is
                       ``SimSet.DEF_TIT``
          'topol'    : str, alias for below, default is
                       ``SimSet.DEF_TOP``
          'topology' : str, system topology filename, default is
                       ``topol``
          'posre'    : str, system positions restraints filename,
                       default is ``SimSet.DEF_POS``
          'log'      : str, main log filename, default is
                       ``SimSet.DEF_LOG``
          'shell'    : str, executable log filename, default is
                       ``SimSet.DEF_SHE``
          'wkdir'    : str, working directory path, all input filenames
                       and system generation/simulation output fnames
                       will be relative to ``wkdir``, default is
                       ``SimSet.DEF_WKD``
          'andir'    : str, analysis directory path, all analysis out
                       filenames will be relative to ``andir``, default
                       is ``SimSet.DEF_AND``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        ``args`` are then passed to ``SimSet.ssh`` object initialization
        
        Notes
        -----
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ssh : class
        SimSet.mdp : class
        
        Examples
        --------
        >>> import SimSet
        >>>
        >>> # "mol.pdb" already present in "fol"
        >>> fol = '<path_to_working_directory>'
        >>> ana = '<path_to_analysis_directory>'
        >>> system = SimSet.system(wkdir=fol,andir=ana)
        >>>
        >>> # import pdb file from a different folder
        >>> system = SimSet.system(<path_to_pdb>,wkdir=fol,andir=ana)
        """
        
        #######################################################################
        ############################### PARSING ###############################
        #######################################################################
        
        # input arguments
        self.molec = parse(args,'out'     ,DEF_MOL   )
        self.molec = parse(args,'output'  ,self.molec)
        self.title = parse(args,'title'              )
        self.topol = parse(args,'topol'   ,DEF_TOP   )
        self.topol = parse(args,'topology',self.topol)
        self.posre = parse(args,'posre'   ,DEF_POS   )
        self.logre = parse(args,'log'     ,DEF_LOG   )
        self.logsh = parse(args,'shell'   ,DEF_SHE   )
        self.wkdir = parse(args,'wkdir'   ,DEF_WKD   )
        self.andir = parse(args,'andir'   ,DEF_AND   )
        verbo = parse(args,'verbose',VERBOSE)
        
        # system generation attributes
        self.force = DEF_FFD
        self.boxsh = DEF_BOX
        self.dista = DEF_DIS
        self.water = DEF_WAT
        self.wions = DEF_ION
        self.nions = DEF_NIO
        self.mions = DEF_MIO
        
        # other attributes
        self.unive = mda.Universe.empty(1)
        self.segid = []
        self.ssh   = SSH(**args)
        self.mdp   = MDP()
        self.last  = self.molec
        
        #######################################################################
        ############################## LAUNCHING ##############################
        #######################################################################

        move(self.wkdir,self.logre,self.logsh,verbo)
        descr = f'\nLAUNCHED "SimSet.system" with:\n'
        descr+= f'name = "{name}", ' if name else ''
        descr+= f'args = {args}\n'
        comment(descr,self.logre,self.logsh,verbo)
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # self.molec
        self.molec = extension(self.molec)[0]
        
        # name
        name = abspath('.'.join(extension(name))) if name else ''
        if not isfile(name):
            if isfile(f'{self.molec}.pdb'):
                name = f'{self.molec}.pdb'
            elif isfile(f'{self.molec}.gro'):
                name = f'{self.molec}.gro'
            else:
                descr = f'ERROR: no input found\n'
                descr = ansi(descr,'red')
                comment(descr,self.logre,self.logsh,verbo)
                raise Exception
        else:
            descr = f'REMOVING "{self.molec}" pdb/gro files\n'
            commd = f'rm -f "{self.molec}.pdb" "{self.molec}.gro"'
            comment(descr,self.logre,self.logsh,verbo)
            execute(commd,self.logre,self.logsh,verbo)
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # generate pdb/gro output files and universe with correct title
        descr = f'COPYING "{name}" to "{self.molec}.pdb/gro"'
        comment(descr,self.logre,self.logsh,verbo)
        if self.title is None:
            self.title = Get_Title(name)
        
        # avoid data loss
        HASH = round(now())
        tmp_nam = f'{DEF_TMP}{HASH}{name}'
        tmp_pdb = f'{DEF_TMP}{HASH}{self.molec}.pdb'
        tmp_gro = f'{DEF_TMP}{HASH}{self.molec}.gro'
        sh(f'cp "{name}" "{tmp_nam}" 2>/dev/null')
        sh(f'cp "{self.molec}.pdb" "{tmp_pdb}" 2>/dev/null')
        sh(f'cp "{self.molec}.gro" "{tmp_gro}" 2>/dev/null')
        
        # update universe & files with correct positions and topology
        try:
            self.unive = Update(name,f'{self.molec}.gro',
                                     f'{self.molec}.pdb',
                                     title=self.title)
            sh(f'rm -f "{tmp_nam}" "{tmp_pdb}" "{tmp_gro}"')
        
        # retrieve data
        except KeyboardInterrupt:
            sh(f'mv "{tmp_nam}" "{name}" 2>/dev/null')
            sh(f'mv "{tmp_pdb}" "{self.molec}.pdb" 2>/dev/null')
            sh(f'mv "{tmp_gro}" "{self.molec}.gro" 2>/dev/null')
            self.unive = mda.Universe.empty(1)
            return
        
        # generate segids list
        descr = f'RETRIEVING segids from {self.molec}.pdb\n'
        comment(descr,self.logre,self.logsh,verbo)
        for residue in self.unive.residues:
            self.segid.append(residue.segid)
    
    ###########################################################################
    ######################### PRINT AND REPRESENT SELF ########################
    ###########################################################################
    
    def __str__(self):
        
        """
        Return info about ``self``.
        """
        
        # terminal width and scaling
        try:    width = min(max(40,get_terminal_size()[0]),120)
        except: width = DEF_WID
        scale = width//4
        width = scale*4
        
        # padding
        padA = scale-6
        padB = padA*2+6
        padC = padA+1
        padD = 2*scale-6
        padE = padD+2
        
        # process infos
        dista = format(self.dista+["A"],padA)
        atoms = len(self.unive.atoms)
        resid = len(self.unive.residues)
        segid = len(set(self.segid))
        
        # info about universe
        text =[ansi('universe','bold'),
               f' with {atoms:d} atom{"s"*(atoms!=1)}, ',
               f'{resid:d} residue{"s"*(resid!=1)}, and ',
               f'{segid:d} segment{"s"*(segid!=1)}\n',
               
               # info about attributes
               ansi('ATTRIBUTES','bold','yellow'),f' {"-"*(width-11)}\n',
               ansi('molec','bold','yellow'),' ',format(self.molec,+padA),
               ansi('title','bold','yellow'),' ',format(self.title,+padB,0),
          '\n',ansi('topol','bold','yellow'),' ',format(self.topol,+padA),
               ansi('posre','bold','yellow'),' ',format(self.posre,+padA),
               ansi('logsh','bold','yellow'),' ',format(self.logsh,+padA),
               ansi('logre','bold','yellow'),' ',format(self.logre,+padA,0),
               
               # info about generation
          '\n',ansi('GENERATION','bold','green'),f' {"-"*(width-11)}\n',
               ansi('force','bold','green'),' ',format(self.force,+padA),
               ansi('boxsh','bold','green'),' ',format(self.boxsh,+padA),
               ansi('dista','bold','green'),' ',dista,
               ansi('water','bold','green'),' ',format(self.water,+padA,0),
          '\n',ansi(' ions','bold','green'),' ',format(self.wions,+padA),
               ansi('mions','bold','green'),' ',format(self.mions,+padA),
               ansi('nions','bold','green'),' ',format(self.nions,+padC),
               ansi( 'last','bold','green'),' ',format(self.last ,+padA,0),
               
               # info about directories
          '\n',ansi('DIRECTORIES','bold','cyan'),f' {"-"*(width-12)}\n',
               ansi('wkdir','bold','cyan'),' ',format(self.wkdir,-padD),
               ansi('andir','bold','cyan'),' ',format(self.andir,-padD,0),
               
               # info about ssh communication
          '\n',ansi('SSH PATHS','bold','magenta'),f' {"-"*(width-10)}\n',
               ansi('add'  ,'bold','magenta'),' ',format(self.wkdir,-padE),
               ansi('dir'  ,'bold','magenta'),' ',format(self.andir,-padE,0),
          '\n',ansi('email','bold','magenta'),' ',format(self.wkdir,-padD),
               ansi('gmx'  ,'bold','magenta'),' ',format(self.andir,-padE)]
        
        return ''.join(text)
    
    def __repr__(self):
        
        """
        Return a string representation of ``self``.
        """
        
        # process infos
        atoms = len(self.unive.atoms)
        resid = len(self.unive.residues)
        segid = len(set(self.segid))
        
        return f'universe with {atoms:d} atom{"s"*(atoms!=1)}, '\
               f'{resid:d} residue{"s"*(resid!=1)}, and '\
               f'{segid:d} segment{"s"*(segid!=1)}'
    
    def __format__(self,spec):
        
        """
        Return a proper formatted string of ``self``.
        """
        
        return f'{no_ansi(self.__str__())}\n'
    
    ###########################################################################
    ############################# LOCAL TO REMOTE #############################
    ###########################################################################
    
    def forward(self,*inpt,ext='*',dest='',verbose=VERBOSE):
        
        """
        Rsync files to cluster. Regex matching is implemented. See the
        Examples for details.
        
        Parameters
        ----------
        inpt  : tuple of str/array-like of str, input filenames; if no
                extension is provided: look for existing files
                according to the ``ext`` option; regular expressions
                characters "*" and "?" are also accepted; if no input
                is found: raise error
        ext   : str or array-like of str, allowed file extensions
                for extension-less ``inpt`` elements; for each of those
                elements: look for existing files with ``ext`` as
                extensions; if none is found: print a warning; default
                is ``"*"`` which means: match whatever extension
        dest  : str, destination folder; if not defined: match the
                relative path of the input folder; default is ``""``
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        files : list of str, transferred files
        
        Notes
        -----
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        SimSet.system.backward : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> # send to cluster "mol.*" files
        >>> s.forward('mol')
        >>>
        >>> # send to cluster "equilibration.xtc/gro" and
        >>> # "folding.xtc/gro" files
        >>> s.forward('equilibration',folding',ext=['xtc','gro'])
        >>>
        >>> # send to cluster folding*.xtc" and unfolding*.xtc files
        >>> s.forward('folding*.xtc','unfolding*.xtc')
        >>>
        >>> # send everything to cluster
        >>> s.forward('*')
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # input - check if ok
        inpt = ls(inpt,str)
        if not len(inpt):
            descr = f'ERROR: no files to rsync\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # extensions
        exten = ls(ext,str)
        if not exten:
            exten = ['']
        
        # process input and relative path
        later= [] # to be run later
        rela = '/'.join(inpt[0].split('/')[:-1])
        for i,inp in enumerate(inpt):
            if '/'.join(inp.split('/')[:-1]) != rela:
                later= [inpt[i:],{'ext':exten,'dest':dest,'verbose':verbose}]
                inpt = inpt[:i]
                break
            
            # match regular expression
            if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
                inpt[i] = [] # filled with matching results
                regex,displ = [],[]
                for ext in exten:
                    p,e = extension(inp,ext)
                    if '*' in p or '?' in p:
                        reg = f'{ p }{"."*bool(e)}{e}'
                    else:
                        reg = f'"{p}"{"."*bool(e)}{e}'
                    if reg not in regex:
                        dis = path_name(reg)
                        if not ('*' in p or '?' in p) and dis[0]!='"':
                            dis = f'"{dis}'
                        regex.append(reg)
                        displ.append(dis)
                regex = ' '.join(regex)
                displ = ' '.join(displ)
                HASH = hash(regex)
                tmp_lst = f'{DEF_TMP}{HASH}list'
                sh(f'ls {regex} 2>/dev/null > "{tmp_lst}"')
                with open(tmp_lst,'r') as f:
                    for name in f.readlines():
                        inpt[i].append(name[:-1])
                sh(f'rm -f "{tmp_lst}"')
                if inpt[i]:
                    continue
            
            # match existing file
            if inp == '.'.join(extension(inp)):
                if isfile(inp):
                    continue
            
            # match required extensions
            inpt[i] = []
            for ext in exten:
                fname = f'{inp}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    inpt[i].append(fname)
        
        # reconstruct input
        inpt = ls(inpt)
        n = len(inpt)
        HASH = hash(''.join(inpt))
        tmp_lst = f'{DEF_TMP}{HASH}list'
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # absolute path of "rela" folder
        sh(f'realpath "{self.wkdir}{(len(rela)>0)*"/"}{rela}" '\
           f'2>/dev/null > "{tmp_lst}"')
        try:
            with open(tmp_lst) as f:
                realpath = f.readlines()[0][:-1]
        except:
            descr = f'ERROR: failed folder identification\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # destination folder
        if not dest:
            dest = f'{self.ssh.dir}{"/"*(len(rela)>0)}{rela}'
        
        # make destination path's directory if it does not exist
        if sh(f'ssh {self.ssh.add} \' mkdir -p "{dest}" \' 2>/dev/null'):
            descr = f'ERROR while creating destination folder "{dest}"\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # build files list
        with open(tmp_lst,'w') as f:
            f.write('\n'.join([inp.split('/')[-1] for inp in inpt]))
        
        # rsyncing
        rela  = path_name(rela) if rela else 'current directory'
        descr = path_name(dest) if dest != '.' else 'home folder'
        descr = f'RSYNC-ING {n} files from {ansi(rela,"bold","yellow")} '\
                f'to {ansi(descr,"bold","yellow")} on cluster'
        commd = f'rsync -avuz --files-from="{tmp_lst}" '\
                f'"{realpath}" {self.ssh.add}:"{dest}/"\n'\
                f'rm -f "{tmp_lst}"'
        comment(descr,self.logre,self.logsh,verbose)
        sh(f'{commd}{" >/dev/null"*verbose}')
        
        # remaining tasks
        files = inpt.copy()
        if later:
            files += self.backward(*later[0],**later[1])
        return files
    
    ###########################################################################
    ############################# REMOTE TO LOCAL #############################
    ###########################################################################
    
    def backward(self,*inpt,ext='*',dest='',verbose=VERBOSE):
        
        """
        Rsync files from cluster. Regex matching is implemented. See the
        Examples for details.
        
        Parameters
        ----------
        inpt  : tuple of str/array-like of str, input filenames; if no
                extension is provided: look for existing files
                according to the ``ext`` option; regular expressions
                characters "*" and "?" are also accepted; if no input
                is found: raise error
        ext   : str or array-like of str, allowed file extensions
                for extension-less ``inpt`` elements; for each of those
                elements: look for existing files with ``ext`` as
                extensions; if none is found: print a warning; default
                is ``"*"`` which means: match whatever extension
        dest  : str, destination folder; if not defined: match the
                relative path of the input folder; default is ``""``
        verbose : bool, be loud and noisy, def is ``SimSet.VERBOSE``
        
        Returns
        -------
        files : list of str, transferred files
        
        Notes
        -----
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        SimSet.system.back : alias for this method
        SimSet.system.forward : method
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> # receive from to cluster "mol.*" files
        >>> s.backward('mol')
        >>>
        >>> # receive from cluster "equilibration.xtc/gro" and
        >>> # "folding.xtc/gro" files
        >>> s.backward('equilibration',folding',ext=['xtc','gro'])
        >>>
        >>> # receive folding*.xtc" and unfolding*.xtc files
        >>> s.backward('folding*.xtc','unfolding*.xtc')
        >>>
        >>> # receive everything from cluster
        >>> s.backward('*')
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # input - check if ok
        inpt = ls(inpt,str)
        if not len(inpt):
            descr = f'ERROR: no files to rsync\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # extensions
        exten = ls(ext,str)
        if not exten:
            exten = ['']
        
        # process input and relative path
        later= [] # to be run later
        rela = '/'.join(inpt[0].split('/')[:-1])
        for i,inp in enumerate(inpt):
            if '/'.join(inp.split('/')[:-1]) != rela:
                later= [inpt[i:],{'ext':exten,'dest':dest,'verbose':verbose}]
                inpt = inpt[:i]
                break
            
            # match regular expression
            if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
                inpt[i] = [] # filled with matching results
                regex,displ = [],[]
                for ext in exten:
                    p,e = extension(inp,ext)
                    if '*' in p or '?' in p:
                        reg = f'{ p }{"."*bool(e)}{e}'
                    else:
                        reg = f'"{p}"{"."*bool(e)}{e}'
                    if reg not in regex:
                        dis = path_name(reg)
                        if not ('*' in p or '?' in p) and dis[0]!='"':
                            dis = f'"{dis}'
                        regex.append(reg)
                        displ.append(dis)
                regex = ' '.join(regex)
                displ = ' '.join(displ)
                HASH = hash(regex)
                tmp_lst = f'{DEF_TMP}{HASH}list'
                descr = f'MATCHING regex {ansi(displ,"italic")}'
                sh(f'ssh {self.ssh.add} \' cd {self.ssh.dir}\n'\
                   f'ls {regex} \' 2>/dev/null > "{tmp_lst}"')
                with open(tmp_lst,'r') as f:
                    for name in f.readlines():
                        inpt[i].append(name[:-1])
                sh(f'rm -f "{tmp_lst}"')
                if inpt[i]:
                    continue
            
            # match existing file
            if inp == '.'.join(extension(inp)):
                result = sh(f'ssh {self.ssh.add} \' ls {inp} \''\
                            f' 2>/dev/null >/dev/null')
                if not result:
                    continue
            
            # match required extensions
            inpt[i] = []
            for ext in exten:
                fname = f'{inp}{"."*bool(ext)}{ext}'
                result = sh(f'ssh {self.ssh.add} \' ls {inp} \''\
                            f' 2>/dev/null >/dev/null')
                if not result:
                    inpt[i].append(fname)
        
        # reconstruct input
        inpt = ls(inpt)
        n = len(inpt)
        HASH = hash(''.join(inpt))
        tmp_lst = f'{DEF_TMP}{HASH}list'
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # absolute path of "rela" folder
        sh(f'ssh {self.ssh.add} \' '\
           f'realpath {self.ssh.dir}{(len(rela)>0)*"/"}{rela} \' '\
           f'2>/dev/null > "{tmp_lst}"')
        try:
            with open(tmp_lst) as f:
                realpath = f.readlines()[0][:-1]
        except:
            descr = f'ERROR: folder identification failed\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # destination folder
        if not dest:
            dest = f'{self.wkdir}{"/"*(len(rela)>0)}{rela}'
        
        # make destination path's directory if it does not exist
        if sh(f'mkdir -p "{dest}" 2>/dev/null'):
            descr = f'ERROR while creating destination folder "{dest}"\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbose)
            return
        
        # build files list
        with open(tmp_lst,'w') as f:
            f.write('\n'.join([inp.split('/')[-1] for inp in inpt]))
        
        # rsyncing
        rela  = path_name(rela) if rela else 'home folder'
        descr = path_name(dest) if dest != '.' else 'current directory'
        descr = f'RSYNC-ING {n} files from {ansi(rela,"bold","yellow")} '\
                f'on cluster to {ansi(descr,"bold","yellow")}'
        commd = f'rsync -avuz --files-from="{tmp_lst}" '\
                f'{self.ssh.add}:"{realpath}" "{dest}/"\n'\
                f'rm -f "{tmp_lst}"'
        comment(descr,self.logre,self.logsh,verbose)
        sh(f'{commd}{" >/dev/null"*verbose}')
        
        # remaining tasks
        files = inpt.copy()
        if later:
            files += self.backward(*later[0],**later[1])
        return files
    
    def back(self,*inpt,ext='*',dest='',verbose=VERBOSE):
    
        """
        Alias for ``self.backward``.
        """
        
        return self.backward(*inpt,ext=ext,dest=dest,verbose=verbose)
    
    ###########################################################################
    ############################# GENERATE SYSTEM #############################
    ###########################################################################
    
    def generate(self,**args):
        
        """
        Solvate, ionize, create a topology for the system, according to
        ``self.force``, ``self.boxsh``, ``self.dista``, ``self.water``,
        ``self.wions``, ``self.nions`` attributes. Update
        ``self.topol``, ``self.posre``, ``self.molec`` pdb/gro files.
        
        Parameters
        ----------
        args: dict, optional arguments
          'ff'      : str, alias for below, lower priority
          'force'   : str, alias for below, lower priority
          'forcefield' : str, update ``self.force``, force field name;
                         choose between ``SimSet.DIC_FFD`` keys
          'box'     : str, update ``self.boxsh``, box shape
          'd'       : float or array-like of floats, alias for below,
                      lower priority
          'distance': float or list of float, update ``self.dista``
                      solvation box size (x-y-z, in Angstroms)
          'water'   : str, update ``self.water``, water model
          'ion'     : str, alias for below, lower priority
          'ions'    : str, ions names separated by empty spaces,
                      update ``self.wions``
          'nions'   : array-like of int, update ``self.nions`` ions
                      numbers (names-wise); see the Examples section
                      section for details
          'mions'   : array-like of float, update ``self.mions``
                      ions molar concentrations (names-wise)
                      molar concentrations are converted in atom
                      numbers and then added to ``nions`` names-wise
                      see the Examples section for details
          'ignh'    : bool, ignore and re-build hydrogen atoms, default
                      is ``DEF_IGN``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        N : int, number of atoms added
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_generate``
        Argument parsing uses ``SimSet.parse`` function.
        The program calls ``gmx pdb2gmx``, ``gmx solvate``,
        ``gmx grompp``, ``gmx genion`` (if generating with Gromacs),
        ``tleap`` (if generating with Ambertools), ``gmx genrestr`` .
        
        See Also
        --------
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> # neutralize the charge with NaCl (default)
        >>> system.generate(ff='ff14SB',d=2)
        >>>
        >>> # add 2 K ions and then neutralize the charge with NaCl
        >>> system = SimSet.system()
        >>> system.generate(ff='ff99SB',d=2,ions='K Cl',nions=[2,0])
        >>>
        >>> # add 2 K ions and then neutralize the charge with MgCl2
        >>> system = SimSet.system()
        >>> system.generate(ff='ff99SB',d=2,ions='K Mg',nions=[2,0])
        >>> # Cl is the default negative ion for equilibration
        >>>
        >>> # add 2 Mg ions, 0.15 mol of KCl, then neutralize
        >>> system = SimSet.system()
        >>> system.generate(ff='gromos',d=2,ions='Mg Cl K',
        ...                 nions=[2,0,0],mions=[0,0.15,0])
        """
        
        return _generate(self,**args)
    
    ###########################################################################
    ############################# SET MD PARAMETERS ###########################
    ###########################################################################
    
    def mdset(self,name='',**args):
        
        """
        Create mdp file for ``gmx mdrun``.
        
        Parameters
        ----------
        name : str, input mdp filename
               if not specified: take ``SimSet.DEF_MDP``
        args: dict, optional arguments
          'out'         : str, alias for below
          'output'      : str, save mdp to ``output`` with correct ext
                          if ``""``, save with the same name of
                          ``self.mdp.root``, but in the working
                          directory
          'define'      : str, simulation prescriptions
          'integrator'  : str, integrator type, e.g. "md", "steep"
          'coulomb'     : str, coulomb potential type
          'emtol'       : float, max force threshold in energy minim.
          'pbc'         : str, periodic boundary conditions
          'dt'          : float, integration time step (fs)
          't0'          : float, initial simulation time (ps)
          'init_step'   : int, start simulating from ``init_step``
          't'           : float, simulation time (ns)
          'nsteps'      : int, simulation steps
                          higher priority than ``t``
          'log_freq'    : int, write to log every ``log_freq`` steps
          'trr_freq'    : int, write to trr every ``trr_freq`` steps
          'xtc_freq'    : int, write to xtc every ``xtc_freq`` steps
          'xtc_grps'    : str or array-like, write only ``xtc_grps``
                          selection to xtc, so to save space
          'tcoupl'      : str or bool, thermostat type; if ``tcoupl``
                          is "no" or ``False``, don't set thermostat
          'tc_grps'     : str or array-like, temperature coupling groups
          'tau'         : float or array-like, alias for below
          'tau_t'       : float or array-like, temperature coupling
                          damping coefficient; authomatically adjust
                          array length to ``self.tcgr``; higher priority
                          than ``tau``
          'T'           : float or array-like, temperature coupling
                          and initial value; authomatically adjust array
                          length to match ``tc_grps``; if all elements
                          are zero: ``self.tcpl`` is set to "no"
          'pcoupl'      : str, barostat type; if ``pcoupl`` is "no" or
                          ``False``, don't set thermostat
          'pcoupltype'  : str, pressure coupling type, e.g. "isotropic"
          'comp'        : float or array-like, authomatically adjust
                          array length to match ``pcoupltype``
          'tau_p'       : float or array-like, pressure coupling damping
                          coefficient
          'P'           : float or array-like, pressure coupling value
                          (ATM); authomatically adjust array length to
                          match ``pcoupltype``
          'cmap'        : str, contact map used for ratchet simulation
          'prog_force'  : float, ratchet progress force
          'clos_force'  : float, ratchet closeness force
          'prog_rappa'  : float, ratchet progress rappa
          'clos_rappa'  : float, ratchet closeness rappa
          'prog_target' : float, ratchet progress target
          'clos_target' : float, ratchet closeness target
          'bias_max'    : float, ratchet maximum bias
          'bias_target' : float, ratchet bias target
          'bias_tau'    : float, ratchet bias tau coefficient
          'gen_vel'     : str or bool, generate velocities
          'cont'        : str or bool, if "yes": take velocities from
                          input gro file; higher priority than
                          ``gen_vel``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        ``args`` are then passed to ``self.mdp.update`` method.
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_generate``
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.mdp : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> # load default mdp file and save in "param.mdp"
        >>> system.mdset(out='param') # equivalent to plain "copy"
        >>>
        >>> # modify temperature, simulation length and int. step
        >>> system.mdset(out='param',T=800,t=2,dt=1)
        >>>
        >>> # don't write trr, write xtc for a system subset
        >>> # save mdp in working directory with default name
        >>> system.mdset(trr_freq=0,xtc_freq=500,xtc_grps='protein')
        """
        
        _mdset(self,name,**args)
    
    ###########################################################################
    ############################# RUN MD SIMULATION ###########################
    ###########################################################################
    
    def mdrun(self,*output,**args):
        
        """
        Run MD/ratchet simulation. The symulation parameters are those
        of ``self.mdp.root`` file. Input files regex matching is
        implemented.
        
        Parameters
        ----------
        *output : tuple of str/array-like of str, output files prefix
                  if empty: take ``self.mdp.root`` prefix
        args: dict, optional arguments
          'input'  : tuple of str/array-like of str, input filenames;
                     if empty: ``self.molec`` is taken as prefix;
                     regular expressions characters "*" and "?" are
                     allowed; if no extension is provided: look for
                     existing files with extensions found in
                     ``SimSet.EXT_REF``; for each matched prefix: stop
                     at the first existing file found; if no file is
                     found: raise error
          'extend' : bool, if ``True``: restart from cpt file if
                     available, default is ``SimSet.EXTEND``
          'update' : bool, update system pdb/gro files with results,
                     default is ``True`` when ``output`` has just
                     one element and it is equal to ``self.mdp.root``
                     prefix
          'ssh'    : int, if >0: run on cluster with at most ``ssh``
                     jobs at once; if <0: run on cluster with all jobs
                     minus ``ssh+1`` at once; if equal to 0: run on
                     local machine; cannot be higher than
                     ``SimSet.SSHRUN``; default is ``0``
          'after'  : int, str, or array-like of both, wait for job to
                     complete correctly, 0 means "wait for none", if
                     ``output`` has length 1: wait for all jobs in
                     in ``after`` to complete before running; otherwise:
                     match ``after`` and ``output`` elements; def is 0
          'sync'   : bool, sync all files to and fro cluster, default is
                     ``SimSet.SYNC``
          'qsub'   : bool, if ``True``: auto-send pbs; default is
                     ``SimSet.QSUB``
          'wait'   : bool, wait and check for all jobs to complete;
                     lower priority than ``qsub``; default is
                     ``SimSet.WAIT``
          'tolerance' : int, if ``>0``: maximum queue time before re-
                        sending jobs when ``wait`` option is ``True``;
                        if ``<=0``: set infinite waiting time; if lower
                        priority than ``wait`` and ``qsub``; default is
                        ``SimSet.TOLERANCE``
          'nodes_list' : str or array-like of str, list of available
                         nodes for job submission; regular expressions
                         characters "*" and "?" are also accepted; if
                         ``None``, ``[]``, or ``""``: don't select
                         specific nodes; may; default is ``[]``
          'queues' : str, submit jobs on ``queues``; regular expression
                     characters "*" and "?" are allowed, in this case:
                     take all the cluster's queues whose name match
                     ``queue``; if ``None``, ``[]``, or ``""``: select
                     ``self.ssh.queue`` nodes; don't select specific
                     nodes; priority order of ``queues`` is kept;
                     default is ``[]``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        ``args`` are then passed to ``self.ssh.update`` method.
        
        Returns
        -------
        jobids : last successfully launched jobs ids on cluster; if job
                 is run locally: ``None``
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_mdrun``
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function,
        except for ``after`` when ``output`` length is >1.
        ``gmx grompp`` and ``gmx mdrun`` are called.

        Nodes are selected among ``nodes_list`` according to
        the principle of highest availability. If there are no nodes
        available for running all the required jobs at the same time,
        also nodes with not enough available processors shall be
        returned. If looking for an appropriate list of nodes:
        adapt ``SimSet.NODES`` list.
        
        See Also
        --------
        SimSet.system.mdset : method
        SimSet.parse : function
        SimSet.ls : function
        SimSet.ssh : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # run equilibration, update mol.gro/pdb with results
        >>> system.mdrun()
        >>>
        >>> # do 10 runs from the same initial condition, mol.gro
        >>> output = [f'unfolding{i} for i in range(10)]
        >>> system.mdset(T=800,dt=1,P=0) # high temperature unfolding
        >>> system.mdrun(output)
        >>>
        >>> # do 20 runs for 10 different initial conditions
        >>> inpt = [f'unfolding{i} for i in range(10)]
        >>> output = [f'folding{i}_{j} for i in range(10)\
        ...                            for j in range(20)]
        >>> system.mdset(T=350,dt=2,P=1,cmap='native.cmp',
        ...                             prog_force=5e-3)
        >>> # run simulation on cluster, stacks of 10 jobs
        >>> system.mdrun(output,input=inpt,ssh=10)
        >>>
        >>> # equivalently, if only unfolding0...10 exist
        >>> system.mdrun(output,inpt='unfolding*',ssh=10)
        >>>
        >>> # run on "short" queues
        >>> system.mdrun(output,inpt='unfolding*',queues='short*')
        """
        
        return _mdrun(self,*output,**args)
    
    ###########################################################################
    ################################ PROCESS DATA #############################
    ###########################################################################
    
    def process(self,*inpt,**args):
        
        """
        Wrapper for ``gmx trjconv``. Edit coordinate files/trajectories.
        Regex matching of input/output elements is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str or array-like or str, alias for below,
                        lower priority
          'output'    : str or array-like of str, output filenames;
                        if specified: MUST match ``inpt`` length; if no
                        ``output`` provided: overwrite the input files
                        with the results of the processing; if a
                        regular expression is provided: try to gues the
                        correct output names from ``inpt``; if no
                        extension is provided: assign the same extension
                        as the input files
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, trjconv structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if file not found: take existing
                        files with extension according to priority tpr
                        > pdb > gro; if still no reference file is
                        found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be saved in output; if ``""``:
                        take all atoms; will adjust length to match
                        ``inpt``; default is "all"
          'title'     : str or array-like of str, output system title;
                        will adjust length to match ``inpt``; if "" or
                        ``None``: keep ``inpt`` title if available
          'center'    : str or array-like of str, center coordinates
                        according to ``center`` ``MDAnalysis``
                        selection; will adjust length to match ``inpt``;
                        if "" or ``None``: do nothing; default is
                        ``['']``
          'align'     : str or array-like of str, progressively align
                        frames according to ``align`` ``MDAnalysis``
                        selection; will adjust length to match ``inpt``;
                        if "" or ``None``: do nothing; default is
                        ``['']``
          'pbc'       : str or array-like of str, ``gmx trjconv`` "pbc"
                        option; will adjust length to match ``inpt``;
                        default is ``SimSet.DEF_PBC`` when align is ""
                        or ``None``, otherwise it is ""; if "": do
                        nothing; default is ``SimSet.DEF_PBC``
          'trans'     : array-like of float or array of array-like of
                        float, alias for below, lower priority
          'translate' : array-like of float or array of array-like of
                        float, translate atoms by ``trans`` (A); at
                        least three numbers (3D vector) must be given;
                        will adjust length to match ``inpt``; default is
                        ``[None]`` (do nothing)
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, alias for
                        below, lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the
                        time points indexes to be picked for each input
                        file; if float: pick the indexes corresponding
                        to the time points in ``frames`` (when
                        available); if not specified: take all frames
                        according to ``start``, ``stop``, and ``step``
                        options; if specified: higher priority than
                        ``start``, ``stop``, and ``step``; will adjust
                        length to match ``inpt`` length; default is
                        ``[[]]``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        N : int, number of correctly run processes
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_process``
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``gmx trjconv``.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # center protein in "unfolding*.xtc"
        >>> # overwrite with just protein and ions, remove pbc
        >>> inpt = [f'unfolding{i}' for i in range(10)]
        >>> system.process(inpt,center='protein',
        ...                select='protein or type NA CL',pbc='nojump')
        >>>
        >>> # align to protein, halve the frames
        >>> system.proces(inpt,align='protein',step=2)
        >>>
        >>> # take ufolding*.gro, and save only alanines as pdb
        >>> inpt = [f'unfolding{i}.gro' for i in range(10)]
        >>> out  = [f'unfolding{i}_ALA.pdb' for i in range(10)]
        >>> system.process(inpt,out=out,select='resname ALA')
        >>>
        >>> # take ufolding*.gro, and save only lysines as pdb files
        >>> system.process('unfolding*.gro',out='*_LYS.pdb',
                                            sel='resname LYS')
        """
        
        return _process(self,*inpt,**args)
    
    ###########################################################################
    ################################## VMD ####################################
    ###########################################################################
    
    def vmd(self,*inpt,**args):
        
        """
        Visualize coordinates/trajectories on VMD. Regex matching is
        implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if not specified: look for
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``VMD`` selection
                        to visualize on ``VMD``; if ``""``: take all
                        atoms; will adjust length to match ``inpt``;
                        default is "all"
          'bond'      : array-like of int or ``(n_of_couples,-1)``-shape
                        array of int ("signature" format), alias for
                        below, lower priority
          'bonds'     : array-like of int or ``(n_of_couples,-1)``-shape
                        array of int ("signature" format), additional
                        bonds to be drawn on ``VMD``
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        trajectory frame to be visualized; if negative:
                        start counting from last frame; if float: only
                        time to be visualized (when available); will
                        adjust length to match ``inpt`` higher priority
                        than ``start``, ``stop``, ``step``; default is
                        ``[None]``, which means: don't take ``frame`
                        into consideration
          'smooth'    : int or array-like of int, smooth representation
                        parameter, will adjust length to match ``inpt``;
                        default is ``[0]``
          'repr'      : str or array-like or str, representation method
                        to be assigned to all but water and ions
                        selections of each ``inpt`` file; the syntax
                        follows that of ``VMD``; if numeric parameters
                        are not specified, those are assigned
                        authomatically; will adjust length to match
                        that of ``inpt``; if not defined: use default
                        coloring; default is ``[""]``
          'col'       : str/int/float or array-like of str/int/float,
                        alias for below, lower priority
          'color'     : str/int/float or array-like of str/int/float,
                        alias for below, lower priority
          'colors'    : str/int/float or array-like of str/int/float,
                        the coloring method to be assigned to non-water
                        selections of each ``inpt`` file; the syntax
                        follows that of ``VMD``; to have a single color,
                        specify an integer; will adjust length to match
                        that of ``inpt``; if not defined: use default
                        coloring; default is ``[""]``
          'res'       : float, alias for below, lower priority
          'resolution': float, representations' drawing resolution
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_vmd``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # visualize the frames, halve the frames, don't save
        >>> system.vmd('unfolding*.xtc',ref='ref',step=2)
        >>>
        >>> # visualize two molecules, the former is red...
        >>> # the latter with the "Name" coloring method
        >>> system.vmd('ref.pdb','rna.pdb',col=[1,'Name'])
        >>>
        >>> # represent all selections but water and ions as licorice
        >>> system.vmd('ref.pdb','rna.pdb',repr='licorice')
        """
        
        return _vmd(self,*inpt,**args)
    
    ###########################################################################
    ################################ ENERGY ###################################
    ###########################################################################
    
    def energy(self,*inpt,**args):
        
        """
        Wrapper for ``gmx energy``. Get energy time series from mdp
        files. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files with "edr" as
               def. extension ; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory if no extension is specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        "log" suffix
          'obs'       : str or array-like of str, alias for below,
                        lower priority
          'obser'     : str or array-like of str, alias for below,
                        lower priority
          'observable': str or array-like of str, alias for below,
                        lower priority
          'observables' : str or array-like of str, observables to be
                        retrieved from ``inpt``, default is
                        ``SimSet.DEF_OBS``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``obser``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``obser``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float or array-
                        like of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the
                        time points indexes to be picked for each
                        selected observable; if float: pick the indexes
                        corresponding to the time points in ``frames``
                        (when available); if not specified: take all
                        frames according to ``start``, ``stop``, and
                        ``step`` options; if specified: higher priority
                        than ``start``, ``stop``, and ``step``; will
                        adjust length to match number of observables;
                        default is ``[[]]``
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all ``obser``, increasing ``inpt``
                        and other parameters length; otherwise: adjust
                        ``obser`` length to match ``inpt``; default is
                        ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_energy``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``gmx energy``.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # retrieve potential, halve the frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.energy('unfolding*',step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.energy('unfolding*',step=2,output=None)
        >>>
        >>> # retrieve bond, angles, last 100 frames, save to default
        >>> xvg = system.energy(traj,obs=['bond','angles'],start=-100)
        >>>
        >>> # retrieve dihed, save to "myenergy.xvg"
        >>> xvg = system.energy(traj,obs='dihed',output='myenergy')
        >>>
        >>> # bond for even trajectories, angles for odd
        >>> xvg = system.energy(traj,obs=['bond','angles'],recur=False)
        """
        
        return _energy(self,*inpt,**args)

    ###########################################################################
    ################################## PCA ####################################
    ###########################################################################
    
    def pca(self,*inpt,**args):
        
        """
        Compute n-principal components time series from input
        trajectories. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory if no extension is specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'n'         : int, alias for below, lower priority
          'num'       : int, alias for below, lower priority
          'number'    : int, max n of principal components to be saved,
                        if <=0: save all; default is -1
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro struct file
                        to be taken as reference; if file not
                        found: take existing files with extension
                        according to priority pdb > gro; if still
                        no reference file is found: take related input
                        if a pdb/gro file; if still no reference is
                        found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'th'        : float or array-lik of float, alias for below,
                        lower priority
          'thres'     : float or array-lik of float, alias for below,
                        lower priority
          'threshold' : float or array-lik of float, select components
                        that explain at least ``threshold`` of the
                        cumulated variance, compatibily with ``n`` option,
                        default is ``SimSet.VARIANCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_pca``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Pca`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Pca : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute rmsd, halve the frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.pca(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.pca('unfolding*',step=2,output=None)
        >>>
        >>> # compute rmsd for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.pca(traj,sel=[sel1,sel2],
        ...                   labels=['sel1','sel2'],start=-100)
        >>>
        >>> # compute pca with respect to last trajectory frame
        >>> xvg = system.pca(traj,ref='prot',frame=-1)
        """
        
        return _pca(self,*inpt,**args)
    
    ###########################################################################
    ################################# RMSD ####################################
    ###########################################################################
    
    def rmsd(self,*inpt,**args):
        
        """
        Compute rmsd time series from input trajectories. Regex
        matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension is specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; if file not
                        found: take existing files with extension
                        according to priority pdb > gro; if still
                        no reference file is found: take related input
                        if a pdb/gro file; if still no reference is
                        found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_rmsd``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Rmsd`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Rmsd : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute rmsd, halve the frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.rmsd(traj,step=2,output=None)
        >>>
        >>> # equivalenty, with regex matching
        >>> xvg = system.rmsd('unfolding*',step=2,output=None)
        >>>
        >>> # compute rmsd for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.rmsd(traj,sel=[sel1,sel2],
        ...                   labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when reached threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.rmsd(traj,ref='prot',thres=10)
        >>>
        >>> # compute rmsd with respect to last trajectory frame
        >>> xvg = system.rmsd(traj,ref='prot',frame=-1)
        >>>
        >>> # split rmsd computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.rmsd(['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        return _rmsd(self,*inpt,**args)
    
    ###########################################################################
    ############################ GYRATION RADIUS ##############################
    ###########################################################################
    
    def gyration(self,*inpt,**args):
        
        """
        Compute gyration radius time series from input trajectories.
        Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension is specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if file not found: take existing
                        files with extension according to priority
                        pdb > gro; if still no reference file is found:
                        take related input if a pdb/gro file; if still no
                        reference is found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float or array-
                        like of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the time
                        points indexes to be picked for each computed
                        observable; if float: pick the indexes
                        corresponding to the time points in ``frames``
                        (when available); if not specified: take all
                        frames according to ``start``, ``stop``, and
                        ``step`` options; if specified: higher priority
                        than ``start``, ``stop``, and ``step``; will
                        adjust length to match ``inpt`` length; default
                        is ``[[]]``
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections, increasing
                        ``inpt`` and other parameters length; default
                        is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_gyration``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Gyration`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Gyration : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute gyration radius, halve the frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.gyration(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.gyration('unfolding*',step=2,output=None)
        >>>
        >>> # compute gyration for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.gyration(traj,sel=[sel1,sel2],
        ...                       labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when reached threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.gyration(traj,ref='prot',thres=10)
        >>>
        >>> # split gyr computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.gyration(['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _gyration(self,*inpt,**args)
    
    ###########################################################################
    ############################ AXES OF INERTIA ##############################
    ###########################################################################
    
    def axes(self,*inpt,**args):
        
        """
        Compute principal axes of inertia time series from input
        trajectories. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'ext'       : first available
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if file not found: take existing
                        files with extension according to priority
                        pdb > gro; if still no reference file is found:
                        take related input if a pdb/gro file; if still no
                        reference is found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float or array-
                        like of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the time
                        points indexes to be picked for each computed
                        observable; if float: pick the indexes
                        corresponding to the time points in ``frames``
                        (when available); if not specified: take all
                        frames according to ``start``, ``stop``, and
                        ``step`` options; if specified: higher priority
                        than ``start``, ``stop``, and ``step``; will
                        adjust length to match ``inpt`` length; default
                        is ``[[]]``
          'n'         : int, alias for below, lower priority
          'num'       : int, alias for below, lower priority
          'number'    : int, number of axes to be saved in ``x,y,z``
                        format for each trajectory
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections, increasing
                        ``inpt`` and other parameters length; default
                        is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_gyration``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Gyration`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Gyration : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute moment of inertia, halve the frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.axes(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.axes('unfolding*',step=2,output=None)
        >>>
        >>> # compute axes for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.axes(traj,sel=[sel1,sel2],
        ...                   labels=['sel1','sel2'],start=-100)
        >>>
        >>> # split axes computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.axes(['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _axes(self,*inpt,**args)

    ###########################################################################
    ######################### DISTANCE BTW SELECTIONS #########################
    ###########################################################################
    
    def distance(self,*inpt,**args):
        
        """
        Compute time series of distance between centers of mass of two
        selections from input trajectories (A). Regex matching is
        implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if file not found: take existing
                        files with extension according to priority
                        pdb > gro; if still no reference file is found:
                        take related input if a pdb/gro file; if still no
                        reference is found: take ``self.molec`` pdb/gro
          'sel'       : array-like of str, alias for below, lower
                        priority
          'select'    : array-like of str, alias for below, lower
                        priority
          'selection' : array-like of str, alias for below, lower
                        priority
          'selections': array-like of str, ``MDAnalysis`` selection
                        couples to be used in computations; if ``""``:
                        take all atoms; if not ``recursive``: cut to
                        even numbers and adjust length to match 2 times
                        ``inpt`` length; if ``selections`` has length
                        < 2: return empty ``SimSet.xvg`` object; default
                        is ``["all",SimSet.DEF_SEL]``
          'axis'      : str, restrict to axis, e.g. "xy" projects
                        distance to horizontal plane, def. is "xyz"
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float or array-
                        like of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the time
                        points indexes to be picked for each computed
                        observable; if float: pick the indexes
                        corresponding to the time points in ``frames``
                        (when available); if not specified: take all
                        frames according to ``start``, ``stop``, and
                        ``step`` options; if specified: higher priority
                        than ``start``, ``stop``, and ``step``; will
                        adjust length to match ``inpt`` length; default
                        is ``[[]]``
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all ``selections`` couples,
                        increasing ``inpt`` and other parameters length;
                        default is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_distance``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Distance`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Distance : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute distance between residues, halve the frames
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.distance(traj,step=2,labels=['first','last'],
        ...                       selections=['resid 1','resid 20'])
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.distance('unfolding*',step=2,
        ... labels=['first','last'],selections=['resid 1','resid 20'])
        >>>
        >>> # compute distance between residues, projected on z axis
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.distance(traj,axis='z',labels=['first','last'],
        ...                       selections=['resid 1','resid 20'])
        >>>
        >>> # split dist computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.native(['equilibration']*100,ref='ref',
                              selections=['resid 1','resid 20'],
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _distance(self,*inpt,**args)
    
    ###########################################################################
    ###################### FRACTION OF NATIVE CONTACTS ########################
    ###########################################################################
    
    def native(self,*inpt,**args):
        
        """
        Compute fraction of native contacts time series from input
        trajectories. See ``SimSet.analysis.Native`` for details.
        Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; if file not
                        found: take existing files with extension
                        according to priority pdb > gro; if still
                        no reference file is found: take related input
                        if a pdb/gro file; if still no reference is
                        found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'sig'       : ``(n_of_couples,2)``-shaped array of int, alias
                        for below, lower priority
          'signature' : ``(n_of_couples,2)``-shaped array of int,
                        atoms couples the computation has to be
                        restricted to; default is ``[[]]`` which means:
                        look for contacts among all couples matching
                        ``cutoff`` and ``distance`` parameters
          'cutoff'    : float, cutoff for native contacts (A); default
                        is ``SimSet.NATCUT``
          'distance'  : int, residues distance between contacts (A);
                        default is ``SimSet.DIST``
          'beta'      : float, coefficient in fraction of native
                        contacts computation; def is ``cutoff**(-2)``
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_native``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Native`` and
        ``SimSet.analysis.Cmap`` functions.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Native : function
        SimSet.analysis.Cmap : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute Q with respect to mol.pdb, 1/2 frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.native(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.native('unfolding*',step=2,output=None)
        >>>
        >>> # compute Q for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.native(traj,sel=[sel1,sel2],
        ...                     labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when above threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.native(traj,ref='prot',thres=0.5)
        >>>
        >>> # compute Q with respect to last trajectory frame
        >>> xvg = system.native(traj,ref='prot',frame=-1)
        >>>
        >>> # split Q computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.native(['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _native(self,*inpt,**args)
    
    ###########################################################################
    ####################### FRACTION OF HYDROGEN BONDS ########################
    ###########################################################################
    
    def hbonds(self,*inpt,**args):
        
        """
        Compute fraction of hydrogen bonds time series from input
        trajectories. See ``SimSet.analysis.Hbonds`` for details.
        Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; if file not
                        found: take existing files with extension
                        according to priority pdb > gro; if still
                        no reference file is found: take related input
                        if a pdb/gro file; if still no reference is
                        found: take ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'sig'       : ``(n_of_couples,-1)``-shaped array of int, alias
                        for below, lower priority
          'signature' : ``(n_of_couples,2)``-shaped array of int, donor-
                        -acceptor couples the computation has to be
                        restricted to; default is ``[[]]`` which means:
                        look for contacts among all couples matching the
                        following parameters through the ``SimSet.HBmap``
                        function
          'donors'    : list of ``(residue,donor,hydrogen)`` strings
                        tuple which specifies ``donor,hydrogen``
                        selections inside ``residue``; default is
                        ``SimSet.DONORS``
          'acceptors' : list of ``(residue,acceptor)`` tuple, which
                        specifies ``acceptor`` selections inside
                        ``residue``; default is ``SimSet.ACCEPTORS``
          'cutoff'    : float, distance cutoff for hydrogen bonds (A);
                        default is ``SimSet.HBCUT``
          'angle'     : float, angle cutoff for hydrogen bonds (deg);
                        default is ``SimSet.ANGLE``
          'alpha'     : float, angles coefficient in fraction of native
                        contacts computation; default is ``0``.
          'beta'      : float, pos coefficient in fraction of native
                        contacts computation; def is ``cutoff**(-2)``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_native``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.HBonds`` and
        ``SimSet.analysis.HBmap`` functions.
        
        ``signature`` is processed by the ``SimSet.HBmap`` function so
        that only the native donors-acceptors couples are included,
        along with up to two hydrogen that may participate to that
        bond. If there is ambiguity, the one that is more likely to
        form a bond shall be chosen frame by frame.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.HBonds : function
        SimSet.analysis.HBmap : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute fraction of HB
        >>> # with respect to mol.pdb, 1/2 frames, don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.native(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.native('unfolding*',step=2,output=None)
        >>>
        >>> # compute fraction for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname G C'
        >>> sel2 = 'resname U A'
        >>> xvg = system.native(traj,sel=[sel1,sel2],
        ...                     labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when going above threshold, "rna.pdb" as ref
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.native(traj,ref='rna',thres=-0.5)
        >>>
        >>> # compute fraction with respect to last trajectory frame
        >>> xvg = system.native(traj,ref='rna',frame=-1)
        >>>
        >>> # split hb computation over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.hbonds(['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _hbonds(self,*inpt,**args)
    
    ###########################################################################
    ################################# RMSF ####################################
    ###########################################################################
    
    def rmsf(self,*inpt,**args):
        
        """
        Compute residue-wise rmsf from input trajectories. Regex
        matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; align trajectories to
                        reference; if file not found: take
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        residues : list of lists of str, ordered residues name of
                   selection, e.g. ['GLY','LYS',MET']
        rmsf : list of lists of floats, for each trajectory:
               ordered rmsf by residues of selection
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_rmsf``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Rmsf`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Rmsf : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute rmsf aligning to mol.pdb, 1/2 frames
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> RES,RMSF = system.rmsf(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> RES,RMSF = system.rmsf('unfolding*',step=2,output=None)
        >>>
        >>> # compute rmsf for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> RES,RMSF = system.rmsf(traj,sel=[sel1,sel2],start=-100)
        >>> # some elements in RMSF will be NaN, because not all
        >>> # residues are included in selection
        >>>
        >>> # stop when reached threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> RES,RMSF = system.rmsf(traj,ref='prot')
        """
        
        return _rmsf(self,*inpt,**args)
    
    ###########################################################################
    ################################## SASA ###################################
    ###########################################################################
    
    def sasa(self,*inpt,**args):
        
        """
        Compute Solvent-Accessible Surface Area time series from input
        trajectories. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; will adjust length to
                        match ``inpt``; if file not found: take existing
                        files with extension according to priority
                        tpr > pdb > gro; if still no reference file is
                        found: take related input if a pdb/gro file; if
                        still no reference is found: take ``self.molec``
                        pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float or array-
                        like of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int/float or array-like of int/float or array-
                        like of array-like of int/float, if int: the time
                        points indexes to be picked for each computed
                        observable; if float: pick the indexes
                        corresponding to the time points in ``frames``
                        (when available); if not specified: take all
                        frames according to ``start``, ``stop``, and
                        ``step`` options; if specified: higher priority
                        than ``start``, ``stop``, and ``step``; will
                        adjust length to match ``inpt`` length; default
                        is ``[[]]``
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections, increasing
                        ``inpt`` and other parameters length; default
                        is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_sasa``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Sasa`` function.
        
        See Also
        --------
        SimSet.analysis.Sasa : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # compute sasa with respect to mol.pdb, 1/2 frames,
        >>> # don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.sasa(traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.sasa('unfolding*',step=2,output=None)
        >>>
        >>> # compute sasa for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.sasa(traj,sel=[sel1,sel2],
        ...                   labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when reached threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.sasa(traj,ref='prot',thres=10)
        """
        
        return _sasa(self,*inpt,**args)
    
    ###########################################################################
    ########################### ARBITRARY COLVAR ##############################
    ###########################################################################
    
    def colvar(self,f,*inpt,**args):
        
        """
        Compute arbitrary colvar ``f`` time series from input
        trajectories. Regex matching is implemented.
        
        Parameters
        ----------
        f : function of atom positions (3,) ndarray, colvar
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output xvg filename; path is relative to
                        analysis directory; if no extension specified:
                        assign xvg extension; if "", ``None`` or
                        ``False``: don't save xvg; default is built
                        taking ``inpt`` prefix if ``inpt`` has just one
                        element, ``self.molec`` if it has more, and a
                        suffix based on the computed observable
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, pdb/gro structure file
                        to be taken as reference; align trajectories to
                        reference; if file not found: take
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'lab'       : str or array-like of str, alias for below,
                        lower priority
          'label'     : str or array-like of str, alias for below,
                        lower priority
          'labels'    : str or array-like of str, label assigned to
                        each observable in ``SimSet.xvg`` output object;
                        does not have to be of the same length of
                        ``selection``, but in that case: will print
                        a warning and adjust length to this;
                        default is built upon input ``selection``
          'title'     : str or array-like of str, ``SimSet.xvg``/
                        output title; default is built according to
                        ``self.title`` and observable name
          'start'     : int/float or array-like of int/float, if int:
                        first trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: first time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[0]``
          'stop'      : int/float or array-like of int/float, if int:
                        last trajectory frame to be considered; if
                        negative: start counting from last frame; if
                        floats: last time point of the trajectory to be
                        considered: will adjust length to match ``inpt``;
                        default is ``[-1]``
          'step'      : int/float or array-like of int/float, if int:
                        interval between frames; if float: minimum time
                        gap between frames; will adjust length to match
                        ``inpt``; default is ``[1]``
          'frame'     : int/float or array-like of int/float, if int:
                        ``inpt`` trajectory frame to be taken as
                        reference, with ``reference`` remaining there
                        just for atoms/residues/segments names; if
                        negative: start counting from last frame; if
                        float: trajectory time to be taken as reference
                        (when available); will adjust length to match
                        ``inpt``; higher priority than ``start``,
                        ``stop``, ``step``; default is ``[None]``, which
                        means: don't take ``frame`` into consideration
          'th'        : float or array-like of float: alias for below
          'thres'     : float or array-like of float: alias for below
          'threshold' : float or array-like of float: stop computing
                        when observable goes below ``thres`` value
                        (if ``thres`` >0) or when observable goes
                        above ``-thres`` value (if ``thres`` < 0); will
                        adjust length to match ``inpt``; default is
                        ``0.``
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; will adjust
                          length to match ``inpt``; default is
                          ``SimSet.PERSISTENCE``
          'recur'     : bool, alias for below, lower priority
          'recursive' : bool, if ``True``: for all ``inpt`` compute the
                        results for all possible selections
                        combinations, increasing ``inpt`` and other
                        parameters length; default is ``SimSet.RECUR``
          'memory'  : bool, pass to output ``xvg`` initialization
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        xvg : ``SimSet.xvg`` class with loaded results
        
        Notes
        -----
        The core of the function is found in ``SimSet.system_colvar``.
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.analysis.Colvar`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.analysis.Colvar : function
        SimSet.parse : function
        SimSet.ls : function
        SimSet.xvg : class
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: unfolding*.*
        >>> system.back('unfolding*')
        >>>
        >>> # define colvar
        >>> def f(x):
        >>>    # stuff
        >>>    return result
        >>>
        >>> # compute colvar with respect to mol.pdb, 1/2 frames,
        >>> # don't save
        >>> traj = [f'unfolding{i}' for i in range(10)]
        >>> xvg = system.colvar(f,traj,step=2,output=None)
        >>>
        >>> # equivalently, with regex matching
        >>> xvg = system.colvar(f,'unfolding*',step=2,output=None)
        >>>
        >>> # compute sasa for two different selections
        >>> # last 100 frames, save to default
        >>> sel1 = 'resname ALA'
        >>> sel2 = 'resname LYS'
        >>> xvg = system.colvar(f,traj,sel=[sel1,sel2],
        ...                     labels=['sel1','sel2'],start=-100)
        >>>
        >>> # stop when reached threshold, "prot.pdb" as reference
        >>> # because traj doesn't have all the system's atoms
        >>> xvg = system.colvar(f,traj,ref='prot',thres=10)
        >>>
        >>> # compute colvar with respect to last trajectory frame
        >>> xvg = system.colvar(f,traj,ref='prot',frame=-1)
        >>>
        >>> # split colvar comp. over a long trajectory in 100 jobs
        >>> # so to use more processors, and then merge the results
        >>> xvg = system.colvar(f,['equilibration']*100,ref='ref',
        ...                   out=None,start=range(0,100001-1000,1000),
        ...                   stop=[range(999,100001-1000,1000),100001],
        ...                   step=100).pack()
        """
        
        return _colvar(self,f,*inpt,**args)
    
    ###########################################################################
    ######################### TRAJECTORIES AVERAGE ############################
    ###########################################################################
    
    def average(self,*inpt,**args):
        
        """
        Compute average positions for a list of input files, either
        frame-wise, input files-wise, or both, depending to ``axis``
        option. Return a ``MDAnalysis.Universe`` with the results, and
        occasionally write them to an output file. Regex matching is
        implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``SimSet.DEF_EXT``
          'out'       : str, alias for below, lower priority
          'output'    : str, output filename; if no ``output`` is
                        specified: don't save; if no extension
                        provided: assign same extension as input
                        files (if they are all the same, otherwise
                        assign pdb extension); default is ``None``
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, atoms / residues /
                        segments names reference; will adjust length
                        to match ``inpt``; if file not found: take
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str or array-like of str, alias for below,
                        lower priority
          'select'    : str or array-like of str, alias for below,
                        lower priority
          'selection' : str or array-like of str, ``MDAnalysis``
                        selection to be taken in consideration; if
                        ``""``: take all atoms; if not ``recursive``:
                        will adjust length to match ``inpt``;  default
                        is ``SimSet.DEF_SEL``
          'start'     : int or array-like of int, first trajectories
                        frame; if negative: start counting from last
                        frames; will adjust length to match ``inpt``;
                        default is ``0``
          'stop'      : int or array-like of int, last trajectories
                        frame; if negative: start counting from last
                        frames; will adjust length to match ``inpt``;
                        default is ``-1`` (last frame)
          'step'      : int or array-like of int, interval between
                        frames; will adjust length to match ``inpt``;
                        default is ``1``
          'frame'     : int or array-like of int, or array-like of
                        array-like of int, alias for below
          'frames'    : int or array-like of int, or array-like of
                        array-like of int, trajectory frame to be
                        considered for computation; if array-like of
                        int: compute the average for each frame in
                        ``frame``; if array-like of array-like of int:
                        take a different set of frames for each input
                        trajectory (still, they must all have the same
                        length); will adjust length to match ``inpt``;
                        if specified, it has a higher priority than
                        ``start``, ``stop``, and ``step``; otherwise,
                        take all frames according to ``start``,
                        ``stop``, and ``step``; if negative values are
                        provided: start counting from last frame;
                        default is ``[[]]``
          'last'      : bool, if ``True``: include a final frame with
                        ``reference`` positions for ALL ``inpt`` files;
                        default is ``False``
          'axis'      : int or array-like of int, alias for below,
                        lower priority
          'axes'      : int or array-like of int; if ``0``: compute
                        averages frames-wise; if ``1``: compute
                        averages input files-wise; if ``[0,1]`` or
                        ``2``: compute a single average from all frames
                        and all positions; if not specified: don't do
                        averages; default is ``1`` when there is just
                        one input file, ``0`` when there is more than
                        one
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        universe : ``MDAnalysis.Universe`` object
        
        Notes
        -----
        If ``axes`` option is ``1``: ``start``, ``stop``, ``step``,
        and ``frame`` must be provided such that the number of frames
        are the same for each input trajectory, otherwise raise error.
        
        Atoms, residues, segments names will be the ones specified in
        the LAST input file provided, according to selection.
        
        Trajectories' time infos are lost in the process.
        
        The core of the function is found in ``SimSet.system_average``
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # compute average of all folding trajectories, halve frames
        >>> folding_traj = [f'folding{i}_{j}' for i in range(10)\
        ...                                   for j in range(10)]
        >>> system.average(folding_traj,ref='ref',
        ...                step=2,out='folding_avg')
        >>>
        >>> # equivalently, with regex matching
        >>> system.average('folding*',ref='ref',
        ...                step=2,out='folding_avg')
        >>>
        >>> # save the average of the even frames
        >>> # ...and that of the odd frames in a two-framed pdb
        >>> system.average('traj','traj',ref='ref',axis=1,
                           start=[0,1],step=2,out='even_odd.pdb')
        >>>
        >>> # compute the averages of folding1.xtc ... folding10.xtc
        >>> system.average('folding*.xtc',step=10,out='folding_avg')
        >>>
        >>> # visualize results
        >>> system.vmd('folding_avg',ref='ref',smooth=5)
        """
        
        return _average(self,*inpt,**args)
    
    ###########################################################################
    ######################### RATCHET CONTACT MAPS ############################
    ###########################################################################
    
    def cmap(self,*inpt,**args):
        
        """
        Compute (average) contact maps from a list of input files,
        either frame-wise, input files-wise, or both, depending to
        ``axis`` option. Occasionally write the results to an output
        file. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``["pdb","gro"]``
          'out'       : str, alias for below, lower priority
          'output'    : str, output filename; if no ``output`` is
                        specified and ``inpt`` length is one: match
                        input prefix; if ``inpt`` length is > 1: take
                        the ``SimSet.DEF_CMP``; if no extension
                        specified: take ".npy"; if ``None``: don't
                        write output
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, atoms / residues /
                        segments names reference; will adjust length to
                        match ``inpt``; if file not found: take
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str, alias for below, lower priority
          'select'    : str, alias for below, lower priority
          'selection' : str, ``MDAnalysis`` selection to be taken in
                        consideration WHEN COMPUTING THE SIGNATURE ON
                        THE FIRST ``reference`` FILE; if ``""``: take
                        all atoms; will adjust length to match ``inpt``;
                        default is ``SimSet.DEF_SEL``
          'sig'       : ``(n_of_couples,2)``-shaped array of int, alias
                        for below, lower priority
          'signature' : ``(n_of_couples,2)``-shaped array of int,
                        signature of the contact matrices, which means:
                        atoms couples to be taken into account; may be
                        a list or a tuple; may be a one-dimensional
                        list, which means: parse indexes pair-wise;
                        default is ``[[]]``, which means: compute
                        default signature
          'f'         : function, alias for below, lower priority
          'function'  : function used to compute bare cmap; just ONE
                        positional argument required (distance between
                        two atoms, ``r``); just ONE optional argument
                        required (characteristic length scale ``R``, in
                        Angstroms); default is ``SimSet.sigmoid``
          'start'     : int or array-like of int, first trajectories
                        frame; if negative: start counting from last
                        frames; will adjust length to match ``inpt``;
                        default is ``0``
          'stop'      : int or array-like of int, last trajectories
                        frame; if negative: start counting from last
                        frames; will adjust length to match ``inpt``;
                        default is ``-1`` (last frame)
          'step'      : int or array-like of int, interval between
                        frames; will adjust length to match ``inpt``;
                        default is ``1``
          'frame'     : int or array-like of int, or array-like of
                        array-like of int, alias for below
          'frames'    : int or array-like of int, or array-like of
                        array-like of int, trajectory frame to be
                        considered for computation; if array-like of
                        int: compute a contact map for each frame in
                        ``frame``; if array-like of array-like of int:
                        take a different set of frames for each input
                        trajectory (still, they must all have the same
                        length); will adjust length to match ``inpt``;
                        if specified, it has a higher priority than
                        ``start``, ``stop``, and ``step``; otherwise,
                        take all frames according to ``start``,
                        ``stop``, and ``step``; if negative values are
                        provided: start counting from last frame;
                        default is ``[[]]``
          'last'      : bool, if ``True``: include a final frame with
                        ``reference`` positions for ALL ``inpt`` files;
                        default is ``False``
          'axis'      : int or array-like of int, alias for below,
                        lower priority
          'axes'      : int or array-like of int; if ``0``: compute
                        averages frames-wise; if ``1``: compute
                        averages input files-wise; if ``[0,1]`` or
                        ``2``: compute a single average from all frames
                        and all positions; if not specified: don't do
                        averages; default is ``[]``
          'scale'     : float, ``function`` characteristic length scale,
                        in Angstroms; default is ``SimSet.CUTOFF``
          'cutoff'    : float, don't consider couples whose atoms are
                        further than ``horizon`` in Angstrom; default is
                        ``SimSet.HORIZON``
          'd'         : int, alias for below, lower priority
          'dist'      : int, alias for below, lower priority
          'distance'  : int, minimum residues distance for a couple of
                        atoms to be included in ``signature`` (if not
                        provided); default is ``SimSet.DIST``
          'prog'      : float or array-like of float, alias for below
          'progress'  : float or array-like of float, progress value
                        associated to each (averaged) contact map;
                        must have the same length as the number of
                        (averaged) contact maps; if ``last`` option
                        is ``True``: authomatically append 1 to
                        resulting vector
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        ctype  : str, type of the contact maps e.g. 'ABCC'
        unit   : float
        signat : ``(n_of_couples,2)``-shaped array of int, signature of
                 the contact matrices
        colvar : ``(n_of_cmaps)``-shaped array of float, progress value
                 associated to each cmap (0 to 1)
        lambdas: ``(n_of_cmaps)``-shaped array of float, smoothing
                 factor associated to each cmap
        cmap : list of array of floats, the (average) contact maps
               computed for each frame
        
        Notes
        -----
        It is very stupid to compute average cmaps instead of computing
        cmaps on average positions. ``Axes`` options is here for
        completeness, and because the ``cutoff`` option makes the two
        operations slightly different, when considering contacts that
        fluctuate around the cutoff value.
        
        The signature indexes are the same for all ``inpt`` files.
        If ``axes`` option is ``1``: ``start``, ``stop``, ``step``,
        and ``frame`` must be provided such that the number of frames
        are the same for each input trajectory, otherwise raise error.
        
        Returned list ``cmap`` may be saved to / loaded from file later
        with the aid of ``write_cmap``, ``read_cmap`` functions.
        
        The core of the function is found in ``SimSet.system_cmap``
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.write_cmap : function
        SimSet.read_cmap  : function
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # after equilibration: compute cmap for protein
        >>> system.cmap(sel='protein')
        >>>
        >>> # self consistent path sampling: compute average cmap for
        >>> # all the folding trajectories
        >>> folding_traj = [f'folding{i}_{j}' for i in range(10)\
        ...                                   for j in range(10)]
        >>> system.cmap(folding_trajectories,sel='protein',
        ...             frames=range(0,500,100)) # 5 frames
        >>>
        >>> # equivalently, with regex matching
        >>> system.cmap('folding*',sel='protein',
        ...             frames=range(0,500,100))
        """
        
        return _cmap(self,*inpt,**args)
    
    ###########################################################################
    ############################# PATH SIMILARITY #############################
    ###########################################################################
    
    def order(self,*inpt,**args):
        
        """
        Compute order of native contacts fomation a list of input
        trajectories. Regex matching is implemented.
        
        Parameters
        ----------
        inpt : tuple of str/array-like of str, input filenames; if
               empty: ``self.last`` is taken as prefix; if no extension
               is provided: look for existing files according to the
               ``ext`` option; regular expressions characters "*" and
               "?" are also accepted; if no input is found: raise error
        args: dict, optional arguments
          'ext'       : str or array-like of str, allowed file
                        extensions for extension-less ``inpt`` elements;
                        for each of those elements: look for existing
                        files with ``ext`` as extensions, and stop at
                        the first one found; if none is found: print a
                        warning; default is ``["xtc","trr","dcd"]``
          'out'       : str, alias for below, lower priority
          'output'    : str, output filename for order matrix; path is
                        relative to analysis directory; if no
                        ``output`` is specified and ``inpt`` length is
                        one: match input prefix; if ``inpt`` length is
                        > 1: take the ``SimSet.DEF_ORD``; if no
                        extension is specified: take ".npy"; if
                        ``None``: don't write output
          'mol'       : str or array-like of str, alias for below,
                        lower priority
          'ref'       : str or array-like of str, alias for below,
                        lower priority
          'reference' : str or array-like of str, atoms / residues /
                        segments names reference; will adjust length to
                        match ``inpt``; if file not found: take
                        existing files with extension according to
                        priority pdb > gro; if still no reference file
                        is found: take related input if a pdb/gro file;
                        if still no reference is found: take
                        ``self.molec`` pdb/gro
          'sel'       : str, alias for below, lower priority
          'select'    : str, alias for below, lower priority
          'selection' : str, ``MDAnalysis`` selection to be taken in
                        consideration WHEN COMPUTING NATIVE CONTACTS ON
                        THE FIRST ``reference`` FILE; if ``""``: take
                        all atoms; will adjust length to match ``inpt``;
                        default is ``SimSet.DEF_SEL``
          'sig'       : ``(n_of_couples,2)``-shaped array of int, alias
                        for below, lower priority
          'signature' : ``(n_of_couples,2)``-shaped array of int,
                        signature of the contact matrices, which means:
                        atoms couples to be taken into account; may be
                        a list or a tuple; may be a one-dimensional
                        list, which means: parse indexes pair-wise;
                        default is ``[[]]``, which means: compute
                        default signature
          'start'     : int or array-like of int, first trajectories
                        frame used to check contacts; if negative:
                        start counting from last frame; will adjust
                        length to match ``inpt``; default is ``0``
          'stop'      : nt or array-like of int, last trajectories
                        frame used to check contacts; if negative:
                        start counting from last frame; will adjust
                        length to match ``inpt``; default is ``-1``
          'step'      : int or array-like of int, interval between
                        frames used to check contacts; will adjust
                        length to match ``inpt``; default is ``1``
          'frame'     : int or array-like of int, or array-like of
                        array-like of int, alias for below
          'frames'    : int or array-like of int, or array-like of
                        array-like of int, trajectory frame to be
                        considered for computation; if array-like of
                        int: check contacts for each frame in
                        ``frame``; if array-like of array-like of int:
                        take a different set of frames for each input
                        trajectory (still, they must all have the same
                        length); will adjust length to match ``inpt``;
                        if specified, it has a higher priority than
                        ``start``, ``stop``, and ``step``; otherwise,
                        take all frames according to ``start``,
                        ``stop``, and ``step``; if negative values are
                        provided: start counting from last frame;
                        default is ``[[]]``
          'cutoff'    : float, maximum distance between atoms couples
                        in signature; default is ``SimSet.NATCUT``
          'd'         : int, alias for below, lower priority
          'dist'      : int, alias for below, lower priority
          'distance'  : int, minimum residues distance for a couple of
                        atoms to be included in ``signature`` (if not
                        provided); default is ``SimSet.DIST``
          'pers'       : float, alias for below, lower priority
          'persistence': float, min consecutive time a contact has to
                         be present to assess its formation (ps); default
                         is ``SimSet.PERSISTENCE``
          'compress'  : bool, alias for below, lower priority
          'compressed': bool, compress output matrices to a single
                        integer to save space; default is ``True``
          'verbose' : bool, be loud, default is ``SimSet.VERBOSE``
        
        Returns
        -------
        order : if not ``compressed``: array of array of float,
                (linearized) path similarity matrices; otherwise:
                array of integer: each one is a compressed path
                similarity matrix
        
        Notes
        -----
        The signature indexes are the same for all ``inpt`` files.
        Frames don't have to be the same for all input files.
        
        (Uncompressed) prder matrices elements are either -1 (contact
        in row forms before contact in column), or +1 (contact in
        column forms before ontact in row), or 0 (the two contacts
        form at the same time).
        
        Let ``n`` be the length of ``inpt``, ``nc`` be the number
        of contacts to be assessed in the computation. Suppose
        ``computation`` is ``False``: then ``order`` is a ``(n,nc)``
        array, the rows corresponding to a linearized order matrix for
        each input. It can be read by ``SimSet.read_order`` function.
        The program calls ``SimSet.write_order`` function.
        
        Suppose ``computation`` is ``True``: then ``order`` is a
        ``n``-size array, the elements corresponding to a compressed
        order matrix for each input. It can be read by
        ``SimSet.read_order`` function, with optional argument
        ``compressed=True``. The program calls ``SimSet.write_order``.
        
        Path similarity between order matrices can be computed
        through ``SimSet.Similarity`` function.
        
        The core of the function is found in ``SimSet.system_order``
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        
        Implemented multiprocessing according to ``SimSet.PROCESSES``
        value.
        
        See Also
        --------
        SimSet.parse : function
        SimSet.ls : function
        SimSet.encode_order : function
        SimSet.decode_order : function
        SimSet.write_order : function
        SimSet.read_order  : function
        SimSet.Similarity: function
        
        Examples
        --------
        >>> import SimSet
        >>> system = SimSet.system() # load def system in current dir
        >>>
        >>> # compute order matrices for all folding trajectories
        >>> folding_traj = [f'folding{i}_{j}' for i in range(10)\
        ...                                   for j in range(10)]
        >>> order = system.order(folding_trajectories,sel='protein')
        >>>
        >>> # equivalently, with regex matching
        >>> order = system.order('folding*',sel='protein')
        """
        
        return _order(self,*inpt,**args)
    
    ###########################################################################
    ############################ BIAS FUNCTIONAL ##############################
    ###########################################################################
    
    def functional(self,*inpt,**args):
        
        """
        Find the bias functional linked to the folding events of a list
        of input trajectories. The folding events are triggered by the
        data found in provided xvg objects / files.

        Parameters
        ----------
        inpt : tuple of str/array-like of str, ratchet log files of
               input trajectories; if empty: take ``self.last`` as
               prefix; if the extension is not present, look for
               existing files with rat.out extension; if at least one
               of the ``inpt`` files do not exist: print warning;
               regular expressions characters "*" and "?" are also
               accepted; if no input is found: raise error
        args: dict, optional arguments
          'start'     : int/float or array-like of int/float; if int:
                        for each ratchet log loaded: first datapoint
                        to be considered (NaNs included); if negative:
                        start counting from last point; if float: for
                        each ratchet log loaded selected: first time
                        point to be considered; will adjust length to
                        match number of ``inpt`` files; default is
                        ``[0]``
          'stop'      : int/float or array-like of int/float; if int:
                        for each ratchet log loaded: last datapoint to
                        be considered (NaNs included); if negative:
                        start counting from last point; if float: for
                        each observable selected: first time point to
                        be considered; will adjust length to match
                        number of ``inpt`` files; default is ``[-1]``
          'step'      : int/float or array-like of int/float; if int:
                        for each ratchet log loaded: interval between
                        data points frames (NaNs included); if float:
                        for each observable selected: minimum time
                        interval between frames; will adjust length to
                        math number of ``inpt`` files; default is
                        ``[0]``
          'frame'     : int or array-like of int/float or array-like
                        of array-like of int/float, alias for below,
                        lower priority
          'frames'    : int or array-like of int/float or array-like
                        of array-like of int/float, if int: the time
                        points indexes to be picked for each ratchet
                        log loaded; if float: take indexes corresponding
                        to the time points in ``frames``; if not
                        specified: take all frames according to
                        ``start``, ``stop``, and ``step`` options; if
                        specified: higher priority than ``start``,
                        ``stop``, and ``step``; will adjust length to
                        match number of ratchet logs loaded; default
                        is ``[[]]``
          'x'         : alias for below, lower priority
          'xvg'       : ``SimSet.xvg`` objects / str (xvg filenames)
                        or tuple of array-like of int/float; those
                        will be loaded in a unique ``SimSet.xvg``
                        object that will be taken as reference to
                        assess folding events according to the
                        ``threshold`` option
          'col'       : str/int or array-like of str/int, alias for
                        below, lower priority
          'column'    : str/int or array-like of str/int, alias for
                        below, lower priority
          'columns'   : str/int or array-like of str/int, full
                        observables names in ``xvg`` to be taken
                        into consideration to assess folding, same
                        syntax as the ``column`` input of the method
                        ``SimSet.xvg.select``
          'th'        : float or array-like of float, alias for below,
                        lower priority
          'thres'     : float or array-like of float, alias for below,
                        lower priority
          'threshold' : float or array-like of float, ordered list of
                        thresholds required for each ``inpt`` file to
                        assess folding; each threshold refers to a
                        unique observable linked to each ``inpt`` file;
                        will adjust length to match the number of
                        unique observables linked to each ``inpt``file;
                        if ``t > 0``: look for the time when related
                        observable goes below ``t``; if ``t < 0``: look
                        for the time when related observable goes above
                        ``-t``, if 0: do nothing; all thresholds
                        conditions must be met at the same time to
                        assess folding
          'pers'        : float or array-like of float: alias for below
          'persistence' : float or array-like of float: threshold has
                          to be crossed for at least ``persistence`` ps
                          before activating the trigger; each
                          persistence refers to a unique observable
                          linked to each ``inpt`` file; will adjust
                          length to match the number of unique
                          observables linked to each ``inpt``file;
                          default is ``SimSet.PERSISTENCE``
          'verbose'  : bool, if ``True`` be loud and noisy, and print a
                       final RECAP of the computation; default is
                       ``SimSet.VERBOSE``
        ``args`` are also passed to ``SimSet.xvg`` object
        initialization, and to ``SimSet.xvg.select`` method, with
        ``nan`` options set to ``False`` by default; ``start``, 
        ``stop``, ``step``, ``frame``, and ``frames`` are relative to
        ``SimSet.xvg.select``.
        
        Returns
        -------
        result: ``(-1,2)``-shaped array of float, each row containing:
                the folding time and the bias functional for each
                ``inpt`` file; if folding conditions are not met, the
                row is ``[numpy.inf,numpy.NaN]``
        
        Notes
        -----
        If no ``xvg`` or no ``threshold`` options are provided, all
        ``inpt`` trajectories will be considered folding trajectories
        with folding time equal to the last point of the trajectory
        minus the first.
        
        In a preliminary fase, the program loads the ``xvg`` object
        required to assess a folding event and selects the required
        columns. Then it assigns to each ``inpt`` file a specific list
        of the ``xvg`` columns, according to the following: let ``n``
        be the length of ``inpt``, then the ``i``-th selected column
        goes to the ``i%n``-th ``input`` file. If the total number of
        columns is not a multiple of ``n``, the last columns will be
        cut out.
        
        The core of the function is found in
        ``SimSet.system_functional``.
        
        Argument parsing uses ``SimSet.parse`` function.
        Lists' depth is reduced to one through ``SimSet.ls`` function.
        The program calls ``SimSet.xvg`` class and
        ``SimSet.xvg.select`` method.
        
        See Also
        --------
        SimSet.xvg : class
        SimSet.xvg.select : method
        SimSet.parse : function
        SimSet.ls : function
        
        Examples
        --------
        >>> import SimSet, numpy
        >>> system = SimSet.system() # load def system in current dir
        >>> # loaded default mdp file parameters
        >>>
        >>> # rsync back results of simulations: folding*.*
        >>> system.back('folding*')
        >>>
        >>> # assess folding when native contacts gets above 0.9 wrt
        >>> # native configuration "rna.pdb", and rmsd gets below 5
        >>> traj = [f'folding{i}' for i in range(10)]
        >>> xvg = system.native(traj,ref='rna')    # compute Q for all
        >>> xvg.merge(system.rmsd(traj,ref='rna')) # compute rmsd f.a.
        >>>
        >>> # compute bias functional
        >>> # a trajectory folds when Q > .8 and rmsd < 5
        >>> # for at least 10 ps
        >>> result = system.functional(tra,xvg,
        ...          obs=['Q','rmsd'],th=[-.8,5],persistence=10)
        >>>
        >>> # rearrange results to TIME, BIAS arrays
        >>> TIME,BIAS = result.reshape((2,-1))
        >>>
        >>> # find "best" trajectory (lower bias functional)
        >>> best = traj[numpy.argmin(BIAS)]
        """
        
        return _functional(self,*inpt,**args)
