# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _write(self,output,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    cursor.show()
    
    ###########################################################################
    ################################ PARSE OPTIONS ############################
    ###########################################################################
    
    # defaults
    def_tit = lcss(self.title).replace('_',' ').replace(': ',' ')
    if def_tit:
        if def_tit[-1]==' ':
            def_tit = def_tit[:-1]
        if def_tit[0] ==' ':
            def_tit = def_tit[1:]
    def_xax = lcss(self.xaxis)
    def_xax = '' if def_xax in '()' else def_xax
    def_yax = lcss(self.yaxis)
    def_yax = '' if def_yax in '()' else def_yax
    def_lab = []
    
    # parameters initialization
    title = None
    xaxis = None
    yaxis = None
    
    # devoid "args" of main options, pass the rest to "self.select"
    if 'title' in args:
        title = args.pop('title')
    if 'xaxis' in args:
        xaxis = args.pop('xaxis')
    if 'axis'  in args:
        yaxis = args.pop('axis')
    if 'yaxis' in args:
        yaxis = args.pop('yaxis')

    # verbose
    verbo = parse(args,'verbose',VERBOSE)
    args.pop('verbose')
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # output
    output = '.'.join(extension(output,'xvg'))
    
    # make directory if it does not exist
    directory = '/'.join(output.split('/')[:-1])
    if directory: sh(f'mkdir -p {directory}')
    
    # title
    if title is None:
        title = def_tit
    
    # xaxis
    if xaxis is None:
        xaxis = def_xax
    
    # yaxis
    if yaxis is None:
        yaxis = def_yax
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # select subset of ``self``
    x = self.select(*columns,**args,verbose=False)
    times = x.get_times()
    datap = x.get_datap()
    M = len(times)
    K = len(datap)
    if verbo: print(f'WRITING {K} columns')
    
    # padding
    padA = XVG_DIG-1
    padB = XVG_DIG+6
    
    # write data to xvg file
    with open(output,'w') as f:
        f.write(f'# Created with SimSet.xvg tool\n')
        f.write(f'@    title "{title}"\n')
        f.write(f'@    xaxis   label "{xaxis}"\n')
        f.write(f'@    yaxis   label "{yaxis}"\n')
        f.write(f'@TYPE xy\n')
        f.write(f'@ view 0.15, 0.15, 0.75, 0.85\n')
        f.write(f'@ legend on\n')
        f.write(f'@ legend box on\n')
        f.write(f'@ legend loctype view\n')
        f.write(f'@ legend length {K+1}\n')
        f.write(f'@ s0 legend "time"\n')
        for k,lab in enumerate(x.obser):
            f.write(f'@ s{k+1} legend "{lab}"\n')
        for i in range(M):
            values = [f'{str(f"{times[i]:.{padA}e}"):>{padB}}']
            for k in range(K):
                values.append(f'{str(f"{datap[k][i]:.{padA}e}"):>{padB}}')
            f.write(' '.join(values)+'\n')
    
    # verbose output
    if verbo: print(f'SAVED {self.__repr__()} to "{path_name(output)}"')
    
    return K
