# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _cmap(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.cmap" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ############################### PARSE OPTIONS #############################
    ###########################################################################
    
    # defaults
    inpt = ls(inpt,str)
    def_ext = ['pdb','gro']
    def_out = extension(inpt[0])[0] if len(set(inpt))==1 else\
              lcss(inpt) if lcss(inpt) else self.last
    def_out = def_out.replace('?','').replace('*','')
    def_ref = ['']
    def_sel = DEF_SEL
    def_sig = [[]]
    def_fun = sigmoid
    def_sta = [ 0]
    def_sto = [-1]
    def_ste = [ 1]
    def_fra = [[]]
    def_axi =  []
    def_sca = SCALE
    def_cut = HORIZON
    def_dis = DIST
    def_col =  []
    
    # empties - to be returned in case of failure
    ctype  = 'ABCC'
    unit   = def_sca
    signature = array([[]],dtype=int)
    tmp_col= array([],dtype=float)
    tmp_lam= array([],dtype=float)
    cmap   = array([[]],dtype=float)
    
    # specific parameters (except axes)
    try:
        exten  = parse(args,'ext'      ,def_ext)
        output = parse(args,'out'      ,def_out)
        output = parse(args,'output'   ,output )
        refere = parse(args,'mol'      ,def_ref)
        refere = parse(args,'ref'      ,refere )
        refere = parse(args,'ref'      ,def_ref)
        refere = parse(args,'reference',refere )
        select = parse(args,'sel'      ,def_sel)
        select = parse(args,'select'   ,select )
        select = parse(args,'selection',select )
        signat = parse(args,'sig'      ,def_sig)
        signat = parse(args,'signature',signat )
        funct  = None
        if 'f' in args and type(args['f']) in FUNCTION:
            funct = args['f']
        if 'function' in args and type(args['function']) in FUNCTION:
            funct = args['function']
        if not funct:
            funct = def_fun
        start  = parse(args,'start'    ,def_sta)
        stop   = parse(args,'stop'     ,def_sto)
        step   = parse(args,'step'     ,def_ste)
        frame  = parse(args,'frame'    ,def_fra)
        frame  = parse(args,'frames'   ,frame  )
        final  = parse(args,'last'     ,False  )
        axis   = parse(args,'axis'     ,def_axi)
        axis   = parse(args,'axes'     ,axis   )
        scale  = parse(args,'scale'    ,def_sca)
        cutoff = parse(args,'cutoff'   ,def_cut)
        dista  = parse(args,'d'        ,def_dis)
        dista  = parse(args,'dist'     ,dista  )
        dista  = parse(args,'distance' ,dista  )
        colvar = parse(args,'prog'     ,def_col)
        colvar = parse(args,'progress' ,colvar )
        unit   = scale
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # default input
    found = False
    for ext in exten:
        fname = f'{self.last}{"."*bool(ext)}{ext}'
        if isfile(fname):
            def_inp = [fname]
            found = True
            break
    if not found:
        for ext in exten:
            fname = f'{self.molec}{"."*bool(ext)}{ext}'
            if isfile(fname):
                def_inp = [fname]
                found = True
                break
    
    # no input
    if not len(inpt):
        if not found:
            descr = f'ERROR: no input files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return xvg()
        inpt = def_inp
        descr = f'TAKING "{path_name(def_inp[0])}" as input file\n'
        comment(descr,self.logre,self.logsh,verbo)
    
    # process input
    line = False # formatting issues
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or '*' in exten or '?' in exten:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'WARNING: no files found\n'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: warning
        line = True
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'WARNING: none of {names} found'
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
        continue
    
    # reconstruct input
    inpt = ls(inpt)
    n = len(inpt)
    if not n:
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    elif verbo and line: print()
    
    # output
    if output:
        output = ".".join(extension(output,'cmp'))
    else:
        output = ''
    
    # reference - check if ok
    refere = ls(refere,str)
    if not len(refere):
        descr = f'ERROR: bad "reference" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    
    # signature
    if not signat or type(signat) not in ITERABLE:
        signat = def_sig
    temp = [] # adjust formatting - convert in couples
    for sig in signat:
        if type(sig) in INTEGER:
            temp.append(sig)
        elif type(sig) in ITERABLE and len(sig)>=2:
            temp.append(sig[ 0])
            temp.append(sig[-1])
    signat = [temp[i:i+2] for i in range(0,len(temp),2)]
    
    # function - implement scale and cutoff
    def function(x):
        return funct(x/scale) if x < cutoff else 0
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # colvar
    colvar = ls(colvar,int,float)
    
    # adjust lengths
    refere = adjust_list(refere,n)
    start  = adjust_list(start, n)
    stop   = adjust_list(stop,  n)
    step   = adjust_list(step,  n)
    frame  = adjust_list(frame, n)
    
    # reference list
    line = False # formatting issues
    for i,ref in enumerate(refere):
        if ref == '.'.join(extension(ref)):
            if isfile(ref):
                continue
        
        # match required extensions
        found = False
        for ext in REF_EXT:
            fname = f'{ref}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if found:
            continue
        
        # input file is also reference
        if extension(inpt[i])[1] in REF_EXT:
            refere[i] = inpt[i]
            continue
        
        # look for alternatives
        found = False
        inp = extension(inpt[i])[0]
        for ext in REF_EXT:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if not found:
            for ext in REF_EXT:
                fname = f'{self.molec}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    refere[i] = fname
                    found = True
                    break
        
        # found alternative
        if found:
            line = True
            descr = f'WARNING: no reference specified '\
                    f'for input file "{path_name(inpt[i])}", '\
                    f'assigning "{path_name(refere[i])}" instead'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # found no alternatives
        descr = f'ERROR: no reference found '\
                f'for input file "{path_name(inpt[i])}"\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    if verbo and line: print()
    
    # axes
    temp = ls(axis,int,float)
    axis = 0
    for i in range(3):
        if i in temp:
            axis+= i+1
    
    # process time options
    temp,frame = frame,[]
    numframes  = 0
    for inp, sta,  sto, ste, fra in\
    zip(inpt,start,stop,step,temp):
        
        # retrieve trajectory infos
        universe = Universe(inp)
        N = len(universe.trajectory)
        dt = universe.trajectory.dt
        T0 = universe.trajectory.time
        raw_times = []
        
        # process start
        if type(sta) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sta = search_bigger(raw_times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sto = search_smaller(raw_times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frames accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra = [sta]
                for i,t in enumerate(raw_times[sta:sto]):
                    if t-raw_times[frames[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
        
        # process already existing "frames"
        else:
            tmp,fra = fra,[]
            for f in tmp:
                if type(f) in FLOAT:
                    if len(raw_times) != N:
                        raw_times = [t.time for t in universe.trajectory]
                    f = search_equal(raw_times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        
        # check frames
        if not len(fra):
            descr = f'ERROR: no selected frames for "{inp}"\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return ctype,unit,signature,tmp_col,tmp_lam,cmap
        if frame:
            if axis==1 and len(frame[-1])!=len(fra):
                descr = f'ERROR: selected frames for "{inp}" are '\
                        f'{len(fra)} != {len(frame[-1])} for the previous\n'
                descr = ansi(descr,'red')
                comment(descr,self.logre,self.logsh,verbo)
                return ctype,unit,array(signat,ndmin=2),tmp_col,tmp_lam,cmap
        frame.append(fra)
        numframes+= len(fra)
    
    # target signature
    signature,r0 = Cmap(refere[0],select,signat,inf,dista,verbo)
    if not signature.shape[-1]:
        descr = f'ERROR: empty target signature array\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    
    # reference contact maps
    last_cmap = {}
    if final:
        for ref in set(refere):
            descr = f'BUILDING contact map for reference file "{ref}"\n'
            comment(descr,self.logre,self.logsh,verbo)
            positions = Universe(refere[0]).atoms.positions
            last_cmap[ref] = get_cmap(positions,signature,function,verbo)
    
    ###########################################################################
    ######################### INITIALIZE MULTIPROCESSING ######################
    ###########################################################################
    
    # multiprocessing global variables
    current = Manager().Value('i',0 ) # current iteration
    lock = Manager().Lock()
    
    # multiprocessing task
    def task(index,inpt,refere,frame):
        
        # sub result initialization
        if not  axis: # no average
            length = sum([len(fra) for fra in frame])
        elif axis==1: # frames-wise
            length = len(frame[0])
        elif axis==2: # input files-wise
            length = len(inpt)
        else:         # both
            length = 1
        cmap = array([[0.]*len(signature)]*length)
        
        # global index initialization
        k = -1
        
        # loop over input files
        for           i,     inp, ref,   fra in\
        zip(range(len(inpt)),inpt,refere,frame):
            
            # load universe - check if ok
            try:
                universe  = Universe(ref,inp)
            except:
                descr = f'unable to load universe with input file '\
                        f'"{inp}" and reference "{ref}"\n'
                raise Exception(descr)
            
            # loop over frames
            for j,t in enumerate(fra):
                k+= 1
                universe.trajectory[t]
                positions = universe.atoms.positions
                if not  axis: # no average
                    cmap[k]+= get_cmap(positions,signature,function,
                                      (numframes==1)*verbo)
                elif axis==1: # frames-wise
                    cmap[j]+= get_cmap(positions,signature,function,
                                      (numframes==1)*verbo)
                elif axis==2: # input files-wise
                    cmap[i]+= get_cmap(positions,signature,function,
                                      (numframes==1)*verbo)
                else:         # both
                    cmap[0]+= get_cmap(positions,signature,function,
                                      (numframes==1)*verbo)
                
                # measure & print progress
                if not verbo or numframes==1:
                    continue
                lock.acquire()
                try:     current.set(current.get()+1)
                finally: lock.release()
                elapsed = now()-t0
                speed = current.value/elapsed
                etr = (numframes-current.value)/speed
                bar = progress_bar(current.value,numframes,
                                   elapsed,etr,speed,'frames',width)
                print(f'{bar}\r',end='')
            
            # reference contact map
            if final:
                k+= 1
                if not  axis: # no average
                    cmap[k  ]+= last_cmap[ref]
                elif axis==1: # frames-wise
                    cmap[j+1]+= last_cmap[ref]
                elif axis==2: # input files-wise
                    cmap[i  ]+= last_cmap[ref]
                else:         # both
                    cmap[  0]+= last_cmap[ref]
        
        # task output
        return index,cmap
    
    ###########################################################################
    ############################# RUN MULTIPROCESSING #########################
    ###########################################################################
    
    # distribute parameters among processes
    span1  = n//PROCESSES+(n<PROCESSES)
    span2  = n%PROCESSES*(n>PROCESSES)
    params = []
    index,first = 0,0
    while first < n:
        last = min(n,first+span1+(span2>0))
        params.append([index,inpt[first:last],
        refere[first:last], frame[first:last]])
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(params)
    params = list(map(list,zip(*params)))
    # now ``params[i][0],...,params[i][j]`` are the inputs of process i
    
    # reset pool of processes
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes)
    descr = f'\nRUNNING on {processes} core{"s"*(processes!=1)}'
    comment(descr,self.logre,self.logsh,verbo)
    
    # initialize progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,numframes,0,etr,speed,'frames',width)
    if verbo and numframes>1: print(f'{bar}\r',end='')
    
    # run pool of processes
    try:
        raw = list(pool.uimap(task,*params))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return ctype,unit,signature,tmp_col,tmp_lam,cmap
    
    # terminate pool and progress bar
    pool.join()
    pool.clear()
    cursor.show()
    if numframes>1:
        elapsed = now()-t0
        speed = numframes/elapsed
        etr = 0
        bar = progress_bar(numframes,numframes,
                           elapsed,etr,speed,'frames',width)
        comment(f'{bar}\n',self.logre,self.logsh,verbo)
    
    ###########################################################################
    ################################ PROCESS OUTPUT ###########################
    ###########################################################################
    
    # reconstruct output
    if axis in [0,2]:
        cmap = []
        for sorted_raw in sorted(raw,key=lambda x:x[0]):
            cmap.append(sorted_raw[1])
        cmap = array(cmap)
        cmap = cmap.sum(axis=1) if axis==2 else\
               cmap.reshape((-1,*cmap.shape[2:]))
    else:
        cmap = array([raw[1] for raw in raw]).sum(axis=0)
    
    # compute average cmaps
    if   axis==1:
        descr = f'COMPUTING {len(fra)} average contact '\
                f'map{"s"*(len(fra)!=1)} from {n} input file{"s"*(n!=1)}'
        comment(descr,self.logre,self.logsh,verbo)
        cmap = cmap/n
    elif axis==2:
        descr = f'COMPUTING {n} average contact map{"s"*(n!=1)} '\
                f'from {numframes} selected frame{"s"*(numframes!=1)}'
        comment(descr,self.logre,self.logsh,verbo)
        for j,fra in enumerate(frame):
            cmap[j] = cmap[j]/(len(fra)+final)
    elif axis>=3:
        descr = f'COMPUTING 1 average contact map from {numframes} selected'\
                f' frame{"s"*(numframes!=1)} of {n} input file{"s"*(n!=1)}'
        comment(descr,self.logre,self.logsh,verbo)
        cmap = cmap/(numframes+n*final)
    
    # adjust colvar
    N = len(cmap)
    if N > 1:
        if not colvar:
            colvar = [i/(N-1) for i in range(N)]
        else:
            final = colvar[-1]
            L = len(colvar)
            for i in range(1,N-len(colvar)+1):
                colvar.append(final+(1-final)*i/(N-L))
            colvar = colvar[:N]
        
        # adjust lambdas
        lambdas = [1/(linalg.norm(cmap[i]-cmap[i+1])**2)\
                  for i in range(N-1)]
        lambdas.append(lambdas[-1])
    
    # just one frame
    else:
        colvar  = [1]
        lambdas = [1/(linalg.norm(cmap[0])**2)]
    lambdas   = array(lambdas,dtype=float)
    colvar    = array(colvar ,dtype=float)
    
    # save binary cmaps
    if output:
        descr = f'SAVING binary contact maps to "{path_name(output)}"\n'
        comment(descr,self.logre,self.logsh,verbo)
        write_cmap(output,ctype,unit,signature,colvar,lambdas,cmap)
    
    return ctype,unit,signature,colvar,lambdas,cmap
