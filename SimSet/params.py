###############################################################################
################################## CURRENT PATH ###############################
###############################################################################

from os.path import abspath
PATH = '/'.join(f'{abspath(__file__)}'.split('/')[:-1])

###############################################################################
############################ SYSTEM INITIALIZATION ############################
###############################################################################

# default output name
DEF_MOL = 'mol'
# default topology name
DEF_TOP = 'topol.top'
# default position restraints name
DEF_POS = 'posre.itp'
# default system title (in not found in "DEF_MOL" pdb/gro)'
DEF_TIT = 'system'
# default system output log
DEF_LOG = 'log.log'
# default system commands log
DEF_SHE = 'log.sh'
# default working directory
DEF_WKD = '.'
# default analysis directory
DEF_AND = '.'

###############################################################################
######################### SSH WITH CLUSTER, *CUSTOMIZE* #######################
###############################################################################

# default remote working directory
DEF_SHD = '.'
# default ssh address
DEF_SHA = 'gianmarco.lazzeri@hpc2.unitn.it'
# default ssh email
DEF_EMA = 'gianmarco.lazzeri@studenti.unitn.it'
# default remote path to Gromacs
DEF_GMX = '/home/gianmarco.lazzeri/gmx/bin'

###############################################################################
################################ SYSTEM GENERATION ############################
###############################################################################

# ignore hydrogens in pdb/gro source file
DEF_IGN = False
# default force field
DEF_FFD = 'ff14SB'
# default solvation box shape
DEF_BOX = 'cubic'
# default solvation box padding (nm)
DEF_DIS = [1.]
# default water model
DEF_WAT = 'tip3p'
# default ion names
DEF_ION = 'NA CL'
# default ions numbers to be added
DEF_NIO = [0,0]
# default ions concentrations to be added
DEF_MIO = [0.,0.]
# default Gromacs ionization mdp filename
MDP_ION = f'{PATH}/ionization.mdp'
# default Gromacs ionization mdp/tpr filenames
DEF_GEN = 'ionize'
# default Ambertools system generation fileanme
DEF_OPE = 'generate'

###############################################################################
################################# MD SIMULATION ###############################
###############################################################################

# default mdp filename
DEF_MDP = f'{PATH}/params.mdp'
# default velocity generation at the start
GEN_VEL = 'yes'
# default constraints utilization
DEF_CST = False
# default jobs nodes on cluster
DEF_NOD = 1
# default jobs processors-per-node on cluster
DEF_PRO = 16
# default jobs walltime on cluster
DEF_WAL = '5:50:00'
# default jobs queues to be used on cluster
DEF_QUE = ['short_gpuQ','short_cpuQ']
# default jobs allocated memory per node on cluster
DEF_MEM = ''
# max number of warnings in Gromacs commands execution
MAXWARN = 1

###############################################################################
######################### MD REMOTE CONTROL, *CUSTOMIZE* ######################
###############################################################################

# maximum allowed number of jobs
SSHRUN = 30
# maximum number of contemporay re-subs
SSHSUB = 8
# time before resetting paused nodes (sec)
RESET = 7200
# maximum waiting time before aborting/resubmitting (sec)
TOLERANCE = 300
# auto-update system pdb/gro after MD if there is no ambiguity
UPDATE = True
# auto-extend simulation if homonymous cpt file is found
EXTEND = True
# auto-send jobs on cluster if required
QSUB = True
# auto-sync local and remote working directory if required
SYNC = True
# auto-remote control up to the end of ALL submitted jobs
WAIT = True
# interval between jobs checking in auto-remote control mode (sec)
CHECK = 30
# interval of checkpoint in MD run on cluster (min)
CHECKPT = 3
# file of cached currently active jobs
ACTIVE = '.active'

###############################################################################
############################## DATA ANALYSIS/PROCESSING #######################
###############################################################################

# expand recursively selection/references in data analysis/processing
RECUR = True
# default Gromacs trjconv PBC settings (if not "aligning")
DEF_PBC = 'atom'
# default "gmx energy" observable to be loaded
DEF_OBS = 'Potential'
# default PCA data variance of saved components (max is 1)
VARIANCE = 0.95
# default contacts analysis minimum distance between atom-atom resids
DIST = 4
# default contacts analysis maximum atom-atom distance (nm)
NATCUT = 4.5
# default H-bonds analysis maximum hydrogen-acceptor distance (nm)
HBCUT = 3.
# default H-bonds analysis minimum donor-hydrogen-acceptor angle (degrees)
ANGLE = 120.
# default contact maps output filename if there are more than one input file
DEF_CMP = 'native.cmp'
# default contact maps generation characteristic atom-atom distance
SCALE = 7.5
# default maximum distance to include in a signature matrix
HORIZON = 12.3
# default order matrix output filename if there are more than one input file
DEF_ORD = 'order.npy'
# default minimum persistence time of a contact to assess its formation (ps)
PERSISTENCE = 5.

###############################################################################
############################ XVG DATA HANDLING/PLOTTING #######################
###############################################################################

# vectors in ram initialization
MEMORY = True
# nest file names while saving/loading/selecting new xvg file
NEST = False
# include NaNs while saving/loading/selecting new xvg file
DEF_NAN = False
# origin filename/observable name separator in saving/loading/selecting xvg
XVG_DIV = ' | '
# default number of digits for a data point when writing
XVG_DIG = 9
# default separators to be discarded in plot legend/title
DEF_SEP = [' ','_','/']
# default plot lines style
DEF_STY = '-'
# default "average" lines style
DEF_AS = '-.'
# show grid in plot by default (if not heatmap)
GRID = True
# default KDE bandwidth
BANDW = 'scott'
# default number of replicas in KDE bandwith estimation trough cross-validation
DEF_CV = 10
# default heatmap's number of bins per dimension
NBINS = 100
# heatmap frequency cutoff "e**(-HEATCUT)"
HEATCUT = 10
# default heatmap color map
COLMAP = 'RdGy'
# default figure prefix name
DEF_FIG = 'figure'
# figure size
DEF_WIN = (8.,6.)
# fonts size (title / other)
DEF_FON = (12.,10.)
# show plots by default
DEF_SHW = True
# save plots to figure by default
DEF_SAV = False
# compulsory pause time required before showing plot (sec)
PAUSE = .001
# vmd drawing resolution
DEF_RES = 100.

###############################################################################
################################ GENERAL SETTINGS #############################
###############################################################################

# temporary file prefix (multipurpose)
DEF_TMP = '.temp'
# default extension hierarchy
DEF_EXT = ['xtc','trr','dcd','pdb','gro']
# default reference extension hierarchy
REF_EXT = ['pdb','gro','tpr']
# generate backup in Gromacs "gmx ..." execution
BACKUP = False
# verbose output
VERBOSE = True
# default terminal width
DEF_WID = 80
# default maximum folder depth shown
DEPTH = 2

###############################################################################
############################### UTILS, *CUSTOMIZE* ############################
###############################################################################

# molars tu number of atoms conversion
MOL_NUM = 0.0187
# atmosphere to bars conversion
ATM_BAR = 1.01325
# default selection
DEF_SEL = 'not (resname SOL WAT SPC TIP3 TIP3P TIP4 TIP4P NA CL MG K LI)'
# extended protein selection
PROTEIN = '(protein or resname GLYM)'
# extended nucleic selection
NUCLEIC = 'resname I I3 I5 A A3 A5 G G3 G5 U U3 U5 T T3 T5 C C3 C5'
# extended lipid selection
LIPID = 'lipid'
# extended water selection
WATER = 'resname SOL WAT SPC TIP3 TIP3P TIP4 TIP4P'
# extended ions selection
IONS = 'type NA K CAL MG LI CL'
# Force-Field name to tool and path conversion
DIC_FFD = {'ff14sb'  : ('ambertools','oldff/leaprc.ff14SB'),
           'ff99sb'  : ('ambertools','oldff/leaprc.ff99SB'),
           'amber14' : ('ambertools','oldff/leaprc.ff14SB'),
           'amber99' : ('ambertools','oldff/leaprc.ff99SB'),
           'amber'   : ('ambertools','oldff/leaprc.ff14SB'),
           'opls'    : ('gromacs'   ,'oplsaa'             ),
           'charmm'  : ('gromacs'   ,'charmm27'           ),
           'gromos'  : ('gromacs'   ,'gromos54a7'         )}
# Gromacs to MDAnalysis selection keywords conversion
DIC_SEL = {''               : ''                  ,
           'System'         : 'all'               ,
           'Water'          : WATER               ,
           'Ion'            : IONS                ,
           'Water_and_ions' : f'{WATER} or {IONS}',
           'non-Water'      : f'not ({WATER})'    }
# water model to Gromacs filename conversion
DIC_WAT = {'tip3p' : 'spc216.gro',
           'spc'   : 'spc216.gro',
           'spce'  : 'spc216.gro',
           'tip4p' : 'tip4p.gro' }
# solvation box specification to Amber command conversion
AMB_BOX = {'cubic'      : 'box',
           'box'        : 'box',
           'oct'        : 'oct',
           'octahedron' : 'oct'}
# solvation box specification to Gromacs command conversion
GRO_BOX = {'cubic'        : 'cubic'       ,
           'box'          : 'box'         ,
           'oct'          : 'octahedron'  ,
           'octahedron'   : 'octahedron'  ,
           'dod'          : 'dodecahedron',
           'dodecahedron' : 'dodecahedron',
           'triclinic'    : 'triclinic'   }
# positive ions set
POS_ION = {'NA','K','CA','MG','LI'}
# negative ions set
NEG_ION = {'CL'}
# pressure coupling types to number of required values
DIC_PRE = {'isotropic'       : 1,
           'semiisotropic'   : 3,
           'anisotropic'     : 6,
           'surface_tension' : 2}
# hbond analysis: base pairing donors-hydrogens/acceptors list
DONORS = [(f'resname {res}','N3', 1) for res in ['T','T3','T5']]
DONORS+= [(f'resname {res}','N3', 1) for res in ['U','U3','U5']]
DONORS+= [(f'resname {res}','N4', 2) for res in ['C','C3','C5']]
DONORS+= [(f'resname {res}','N6', 2) for res in ['A','A3','A5']]
DONORS+= [(f'resname {res}','N1', 1) for res in ['I','I3','I5']]
DONORS+= [(f'resname {res}','N1', 1) for res in ['G','G3','G5']]
DONORS+= [(f'resname {res}','N2', 2) for res in ['G','G3','G5']]
ACCEPTORS = [(f'resname {res}','O4') for res in ['T','T3','T5']]
ACCEPTORS+= [(f'resname {res}','O4') for res in ['U','U3','U5']]
ACCEPTORS+= [(f'resname {res}','N1') for res in ['A','A3','A5']]
ACCEPTORS+= [(f'resname {res}','N6') for res in ['I','I3','I5']]
ACCEPTORS+= [(f'resname {res}','O6') for res in ['G','G3','G5']]
ACCEPTORS+= [(f'resname {res}','N7') for res in ['G','G3','G5']]
ACCEPTORS+= [(f'resname {res}','O2') for res in ['C','C3','C5']]
ACCEPTORS+= [(f'resname {res}','N3') for res in ['C','C3','C5']]
ACCEPTORS+= [(f'resname {res}','N7') for res in ['C','C3','C5']]
# hbond analysis: RNA secondary structure donors-hydrogens/acceptors list
# source: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2915461/
DONORS+= [(NUCLEIC,"O2'",1)]
ACCEPTORS+= [(f'resname {res}','O2') for res in ['T','T3','T5','U','U3','U5']]
ACCEPTORS+= [(f'resname {res}','N7') for res in ['A','A3','A5','G','G3','G5']]
ACCEPTORS+= [(f'resname {res}','N3') for res in ['A','A3','A5','G','G3','G5']]
ACCEPTORS+= [(NUCLEIC,acc)  for acc in  ['OP1','OP2',"O3'","O5'","O2'","O4'"]]
# hbond analysis: protein donors-hydrogens/acceptors list, source:
# https://mdanalysis.org/mdanalysis/documentation_pages/analysis/hbond_analysis.html
DONORS+= [(f'{PROTEIN} and same resid as name H1','N',3)] # N terminus
DONORS+= [(f'{PROTEIN} and same resid as name H' ,'N',1)]
DONORS+= [( 'resname ARG','NE' ,1)]
DONORS+= [( 'resname ARG','NH1',2)]
DONORS+= [( 'resname ARG','NH2',2)]
DONORS+= [( 'resname ASN','ND2',2)]
DONORS+= [( 'resname CYS','SG' ,1)]
DONORS+= [( 'resname GLN','NE2',2)]
DONORS+= [( 'resname HIS and same resid as name HD1','ND1',1)]
DONORS+= [( 'resname HIS and same resid as name HE2','NE2',1)]
DONORS+= [( 'resname HID','ND1',1)]
DONORS+= [( 'resname HIE','NE2',1)]
DONORS+= [( 'resname HIP','NE2',1)]
DONORS+= [( 'resname HIP','ND1',1)]
DONORS+= [( 'resname HSD','ND1',1)]
DONORS+= [( 'resname HSE','NE2',1)]
DONORS+= [( 'resname HSP','NE2',1)]
DONORS+= [( 'resname HSP','ND1',1)]
DONORS+= [( 'resname LYS','NZ' ,3)]
DONORS+= [( 'resname SER','OG' ,1)]
DONORS+= [( 'resname THR','OG1',1)]
DONORS+= [( 'resname TRP','NE1',1)]
DONORS+= [( 'resname TYR','OH' ,1)]
ACCEPTORS+= [(PROTEIN,acc) for acc in ['O','OC1','OC2']]
ACCEPTORS+= [('resname ASN','OD1')]
ACCEPTORS+= [('resname ASP','OD1')]
ACCEPTORS+= [('resname ASP','OD2')]
ACCEPTORS+= [('resname CYH','SG' )]
ACCEPTORS+= [('resname GLN','OE1')]
ACCEPTORS+= [('resname GLU','OE1')]
ACCEPTORS+= [('resname GLU','OE2')]
ACCEPTORS+= [('resname HIS and not same resid as name HE2','ND1')]
ACCEPTORS+= [('resname HIS and not same resid as name HD1','NE2')]
ACCEPTORS+= [('resname HID','NE2')]
ACCEPTORS+= [('resname HIE','ND1')]
ACCEPTORS+= [('resname HSD','NE2')]
ACCEPTORS+= [('resname HSE','ND1')]
ACCEPTORS+= [('resname MET','SD' )]
ACCEPTORS+= [('resname SER','OG' )]
ACCEPTORS+= [('resname THR','OG1')]
ACCEPTORS+= [('resname TYR','OH' )]
# hbond analysis: water donors-hydrogens/acceptors list
DONORS+= [(WATER,don,2)  for don in ['O','OW','OH2']]
ACCEPTORS+= [(WATER,acc) for acc in ['O','OW','OH2']]
