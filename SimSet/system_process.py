# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _process(self,*inpt,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################## LAUNCH #################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.process" with:\n'
    descr+= f'  input = {inpt}\n' if inpt else ''
    descr+= f'  args  = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ################################# PARSING #################################
    ###########################################################################
    
    # defaults
    inpt = ls(inpt,str)
    def_ext = DEF_EXT
    def_tit = ['']
    def_ref = ['']
    def_sel = ['all']
    def_cen =[None]
    def_aln =[None]
    def_pbc = [DEF_PBC]
    def_tra =[None]
    def_sta = [0]
    def_sto =[-1]
    def_ste = [1]
    def_fra =[None]
    
    # specific parameters (except output and pbc)
    exten  = parse(args,'ext'      ,def_ext)
    title  = parse(args,'title'    ,def_tit)
    refere = parse(args,'ref'      ,def_ref)
    refere = parse(args,'reference',refere )
    select = parse(args,'sel'      ,def_sel)
    select = parse(args,'select'   ,select )
    select = parse(args,'selection',select )
    center = parse(args,'center'   ,def_cen)
    align  = parse(args,'align'    ,def_aln)
    trans  = parse(args,'trans'    ,def_tra)
    trans  = parse(args,'traslate' ,trans  )
    start  = parse(args,'start'    ,def_sta)
    stop   = parse(args,'stop'     ,def_sto)
    step   = parse(args,'step'     ,def_ste)
    frame  = parse(args,'frame'    ,def_fra)
    frame  = parse(args,'frames'   ,frame  )
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # exten
    exten = ls(exten,str)
    if not exten:
        exten = ['']
    
    # no input
    if not len(inpt):
        descr = f'ERROR: no input files found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return xvg()
    
    # process input
    match = '*' in ''.join(exten) or '?' in ''.join(exten)
    for i,inp in enumerate(inpt):
        
        # match regular expression
        if '*' in inp or '?' in inp or match:
            inpt[i] = [] # filled with matching results
            regex,displ = [],[]
            for ext in exten:
                p,e = extension(inp,ext)
                if '*' in p or '?' in p:
                    reg = f'{ p }{"."*bool(e)}{e}'
                else:
                    reg = f'"{p}"{"."*bool(e)}{e}'
                if reg not in regex:
                    dis = path_name(reg)
                    if not ('*' in p or '?' in p) and dis[0]!='"':
                        dis = f'"{dis}'
                    regex.append(reg)
                    displ.append(dis)
            regex = ' '.join(regex)
            displ = ' '.join(displ)
            HASH = hash(regex)
            tmp_lst = f'{DEF_TMP}{HASH}list'
            descr = f'MATCHING regex {ansi(displ,"italic")}'
            commd = f'ls {regex} 2>/dev/null > "{tmp_lst}"'
            comment(descr,self.logre,self.logsh,verbo)
            sh(commd)
            found = False
            with open(tmp_lst,'r') as f:
                for name in f.readlines():
                    inpt[i].append(name[:-1])
                    found = True
            sh(f'rm -f "{tmp_lst}"')
            if found:
                descr = '\n'.join(inpt[i])+'\n'
                comment(descr,self.logre,self.logsh,verbo)
                continue
            descr = f'ERROR: no files found\n'
            descr = ansi(descr,'red')
            comment(descr,self.logre,self.logsh,verbo)
            return 0
        
        # match existing file
        if inp == '.'.join(extension(inp)):
            if isfile(inp):
                continue
        
        # match required extensions
        found = False
        inpt[i] = []
        for ext in exten:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                inpt[i].append(fname)
                found = True
                break
        if found:
            continue
        
        # nothing found: error
        displ = path_name(inp)
        names = ', '.join([f'"{displ}"']+[f'"{displ}{"."*bool(ext)}{ext}"'\
                                                         for ext in exten])
        descr = f'ERROR: none of {names} found\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    # reconstruct input and extension
    inpt = ls(inpt)
    n = len(inpt)
    exten = [extension(inp)[1] for inp in inpt]
    
    # output
    def_out = inpt.copy()
    output = parse(args,'out'   ,def_out)
    output = parse(args,'output',output )
    
    # process output
    line = False # formatting issues
    output = ls(output,str)
    for i,out in enumerate(output):
        if '*' in out or '?' in out:
            line  = True
            descr = f'BUILDING output prefixes from {path_name(out)} regex'
            comment(descr,self.logre,self.logsh,verbo)
            pieces = ls([out.split('*') for out in out.split('?')])
            pruned = [extension(inp)[0] for inp in inpt[i:]]
            for p in pieces:
                pruned = [pru.replace(p,'',1) for pru in pruned]
            output[i] = []
            for p in pruned:
                tmp = list(out)
                k = 0
                for j,c in enumerate(out):
                    if   c=='?':
                        tmp[j] = p[k:k+1]
                        k+= 1
                    elif c=='*':
                        tmp[j] = p[k:]
                        k+= len(p)
                output[i].append(''.join(tmp))
            descr = '\n'.join([path_name(out) for out in output[i]])
            comment(descr,self.logre,self.logsh,verbo)
            break
    
    # reconstruct output
    output = ls(output,str)
    if len(output) != n:
        descr = f"ERROR: input ({n}) and output ({len(output)} "\
                f"lengths don't match\n"
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    # assign custom extensions (if needed) to output
    output = [out if out=='.'.join(extension(out)) else\
              f'{out}.{ext}' for out,ext in zip(output,exten)]
    if len(set(output)) != len(output):
        descr = f"WARNING: some output data will be overwritten"
        descr = ansi(descr,'yellow')
        comment(descr,self.logre,self.logsh,verbo)
    if verbo and line: print()
    
    # title, center, align
    title = ls(title ,str,None)
    center= ls(center,str,None)
    align = ls(align ,str,None)
    center= ['all' if cen=='' else cen for cen in center]
    align = ['all' if aln=='' else aln for aln in align ]
    
    # reference - check if ok
    refere = ls(refere,str)
    if not len(refere):
        descr = f'ERROR: bad "reference" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    # selection - check if ok
    select = ls(select,str)
    if not len(select):
        descr = f'ERROR: bad "selection" option\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    # pbc conditions (pbcond)
    def_pbc = [DEF_PBC*(not bool(aln)) for aln in align]
    pbcond = parse(args,'pbc',def_pbc)
    pbcond = ls(pbcond,str,None)
    
    # translate
    if not sum([type(tra) not in (list,tuple,ndarray) for tra in trans]):
        # array-like of array-like of int/float
        # different trans option for all inputs
        trans = [ls(tra,int,float)[:3] for tra in trans]
    else:
        # array-like of int/float
        # same translate option for all inputs
        trans = [ls(trans,int,float,None)[:3]]
    
    # start, stop, step - check if ok
    start = ls(start,int,float)
    stop  = ls(stop, int,float)
    step  = ls(step, int,float)
    if not len(start)*len(stop)*len(step):
        descr = f'ERROR: bad "start"/"stop"/"step" options\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    # frames
    if not frame:
        frame = def_fra
    elif not sum([type(fra) not in ITERABLE for fra in frame]):
        # array-like of array-like of int/float
        # different frames option for all inputs
        frame = [ls(fra,int,float) for fra in frame]
    else:
        # array-like of int/float
        # same frames option for all inputs
        frame = [ls(frame,int,float)]
    
    # adjust lengths
    title  = adjust_list(title, n)
    refere = adjust_list(refere,n)
    select = adjust_list(select,n)
    center = adjust_list(center,n)
    align  = adjust_list(align, n)
    pbcond = adjust_list(pbcond,n)
    trans  = adjust_list(trans, n)
    start  = adjust_list(start, n)
    stop   = adjust_list(stop,  n)
    step   = adjust_list(step,  n)
    frame  = adjust_list(frame, n)
    
    # reference list
    line = False # formatting issues
    for i,ref in enumerate(refere):
        if ref == '.'.join(extension(ref)):
            if isfile(ref):
                continue
        
        # match required extensions
        found = False
        for ext in REF_EXT+['cpt']:
            fname = f'{ref}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if found:
            continue
        
        # input file is also reference
        if exten[i] in REF_EXT:
            refere[i] = inpt[i]
            continue
        
        # look for alternatives
        found = False
        inp = extension(inpt[i])[0]
        for ext in REF_EXT+['cpt']:
            fname = f'{inp}{"."*bool(ext)}{ext}'
            if isfile(fname):
                refere[i] = fname
                found = True
                break
        if not found:
            for ext in REF_EXT+['cpt']:
                fname = f'{self.molec}{"."*bool(ext)}{ext}'
                if isfile(fname):
                    refere[i] = fname
                    found = True
                    break
        
        # found alternative
        if found:
            line = True
            descr = f'WARNING: no reference specified '\
                    f'for input file "{path_name(inpt[i])}", '\
                    f'assigning "{path_name(refere[i])}" instead'
            descr = ansi(descr,'yellow')
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # found no alternatives
        descr = f'ERROR: no reference found '\
                f'for input file "{path_name(inpt[i])}"\n'
        descr = ansi(descr,'red')
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    if verbo and line: print()
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # number of correctly run processes
    m = 0
    
    # temporary files
    NOW = round(now())
    tmp_ndx = f'{DEF_TMP}{NOW}.ndx'
    tmp_fra = f'{DEF_TMP}{NOW}fra.ndx'
    
    # initialize progress bar
    cursor.hide()
    current = 0
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    bar = progress_bar(0,n,0,etr,speed,'',width)
    if verbo and n>1: print(f'{bar}\r',end='')
    
    # main cycle
    for inp,   out,   ext,   tit,   sel,   cen,   aln,\
        ref,   pbc,   tra,   sta,   sto,   ste,   fra in\
    zip(inpt,  output,exten, title, select,center,align,
        refere,pbcond,trans, start, stop,  step,  frame):
        
        # retrieve trajectory infos
        universe = Universe(inp)
        N = len(universe.trajectory)
        raw_times = []
        
        # process start
        if type(sta) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sta = search_bigger(raw_times,sta)
            sta = sta if sta is not None else N
        sta+= 0 if sta>=0 else N
        sta = max(0,min(sta,N))
        
        # process stop
        if type(sto) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            sto = search_smaller(raw_times,sto)
            sto = sto if sto is not None else -N-1
        sto+= 1 if sto>=0 else N+1
        sto = max(0,min(sto,N))
        
        # build frames accorting to start-stop-step
        if not fra:
            if type(ste) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra= [sta]
                for i,t in enumerate(raw_times[sta:sto]):
                    if t-raw_times[fra[-1]] >= ste:
                        fra.append(sta+i)
            else:
                fra = range(sta,sto,ste)
        
        # process already existing "frames"
        else:
            temp,fra = fra,[]
            for f in temp:
                if type(f) in FLOAT:
                    if len(raw_times) != N:
                        raw_times = [t.time for t in universe.trajectory]
                    f = search_equal(raw_times,f)
                    f = f if f is not None else N
                f+= 0 if f>=0 else N
                if f<0 or f>=N:
                    continue
                fra.append(f)
        
        # temporary frames file
        with open(tmp_fra,'w') as f:
            f.write(f'[ frames ]\n')
            for i,j in enumerate(fra):
                f.write(f'{j+1}')
                f.write('\n' if (i+1)%10==0 else ' ')
            f.write('\n')
        
        # temporary index file
        try:
            Index(tmp_ndx,ref,[sel, cen if cen else sel, aln if aln else sel],
                              ['select', 'center', 'align'])
        except Exception as traceback:
            descr = f'\nERROR: failure in index file computation, '\
                    f'with traceback:'
            descr = ansi(descr,'red')
            descr = f'{descr}\n{traceback}\n'
            comment(descr,self.logre,self.logsh,verbo)
            return xvg()
        
        # translate
        if tra[0]:
            tra = ' '.join([str(t/10) for t in tra]) # A to nm conversion
        else:
            tra = ''
        
        # pass through temporary file
        tmp = f'{DEF_TMP}.{extension(out)[1]}'
        sh(f'rm -f "{tmp}"\n')
        
        # call gmx trjconv
        commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
                f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
                f'echo {"1 " if pbc == "cluster" else ""}'\
                f'{"2 " if aln else ""}{"1 " if cen else ""}0 | '\
                f'gmx trjconv -f "{inp}" -s "{ref}" -o "{tmp}"'
        commd+= f' -n "{tmp_ndx}" -fr "{tmp_fra}"'
        commd+= f' -trans {tra}'if tra else ''
        commd+= f' -pbc {pbc}'  if pbc else ''
        commd+= f' -center'     if cen else ''
        commd+= f' -fit progressive'    if aln else ''
        commd+= f' -nobackup'    if not BACKUP else ''
        commd+= f' 2>/dev/null'*(n>1)
        execute(commd,self.logre,self.logsh,(n==1)*verbo)
        
        # check if success
        if exists(tmp):
            m+= 1
            sh(f'mv "{tmp}" "{out}"')
            sh(f'rm -f "{tmp_ndx}" "{tmp_fra}"')
            
            # retrieve title
            if not tit and ext in ['pdb','gro']:
                tit = Get_Title(inp)
            
            # adjust title / update self.molec
            if tit and extension(out)[1] in ['pdb','gro']:
                if out == self.molec:
                    self.unive = Update(out,f'{self.molec}.pdb',
                                            f'{self.molec}.gro',title=tit)
                else:
                    Set_Title(out,tit)
        
        # handle failure
        else:
            descr = f'\nWARNING: failure in "{path_name(inp)}" processing, '\
                    f'with command:\n'
            descr = f'{ansi(descr,"yellow")}{commd}\n'
            comment(descr,self.logre,self.logsh,verbo)
            continue
        
        # measure & print progress
        if not verbo or n==1:
            continue
        current+= 1
        elapsed = now()-t0
        speed = current/elapsed
        etr = (n-current)/speed
        bar = progress_bar(current,n,elapsed,etr,speed,'',width)
        print(f'{bar}\r',end='')
    
    # terminate progress bar
    cursor.show()
    if n>1:
        elapsed = now()-t0
        speed = n/elapsed
        etr = 0
        bar = progress_bar(n,n,elapsed,etr,speed,'',width)
        comment(bar,self.logre,self.logsh,verbo)
    if verbo and n>1: print()
    
    return m
