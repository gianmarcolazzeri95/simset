# import libraries
from SimSet.params   import *
from SimSet.aux      import *
from SimSet.analysis import *
from SimSet.mdp import mdp
from SimSet.ssh import ssh
from SimSet.xvg import xvg

###############################################################################
################################ MAIN FUNCTION ################################
###############################################################################

def _generate(self,**args):
    
    """
    Infos are found in the ``SimSet.system`` class.
    """
    
    ###########################################################################
    ################################ LAUNCHING ################################
    ###########################################################################
    
    verbo = parse(args,'verbose',VERBOSE)
    move(self.wkdir,self.logre,self.logsh,verbo)
    descr = f'\nLAUNCHED "SimSet.system.generate" with:\n'
    descr+= f'  args = {args}\n'
    comment(descr,self.logre,self.logsh,verbo)
    cursor.show()
    
    ###########################################################################
    ################################# PARSING #################################
    ###########################################################################
    
    # input arguments
    try:
        force = parse(args,'ff'        ,self.force)
        force = parse(args,'force'     ,     force)
        force = parse(args,'forcefield',     force)
        boxsh = parse(args,'box'       ,self.boxsh)
        dista = parse(args,'d'         ,self.dista)
        dista = parse(args,'distance'  ,     dista)
        water = parse(args,'water'     ,self.water)
        wions = parse(args,'ion'       ,self.wions)
        wions = parse(args,'ions'      ,     wions)
        nions = parse(args,'nions'     ,self.nions)
        mions = parse(args,'mions'     ,self.mions)
        ignh  = parse(args,'ignh',DEF_IGN)
        self.force = force
        self.boxsh = boxsh
        self.dista = dista
        self.water = water
        self.wions = wions
        self.nions = nions
        self.mions = mions
    except Exception as traceback:
        descr = f'ERROR while parsing options, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        comment(descr,self.logre,self.logsh,verbo)
        return 0
    
    ###########################################################################
    ############################ OPTIONS PROCESSING ###########################
    ###########################################################################
    
    # number of initial atoms
    N = len(self.unive.atoms)
    
    # force field
    if not self.force.lower() in DIC_FFD:
      descr = f'ERROR: force field not recognized\n'
      descr = ansi(descr,'red')
      comment(descr,self.logre,self.logsh,verbo)
      return
    tool,self.force = DIC_FFD.get(self.force.lower())
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # generate posre
    if not BACKUP:
        commd  = f'rm -f "{self.posre}"\nrm -f "{self.topol}"'
        execute(commd,self.logre,self.logsh,verbo)
    descr = f'GENERATING "{self.posre}" (edit by yourself)'
    comment(descr,self.logre,self.logsh,verbo)
    commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
            f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
            f'echo 0 | gmx genrestr -f "{self.molec}.pdb" -o "{self.posre}"\n'\
            f'touch "{self.posre}"\n'
    execute(commd,self.logre,self.logsh,verbo)
    
    # choose gromacs or ambertools
    if tool == 'gromacs':
      __gromacs(self,ignh,verbo)
    if tool == 'ambertools':
      __ambertl(self,ignh,verbo)
    
    # update "self.segid" according to system gro file
    nresi = len(self.unive.residues)
    unive = Universe(f'{self.molec}.gro')
    for residue in unive.residues[nresi:]:
        self.segid.append(residue.resname)
    
    # rebuild "self.unive" with correct segids
    self.unive = mda.Universe.empty(1) # reinitialize
    nresi = len(unive.residues)
    segid = self.segid[0]
    start = 0
    for stop in range(1,nresi):
        if self.segid[stop] != segid:
            local = unive.residues[start:stop].atoms
            local.segments[0].segid = segid # assign correct segid
            if hasattr(self.unive,'trajectory'):
                self.unive = mda.Merge(self.unive.atoms,local)
            else:
                self.unive = mda.Merge(local)
            segid = self.segid[stop]
            start = stop
    local = unive.residues[start:stop+1].atoms
    local.segments[0].segid = segid # assign correct segid
    self.unive = mda.Merge(self.unive.atoms,local)
    self.unive.dimensions = unive.dimensions # correct size
    
    # write pdb with correct segids
    self.unive.atoms.write(f'{self.molec}.pdb')
    
    # fix titles in .top .pdb .gro
    descr = f'FIXING titles in "{self.molec}.pdb/gro" "{self.topol}"\n'
    comment(descr,self.logre,self.logsh,verbo)
    Set_Title(f'{self.molec}.pdb',self.title)
    Set_Title(f'{self.molec}.gro',self.title)
    Set_Title(self.topol,self.title)
    
    # number of added atoms
    return len(self.unive.atoms)-N

###############################################################################
############################## PRIVATE FUNCTION ###############################
###############################################################################

def __gromacs(self,ignh=DEF_IGN,verbo=VERBOSE):
    
    """
    Generate system with ``gromacs``.
    """
    
    ###########################################################################
    ############################ OPTIONS PROCESSING ###########################
    ###########################################################################
    
    force = self.force
    boxsh = GRO_BOX.get(self.boxsh.lower())
    solvt = DIC_WAT.get(self.water.lower())
    wions = self.wions
    dista = [dis/10 for dis in self.dista] # A to nm conversion
    nions = self.nions
    mions = self.mions
    wions = wions.split()
    nions = nions[:len(wions)]+[0]*(len(wions)-len(nions))
    mions = mions[:len(wions)]+[0]*(len(wions)-len(mions))
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################    
    
    # generate topology
    descr = f'GENERATING topology with Gromacs'
    commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
            f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
            f'gmx pdb2gmx -f "{self.molec}.pdb" -o "{self.molec}.gro" '\
            f'-i "{self.posre}" -p "{self.topol}" '\
            f'-water {self.water} -ff {force}'\
            f'{" -ignh"*(ignh)}{" -nobackup"*(not BACKUP)}'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
    
    # shape solvation box
    descr = f'SHAPING solvation box for "{self.molec}.gro"'
    commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
            f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
            f'gmx editconf -f "{self.molec}.gro" -o "{self.molec}.gro" -c '
    commd+= f'-d {dista[0]} ' if len(dista)==1 else\
            f'-box {" ".join(dista[:3])} '
    commd+= f'-bt {boxsh}{" -nobackup"*(not BACKUP)}'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
    
    # solvate the system
    descr = f'SOLVATING "{self.molec}.gro"'
    commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
            f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
            f'gmx solvate -cp "{self.molec}.gro" -o "{self.molec}.gro" '\
            f'-cs "{solvt}" -p "{self.topol}"{" -nobackup"*(not BACKUP)}'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
    
    # ionize the system - preparation
    descr = f'RUNNING grompp ionization.mdp'
    commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
            f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
            f'gmx grompp -f "{MDP_ION}" -po "{DEF_GEN}.mdp" '\
            f'-c "{self.molec}.gro" -p "{self.topol}" '\
            f'-o "{DEF_GEN}.tpr"{" -nobackup"*(not BACKUP)}'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
    
    # retrieve n of water molecules
    n = len(Universe(f"{self.molec}.gro").select_atoms('resname SOL').residues)
    descr = f'ADDING {n} water residues\n'
    comment(descr,self.logre,self.logsh,verbo)
    
    # add concentrations to nions
    nions = [round(nio+mio*MOL_NUM*n) for nio,mio in zip(nions,mions)]
    
    # retrieve default positive and negative ions
    def_pos,def_neg = '',''
    for ion,nio in zip(wions,nions):
        if ion.upper() in POS_ION and nio==0:
            def_pos = ion.upper() if not def_pos else def_pos
        if ion.upper() in NEG_ION and nio==0:
            def_neg = ion.upper() if not def_neg else def_neg
    def_pos = def_pos if def_pos else 'NA'
    def_neg = def_neg if def_neg else 'CL'
    
    # add ions
    descr = ", ".join([f'{ion} ({nio if nio else "neutr."})'\
                       for ion,nio in zip(wions,nions)])
    descr = f'ADDING ions: {descr}'
    comment(descr,self.logre,self.logsh,verbo)
    commd = ''
    for ion,nio in zip(wions,nions):
        commd = f'module load cuda-10.0 gcc54 2>/dev/null\n'\
                f'source {self.ssh.gmx}/GMXRC 2>/dev/null\n'\
                f'echo SOL | gmx genion -s "{DEF_GEN}.tpr" '\
                f'-o "{self.molec}.gro" -p "{self.topol}" '
        if ion.upper() in POS_ION:
            if nio:
                commd+= f'-pname {ion} -np {nio}'
            else:
                commd+= f'-pname {ion} -nname {def_neg} -neutral'
        if ion.upper() in NEG_ION:
            if nio:
                commd+= f'-nname {ion} -nn {nio}'
            else:
                commd+= f'-nname {ion} -pname {def_pos} -neutral'
        commd+= f'{" -nobackup"*(not BACKUP)}\n'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)

###############################################################################
############################## PRIVATE FUNCTION ###############################
###############################################################################

def __ambertl(self,ignh=DEF_IGN,verbo=VERBOSE):
    
    """
    Generate system with ``tleap``.
    """
    
    ###########################################################################
    ############################ OPTIONS PROCESSING ###########################
    ###########################################################################
    
    force = self.force
    boxsh = AMB_BOX.get(self.boxsh.lower())
    solvt = DIC_WAT.get(self.water.lower())
    wions = self.wions
    dista = self.dista
    nions = self.nions
    mions = self.mions
    wions = wions.split()
    nions = nions[:len(wions)]+[0]*(len(wions)-len(nions))
    mions = mions[:len(wions)]+[0]*(len(wions)-len(mions))
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # temp pdb file
    HASH = round(now())
    tmp_pdb = f'{DEF_TMP}{HASH}.pdb'
    
    # run ignh option
    if ignh: # go through temp pdb file
        descr = f'REMOVING hydrogens in "{self.molec}.pdb"'
        commd = f'reduce -Trim   "{self.molec}.pdb" > "{tmp_pdb}"\n'\
                f'mv "{tmp_pdb}" "{self.molec}.pdb"'
        comment(descr,self.logre,self.logsh,verbo)
        execute(commd,self.logre,self.logsh,verbo)
    
    # tleap - first call to get n of water molecules
    tleap = f'# prepare amber topology with tleap\n'\
            f'source {force}\n'\
            f'source leaprc.water.{self.water}\n'\
            f'mol = loadpdb "{self.molec}.pdb"\n'\
            f'solvate{boxsh.lower()} mol {self.water.upper()}BOX '\
            f'{"".join([str(dis) for dis in dista])}\n'
    with open(f'{DEF_OPE}.tleap','w') as f:                   
        f.write(f'{tleap}\nsavepdb mol "{tmp_pdb}"\nquit')
    descr = f'RUNNING "{DEF_OPE}.tleap"'
    commd = f'tleap -s -f "{DEF_OPE}.tleap"'
    comment(descr,self.logre,self.logsh,verbo)
    execute(commd,self.logre,self.logsh,verbo)
    
    # retrieve n of water molecules
    n = len(Universe(tmp_pdb).select_atoms('resname SOL WAT TIP3').residues)
    descr = f'ENDED UP with {n} water residues\n'
    comment(descr,self.logre,self.logsh,verbo)
    
    # add concentrations to nions
    nions = [round(nio+mio*MOL_NUM*n) for nio,mio in zip(nions,mions)]
    descr = ", ".join([f'{ion} ({nio if nio else "neutr."})'\
                       for ion,nio in zip(wions,nions)])
    descr = f'ADDING ions: {descr}'
    comment(descr,self.logre,self.logsh,verbo)
    
    # retrieve default positive and negative ions
    def_pos,def_neg = '',''
    for ion,nio in zip(wions,nions):
        if ion.upper() in POS_ION and nio==0:
            def_pos = ion.upper() if not def_pos else def_pos
        if ion.upper() in NEG_ION and nio==0:
            def_neg = ion.upper() if not def_neg else def_neg
    def_pos = def_pos if def_pos else 'NA'
    def_neg = def_neg if def_neg else 'CL'
    
    # tleap - second call to ionize and generate topology
    for ion,nio in zip(wions,nions):
        tleap += f'addions mol {ion.upper()} {nio}\n'
        if ion.upper() in POS_ION and nio==0:
            tleap += f'addions mol {def_neg} 0\n'
        if ion.upper() in NEG_ION and nio==0:
            tleap += f'addions mol {def_pos} 0\n'
    tleap+= f'saveamberparm mol "{self.molec}.prmtop" "{self.molec}.rst7"\n'
    with open(f'{DEF_OPE}.tleap','w') as f:                   
        f.write(f'{tleap}quit')
    commd = f'tleap -s -f "{DEF_OPE}.tleap"'
    execute(commd,self.logre,self.logsh,verbo)
    
    # convert position and topology to GROMACS
    descr = f'CONVERTING position and topology to GROMACS format\n'
    comment(descr,self.logre,self.logsh,verbo)
    mol = pmd_load(f'{self.molec}.prmtop',f'{self.molec}.rst7')
    mol.save(f'{self.molec}.pdb',overwrite=True) # check if retains segids
    mol.save(f'{self.molec}.gro',overwrite=True)
    mol.save(f'{self.topol}',overwrite=True)
    
    # include posre .itp file in topology 
    with open(self.topol) as f:
        lines = f.readlines()
    descr = f'INCLUDING "{self.posre}" in "{self.topol}"\n'
    comment(descr,self.logre,self.logsh,verbo)
    posre = f'\n; Include Position restraint file\n'\
            f'#ifdef POSRES\n'\
            f'#include "{relpath(self.posre)}"\n'\
            f'#endif\n'
    index = search(lines,'[ moleculetype ]',one=False)
    lines.insert(index[1]-1,posre)
    with open(self.topol,'w') as f:
        f.write(''.join(lines))
