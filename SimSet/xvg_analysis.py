# import libraries
from SimSet.params import *
from SimSet.aux    import *
from SimSet.kde    import *
from SimSet.binary import *

###############################################################################
############################ COMPUTE TIME DERIVATIVE ##########################
###############################################################################

def _derivative(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    cursor.show()
    
    ###########################################################################
    ######################### RETRIEVE SPECIFIC OPTIONS #######################
    ###########################################################################
    
    # defaults
    def_smo = [-1.]
    
    # parse options
    verbo = parse(args,'verbose',VERBOSE)
    smooth= parse(args,'smooth' ,def_smo)
    
    ###########################################################################
    ############################## PROCESS OPTIONS ############################
    ###########################################################################
    
    # select data according to options
    xvg = self.select(*columns,**args)
    
    # retrieve system infos
    times = xvg.get_times()
    datap = xvg.get_datap()
    N = len(times)
    H = len(xvg)
    
    # nothing to compute
    if not H: return res
    
    # smooth
    smooth = ls(smooth,int,float,None)
    if not smooth:
        descr = 'ERROR: invalid "smooth" option'
        if verbo: print(ansi(descr,'red'))
        return res
    smooth = [max(smo,0) for smo in smooth]
    smooth = adjust_list(smooth,H)
    
    ###########################################################################
    ########################### CORE OF THE FUNCTION ##########################
    ###########################################################################
    
    # iterate through columns
    for h,smo in enumerate(smooth):
        raw_times = []
        raw_datap = []
        data0,time0,i0 = NaN,-inf,-inf
        data1,time1,i1 = NaN,-inf,-inf
        for i,d,t in zip(range(N),datap[h],times):
            if isnan(d): continue
            data0,time0,i0 = data1,time1,i1
            data1,time1,i1 = d,t,i
            if isnan(data0) or time0==-inf or i0==-inf: continue
            try:    raw_datap.append((data1-data0)/(time1-time0))
            except: data1,time1,i1 = data0,time0,i0; continue
            raw_times.append((time0+time1)/2)
        
        # smoothen
        new_times = []
        new_datap = []
        i0,time0  = 0,raw_times[0]
        for i0,time0 in zip(range(N),raw_times):
            for i,t  in zip(range(i0+1,N),raw_times[i0+1:]):
                if type(smo) in INTEGER and i-i0    < smo: continue
                if type(smo) in FLOAT   and t-time0 < smo: continue
                new_times.append(mean(raw_times[i0:i]))
                new_datap.append(mean(raw_datap[i0:i]))
                break 
        
        # merge
        res.merge(new_times,new_datap,
                  obser=f'{lcss(xvg.obser[h])} derivative',
                  files=xvg.files[h],title=xvg.title[h],
                  xaxis=xvg.xaxis[h],yaxis=xvg.yaxis[h],verbose=False)
    return res

###############################################################################
############################## COMPUTE TIME INTEGRAL ##########################
###############################################################################

def _integral(self,res,*columns,**args):
    
    """
    Infos are found in the ``SimSet.xvg`` class.
    """
    
    return
