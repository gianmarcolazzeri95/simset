from SimSet.params   import *
from SimSet.aux      import *

###############################################################################
#################################### RMSD #####################################
###############################################################################

def Rmsd(tra,ref='',sel='all',start=0,stop=-1,step=1,frame=None,
             thres=0.,pers=0.,verbose=VERBOSE):
    
    """
    Compute the root mean square deviation for a trajectory ``tra``,
    with ``sel`` as selection, ``ref`` as reference (any topology file,
    when reference ``frame`` is not ``None``), ``start`` as first
    trajectory frame, ``stop`` as last trajectory frame, ``step`` as
    interval between frames. If ``thres`` is positive, stop when the
    rmsd goes below it. If negative, stop when the rmsd goes above it.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frame : int or float or None, if int, frame of ``tra`` to be taken
            as reference; if float: time to be taken as reference; if
            ``None``: don't take into account; default is ``None``
    thres : float, trigger; if positive: stop when the rmsd goes below
            ``thres`` (that frame included) if negative: stop when the
            rmsd goes above ``thres`` (that frame included); default is
            ``0.``
    pers  : float, persistence value (ps) required to activate the
            trigger; if ``thres`` is not zero: stop when the rmsd
            goes above/below ``thres`` for more than ``pers`` ps;
            default is ``0.``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    rmsd : list of floats, rmsd time series
    """
    
    # load universe and selection
    if ref:
        universe = Universe(ref,tra)
    else:
        universe = Universe(tra)
        universe.add_TopologyAttr('mass', values=[1.]*len(universe.atoms))
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process frame and retrieve reference
    if type(frame) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frame = search_equal(raw_times,frame)
    if frame is not None:
        frame+= 0 if frame>=0 else N
        frame = max(0,min(frame,N))
        temp = universe.copy()
        temp.trajectory[frame]
        reference = temp.select_atoms(sel) if sel else temp.atoms
    else:
        temp = Universe(ref) if ref else universe.copy()
        reference = temp.select_atoms(sel) if sel else temp.atoms
    reference = mda.Merge(reference)
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        frames = [start]
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load selection positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms selection positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    universe = Build(selection,array(positions))
    N = len(universe.trajectory)
    
    # align selection to reference
    ref = path_name(ref) if ref else 'reference'
    if type(frame) is int:
        if verbose: print(f'ALIGNING "{sel_name(sel)}" to frame {frame}')
    elif   verbose: print(f'ALIGNING "{sel_name(sel)}" to "{ref}"')
    align.AlignTraj(universe,reference,weights='mass',in_memory=True).run()
    
    # routine: rmsd
    def compute():
        return rms.rmsd(universe.atoms.positions,reference.atoms.positions)
    
    # compute - with threshold or verbosely
    rmsd = []
    if thres or verbose:
        
        # initialize trigger
        breach = -inf  # last time threshold was crossed
        status = False # being at the right side of the threshold
        
        # initialize progress bar
        if verbose:
            print(f'COMPUTING rmsd on "{path_name(tra)}"')
            cursor.hide()
            t0,i,elapsed,etr,speed = now(),0,0,None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        
        # iterate through frames
        for i,t in enumerate(universe.trajectory):
            rmsd.append(compute())
            if thres and (rmsd[-1]<thres if thres>0 else rmsd[-1]>-thres):
                if not status:
                    breach = time[i]
                    status = True
                if time[i]-breach >= pers: 
                    break
            else:
                status = False
            
            # update progress bar
            if not verbose:
                continue
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (N-i-1)/speed
            bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # print trigger status
        if i+1 < N:
            time = time[:i+1]
            descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
        elif thres:
            descr = ansi(f'\nthreshold not triggered\n','yellow')
        else:
            descr = '\n'
        if verbose: print(descr)
        return time,rmsd
    
    # compute - the fast way
    rmsd = [compute() for t in universe.trajectory]
    return time,rmsd

###############################################################################
##################################### PCA #####################################
###############################################################################

def PCA(tra,ref='',sel='all',start=0,stop=-1,step=1,frame=None,
            thres=VARIANCE,num=-1,verbose=VERBOSE):
    
    """
    Compute the first ``num`` principal components for a trajectory,
    ``tra`` with ``sel`` as selection, ``ref`` as reference (any
    topology file, when reference ``frame`` is not ``None``), ``start``
    as first trajectory frame, ``stop`` as last trajectory frame,
    ``step`` as interval between frames. Stop collecting components
    when cumulated variance goes above ``thres``.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frame : int or float or None, if int, frame of ``tra`` to be taken
            as reference; if float: time to be taken as reference; if
            ``None``: don't take into account; default is ``None``
    thres : float, if positive: stop collecting components when
            cumulated variance goes above ``thres``; default is
            ``SimSet.VARIANCE``
    num   : int, number of principal components to collect; if
            negative: take all compatibly with ``thres`` value;
            default is ``-1``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    comp : (components time series) x (number of componentes collected)
    """
    
    # load universe and selection
    if ref:
        universe = Universe(ref,tra)
    else:
        universe = Universe(tra)
        universe.add_TopologyAttr('mass', values=[1.]*len(universe.atoms))
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process frame and retrieve reference
    if type(frame) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frame = search_equal(raw_times,frame)
    if frame is not None:
        frame+= 0 if frame>=0 else N
        frame = max(0,min(frame,N))
        temp = universe.copy()
        temp.trajectory[frame]
        reference = temp.select_atoms(sel) if sel else temp.atoms
    else:
        temp = Universe(ref) if ref else universe.copy()
        reference = temp.select_atoms(sel) if sel else temp.atoms
    reference = mda.Merge(reference)
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frames = [start]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load selection positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms selection positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    positions = array(positions)
    N = len(positions)
    M = len(selection)
    
    # align selection to reference
    ref = path_name(ref) if ref else 'reference'
    if type(frame) is int:
        if verbose: print(f'ALIGNING "{sel_name(sel)}" to frame {frame}')
    elif   verbose: print(f'ALIGNING "{sel_name(sel)}" to "{ref}"')
    align.AlignTraj(universe,reference,weights='mass',in_memory=True).run()
    
    # compute covariance matrix
    # linearize positions, separate time series
    positions = positions.reshape(len(positions),3*M).T
    covariance = cov(positions)
    
    # compute eigenvalues/vectors
    dis = path_name(tra)
    if verbose: print(f'COMPUTING eigenvalues/eigenvectors for "{dis}"')
    eigval,eigvec = linalg.eig(covariance)
    eigval,eigvec = realcomp(eigval),realcomp(eigvec)
    
    # sort eigenvalues - eigenvectors
    index = list(argsort(eigval))
    index.reverse()
    
    # find when reached threshold
    total = sum(eigval)
    perc = 0
    for i in range(3*M):
        perc+= eigval[index[i]]/total
        if perc>=thres:
            break
    
    # print recap
    if verbose:
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        padA = max(2,int(log10(max(i,abs(num))))+1)
        padB = 10
        descr = [ansi(f'{"id":{padA}}{"eigval":{padB}}{"perc var":{padB}}'\
                      f'{"cum var":{padB}}','bold'),'-'*width]
        perc = 0
        for k,ind in zip(range(3*M),index):
            perc+= eigval[ind]/total
            txtA = f'{k:g}'
            txtB = f'{eigval[ind]:.{padB}f}'
            txtC = f'{eigval[ind]/total:.{padB}f}'
            txtD = f'{perc:.{padB//2}f}'+ (f' *n' if k==num-1 else '')+\
                                          (f' *thres' if k==i else '')
            descr.append(f'{format(txtA,padA)}{format(txtB,padB)}'\
                         f'{format(txtC,padB)}{format(txtD,padB,0)}')
            if 0 < thres < 1 and perc > 1-(1-thres)/2 and k > num:
                break
        print(f'\n'.join(descr)+'\n')
    
    # select components
    i = min(i+1,num)
    if verbose: print(f'SELECTING {i} components')
    index = index[:i]
    
    # add time
    comp = []
    comp.append(time)
    
    # compute components verbosely
    positions = positions.T # now time-wise
    if verbose:
        for ind in index:
            
            # initialize progress bar
            cursor.hide()
            t0,i,elapsed,etr,speed = now(),0,0,None,None
            bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
            
            # iterate through frames
            temp = []
            for i in range(N):
                temp.append(dot(positions[i],eigvec[:,ind]))
                
                # update progress bar
                elapsed = now()-t0
                speed = (i+1)/elapsed
                etr = (N-i-1)/speed
                bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
                print(f'{bar}\r',end='')
            comp.append(temp)
            cursor.show()
            print()
        
        # return results
        print()
        return comp
    
    # compute components - the fast way
    for ind in index:
        comp.append([dot(positions[i],eigvec[:,ind]) for i in range(N)])
    return comp

###############################################################################
#################################### RMSF #####################################
###############################################################################

def Rmsf(tra,ref='',sel='all',start=0,stop=-1,step=1,frame=None,
             verbose=VERBOSE):
    
    """
    Compute the root mean square fluctuation for a trajectory ``tra``,
    with ``sel`` as selection, ``ref`` as reference (any topology file,
    when reference ``frame`` is not ``None``), ``start`` as first
    trajectory frame, ``stop`` as last trajectory frame, ``step`` as
    interval between frames.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frame : int or float or None, if int, frame of ``tra`` to be taken
            as reference; if float: time to be taken as reference; if
            ``None``: don't take into account; default is ``None``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    residues : list of strings, ordered residues' names
    rmsf     : list of floats, rmsf by residue
    """
    
    # load universe and selection
    if ref:
        universe = Universe(ref,tra)
    else:
        universe = Universe(tra)
        universe.add_TopologyAttr('mass', values=[1.]*len(universe.atoms))
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    residues = [residue.resname for residue in selection.residues]
    M = len(residues)
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process frame and retrieve reference
    if type(frame) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frame = search_equal(raw_times,frame)
    if frame is not None:
        frame+= 0 if frame>=0 else N
        frame = max(0,min(frame,N))
        temp = universe.copy()
        temp.trajectory[frame]
        reference = temp.select_atoms(sel) if sel else temp.atoms
    else:
        temp = Universe(ref) if ref else universe.copy()
        reference = temp.select_atoms(sel) if sel else temp.atoms
    reference = mda.Merge(reference)
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frames = [start]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load selection positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms selection positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    universe = Build(selection,array(positions))
    
    # align selection to reference
    ref = path_name(ref) if ref else 'reference'
    if type(frame) is int:
        if verbose: print(f'ALIGNING "{sel_name(sel)}" to frame {frame}')
    elif   verbose: print(f'ALIGNING "{sel_name(sel)}" to "{ref}"')
    align.AlignTraj(universe,reference,weights='mass',in_memory=True).run()
    
    # compute rmsf verbosely
    if verbose:
        rmsf = []
        print(f'COMPUTING rmsf on "{path_name(tra)}"')
        
        # initialize progress bar
        cursor.hide()
        t0,i,elapsed,etr,speed = now(),0,0,None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(i,M,elapsed,etr,speed,'frames',width)
        print(f'{bar}\r',end='')
        
        # iterate through residues
        for i,residue in enumerate(universe.residues):
            rmsf.append(mean(rms.RMSF(residue.atoms).run().rmsf))
            
            # update progress bar
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (M-i-1)/speed
            bar = progress_bar(i+1,M,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # return results
        print('\n')
        return residues,rmsf
    
    # compute rmsf - the fast way
    rmsf = [mean(rms.RMSF(residue.atoms).run().rmsf)\
            for residue in universe.residues]
    return residues,rmsf

###############################################################################
################################## GYRATION ###################################
############################################################################### 

def Gyration(tra,ref='',sel='all',start=0,stop=-1,step=1,frames=[],
                 thres=0.,pers=0.,verbose=VERBOSE):
    
    """
    Compute the gyration radius for a trajectory ``tra``, for the atoms
    of ``sel`` as selection, ``ref`` as reference (any topology file),
    ``start`` as first trajectory frame, ``stop`` as last trajectory
    frame, ``step`` as interval between frames. If ``thres`` is
    positive, stop when the gyration radius goes below it. If negative,
    stop when the gyration radius goes above it.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frames: int/float or array-like of int/float, if int: the time
            points indexes to be picked; if float: pick the indexes
            corresponding to the time points in ``frames`` (when
            available); if not specified: take all frames according to
            ``start``, ``stop``, and ``step`` options; if specified:
            higher priority than ``start``, ``stop``, and ``step``;
            default is ``[]``
    thres : float, trigger; if positive: stop when the gyration radius
            goes below ``thres`` (that frame included) if negative: stop
            when the gyration radius goes above ``thres`` (that frame
            included); default is ``0.``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    gyra : list of floats, gyration radius time series
    """
    
    # load universe and selection
    universe  = Universe(ref,tra) if ref else Universe(tra)
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build frames accorting to start-stop-step
    if not frames:
        if type(step) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            frames= [start]
            for i,t in enumerate(raw_times[start:stop]):
                if t-raw_times[frames[-1]] >= step:
                    frames.append(start+i)
        else:
            frames = slice(start,stop,step)
    
    # process already existing "frames"
    else:
        temp,frames = frames,[]
        for fra in temp:
            if type(fra) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra = search_equal(raw_times,fra)
                fra = fra if fra is not None else N
            fra+= 0 if fra>=0 else N
            if fra<0 or fra>=N:
                continue
            frames.append(fra)
    
    # build time vector
    time = [t.time for t in universe.trajectory[frames]]
    N = len(time)
    
    # routine - compute gyration radius
    def compute():
        return selection.radius_of_gyration()
    
    # compute - with threshold or verbosely
    gyra = []
    if thres or verbose:
        
        # initialize trigger
        breach = -inf  # last time threshold was crossed
        status = False # being at the right side of the threshold
        
        # initialize progress bar
        if verbose:
            print(f'COMPUTING gyration radius on "{path_name(tra)}"')
            cursor.hide()
            t0,i,elapsed,etr,speed = now(),0,0,None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        
        # iterate through frames
        for i,t in enumerate(universe.trajectory[frames]):
            gyra.append(compute())
            if thres and (gyra[-1]<thres if thres>0 else gyra[-1]>-thres):
                if not status:
                    breach = time[i]
                    status = True
                if time[i]-breach >= pers: 
                    break
            else:
                status = False
            
            # update progress bar
            if not verbose:
                continue
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (N-i-1)/speed
            bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # print trigger status
        if i+1 < N:
            time = time[:i+1]
            descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
        elif thres:
            descr = ansi(f'\nthreshold not triggered\n','yellow')
        else:
            descr = '\n'
        if verbose: print(descr)
        return time,gyra
    
    # compute - the fast way
    gyra = [compute() for t in universe.trajectory[frames]]
    return time,gyra

###############################################################################
############################### AXES OF INERTIA ###############################
############################################################################### 

def Axes(tra,ref='',sel='all',start=0,stop=-1,step=1,frames=[],
             num=3,verbose=VERBOSE):
    
    """
    Compute the direction of the axes of inertia for a trajectory
    ``tra``, with ``sel`` as selection, ``ref`` as reference (any
    topology file), ``start`` as first trajectory frame, ``stop``
    as last trajectory frame, ``step`` as interval between frames.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frames: int/float or array-like of int/float, if int: the time
            points indexes to be picked; if float: pick the indexes
            corresponding to the time points in ``frames`` (when
            available); if not specified: take all frames according to
            ``start``, ``stop``, and ``step`` options; if specified:
            higher priority than ``start``, ``stop``, and ``step``;
            default is ``[]``
    num   : int, number of principal axes to be collected; default
            is ``3`` (maximum)
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    resu : tuple of vectors, first element being the time array,
           the other being the direction axes of inertia
    """
    
    # load universe and selection
    universe  = Universe(ref,tra) if ref else Universe(tra)
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build frames accorting to start-stop-step
    if not frames:
        if type(step) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            frames= [start]
            for i,t in enumerate(raw_times[start:stop]):
                if t-raw_times[frames[-1]] >= step:
                    frames.append(start+i)
        else:
            frames = slice(start,stop,step)
    
    # process already existing "frames"
    else:
        temp,frames = frames,[]
        for fra in temp:
            if type(fra) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra = search_equal(raw_times,fra)
                fra = fra if fra is not None else N
            fra+= 0 if fra>=0 else N
            if fra<0 or fra>=N:
                continue
            frames.append(fra)
    
    # build time vector
    time = [t.time for t in universe.trajectory[frames]]
    N = len(time)
    
    # process n options
    num = num if 1<=num<=3 else 3
    
    # routine - compute principal axes
    def compute():
        return list(selection.principal_axes()[:num].reshape(-1))
    
    # compute
    resu = []
    resu.append(time)
    dis = path_name(tra)
    if verbose: print(f'COMPUTING {num} axes of inertia on "{dis}"')
    
    # initialize results
    for n in range(num):
        for m in range(3):
            resu.append([])
        
    # initialize progress bar
    if verbose:
        cursor.hide()
        t0,i,elapsed,etr,speed = now(),0,0,None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
        print(f'{bar}\r',end='')
    
    # iterate through frames
    for i,frame in enumerate(universe.trajectory[frames]):
        temp = compute()
        for j in range(3*num):
            resu[-j-1].append(temp[-j-1])
        
        # update progress bar
        if not verbose:
            continue
        elapsed = now()-t0
        speed = (i+1)/elapsed
        etr = (N-i-1)/speed
        bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
        print(f'{bar}\r',end='')
    cursor.show()
    
    # return results
    if verbose: print('\n')
    return resu

###############################################################################
################################## DISTANCE ###################################
###############################################################################

def Distance(tra,ref='',sel1='all',sel2=DEF_SEL,axis='xyz',
                 start=0,stop=-1,step=1,frames=[],
                 thres=0.,pers=0.,verbose=VERBOSE):
    
    """
    Compute the distance between the centers of mass of selections
    ``sel1`` and ``sel2``, for a trajectory ``tra``, with ``ref`` as
    reference (any topology file), ``start`` as first trajectory frame,
    ``stop`` as last trajectory frame, ``step`` as interval between
    frames. If ``thres`` is positive, stop when the distance goes below
    it. If negative, stop when the distance goes above it. Project the
    distance on the space defined by axis, eg: ``axis='xy'`` means
    "horizontal plane".
    
    Parameters
    ----------
    tra   : str, input trajectory file
    ref   : str, reference system file; if not specified: try
            using ``tra`` as topology
    ref   : str, reference system file
    sel1  : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    sel2  : str, ``MDAnalysis`` selection syntax; default is
            ``SimSet.DEF_SEL``
    axis  : str, projection (hyper)plane default is ``'xyz'`
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frames: int/float or array-like of int/float, if int: the time
            points indexes to be picked; if float: pick the indexes
            corresponding to the time points in ``frames`` (when
            available); if not specified: take all frames according to
            ``start``, ``stop``, and ``step`` options; if specified:
            higher priority than ``start``, ``stop``, and ``step``;
            default is ``[]``
    thres : float, trigger; if positive: stop when the distance goes
            below ``thres`` (that frame included) if negative: stop when
            the distance goes above ``thres`` (that frame included);
            default is ``0.``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    resu : list of floats, distance time series
    """
    
    # load universe and selections
    universe  = Universe(ref,tra) if ref else Universe(tra)
    if not universe:
        return [],[]
    selection1 = universe.select_atoms(sel1)
    selection2 = universe.select_atoms(sel2)
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build frames accorting to start-stop-step
    if not frames:
        if type(step) in FLOAT:
            if len(raw_times) != N:
                raw_times = [t.time for t in universe.trajectory]
            frames= [start]
            for i,t in enumerate(raw_times[start:stop]):
                if t-raw_times[frames[-1]] >= step:
                    frames.append(start+i)
        else:
            frames = slice(start,stop,step)
    
    # process already existing "frames"
    else:
        temp,frames = frames,[]
        for fra in temp:
            if type(fra) in FLOAT:
                if len(raw_times) != N:
                    raw_times = [t.time for t in universe.trajectory]
                fra = search_equal(raw_times,fra)
                fra = fra if fra is not None else N
            fra+= 0 if fra>=0 else N
            if fra<0 or fra>=N:
                continue
            frames.append(fra)
    
    # build time vector
    time = [t.time for t in universe.trajectory[frames]]
    N = len(time)
    
    # routine: distance between center of mass of selections
    def compute():
        com1 = selection1.center_of_mass()
        com2 = selection2.center_of_mass()
        if 'x' in axis.lower() and 'y' in axis.lower() and 'z' in axis.lower():
            return dist(com1,com2)
        elif 'x' in axis.lower() and 'y' in axis.lower():
            return dist(com1[:2],com2[:2])
        elif 'y' in axis.lower() and 'z' in axis.lower():
            return dist(com1[1:],com2[1:])
        elif 'x' in axis.lower() and 'z' in axis.lower():
            return dist(com1[0:3:2],com2[0:3:2])
        elif 'x' in axis.lower():
            return dist([com1[0]],[com2[0]])
        elif 'y' in axis.lower():
            return dist([com1[1]],[com2[1]])
        elif 'z' in axis.lower():
            return dist([com1[2]],[com2[2]])
    
    # compute - with threshold or verbosely
    resu = []
    if thres or verbose:
        
        # initialize trigger
        breach = -inf  # last time threshold was crossed
        status = False # being at the right side of the threshold
        
        # initialize progress bar
        if verbose:
            print(f'COMPUTING "{sel_name(sel1)}" - "{sel_name(sel2)}" '\
                  f'distance on "{path_name(tra)}"')
            cursor.hide()
            t0,i,elapsed,etr,speed = now(),0,0,None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        
        # iterate through frames
        for i,t in enumerate(universe.trajectory[frames]):
            resu.append(compute())
            if thres and (resu[-1]<thres if thres>0 else resu[-1]>-thres):
                if not status:
                    breach = time[i]
                    status = True
                if time[i]-breach >= pers: 
                    break
            else:
                status = False
            
            # update progress bar
            if not verbose:
                continue
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (N-i-1)/speed
            bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # print trigger status
        if i+1 < N:
            time = time[:i+1]
            descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
        elif thres:
            descr = ansi(f'\nthreshold not triggered\n','yellow')
        else:
            descr = '\n'
        if verbose: print(descr)
        return time,resu
    
    # compute - the fast way
    resu = [compute() for t in universe.trajectory[frames]]
    return time,resu

###############################################################################
##################################### CMAP ####################################
###############################################################################

def Cmap(ref,sel=DEF_SEL,signature=[[]],
         cutoff=NATCUT,distance=DIST,verbose=VERBOSE):
    
    """
    Compute the indexes of the atom-atom contacts for a reference
    system ``ref`` with ``sel`` as selection. Use Best-Hummer
    definition (see Best, Hummer, and Eaton, "Native contacts determine
    protein folding mechanisms in atomistic simulations"
    PNAS (2013) 10.1073/pnas.1311599110).
    
    Parameters
    ----------
    ref      : either str, reference system file; or a
               ``MDAnalysis.Universe`` object
    sel      : str, ``MDAnalysis`` selection syntax, default is
               ``SimSet.DEF_SEL``
    signature: ``(n_of_couples,2)``-shape array of int, if specified:
               restrict computation to the couples in ``signature``;
               default is ``[[]]``
    cutoff   : float, cutoff in Angstrom to assess a contact between
               two atoms, default is ``SimSet.NATCUT``
    distance : int, distance in residues to atoms must have to check
               if they form a contact; default is ``SimSet.DIST``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``

    Returns
    -------
    c0 : ``(n_of_couples,2)``-shape array of int, each row containing
         the indexes RELATIVE TO THE UNIERSE of the atoms in a contact;
         if ``signature`` is specified: a subset of ``signature``
    r0 : array of floats, each element being the native distances
         between atoms in couples
    
    Notes
    -----
    Indexes are relative to the whole universe, and not to selection.
    
    Lists in ``c0`` are ordered and unique. If ``verbose is True``:
    prints a recap of the detected contacs.
    """
    
    # load universe
    universe  = Universe(ref) if type(ref) is str else ref.copy()
    ref       = ref           if type(ref) is str else 'reference'
    
    # load selection
    selection = universe.select_atoms(sel) if sel else universe.atoms
    N = len(selection)
    
    # process signature
    signature = array(signature,ndmin=2)
    if not signature.shape[-1]:
        signature = combinations(selection.indices,2) # relative to universe
        total = N*(N-1)//2
    elif len(signature.shape) == 1:
        signature = [signature[i:i+2] for i in range(0,len(signature),2)]
        total = len(signature)
    else:
        signature = [sig[:2] for sig in signature]
        total = len(signature)
    
    # initialize results
    c0,r0 = [],[]
    
    # initialize progress bar
    if verbose:
        print(f'SEARCHING native contacts on "{ref}" for "{sel_name(sel)}"')
        cursor.hide()
        t0,current,elapsed,etr,speed = now(),0,0,None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(current,total,elapsed,etr,speed,'couples',width)
        print(f'{bar}\r',end='')
        
    # iterate through temporary signature
    for i,j in signature:
        
        # update progress bar
        if verbose and current:
            elapsed = now()-t0
            speed = current/elapsed
            etr = (total-current)/speed
            bar = progress_bar(current,total,elapsed,etr,speed,'couples',width)
            current+= 1
            print(f'{bar}\r',end='')
        else:
            current+= 1
        
        # order and discard identities / atoms not in selection
        i,j = min(i,j),max(i,j)
        if i == j:
            continue
        if i not in selection.indices:
            continue
        if j not in selection.indices:
            continue
        
        # retrieve atoms
        atom1,atom2 = universe.atoms[i],universe.atoms[j]
        
        # now you have atom1, atom2 duplet: verify contact
        # distance between atoms residues >= distance
        if abs(atom1.resid-atom2.resid) < distance:
            continue
        
        # distance between atoms <= cutoff
        r = dist(atom1.position,atom2.position) if 0<cutoff<inf else 1.
        if r > cutoff:
            continue
        
        # add couple
        c0.append([i,j])
        r0.append(r)
    
    # close iterator
    cursor.show()
    if verbose:
        elapsed = now()-t0
        speed = total/elapsed
        bar = progress_bar(total,total,elapsed,0,speed,'couples',width)
        print(bar)
    
    # sort c0,r0
    c0,r0 = array(c0,dtype=int),array(r0)
    if len(c0):
        temp = lexsort((c0[:,1],c0[:,0]))
        c0,r0 = c0[temp],r0[temp]
        c0,temp = unique(c0,axis=0,return_index=True)
        r0 = r0[temp]
    
    # print recap
    if verbose: print(ansi(f'FOUND {len(c0)} native contacts',
                            'green' if len(c0) else 'yellow'))
    return c0,r0

###############################################################################
##################################### HBMAP ###################################
###############################################################################

def HBmap(ref,sel=DEF_SEL,signature=[[]],donors=DONORS,acceptors=ACCEPTORS,
          cutoff=HBCUT,angle=ANGLE,verbose=VERBOSE):
    
    """
    Compute the indexes of the donor-hydrogen acceptor bonds
    for a reference system ``ref`` with ``sel`` as selection.
    A hydrogens and an acceptor must be at least 2 residues away;
    their distance must be ``< cutoff`` (in Angstroms), the
    convex angle with the hydrogen as vertex must by ``> angle``
    (in deg).
    
    Parameters
    ----------
    ref      : either str, reference system file; or a
               ``MDAnalysis.Universe`` object
    sel      : str, ``MDAnalysis`` selection syntax, default is
               ``SimSet.DEF_SEL``
    signature: ``(n_of_couples,2)``-shape array of int, if specified:
               restrict computation to the dononors-acceptors couples
               in ``signature``; default is ``[[]]``
    donors   : list of allowed donor-hydrogen selections by residue;
               default is ``SimSet.DONORS``
    acceptors: list of allowed acceptor selections by residue;
               default is ``SimSet.ACCEPTORS``
    cutoff   : float, cutoff in Angstrom to assess a bond between a
               hydrogen and an acceptor; default is ``SimSet.HBCUT``
    angle    : float, cutoff in deg to assess a contact between
               hydrogen and an acceptor, if we consider the convex
               angle with the hydrogen as vertex; default is
               ``SimSet.ANGLE`` (minimum angle)
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``

    Returns
    -------
    c0 : ``(n_of_couples,3)``-shape array of int, each row containing
         the index of the donor, the number of hydrogens that can
         form the bonds, and the index of the acceptors in a contact;
         indexes are RELATIVE TO THE UNIVERSE; if ``signature`` is
         specified: donors-acceptors couples are a subset of
         ``signature``
    r0 : array of floats, each element being the native distances
         between the hydrogen and the acceptor in a triplet (in
         Angstrom)
    a0 : array of floats, each element being the native convex angle
         with the hydrogen as vertex in a triplet (in deg)
    
    Notes
    -----
    Indexes are relative to the whole universe, and not to selection.
    
    Couples in ``c0`` are ordered and unique. If ``verbose is True``:
    prints a table of the detected contacs.
    """
    
    # load universe
    universe  = Universe(ref) if type(ref) is str else ref.copy()
    ref       = ref           if type(ref) is str else 'reference'
    
    # load selection
    selection = universe.select_atoms(sel) if sel else universe.atoms
    N = len(selection)
    
    # process signature
    signature = array(signature,ndmin=2)
    if signature.shape[-1]:
        signature = [[sig[0],sig[-1]] for sig in signature]
    
    # donors and acceptors selections
    don_select = []
    acc_select = []
    hyd_number = [] # number of hydrogens for each atom in don_select
    total1,total2 = 0,0
    for don in donors:
        select = f'({don[0]}) and name {don[1]}'
        select = selection.select_atoms(select)
        don_select.append(select)
        hyd_number.append(don[2])
        total1+= len(select)*don[2]
    for acc in acceptors:
        select = f'({acc[0]}) and name {acc[1]}'
        select = selection.select_atoms(select)
        acc_select.append(select)
        total2+= len(select)
    tot = total1*total2 # total number of sharanble hydrogens * acceptors
    
    # process signature
    signature = array(signature,ndmin=2)
    if not signature.shape[-1]:
        signature = None # build later
    elif len(signature.shape) == 1:
        signature = [signature[i:i+2] for i in range(0,len(signature),2)]
    else:
        signature = [sig[:2] for sig in signature]
    
    # initialize results
    c0,r0,a0 = [],[],[]
    recap = [] # fill with donors, hydrogens, acceptors indexes
    
    # dic of "already used" donors to avoid multiple assignments
    don_used = {} # key: don.index, value: c0,r0,a0 related to that donor
    
    # dic of "already used" acceptors to avoid multiple assignments
    acc_used = {} # key: acc.index, value: c0,r0,a0 related to that acceptor
    
    # initialize progress bar
    if verbose:
        print(f'SEARCHING hydrogen bonds on "{ref}" for "{sel_name(sel)}"')
        cursor.hide()
        t0,current,ela,etr,speed = now(),0,0,None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(current,tot,ela,etr,speed,'triplets',width)
        print(f'{bar}\r',end='')
    
    # initialize candidate don-hyd-acc dictionary
    candidates = {} # keys: don-hyd-acc indexes; values: loss
    
    # iterate through donors
    for select,n in zip(don_select,hyd_number):
        for i in select.indices:
            don = universe.atoms[i]
            
            # iterate through sharable hydrogens
            for j in range(i+1,i+n+1):
                hyd = universe.atoms[j]
                
                # iterate through acceptors
                for select in acc_select:
                    for k in select.indices:
                        acc = universe.atoms[k]
                        
                        # update progress bar
                        if verbose and current:
                            ela = now()-t0
                            speed = current/ela
                            etr = (tot-current)/speed
                            bar = progress_bar(current,tot,
                                               ela,etr,speed,'triplets',width)
                            current+= 1
                            print(f'{bar}\r',end='')
                        else:
                            current+= 1
                        
                        # ignore atoms not in selection
                        if i not in selection.indices:
                            continue
                        if j not in selection.indices:
                            continue
                        if k not in selection.indices:
                            continue
                        
                        # ignore atoms not in signature (when specified)
                        if signature and [i,k] not in signature:
                            continue
                        
                        # ignore same or sequential residues
                        if abs(don.resid-acc.resid) < 2:
                            continue
                        
                        # distance between acceptor and hydrogen <= CUTOFF
                        r = dist(acc.position,hyd.position)\
                            if 0<cutoff<inf else 1.
                        if r > cutoff:
                            continue
                        
                        # angle with hydrogen as vertex >= ANGLE
                        v0 = acc.position-hyd.position
                        v1 = don.position-hyd.position
                        a = angle_between(v0,v1,True)\
                            if 0<angle<=180 else 1.
                        if a < angle:
                            continue
                        
                        # compute loss for each hydrogen-acceptor couple
                        loss = (r-2)**2+(a-180)**2/(60**2)
                        candidates[loss] = i,j,k,n,r,a
    
    # sort losses and assign best couples
    hyd_used = [] # fill with already "used" hydrogens
    for i,j,k,n,r,a in [candidates[loss] for loss in sorted(candidates)]:
        if j in hyd_used:
            continue
        
        # finally add i,k couple in signature
        hyd_used.append(j)
        c0.append([i,n,k])
        r0.append(r)
        a0.append(a)
        recap.append([i,j,k])
    
    # close iterator
    cursor.show()
    if verbose:
        ela= now()-t0
        speed = tot/ela
        bar = progress_bar(tot,tot,ela,0,speed,'triplets',width)
        print(bar)
    
    # sort c0,r0,a0,recap - unique elements
    c0,r0,a0 = array(c0,dtype=int),array(r0),array(a0)
    recap = array(recap,dtype=int)
    if len(c0):
        temp = lexsort((c0[:,2],c0[:,0]))
        c0,r0,a0,recap = c0[temp],r0[temp],a0[temp],recap[temp]
        c0,temp = unique(c0,axis=0,return_index=True)
        r0,a0,recap = r0[temp],a0[temp],recap[temp]
    
    # print recap
    if verbose and len(c0):
        print(ansi(f'{"residue":10}{"D":5}{"H":5}'\
                   f'{"residue":10}{"A":5}'\
                   f'{"DA dist":10}{"HA dist":10}{"DHA angle":10}','bold'))
        print(f'-'*65)
        for rec,r,a in zip(recap,r0,a0):
            donor = universe.atoms[rec[0]]
            hydro = universe.atoms[rec[1]]
            accep = universe.atoms[rec[2]]
            dres  = f'{donor.resid:3}, {donor.resname}'
            ares  = f'{accep.resid:3}, {accep.resname}'
            dista = dist(donor.position,accep.position)
            print(f'{dres:10}{donor.name:5}{hydro.name:5}'\
                  f'{ares:10}{accep.name:5}'\
                  f'{dista:<4.2f} A    {r:<4.2f} A    {a:<5.2f}°')
    if verbose: print(ansi(f'FOUND {len(c0)} hydrogen bonds',
                            'green' if len(c0) else 'yellow'))
    
    return c0,r0,a0

###############################################################################
#################################### NATIVE ###################################
###############################################################################

def Native(tra,c0,r0,start=0,stop=-1,step=1,thres=0.,pers=0.,
                     beta=1/NATCUT**2,verbose=VERBOSE):
    
    """
    Compute the fraction of native contacts (my proposal of a
    definition) for a trajectory ``tra``, with ``sel`` as selection,
    ``ref`` as reference (any topology file), ``start`` as first
    trajectory frame, ``stop`` as last trajectory frame, ``step`` as
    interval between frames. Take ``c0`` as signature (atom-atom list
    of contacts by index), ``r0`` as the native distances between
    atoms. If ``thres`` is positive, stop when the fraction of native
    contacts goes below it. If negative, stop when the fraction goes
    above it.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    c0    : ``(n,2)`` shaped array, with each raw consisting of
            ``[index1, index2]``: indexes RELATIVE TO THE UNIVERSE of
            each bond (signature)
    r0    : array of floats, each element being the native distance
            between acceptor and hydrogen atoms in Angstrom
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    thres : float, trigger; if positive: stop when the fraction goes
            below ``thres`` (that frame included) if negative: stop when
            the fraction goes above ``thres`` (that frame included);
            default is ``0.``
    beta  : float, weights for atom-atom distances different from
            ``r0``; default is ``SimSet.NATCUT**(-2)
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    frac : list of floats, fraction of native contacs time series
    
    See Also
    --------
    Cmap: related function
    """
    
    # load universe and selection
    universe  = Universe(tra)
    if not universe:
        return [],[]
    selection = universe.atoms
    n = len(c0)
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frames = [start]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    positions = array(positions)
    N = len(positions)
    
    # routine: fraction of native contacts by frame
    def compute(frame):
        exponent = 0
        
        # iterate through couples
        for ij,r0_ij in zip(c0,r0):
            i,j = ij
            atom1,atom2 = positions[frame,i],positions[frame,j]
            
            # distance between atom1 and atom2
            r = dist(atom1,atom2)
            
            # update exponent
            exponent+= 2/(1+exp(beta*(r-r0_ij)**2))
        
        return exponent/n
    
    # compute - with threshold or verbosely
    frac = []
    if thres or verbose:
        
        # initialize trigger
        breach = -inf  # last time threshold was crossed
        status = False # being at the right side of the threshold
        
        # initialize progress bar
        if verbose:
            dis = path_name(tra)
            print(f'COMPUTING fraction of native contacts on "{dis}"')
            cursor.hide()
            t0,frame,elapsed,etr,speed = now(),0,0,None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(frame,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        
        # iterate through frames
        for i in range(N):
            frac.append(compute(i))
            if thres and (frac[-1]<thres if thres>0 else frac[-1]>-thres):
                if not status:
                    breach = time[i]
                    status = True
                if time[i]-breach >= pers:
                    break
            else:
                status = False
            
            # update progress bar
            if not verbose:
                continue
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (N-i-1)/speed
            bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # print trigger status
        if i+1 < N:
            time = time[:i+1]
            descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
        elif thres:
            descr = ansi(f'\nthreshold not triggered\n','yellow')
        else:
            descr = '\n'
        if verbose: print(descr)
        return time,frac
    
    # compute - the fast way
    frac = [compute(i) for i in range(N)]
    return time,frac

###############################################################################
#################################### HBONDS ###################################
###############################################################################

def HBonds(tra,c0,r0,a0,start=0,stop=-1,step=1,thres=0.,pers=0.,
                       alpha=1/(180-ANGLE)**2,beta=1/HBCUT**2,verbose=VERBOSE):
    
    """
    Compute the fraction of Hydrogen-Bonds (my proposal of a
    definition) for a trajectory ``tra``, with ``sel`` as selection,
    ``ref`` as reference (any topology file), ``start`` as first
    trajectory frame, ``stop`` as last trajectory frame, ``step`` as
    interval between frames. Take ``c0`` as donor-acceptor couples
    (signature), ``r0`` as the native distances between hydrogen and
    acceptor atoms for each bond, ``a0`` as the native convex angle
    with the hydrogen as a vertex. If ``thres`` is positive, stop when
    the fraction of Hydrogen-Bonds goes below it. If negative, stop when
    the fraction goes above it.
    
    Parameters
    ----------
    tra   : str, input trajectory file
    c0    : ``(n,3)`` shaped array, with each raw consisting of
            ``[index1, n, index3]``: donor index, number of sharable
            hydrogens, acceptor index RELATIVE TO THE UNIVERSE of
            each hydrogen bond
    r0    : array of floats, each element being the native distance
            between acceptor and hydrogen atoms in Angstrom
    a0    : array of floats, each element being the angle with hydrogen
            as vertex in deg
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    thres : float, trigger; if positive: stop when the fraction goes
            below ``thres`` (that frame included) if negative: stop when
            the fraction goes above ``thres`` (that frame included);
            default is ``0.``
    beta  : float, weights for don-hyd-acc angles different from
            ``ro``; default is ``(180-SimSet.ANGLE)**(-2)
    beta  : float, weights for hyd-acc distances different from
            ``ro``; default is ``SimSet.HBCUT**(-2)
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    frac : list of floats, fraction of HBonds time series
    
    See Also
    --------
    HBmap: related function
    """
    
    # load universe and selection
    universe  = Universe(tra)
    if not universe:
        return [],[]
    selection = universe.atoms
    m = len(c0)
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frames = [start]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    positions = array(positions)
    N = len(positions)
    
    # initialize r,a arrays
    r = array([0. for i in range(m)])
    a = array([0. for i in range(m)])
    
    # routine: fraction of hydrogen bonds by frame
    def compute(frame):
        exponent = 0 # sum of the terms
        hyd_used = [] # fill with already "used" hydrogens
        
        # iterate through donor-acceptor couples
        for ink,r0_ik,a0_ik in zip(c0,r0,a0):
            i,n,k = ink
            don = positions[frame,i]
            acc = positions[frame,k]
            
            # iterate through hydrogens candidates
            candidates = []
            for j in range(i+1,i+n+1):
                hyd = positions[frame,j]
                
                # distance between acceptor and hydrogen
                r = dist(acc,hyd)
                
                # angle with hydrogen as vertex
                v0 = acc-hyd
                v1 = don-hyd
                a = angle_between(v0,v1,True) if alpha else 1
                
                # compute exp argument (~loss) for each hyd-acc couple
                arg  = beta*(r-r0_ik)**2+alpha*(a-a0_ik)**2
                candidates.append((j,arg))
            
            # pick best hydrogen candidate
            for j,arg in sorted(candidates,key=lambda x:x[1]):
                if j in hyd_used:
                    continue
                hyd_used.append(j)
                exponent+= 2/(1+exp(arg))
                break
        
        # return fraction of native bonds
        return exponent/m
    
    # compute - with threshold or verbosely
    frac = []
    if thres or verbose:
        
        # initialize trigger
        breach = -inf  # last time threshold was crossed
        status = False # being at the right side of the threshold
        
        # initialize progress bar
        if verbose:
            dis = path_name(tra)
            print(f'COMPUTING fraction of hydrogen bonds on "{dis}"')
            cursor.hide()
            t0,frame,elapsed,etr,speed = now(),0,0,None,None
            try:    width = get_terminal_size()[0]
            except: width = DEF_WID
            bar = progress_bar(frame,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        
        # iterate through frames
        for i in range(N):
            frac.append(compute(i))
            if thres and (frac[-1]<thres if thres>0 else frac[-1]>-thres):
                if not status:
                    breach = time[i]
                    status = True
                if time[i]-breach >= pers:
                    break
            else:
                status = False
            
            # update progress bar
            if not verbose:
                continue
            elapsed = now()-t0
            speed = (i+1)/elapsed
            etr = (N-i-1)/speed
            bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
            print(f'{bar}\r',end='')
        cursor.show()
        
        # print trigger status
        if i+1 < N:
            time = time[:i+1]
            descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
        elif thres:
            descr = ansi(f'\nthreshold not triggered\n','yellow')
        else:
            descr = '\n'
        if verbose: print(descr)
        return time,frac
    
    # compute - the fast way
    frac = [compute(frame) for frame in range(N)]
    return time,frac

###############################################################################
#################################### COLVAR ###################################
###############################################################################

def Colvar(f,tra,ref='',sel='all',start=0,stop=-1,step=1,frame=None,
                 thres=0.,pers=0.,verbose=VERBOSE):
    
    """
    Compute an arbitrary function of the atoms of a trajectory ``tra``
    -and, occasionally, of the atoms of a reference ``ref`` (any
    topology file, when reference ``frame`` is not ``None``), with
    ``sel`` as selection, ``start`` as first trajectory frame, ``stop``
    as last trajectory frame, ``step`` as interval between frames.
    Preliminary align the trajectory to reference. If ``thres`` is
    positive, stop when the function goes below it. If negative, stop
    when the function goes above it.
    
    Parameters
    ----------
    f     : function of the input trajectory positions and,
            occasionally, of the reference positions
    tra   : str, input trajectory file
    sel   : str, ``MDAnalysis`` selection syntax; default is ``'all'``
    start : int or float, if int: first trajectory frame; if negative:
            start counting from last trajectory frame; if float: first
            trajectory time to be considered; default is ``0``
    stop  : int or float, if int: last trajectory frame; if negative:
            start counting from last trajectory frame; if float: last
            trajectory time to be considered; default is ``-1``
    step  : int or float, if int: interval between frames; if float:
            least time interval between frames; default is ``1``
    frame : int or float or None, if int, frame of ``tra`` to be taken
            as reference; if float: time to be taken as reference; if
            ``None``: don't take into account; default is ``None``
    thres : float, trigger; if positive: stop when the function goes
            below ``thres`` (that frame included) if negative: stop when
            the function goes above ``thres`` (that frame included);
            default is ``0.``
    pers  : float, persistence value (ps) required to activate the
            trigger; if ``thres`` is not zero: stop when the function
            goes above/below ``thres`` for more than ``pers`` ps;
            default is ``0.``
    verbose : bool, be loud and noisy, default is ``SimSet.VERBOSE``
    
    Returns
    -------
    time : list of floats, time points
    resu : list of floats, the function's time series
    """
    
    # check if f is function
    if type(f) not in FUNCTION:
      return [],[]
    pars = len(signature(f).parameters)
    
    # load universe and selection
    if ref:
        universe = Universe(ref,tra)
    else:
        universe = Universe(tra)
        universe.add_TopologyAttr('mass', values=[1.]*len(universe.atoms))
    if not universe:
        return [],[]
    selection = universe.select_atoms(sel) if sel else universe.atoms
    
    # retrieve trajectory infos
    N = len(universe.trajectory)
    dt = universe.trajectory.dt
    T0 = universe.trajectory.time
    raw_times = []
    
    # process frame and retrieve reference
    if type(frame) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frame = search_equal(raw_times,frame)
    if frame is not None:
        frame+= 0 if frame>=0 else N
        frame = max(0,min(frame,N))
        temp = universe.copy()
        temp.trajectory[frame]
        reference = temp.select_atoms(sel) if sel else temp.atoms
    else:
        temp = Universe(ref) if ref else universe.copy()
        reference = temp.select_atoms(sel) if sel else temp.atoms
    reference = mda.Merge(reference)
    
    # process start
    if type(start) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        start = search_bigger(raw_times,start)
        start = start if start is not None else N
    start+= 0 if start>=0 else N
    start = max(0,min(start,N))
    
    # process stop
    if type(stop) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        stop = search_smaller(raw_times,stop)
        stop = stop if stop is not None else -N-1
    stop+= 1 if stop>=0 else N+1
    stop = max(0,min(stop,N))
    
    # build "frames" accorting to start-stop-step (NOT equal to "frame")
    if type(step) in FLOAT:
        if len(raw_times) != N:
            raw_times = [t.time for t in universe.trajectory]
        frames = [start]
        for i,t in enumerate(raw_times[start:stop]):
            if t-raw_times[frames[-1]] >= step:
                frames.append(start+i)
    else:
        frames = slice(start,stop,step)
    
    # load selection positions on memory and build time vector
    if verbose: print(f'RETRIEVING atoms selection positions')
    positions,time = [],[]
    for t in universe.trajectory[frames]:
        positions.append(selection.positions)
        time.append(t.time)
    universe = Build(selection,array(positions))
    N = len(universe.trajectory)
    
    # align selection to reference
    ref = path_name(ref) if ref else 'reference'
    if type(frame) is int:
        if verbose: print(f'ALIGNING "{sel_name(sel)}" to frame {frame}')
    elif   verbose: print(f'ALIGNING "{sel_name(sel)}" to "{ref}"')
    align.AlignTraj(universe,reference,weights='mass',in_memory=True).run()
    
    # routine - find correct input
    def inpt():
        if pars > 1:
            return (universe.atoms.positions,reference.atoms.positions)
        if pars:
            return (universe.atoms.positions,)
        return ()
    
    # compute
    resu = []
    
    # initialize trigger
    breach = -inf  # last time threshold was crossed
    status = False # being at the right side of the threshold
    
    # initialize progress bar
    if verbose:
        print(f'COMPUTING colvar on "{path_name(tra)}"')
        cursor.hide()
        t0,i,elapsed,etr,speed = now(),0,0,None,None
        try:    width = get_terminal_size()[0]
        except: width = DEF_WID
        bar = progress_bar(i,N,elapsed,etr,speed,'frames',width)
        print(f'{bar}\r',end='')
    
    # iterate through frames
    for i,t in enumerate(universe.trajectory):
        resu.append(f(*inpt()))
        if thres and (resu[-1]<thres if thres>0 else resu[-1]>-thres):
            if not status:
                breach = time[i]
                status = True
            if time[i]-breach >= pers: 
                break
        else:
            status = False
        
        # update progress bar
        if not verbose:
            continue
        elapsed = now()-t0
        speed = (i+1)/elapsed
        etr = (N-i-1)/speed
        bar = progress_bar(i+1,N,elapsed,etr,speed,'frames',width)
        print(f'{bar}\r',end='')
    cursor.show()
    
    # print trigger status
    if i+1 < N:
        time = time[:i+1]
        descr = ansi(f'\nthreshold triggered at time {time[-1]}\n','green')
    elif thres:
        descr = ansi(f'\nthreshold not triggered\n','yellow')
    else:
        descr = '\n'
    if verbose: print(descr)
    return time,resu

###############################################################################
############################### PATH SIMILARITY ###############################
###############################################################################

def Similarity(order1,order2=None,verbose=VERBOSE):
    
    """
    Compute path similarity between two pairs of order matrices.
    May be compressed in 32-bit integers. Not compressed elements are
    1 (pair in columns forms before pairs in row), 2 (vice-versa),
    3 (they form both at the same time-or they never form).
    
    Parameters
    ----------
    order1 : array-like of linearized order matrices, either compressed
             or not
    order2 : array-like of linearized order matrices, either compressed
             or not; if ``None``: same as ``order1``; default is
             ``None``
    verbose : bool, be loud and noisy; default is ``SimSet.VERBOSE``
    
    Returns
    -------
    result: linearized array of floats, similarity between elements,
            upper triangular matrix elements (with the diagonal included
            only when ``order2`` is not ``None``)
    
    Notes
    -----
    Implemented multiprocessing according to ``SimSet.PROCESSES`` value.
    """
    
    # parameters initialization
    same = False # if order1 and order2 (once processed) are the same
    
    # options processing
    if type(order1) is str:
        if verbose: print('LOADING 1st set of matrices')
        order1 = read_order(order1,compress=True)
    elif type(order1) not in [list,tuple,ndarray]:
        return
    if type(order2) is str:
        if verbose: print('LOADING 2nd set of matrices')
        order2 = read_order(order2,compress=True)
    elif type(order2) not in [list,tuple,ndarray]:
        order2 = order1
        same = True
    
    # number of matrices
    n = len(order1)
    m = len(order2)
    
    # pre-compress data
    if order1[0][0]<4:
        if verbose: print('COMPRESSING 1st set of matrices')
        length = len(order1[0])
        split = length//16+(length%16>0)
        raw = order1
        order1 = array([[0]*split]*n,dtype=uint32)
        for i in range(n):
            order1[i] = encode_order(raw[i])
    if order2[0][0]<4:
        if verbose: print('COMPRESSING 2nd set of matrices')
        length = len(order1[0])
        split = length//16+(length%16>0)
        raw = order2
        order2 = array([[0]*split]*m,dtype=uint32)
        for i in range(m):
            order2[i] = encode_order(raw[i])
    
    # number of elements and iterable
    N = n*(n-1)//2 if same else n*m
    if not N:
        descr = 'WARNING: not enough order matrices to compare'
        if verbose: print(ansi(descr,'yellow'))
        return array([],dtype=float) 
    
    # length of matrices
    last = len(bin(order1[0][-1]))-2
    L = len(order1[0][:-1])*16+last//2+(last%2>0)
    
    # conversion to 0 to 1 floats
    factor = 1/(2*L)
    
    # multiprocessing progress bar initializer
    current = Manager().Value('i',0)
    lock = Manager().Lock()
    
    # multiprocessing task
    def task(interval):
        first,last = interval
        result = array([0]*(last-first),dtype=float)
        i,j = ij(first,n) if same else (first//m,first%m)
        for index in range(last-first):
            # compute result compressed element-wise
            temp = sum([binary_ones(comp1^comp2)\
                   for comp1,comp2 in zip(order1[i],order2[j])])
            # uncompressed sum of differences between compressed elements:
            # it's the number of 1s in comp1^comp2 binary representation
            result[index] = 1-temp*factor
            # update i,j indeces
            j+= 1
            if j>=m:
                i+= 1
                j = i+1 if same else 0
            if not verbose:
                continue
            lock.acquire()
            try:     current.set(current.get()+1)
            finally: lock.release()
            elapsed = now()-t0
            speed = current.value/elapsed
            etr = (N-current.value)/speed
            bar = progress_bar(current.value,N,
                               elapsed,etr,speed,'couples',width)
            print(f'{bar}\r',end='')
        # return results & x to recognize order
        return interval,result
    
    # distribute parameters among processes
    span1  = N//PROCESSES+(N<PROCESSES)
    span2  = N%PROCESSES*(N>PROCESSES)
    intervals = []
    index,first = 0,0
    while first < N:
        last = min(N,first+span1+(span2>0))
        intervals.append((first,last))
        index+= 1
        first+= span1+(span2>0)
        if span2:
            span2-= 1
    processes = len(intervals)
    
    # initialize options for manual progress bar
    cursor.hide()
    t0,etr,speed = now(),None,None
    try:    width = get_terminal_size()[0]
    except: width = DEF_WID
    
    # run multiprocessing - results not in order
    pool = Pool(1 if processes>1 else 2)
    pool.close()
    pool.join()
    pool.clear()
    pool = Pool(processes) # resetting
    if verbose: print(f'RUNNING on {processes} core{"s"*(processes!=1)}')
    try:
        raw = list(pool.uimap(task,intervals))
        pool.close()
    except Exception as traceback:
        pool.terminate()
        pool.join()
        pool.clear()
        cursor.show()
        descr = f'\nERROR while running, with traceback:'
        descr = f'{ansi(descr,"red")}\n{traceback}\n'
        if verbose: print(descr)
        return array([],dtype=float)
    
    # exit multiprocessing
    pool.join()
    pool.clear()
    cursor.show()
    elapsed = now()-t0
    speed = N/elapsed
    etr = 0
    bar = progress_bar(N,N,elapsed,etr,speed,'couples',width)
    if verbose: print(bar)
    
    # reconstruct result in order
    result = array([0]*N,dtype=float)
    while len(raw):
        interval,sub_result = raw.pop()
        result[interval[0]:interval[1]] = sub_result
    return result
