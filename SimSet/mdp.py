from SimSet.params import *
from SimSet.aux    import *

###############################################################################
################################ CLASS: MDP ###################################
###############################################################################

class mdp:

    """
    Store parameters about a MD simulation. Take an input mdp file as
    reference. It can:
      - update some of the parameters
      - write the parameters to an output mdp file
    
    Attributes
    ----------
    lines : *private* list of str, lines of original mdp filename
    param : dict with keys: ``param_name``; values: list
            ``[line_index>, <args_name>, value]``
            ``line_index`` is -1 when not found, ``args_names`` are
            those of ``self.update``
    group : dict, with keys: group name, values: ``MDAnalysis`` sel
    root  : str, linked mdp filename
    
    Notes
    -----
    Parameters that are not present in the attributes can't be
    modified by ``SimSet.mdp`` class, and must be set manually.
    """
    
    ###########################################################################
    ############################ SET ATTRIBUTES' TYPE #########################
    ###########################################################################
    
    # param
    def _get_param(self):
        return self.__param
    def _set_param(self,value):
        if not isinstance(value,dict):
            raise TypeError('"param" must be set to a dictionary')
        self.__param = value
    param = property(_get_param,_set_param)
    
    # group
    def _get_group(self):
        return self.__group
    def _set_group(self,value):
        if not isinstance(value,dict):
            raise TypeError('"group" must be set to a dictionary')
        self.__group = value
    group = property(_get_group,_set_group)
    
    # root
    def _get_root(self):
        return self.__root
    def _set_root(self,value):
        if not isinstance(value,str):
            raise TypeError('"root" must be set to a string')
        self.__root = value
    root = property(_get_root,_set_root)
    
    ###########################################################################
    ############################## INITIALIZATION #############################
    ###########################################################################
    
    def __init__(self,name=DEF_MDP,**args):
        
        """
        Initialize ``self``.
        
        Parameters
        ----------
        name : str, optional, load parameters from ``name``, if no
               extension is present, use "mdp", default is
               ``SimSet.DEF_MDP``
        args : dict, optional arguments are passed to ``self.update``
               method
        
        See Also
        --------
        self.update : method
        SimSet.system_mdset._mdset : uses this class
        SimSet.system : uses this class
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # name
        name = '.'.join(extension(name,'mdp')) if name else ''
        if not isfile(name):
            raise Exception(ansi('ERROR: no mdp input found','red'))
        self.root = name
        
        # ratchet defaults
        def_pgf = 0.
        def_clf = 0.
        def_pgr = inf
        def_clr = inf
        def_pgt = 1.
        def_clt = 0.
        def_bma = 1000.
        def_btg = inf
        def_bta = inf
        
        # initialization
        self.group = {'':''}
        self.param = {'define'                  :[-1,'define'     ,''],
                      'integrator'              :[-1,'integrator' ,''],
                      'coulombtype'             :[-1,'coulomb'    ,''],
                      'emtol'                   :[-1,'emtol'      ,0.],
                      'pbc'                     :[-1,'pbc'        ,''],
                      'dt'                      :[-1,'dt'         ,0.],
                      'tinit'                   :[-1,'t0'         ,0.],
                      'init_step'               :[-1,'init_step'  ,0 ],
                      'nsteps'                  :[-1,'nsteps'     ,0 ],
                      'nstenergy'               :[-1,'log_freq'   ,0 ],
                      'nstlog'                  :[-1,'log_freq'   ,0 ],
                      'nstxout'                 :[-1,'trr_freq'   ,0 ],
                      'nstvout'                 :[-1,'trr_freq'   ,0 ],
                      'nstxout_compressed'      :[-1,'xtc_freq'   ,0 ],
                      'compressed_x_grps'       :[-1,'xtc_grps'   ,['']],
                      'tcoupl'                  :[-1,'tcoupl'     ,''],
                      'tc_grps'                 :[-1,'tc_grps'    ,['']],
                      'tau_t'                   :[-1,'tau_t'      ,[0.]],
                      'ref_t'                   :[-1,'T'          ,[0.]],
                      'gen_temp'                :[-1,'T'          ,[0.]],
                      'pcoupl'                  :[-1,'pcoupl'     ,''],
                      'pcoupltype'              :[-1,'pcoupltype' ,''],
                      'compressibility'         :[-1,'comp'       ,[0.]],
                      'tau_p'                   :[-1,'tau_p'      ,[0.]],
                      'ref_p'                   :[-1,'P'          ,[0.]],
                      'ratchet_contact_maps'    :[-1,'cmap'       ,''],
                      'ratchet_progress_force'  :[-1,'prog_force' ,def_pgf],
                      'ratchet_closeness_force' :[-1,'clos_force' ,def_clf],
                      'ratchet_progress_rappa'  :[-1,'prog_rappa' ,def_pgr],
                      'ratchet_closeness_rappa' :[-1,'clos_rappa' ,def_clr],
                      'ratchet_progress_target' :[-1,'prog_target',def_pgt],
                      'ratchet_closeness_target':[-1,'clos_target',def_clt],
                      'ratchet_bias_max'        :[-1,'bias_max'   ,def_bma],
                      'ratchet_bias_target'     :[-1,'bias_target',def_btg],
                      'ratchet_bias_tau'        :[-1,'bias_tau'   ,def_bta],
                      'ratchet_output_steps'    :[-1,'log_freq'   ,0 ],
                      'continuation'            :[-1,'cont'       ,''],
                      'gen_vel'                 :[-1,'gen_vel'    ,'']}
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # read lines
        with open(name) as f:
            self.__lines = f.readlines()
        
        # retrieve parameters from lines
        for index,line in enumerate(self.__lines):
            
            # parse param,value
            name,value = '',''
            line = line[:-1].split('=')
            if len(line) > 1:
                name = line[0].replace(' ','').replace('-','_')
                value = line[1].split(';')[0]
                value = value[1:] if value and value[0] is ' ' else value
            if not name:
                continue
            
            for param in self.param:
                if name == param:
                    param_type = type(self.param[param][-1])
                    if param_type is str:
                        value = value.replace(' ','')
                    if param_type in [int,float]:
                        value = abs(param_type(value)) if len(value.split())\
                                else self.param[param][-1] # default if void
                    if param_type is list:
                        value = value.split()
                        element_type = type(self.param[param][-1][0])
                        if element_type is str:
                            for val in value:
                                if not val:
                                    continue
                                if val in DIC_SEL:
                                    self.group[val] = DIC_SEL[val]
                                    continue
                                self.group[val] = val
                        else:
                            value = [abs(element_type(val)) for val in value]
                    self.param[name][ 0] = index
                    self.param[name][-1] = value
                    continue
        
        # update with args
        self.update(**args)
    
    ###########################################################################
    ######################### PRINT AND REPRESENT SELF ########################
    ###########################################################################
    
    def __str__(self):
        
        """
        Return info about the parameters.
        """
        
        # basic
        define     = self.param['define'][-1]
        integrator = self.param['integrator'][-1]
        coulomb    = self.param['coulombtype'][-1]
        emtol      = self.param['emtol'][-1]
        pbc        = self.param['pbc'][-1]
        
        # time
        nsteps     = self.param['nsteps'][-1]
        init_step  = self.param['init_step'][-1]
        dt         = f'{self.param["dt"][-1]*1e3} fs'
        t0         = f'{self.param["tinit"][-1]} ps'
        t          = f'{nsteps*self.param["dt"][-1]/1e3} ns'
        
        # output
        log_freq   = self.param['nstlog'][-1]
        trr_freq   = self.param['nstxout'][-1]
        xtc_freq   = self.param['nstxout_compressed'][-1]
        xtc_grps   = self.param['compressed_x_grps'][-1]
        
        # temperature
        tcoupl     = self.param['tcoupl'][-1]
        tc_grps    = self.param['tc_grps'][-1]
        tau_t      = self.param['tau_t'][-1]
        T          = [round(T,0) for T in self.param['ref_t'][-1]]
        
        # pressure
        pcoupl     = self.param['pcoupl'][-1]
        pcoupltype = self.param['pcoupltype'][-1]
        comp       = self.param['compressibility'][-1]
        tau_p      = self.param['tau_p'][-1]
        P          = [round(P/ATM_BAR,3) for P in self.param['ref_p'][-1]]
        
        # ratchet defaults
        def_pgf = 0.
        def_clf = 0.
        def_pgr = inf
        def_clr = inf
        def_pgt = 1.
        def_clt = 0.
        def_bma = 1000.
        def_btg = inf
        def_bta = inf
        
        # ratchet
        cmap       = self.param['ratchet_contact_maps'][-1]
        prog_force = self.param['ratchet_progress_force'][-1]
        clos_force = self.param['ratchet_closeness_force'][-1]
        prog_rappa = self.param['ratchet_progress_rappa'][-1]
        clos_rappa = self.param['ratchet_closeness_rappa'][-1]
        prog_target= self.param['ratchet_progress_target'][-1]
        clos_target= self.param['ratchet_closeness_target'][-1]
        bias_max   = self.param['ratchet_bias_max'][-1]
        bias_target= self.param['ratchet_bias_target'][-1]
        bias_tau   = self.param['ratchet_bias_tau'][-1]
        
        # gen - continuation rules
        gen = self.param['continuation'][-1] == 'no'
        
        # terminal width and scaling
        try:    width = min(max(50,get_terminal_size()[0]),120)
        except: width = DEF_WID
        scale = width//5
        width = scale*5
        
        # padding
        padA = scale-6
        padB = padA*2+6
        padC = padA+1
        padD = padA+2
        padE = padA+3
        padF = padA+4
        
        # header
        text =['gromacs parameters from ',ansi(\
               f'{format(self.root,-(width-24-(24 if gen else 0)),0)}','bold'),
               f' [generating velocities]' if gen else '','\n',
               
               # info about basic attributes
               ansi('BASIC','bold','yellow'),f' {"-"*(width-6)}\n',
               ansi('  def','bold','yellow'),' ',format(define,    padA),
               ansi('integ','bold','yellow'),' ',format(integrator,padC),
               ansi( 'coul','bold','yellow'),' ',format(coulomb,   padA),
               ansi('emtol','bold','yellow'),' ',format(emtol,     padD),
               ansi(  'pbc','bold','yellow'),' ',format(pbc,     padA,0),'\n',
               
               # info about time
               ansi('TIME','bold','green'),f' {"-"*(width-5)}\n',
               ansi('   dt','bold','green'),' ',format(dt       ,padE),
               ansi(   't0','bold','green'),' ',format(t0       ,padF),
               ansi(    't','bold','green'),' ',format(t        ,padA),
               ansi('steps','bold','green'),' ',format(nsteps   ,padE),
               ansi(   's0','bold','green'),' ',format(init_step,padA,0),'\n',
               
               # info about output
               ansi('OUTPUT','bold','cyan'),f' {"-"*(width-7)}\n',
               ansi('  log','bold','cyan'),' ',format(log_freq,padD),
               ansi(  'trr','bold','cyan'),' ',format(trr_freq,padD),
               ansi(  'xtc','bold','cyan'),' ',format(xtc_freq,padA),
               ansi('group','bold','cyan')+' '+format(xtc_grps,padB,0)\
               if len(xtc_grps) else '','\n',
               
               # info about temperature
               ansi('TEMPERATURE','bold','red'),f' {"-"*(width-12)}\n',
               ansi('coupl','bold','red'),' ',format(tcoupl ,padD),
               ansi(  'tau','bold','red'),' ',format(tau_t  ,padD),
               ansi(  'ref','bold','red'),' ',format(T      ,padA),
               ansi('group','bold','red')+' '+format(tc_grps,padB,0)\
               if len(tc_grps) else '','\n',
               
               # info about pressure
               ansi('PRESSURE','bold','magenta'),f' {"-"*(width-9)}\n',
               ansi('coupl','bold','magenta'),' ',format(pcoupl    ,padD),
               ansi(  'tau','bold','magenta'),' ',format(tau_t     ,padD),
               ansi(  'ref','bold','magenta'),' ',format(P         ,padC),
               ansi( 'type','bold','magenta'),' ',format(pcoupltype,padC),
               ansi( 'comp','bold','magenta'),' ',format(comp    ,padA,0)]
        
        # info about ratchet
        if cmap or prog_force != def_pgf or clos_force != def_pgf\
                or prog_rappa != def_pgr or clos_rappa != def_clr\
                or prog_target!= def_pgt or clos_target!= def_clt\
                or bias_max   != def_bma or bias_target!= def_btg\
                or bias_tau   != def_bta:
            text+= ['\n',ansi('RATCHET','bold','blue'),f' {"-"*(width-8)}\n',
                    ansi(' cmap','bold','blue'),' ',format(cmap,       padA),
                    ansi('progf','bold','blue'),' ',format(prog_force, padA),
                    ansi('progr','bold','blue'),' ',format(prog_rappa, padA),
                    ansi('progt','bold','blue'),' ',format(prog_target,padA),
                    ansi('biasm','bold','blue'),' ',format(bias_max,   padA),
               '\n',ansi('  tau','bold','blue'),' ',format(bias_tau   ,padA),
                    ansi('closf','bold','blue'),' ',format(clos_force, padA),
                    ansi('closr','bold','blue'),' ',format(clos_rappa, padA),
                    ansi('clost','bold','blue'),' ',format(clos_target,padA),
                    ansi('biast','bold','blue'),' ',format(bias_target,padA),0]
        
        return ''.join(text)
    
    def __repr__(self):
        
        """
        Return a string representation of ``self``.
        """
        
        return f'gromacs parameters from {ansi(self.root,"bold")}'
    
    def __format__(self,spec):
        
        """
        Return a proper formatted string of ``self``.
        """
        
        return f'{no_ansi(self.__str__())}\n'
    
    ###########################################################################
    ################################## UPDATE #################################
    ###########################################################################
    
    def update(self,**args):
        
        """
        Update ``self`` parameters in ``self.param`` dictionary.
        
        Parameters
        ----------
        args: dict, optional arguments
          'define'      : str, simulation prescriptions
          'integrator'  : str, integrator type, e.g. "md", "steep"
          'coulomb'     : str, coulomb potential type
          'emtol'       : float, max force threshold in energy minim.
          'pbc'         : str, periodic boundary conditions
          'dt'          : float, integration time step (fs)
          't0'          : float, initial simulation time (ps)
          'init_step'   : int, start simulating from ``init_step``
          't'           : float, simulation time (ns)
          'nsteps'      : int, simulation steps
                          higher priority than ``t``
          'log_freq'    : int, write to log every ``log_freq`` steps
          'trr_freq'    : int, write to trr every ``trr_freq`` steps
          'xtc_freq'    : int, write to xtc every ``xtc_freq`` steps
          'xtc_grps'    : str or array-like, write only ``xtc_grps``
                          selection to xtc, so to save space
          'tcoupl'      : str or bool, thermostat type; if ``tcoupl``
                          is "no" or ``False``, don't set thermostat
          'tc_grps'     : str or array-like, temperature coupling groups
          'tau'         : float or array-like, alias for below
          'tau_t'       : float or array-like, temperature coupling
                          damping coefficient; authomatically adjust
                          array length to ``self.tcgr``; higher priority
                          than ``tau``
          'T'           : float or array-like, temperature coupling
                          and initial value; authomatically adjust array
                          length to match ``tc_grps``; if all elements
                          are zero: ``self.tcpl`` is set to "no"
          'pcoupl'      : str, barostat type; if ``pcoupl`` is "no" or
                          ``False``, don't set thermostat
          'pcoupltype'  : str, pressure coupling type, e.g. "isotropic"
          'comp'        : float or array-like, authomatically adjust 
                          array length to match ``pcoupltype``
          'tau_p'       : float or array-like, pressure coupling damping
                          coefficient
          'P'           : float or array-like, pressure coupling value
                          (ATM); authomatically adjust array length to
                          match ``pcoupltype``
          'cmap'        : str, contact map used for ratchet simulation
          'prog_force'  : float, ratchet progress force
          'clos_force'  : float, ratchet closeness force
          'prog_rappa'  : float, ratchet progress rappa
          'clos_rappa'  : float, ratchet closeness rappa
          'prog_target' : float, ratchet progress target
          'clos_target' : float, ratchet closeness target
          'bias_max'    : float, ratchet maximum bias
          'bias_target' : float, ratchet bias target
          'bias_tau'    : float, ratchet bias tau coefficient
          'gen_vel'     : str or bool, generate velocities
          'cont'        : str or bool, if "yes": take velocities from
                          input gro file; higher priority than
                          ``gen_vel``
        
        Notes
        -----
        Argument parsing uses ``SimSet.parse`` function.
        
        See Also
        --------
        SimSet.parse : function
        
        Examples
        --------
        >>> import SimSet
        >>>
        >>> # load default mdp file
        >>> mdp = SimSet.mdp()
        >>>
        >>> # modify temperature, simulation length and int. step
        >>> mdp.update(out='param',T=800,t=2,dt=1)
        >>>
        >>> # don't write trr, write xtc for a system subset
        >>> # save mdp in working directory with default name
        >>> mdp.update(trr_freq=0,xtc_freq=500,xtc_grps='protein')
        """
        
        #######################################################################
        ############################### PARSING ###############################
        #######################################################################
        
        # simulation time
        try:
            parse(args,'t',self.param['nsteps'][-1]*self.param['dt'][-1]/1e3)
        except Exception as traceback:
            descr = f'ERROR while updating mdp parameters with traceback:'
            descr = f'{ansi(descr,"red")}\n{traceback}\n'
            comment(descr,self.logre,self.logsh,verbo)
            return xvg()
        
        # tau
        if 'tau_t' not in args and 'tau' in args:
            args['tau_t'] = args['tau']
        
        # continuation preprocessing
        if 'cont' in args:
            if args['cont'] in ['yes','y','Y','YES','Yes',True]:
                args['cont'] = 'yes'
                args['gen_vel'] = 'no'
            else:
                args['cont'] = 'no'
                args['gen_vel'] = 'yes'
        elif 'gen_vel' in args:
            if args['gen_vel'] in ['yes','y','Y','YES','Yes',True]:
                args['cont'] = 'no'
                args['gen_vel'] = 'yes'
            else:
                args['cont'] = 'yes'
                args['gen_vel'] = 'no'
        
        # authomatized argument parsing
        for param in self.param:
            name = self.param[param][1]
            default = self.param[param][-1]
            if name == 'dt':
                default = default*1e3
            if name == 'P': # bar to atm conversion
                default = [P/ATM_BAR for P in default]
            if name in ['xtc_grps','tc_grps']:
                default = [self.group[name] for name in default]
            parse(args,name,default)
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # basic
        self.param['define'][-1]      = args['define']
        self.param['integrator'][-1]  = args['integrator']
        self.param['coulombtype'][-1] = args['coulomb']
        self.param['emtol'][-1]       = args['emtol']
        self.param['pbc'][-1]         = args['pbc']
        
        # time
        self.param['dt'][-1] = args['dt']/1e3
        self.param['tinit'][-1] = args['t0']
        if args['nsteps'] != self.param['nsteps'][-1]:
            args['nsteps'] == self.param['nsteps']
        elif args['t'] != self.param['nsteps'][-1]*self.param['dt'][-1]/1e3:
            self.param['nsteps'][-1] =\
            round(args['t']/self.param['dt'][-1]*1e3)
        self.param['init_step'][-1] = args['init_step']
        
        # output
        self.param['nstlog'][-1]  = args['log_freq']
        self.param['nstxout'][-1] = args['trr_freq']
        self.param['nstvout'][-1] = args['trr_freq']
        self.param['nstxout_compressed'][-1] = args['xtc_freq']
        self.param['compressed_x_grps'][-1] = []
        for group in args['xtc_grps']:
            if not group:
                continue
            if group not in DIC_SEL:
                sel = group
                group = first_occurrence(self.group,group)
                if not group: # create unique group name
                    i = len(self.group)
                    group = f'group{i}'
                    while group in self.group:
                        i += 1
                        group = f'group{i}'
            else:
                sel = DIC_SEL[group]
            self.group[group] = sel
            self.param['compressed_x_grps'][-1].append(group)
        
        # temperature
        self.param['tc_grps'][-1] = []
        for group in args['tc_grps']:
            if not group:
                continue
            if group not in DIC_SEL:
                sel = group
                group = first_occurrence(self.group,group)
                if not group: # create unique group name
                    i = len(self.group)
                    group = f'group{i}'
                    while group in self.group:
                        i += 1
                        group = f'group{i}'
            else:
                sel = DIC_SEL[group]
            self.group[group] = sel
            self.param['tc_grps'][-1].append(group)
        n = len(self.param['tc_grps'][-1])
        if n:
            self.param['tau_t'][-1] = [float(tau) for tau in args['tau_t']]
            self.param['ref_t'][-1] = [float(T)   for T   in args['T']    ]
            self.param['tau_t'][-1] = self.param['tau_t'][-1][:n]+\
           [self.param['tau_t'][-1][-1]]*(n-len(self.param['tau_t'][-1]))
            self.param['ref_t'][-1] = self.param['ref_t'][-1][:n]+\
           [self.param['ref_t'][-1][-1]]*(n-len(self.param['ref_t'][-1]))
            self.param['gen_temp'][-1] = [self.param['ref_t'][-1][-1]]
        if args['tcoupl'] == False\
        or not len(self.param['ref_t'][-1])\
        or not min(self.param['ref_t'][-1]):
            self.param['tcoupl'][-1] = 'no'
        else:
            self.param['tcoupl'][-1] = args['tcoupl']
        
        # pressure
        self.param['pcoupltype'][-1] = args['pcoupltype']
        n = DIC_PRE[args['pcoupltype']] if args['pcoupltype'] in DIC_PRE else 1
        self.param['compressibility'][-1] = [float(K) for K in args['comp']]
        self.param['tau_p'][-1] = [float(tau) for tau in args['tau_p']]
        self.param['ref_p'][-1] = [float(P)*ATM_BAR for P in args['P']]
        self.param['compressibility'][-1] =\
        self.param['compressibility'][-1][:n]+\
       [self.param['compressibility'][-1][-1]]*(n-len(\
        self.param['compressibility'][-1]))
        self.param['tau_p'][-1] = self.param['tau_p'][-1][:n]+\
       [self.param['tau_p'][-1][-1]]*(n-len(self.param['tau_p'][-1]))
        self.param['ref_p'][-1] = self.param['ref_p'][-1][:n]+\
       [self.param['ref_p'][-1][-1]]*(n-len(self.param['ref_p'][-1]))
        if args['pcoupl'] == False\
        or not len(self.param['ref_p'][-1])\
        or not min(self.param['ref_p'][-1]):
            self.param['pcoupl'][-1] = 'no'
        else:
            self.param['pcoupl'][-1] = args['pcoupl']
        
        # ratchet
        args['cmap'] = '.'.join(extension(args['cmap'],'cmp'))\
                       if args['cmap'] else args['cmap']
        self.param['ratchet_contact_maps'][-1]     = args['cmap']
        self.param['ratchet_progress_force'][-1]   = args['prog_force']
        self.param['ratchet_closeness_force'][-1]  = args['clos_force']
        self.param['ratchet_progress_rappa'][-1]   = args['prog_rappa']
        self.param['ratchet_closeness_rappa'][-1]  = args['clos_rappa']
        self.param['ratchet_progress_target'][-1]  = args['prog_target']
        self.param['ratchet_closeness_target'][-1] = args['clos_target']
        self.param['ratchet_bias_max'][-1]         = args['bias_max']
        self.param['ratchet_bias_target'][-1]      = args['bias_target']
        self.param['ratchet_bias_tau'][-1]         = args['bias_tau']
        
        # gen
        self.param['gen_vel'][-1] = args['gen_vel']
        self.param['continuation'][-1] = args['cont']
        
        # prune groups
        for group in list(self.group.keys()):
            if group not in self.param['compressed_x_grps'][-1]+\
                            self.param['tc_grps'][-1]:
                self.group.pop(group)
    
    ###########################################################################
    ############################### WRITE TO MDP ##############################
    ###########################################################################
    
    def write(self,output,**args):
        
        """
        Generate output mpd file with ``self`` parameters, after
        ``args`` update.
        
        Parameters
        ----------
        output : str, optional, output path relative to working
                 directory, default is ``SimSet.DEF_MDP`` filename
        args   : dict, optional arguments, passed to ``self.update``
                 method
        
        Notes
        -----
        Parameters that are not present in attributes can't be
        modified by ``SimSet.mdp`` class, and must be set manually.
        
        See Also
        --------
        self.update : method
        
        Examples
        --------
        >>> import SimSet
        >>>
        >>> # load default mdp file
        >>> mdp = SimSet.mdp()
        >>>
        >>> # write to unfolding.mdp with some modifications
        >>> mdp.write(out='unfolding',T=800,t=2,dt=1)
        >>>
        >>> # write to folding.mdp with some modifications
        >>> mdp.write(out='folding.mdp',cmap='native.cmp',progf=.005)
        """
        
        #######################################################################
        ########################## OPTIONS PROCESSING #########################
        #######################################################################
        
        # output
        output = '.'.join(extension(output,'mdp'))
        
        # args
        self.update(**args)
        
        # ratchet defaults
        def_pgf = 0.
        def_clf = 0.
        def_pgr = inf
        def_clr = inf
        def_pgt = 1.
        def_clt = 0.
        def_bma = 1000.
        def_btg = inf
        def_bta = inf
        
        # ratchet
        cmap       = self.param['ratchet_contact_maps'][-1]
        prog_force = self.param['ratchet_progress_force'][-1]
        clos_force = self.param['ratchet_closeness_force'][-1]
        prog_rappa = self.param['ratchet_progress_rappa'][-1]
        clos_rappa = self.param['ratchet_closeness_rappa'][-1]
        prog_target= self.param['ratchet_progress_target'][-1]
        clos_target= self.param['ratchet_closeness_target'][-1]
        bias_max   = self.param['ratchet_bias_max'][-1]
        bias_target= self.param['ratchet_bias_target'][-1]
        bias_tau   = self.param['ratchet_bias_tau'][-1]
        if cmap or prog_force != def_pgf or clos_force != def_pgf\
                or prog_rappa != def_pgr or clos_rappa != def_clr\
                or prog_target!= def_pgt or clos_target!= def_clt\
                or bias_max   != def_bma or bias_target!= def_btg\
                or bias_tau   != def_bta:
            ratchet = True
            self.param['ratchet_output_steps'][-1] =\
            self.param['nstlog'][-1]
        else:
            ratchet = False
        
        #######################################################################
        ######################### CORE OF THE FUNCTION ########################
        #######################################################################
        
        # build mdp file using "self.__lines" as canvas
        for param in self.param:
            if not ratchet and param in list(self.param.keys())[-12:-2]:
                continue # don't do ratchet if not required
            if type(self.param[param][-1]) is list and\
            not len(self.param[param][-1]):
                continue
            if self.param['tcoupl'][-1] == 'no'and\
               param in ['tau_t','ref_t','gen_temp']:
                continue
            index = self.param[param][0]
            value = self.param[param][-1]
            if value == inf:
                value = '' # bug in ratchet
            line  = f'{param:24}= {format(value,50,0)};\n'
            if index >= 0:
                self.__lines[index] = line
            else:
                self.__lines.append(line)
                self.param[param][0] = len(self.__lines)-1
        
        # update parameters according to args
        with open(output,'w') as f:
            f.write(''.join(self.__lines))
        
        # update root
        self.root = output
