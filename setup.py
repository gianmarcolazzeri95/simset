from setuptools import setup

setup(
   name='SimSet',
   version='3.0.0',
   description='Molecular Dynamics systems creation, simulation and analysis',
   author='Gianmarco Lazzeri',
   author_email='gianmarcolazzeri95@gmail.com',
   packages=['SimSet'],
   include_package_data=True,
   install_requires=['pathos>=0.2.6','numpy>=1.12.0','MDAnalysis>=1.0.0',
                     'ParmEd>=3.2.0','matplotlib>=3.0.0','cursor>=1.3.4']
)
